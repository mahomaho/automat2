# This file is part the AutoMAT AUTOSAR product suite.
# AutoMAT product suite (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

These products uses Python and some packages.

To be able to run,..

Install Python 3 on your computer, preferrably the 64-bit variant since the Autosar model if often big and therefore a lot of RAM is needed

Install python package manager if not included in your installation, pip

Install lxml package (python -m pip install lxml)

Install pyside6 package if you plan to use GoMAT (python -m pip install pyside6)

Install rpyc package if you plan to use the remote access server (python -m pip install rpyc)

Done!

You execute the framework by calling the loader with suitable arguments: python AutoMAT/loader.py --help


Note!
AutoMAT is a framework for working with the AUTOSAR model. If you want to do cool stuff then you should write your own
scripts. The GoMAT AUTOSAR model viewer is actually a model script itself running on top of the AutoMAT framework.
To start GoMAT, run python AutoMAT/loader.py -s GoMAT.py -a [arxml files] -l [directory to search for arxml files] -L [directory to search for arxml files, including subdirectories]

/Mattias