# This file is part the GoMAT AUTOSAR viewer which is part of the
# AutoMAT product suite (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.


from AutoMAT import *


loggo='R0lGODdhMQApANUAAP/////VZv+qZsyqZv+qmczVZszVmf/Vmcyqmf+qM8yAZsyqM8yAM5mAM8zVzMyAAJmAAP/VzJlVAJlVM2YrAGZVAMxVAJmqZv//zJmqM/+AM8wAAJkrAJkAAJmAZswrM8wrAJkrM8xVM8wAM5kAM/8rAP8rM2ZVM/8AAP8AM8xVZv9VM////////////////////////////////////////////////////////////////////////////////ywAAAAAMQApAAAG/0CAcEgsGo/IpHLJbDqf0KgyIBAMCAGBVDoUFAoFw2EQKAQOA4IUWggECgZEQRAoBAICQkAgbRqAgkIgcAgIEIPEYCAQBA4BAhAgHA4RCiKRSBQGBIHDoSAYDASJwWIgSCQGAgKBKBwMBoxho0EkEgcIQWEgGCwEi8FAQCQCFgugAwAADAYDBgAAQDAGQIBwSBQIBoiFIDEYJBILIpFIHAwQC8VwwGAQicJBYbEQLASDxWLBeBCJxCFiMBBAhJHB4EEkCoGCxWIxWAwEi8UgIQEChENiETAQDAYTiWAweBiHg0ZjIRgIEovBYJFYSIzGIWIwGAwQiMJAYkwsBAoFQ/8gGAgEgsFgAFwMKBUgQDgkEgeDwaAwGBAKg2JgsGg0LAzGQ7AYDASDhYLRmEiKxWFhMBggDIPLoAgILBgMCARCXAwWwAFjwVAoKpIFECAcEgeDgWAwUBCJGMYi05hMiEQGo8FQMBgVyWCxGAwWguFAgBAMBkSiUMNYNCREokCwYTQYDSBDwZFwNpxOpzMQDAaDAWIwAAKEQyJg4WEwigBB4gMCgUAMhcLjqUg2G06ns+kMBoIBwrAYJBKDgaAIYDCKA80HtAF9Qp8OgwFkMBqNCojT6WwEg4FgMBhwNptNZ7BIJD5AgHAoXIg2IE5n8/mEPptOx9NoKAYSCqSzGQz/BojBoNPhdDqdxWAB6hABjoRoRAJtQB3Q5rP5dIAcDojBYDA8HAikMRgIDINBp7PpcBIJEGgUAgKEwlJpMUgMEqHRZ7MBgTgczobjaTQ4lAoE0hAMEIMOZ8MRDAYg0GfzGQpBIFOpBBIMBILBAAT8gDqcDqfD4EgkFY6IIRoMEBxORzAAbTgf0OYDBAiFoBLoUwKVSqVPabEQCBILTqcjkUgknJNEMoFANpyFILH5gEabzeYzFIJKJZDpYyqZSiVTCVgqgQSLgUCQAEqEQ4nIslgsRqDNB/TZfDYfIEAoLIFKJlPJBAJ9TJ9SqWT6lEqf0kKQGAwWEwlnMBhsQKAN/wjU4YA+oKHQZAKVSqUSqFQymUrAEupTKplKJdOmVGIIFoPBYMQBgTYb0IYDAm1AQIBQaCqZPiVQyfQplVKgEqhkKpVKJRPIVPqAPqUSCAQCfTigz4YDAoE+w6GpVAKZSp8PqGQCgYAlU6lUKqE+JdOmVCqlSh/Sh7MBfUCbEQc0AgKEQ0DJVDJ9QKZSCQRCgT6fEhAkFJZKoJIJVAKNQJvNR7gRbjhAgHAoLJlKpRIIVCqVUKVSqWQqfUygUqlkKoFMplJo09mAPhzQhgQiEoWl0gf0AZVMINAHhCp9gCVQyVQCgUogEAi0+YBApBFoE9pwgADhkEgElUqlD6hUApiZSp8SqFQybUqmUslUKo2AIOFw8wEChENikQgymUCmT6lUApVKpZKpBBqVSiUQiNMBbUAjo9FY3JQ+JhAIWBIOSyBQ6VMCjUCgDUgFBAiHxKLxCCwJh6aSSQj6lD6gDgcEBAiHxKLxWASBSiWQCVQCgT6j0OaDRCKRxVWpZAKVTKDRB4REIpFIIQgEMoFCSCQSiTSCQshiEAA7'
undo = []
redo = []

from PySide6.QtWidgets import *
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtUiTools import QUiLoader
import sys
import os
import time

gomatpath=os.path.dirname(__file__)

PadLockIcon=QIcon(os.path.join(gomatpath,'padlock.svg'))
ErrorIcon=QIcon(os.path.join(gomatpath,'error.svg'))
WarnIcon=QIcon(os.path.join(gomatpath,'warning.svg'))

class TreeDataModel(QAbstractItemModel):
	enableNonExisting=False
	def existing_changed(self,show,modelView):
		self.beginResetModel()
		self.TreeNode.showNonExisting=show
		for e in self._root:
			if hasattr(e,'_subnodes'):
				del e._subnodes
		#self.setRoot(autosar)
		self.endResetModel()
		for i in range(0,self.rowCount(QModelIndex())):
			modelView.setExpanded(self.index(i, 0, QModelIndex()),True)

	def bswMode_changed(self,bswMode,modelView):
		self.beginResetModel()
		self.TreeNode.bswMode=bswMode
		for e in self._root:
			if hasattr(e,'_subnodes'):
				del e._subnodes
		#self.setRoot(autosar)
		self.endResetModel()
		for i in range(0,self.rowCount(QModelIndex())):
			modelView.setExpanded(self.index(i, 0, QModelIndex()),True)
	def showAttributes_changed(self,show,modelView):
		self.beginResetModel()
		self.TreeNode.showAttributes=show
		for e in self._root:
			if hasattr(e,'_subnodes'):
				del e._subnodes
		#self.setRoot(autosar)
		self.endResetModel()
		for i in range(0,self.rowCount(QModelIndex())):
			modelView.setExpanded(self.index(i, 0, QModelIndex()),True)
	class TreeNode:
		modelValues=False
		modelContainers=True
		type=0
		isProperty=False
		showNonExisting=True
		showAttributes=True
		bswMode=True
		def __init__(self,parent,row,model):
			self.parent=parent
			self.row=row
			self.model=model
		@property
		def subnodes(self):
			if hasattr(self,'_subnodes'):
				return self._subnodes
			if self.type==3:
				self._subnodes=[]
				subtype=2
				objs=self.element[:]
				if len(objs)>0 and isinstance(objs[0],autosar_r4p0.Group.Identifiable):
					subtype=4
					objs=sorted(objs,key=lambda e:e.shortName.val())
				for obj in objs:
					t=self.__class__(self,len(self._subnodes),obj)
					t.type=subtype
					t.element=self.element
					t.isProperty=self.isProperty
					self._subnodes.append(t)
			elif not isinstance(self.model,(support.complexbase.ContainerBase,support.complexbase.ValueTypeBase)):
				self._subnodes=[]
			else:
				if self.isProperty:
					self._subnodes=[]
					return self._subnodes
				if self.bswMode and isinstance(self.model,(autosar_r4p0.Group.EcucIndexableValue,autosar_r4p0.Group.EcucModuleConfigurationValues)) and self.model.definition.ref()!=None:
					childs=self.model.ecucAggregations()
					if hasattr(self.model,'shortName'):
						childs.insert(0,self.model.shortName.binding())
				else:
					childs=self.model.aggregations()
				self._subnodes=[]
				for child in childs:
					if child.isProperty() and not self.modelValues or not child.isProperty() and not self.modelContainers:
						continue
					if child.upperMultiplicity() == 1:
						obj=getattr(self.model,child.name())
						if obj==None: # check if empty list
							if not self.showNonExisting:
								continue
							# this is a single container non existing
							t=self.__class__(self,len(self._subnodes),self.model)
							t.type=1
							t._subnodes=[]
						else:
							# this is a single container existing
							t=self.__class__(self,len(self._subnodes),obj)
							t.type=2
					else:
						if next(child.__iter__(),None) is None and not self.showNonExisting: # check if empty list
							continue
						t=self.__class__(self,len(self._subnodes),self.model)
						t.type=3
					t.isProperty=child.isProperty()
					t.element=child
					self._subnodes.append(t)
			return self._subnodes
		def creatableChildren(self):
			if self.type==3:
				if self.isProperty and not self.modelValues or not self.isProperty and not self.modelContainers:
					return []
				if self.element.upperMultiplicity()<=len(self.element):
					return []
				desttype=self.element.desttype()
				if isinstance(desttype,list):
					ret=list(((e.__name__,None) for e in desttype))
					ret.sort(key=lambda e:e[0])
					return ret
				else:
					return [(self.element.name(),None),]
			assert self.type!=1
			ret=[]
			if self.bswMode and isinstance(self.model,(autosar_r4p0.Group.EcucIndexableValue,autosar_r4p0.Group.EcucModuleConfigurationValues)) and self.model.definition.ref()!=None:
				aggregations=self.model.ecucAggregations()
				if isinstance(self.model.definition.ref(),autosar_r4p0.Group.EcucChoiceContainerDef) and len(self.model.subContainer)>0:
					return ()
			else:
				aggregations=self.model.aggregations()
			for aggregation in aggregations:
				if aggregation.isProperty() and not self.modelValues or not aggregation.isProperty() and not self.modelContainers:
					continue
				if aggregation.upperMultiplicity()<=len(aggregation):
					continue
				aggregation_destType=aggregation.desttype()
				if isinstance(aggregation_destType,list):
					ret.append((aggregation.name(),list((e.__name__ for e in aggregation_destType))))						
				else:
					ret.append((aggregation.name(),None))
					
			ret.sort(key=lambda e:e[0])
			return ret
			return ret
		def createnode(self,parentidx,name,groupname,treemodel):
			if self.type==3:
				# this is the group node
				group=self
				desttype=group.element.desttype()
			else:
				group=next((child for child in self._subnodes if child.element.name()==groupname),None)
				if group==None:
					if self.bswMode and isinstance(self.model,(autosar_r4p0.Group.EcucIndexableValue,autosar_r4p0.Group.EcucModuleConfigurationValues)) and self.model.definition.ref()!=None:
						aggregations=self.model.ecucAggregations()
					else:
						aggregations=self.model.aggregations()
					aggregation=next(iter(aggregation for aggregation in aggregations if aggregation.name()==groupname))
					if aggregation.upperMultiplicity()>1:
						group=self.__class__(self,0,self.model)
						group.type=3
					else:
						model=getattr(self.model.editor(),groupname).model()
						group=self.__class__(self,0,model)
						group.type=2
						if hasattr(model,'shortName'):
							model.editor().shortName=groupname
					group.element=aggregation
					group.subnodes
					pos=next((i for i,child in enumerate(self._subnodes) if groupname.lower() < child.element.name().lower()),len(self._subnodes))
					self._subnodes.insert(pos,group)
					for i,child in enumerate(self._subnodes[pos:],pos):
						child.row=i
					parentidx.model().rowsInserted.emit(parentidx, pos, pos)
				parentidx=parentidx.model().index(group.row,0,parentidx)	
				desttype=group.element.desttype()
			if group.type==3:
				if isinstance(desttype,list):
					if name is None:
						name=desttype[0].__name__
					newItem=getattr(getattr(group.model.editor(),groupname),name).append()
				else:
					if name is None:
						name=groupname
					newItem=getattr(group.model.editor(),groupname).append()
				newNode=group.__class__(group,0,newItem.model())
				if hasattr(newItem,'shortName'):
					if group.element.upperMultiplicity() > 1:
						j=0
						while hasattr(group.model,name+'_'+str(j)):
							j+=1
						newItem.shortName=name+'_'+str(j)
					else:
						newItem.shortName=name
					pos=next((i for i,child in enumerate(group._subnodes) if newItem.shortName.val().lower() < child.model.shortName.val().lower()),len(group._subnodes))
					newNode.type=4
				else:
					pos=len(group.element)-1
					newNode.type=2
				newNode.element=group.element
				group._subnodes.insert(pos,newNode)
				for i,child in enumerate(group._subnodes[pos:],pos):
					child.row=i
				parentidx.model().rowsInserted.emit(parentidx, pos, pos)
				parentidx=parentidx.model().index(pos, 0, parentidx)
			treemodel.scrollTo(parentidx)
			treemodel.setCurrentIndex(parentidx)
			
	def __init__(self,root=None):
		super().__init__()
		self.setRoot(root)
	
	def setRoot(self,root,hideroot=False):
		if not root:
			self._root=[]
		elif hideroot:
			self._root=self.TreeNode(None,0,root).subnodes
		else:
			self._root=[self.TreeNode(None,0,root),]
				
	def index(self, row, column , parent):
		node=parent.internalPointer()
		if node is None:
			node=self._root[row]
		else:
			node=node.subnodes[row]
		return self.createIndex(row, column, node)
	def parent(self,child):
		node=child.internalPointer()
		if node.parent is None:
			return QModelIndex()
		return self.createIndex(node.parent.row, 0, node.parent)
	def rowCount(self,parent):
		node=parent.internalPointer()
		if node is None:
			return len(self._root)
		return len(node.subnodes)
	def hasChildren(self, parent):
		node=parent.internalPointer()
		if node is None:
			return len(self._root)
		if len(node.subnodes)>0 or node.type==3:
			return True
		return False
	def columnCount(self,parent):
		return 4
	def headerData(self,section, orientation,role):
		if role!=Qt.DisplayRole:
			return None
		if orientation!=Qt.Horizontal:
			return None
		if section==0:
			return 'Model element'
		if section==1:
			return 'value'
		if section==2:
			return 'type'
		if section==3:
			return 'file'
	def flags(self,index):
		node=index.internalPointer()
		ret=Qt.ItemIsSelectable
		if not (node.type==1 and not self.enableNonExisting):# or node.type==3 and len(node.subnodes)==0):
			ret |= Qt.ItemIsEnabled
		if index.column()==1:
			if node.type!=0 and node.isProperty and not (node.model.editor().readonly() if not isinstance(node.model,support.complexbase.DefaultValueType) else node.model.parent().editor().readonly()):
				return ret | Qt.ItemIsEditable
		return ret
	def data(self,index, role):
		node=index.internalPointer()
		if role==Qt.FontRole:
			if index.column() == 0:
				font=QFont()
				if node.type==3:
					pass
					#font.setItalic(True)
				if node.type in (1,2) and node.isProperty:
					pass
					#font.setBold(True)
				return font
			if index.column()==1 and isinstance(node.model,support.complexbase.DefaultValueType):
				font=QFont()
				font.setItalic(True)
				font.setWeight(QFont.Weight.Thin)
				return font
		if role in (Qt.WhatsThisRole,Qt.ToolTipRole) and node.type!=0:
			ret=None
			if isinstance(node.model,(autosar_r4p0.Group.EcucIndexableValue,autosar_r4p0.Group.EcucModuleConfigurationValues)) and self.TreeNode.bswMode:
				try:
					ret=node.element.__doc__
				except:
					ret=None
			if index.column() == 0:
				ret=node.element.__doc__
			if index.column() in (1,2):
				if node.type in (0,2,4):
					ret=type(node.model).__doc__
				elif node.type==1:
					ret=node.element.desttype().__doc__
				else:
					ret=node.element.__doc__
			try:
				if node.type in (1,3):
					node.element.validate()
				else:
					node.model.validate()
			except AssertionError as e:
				if ret is None:
					return 'Error: '+str(e)
				else:
					return ret+'\n\nError: '+str(e)
			return ret
		#if role==Qt.ForegroundRole:
		#	if index.column() == 0:
		#		if node.type==1 or node.type==3 and len(node.subnodes)==0:
		#			return app.palette().color(QPalette.Disabled, QPalette.Text)
		if role==Qt.DecorationRole:
			if index.column()==0:
				try:
					if node.type in (1,3):
						node.element.validate()
					elif node.type in (2,4):
						node.model.validate()
				except AssertionError as e:
					return ErrorIcon					
				if (node.model.editor().readonly() if not isinstance(node.model,support.complexbase.DefaultValueType) else node.model.parent().editor().readonly()):
					return PadLockIcon
			return None
		if role!=Qt.DisplayRole:
			return None
		if index.column() == 0:
			#if hasattr(e,'shortName'):
			#	return f'{e.shortName}'
			if node.type==0:
				return 'autosar'
			elif node.type==4:
				return node.model.shortName.val()
			elif node.type in (1,2):
				return node.element.name()
			elif node.type==3:
				name=node.element.name()
				return name+('s[' if name[-1]!='s' else 'es[')+str(len(node.subnodes))+']'
		if index.column() == 1:
			if node.isProperty:
				if node.type==2:
					obj=node.model
					if isinstance(obj,support.complexbase.DefaultValueType):
						return '<DEFAULT> '+str(obj.val())
					return str(obj.val())
				elif node.type==1:
					return '-'
			return None
		if index.column()==2:
			if node.type in (0,2,4):
				return type(node.model).__name__
			elif node.type==1:
				return str(node.element.desttype())
			return None
		if node.type in (0,2,4):
			try:
				return str(node.model.file())
			except:
				return None
		return None

class PropertyDataModel(TreeDataModel):
	class TreeNode(TreeDataModel.TreeNode):
		modelValues=True
		modelContainers=False
		showNonExisting=True

class Delegate(QStyledItemDelegate):
	def activatedCalled(self,index,editor):
		#self.parent().setCurrentIndex(index)
		self.commitData.emit(editor)
		self.closeEditor.emit(editor)
	def destroyEditor(self,editor, index):
		node=index.internalPointer()
	def createEditor(self, parent, option, index):
		node=index.internalPointer()
		if hasattr(node.element,'values'):
			ret=QComboBox(parent)
			vals=node.element.values()
			if len(vals)==0:
				return None
			ret.addItems((str(val) for val in vals))
			ret.activated.connect(lambda i: self.activatedCalled(i,ret))
		else:
			ret=QLineEdit(parent)
			if hasattr(node.element,'regex'):
				rx=QRegularExpression(node.element.regex())
				validator=QRegularExpressionValidator(rx,parent)
				ret.setValidator(validator)
			elif hasattr(node.element,'valuetype'):
				valuetype=node.element.valuetype()
				if valuetype==float:
					lo=QLocale(QLocale.C)
					lo.setNumberOptions(QLocale.RejectGroupSeparator)
					validator=QDoubleValidator(ret)# todo: min max
					validator.setLocale(lo)
					ret.setValidator(validator)
				elif valuetype==int:
					validator=QIntValidator(ret) # todo: min max
					ret.setValidator(validator)
		return ret
	def setEditorData(self, editor, index):
		node=index.internalPointer()
		if node.type==1:
			# non existent
			pass
		else:
			if hasattr(editor,'setCurrentText'):
				editor.setCurrentText(str(node.model.val()))
			else:
				editor.setText(str(node.model.val()))
		if isinstance(editor,QComboBox):
			editor.showPopup()
	def setModelData(self, editor, model, index):
		node=index.internalPointer()
		if hasattr(editor,'currentText'):
			val=editor.currentText()
		else:
			val=editor.text()
			if hasattr(node.element,'valuetype'):
				val=node.element.valuetype()(val)
		if node.type==1:
			if node.element.upperMultiplicity()==1:
				setattr(node.model.editor(),node.element.name(),val)
				node.model=getattr(node.model,node.element.name())
			else:
				node.model=node.model.editor().append(val).model()
			node.type=2
		else:
			editor=node.model.editor()
			editor.set(val)
			node.model=editor.model()
		#from enum import Enum
		#if issubclass(node.model.valuetype(),Enum):
		#	val=getattr(node.model.valuetype(),editor.currentText())
		#elif hasattr(editor,'currentText'):
		#	val=node.model.valuetype()(editor.currentText())
		#else:
		#	val=node.model.valuetype()(editor.text())
		#node.model.editor().set(val)
	def updateEditorGeometry(self, editor, option, index):
		editor.setGeometry(option.rect)
	
class ModelTree(QTreeView):
	_refModel=None

	def onExpanded(self,index):
		node=index.internalPointer()
		if node.type!=3:
			for i in range(0,self.datamodel.rowCount(index)):
				idx=self.datamodel.index(i, 0, index)
				if idx.internalPointer().type==3:
					self.setExpanded(idx,True)
		pass	
	def __init__(self,dataModel):
		super().__init__()
		self.datamodel=dataModel
		self.setModel(self.datamodel)
		self.delegate=Delegate()
		self.setItemDelegate(self.delegate)
		self.setEditTriggers(QTreeView.EditTrigger.DoubleClicked | QTreeView.EditTrigger.SelectedClicked | QTreeView.EditTrigger.AnyKeyPressed)
		self.doubleClicked.connect(self.isDoubleClicked)
		self.expanded.connect(self.onExpanded)

	def isDoubleClicked(self,idx):
		node=idx.internalPointer()
		if idx.column()!=0 or node.type!=1 or node.isProperty:
			return
		newcont=getattr(node.model.editor(),node.element.name())
		node.type=2
		node.model=newcont.model()
		self.update(idx)
		self.setCurrentIndex(idx)
	def contextMenuEvent(self, event):
		idx=self.indexAt(event.pos())
		if idx.column()!=0:
			return
		node=idx.internalPointer()
		if node.model.editor().readonly():
			return
		menu=QMenu(self)
		if node.type==1:
			menu.addAction('add').triggered.connect(lambda :self.isDoubleClicked(idx))
		else:
			creatableChildren=node.creatableChildren()
			if len(creatableChildren)>0:
				submenu=menu.addMenu('add')
				#submenu.setStyleSheet("QMenu { menu-scrollable: 1; }")
				def create_lambda(idx,name,groupname=None):
					return lambda : node.createnode(idx,name,groupname,self)# add(t.__name__)
				for child,subtypes in creatableChildren:
					if node.type==3:
						submenu.addAction(child).triggered.connect(create_lambda(idx,child,node.element.name()))
					elif subtypes==None:
						submenu.addAction(child).triggered.connect(create_lambda(idx,None,child))
					else:
						groupmenu=submenu.addMenu(child)
						#groupmenu.setStyleSheet("QMenu { menu-scrollable: 1; }")
						for name in subtypes:
							groupmenu.addAction(name).triggered.connect(create_lambda(idx,name,child))
		if node.type==3 and len(node.subnodes)>0:
			def deleteAll():
				delattr(node.model.editor(),node.element.name())
				self.rowsRemoved(idx,0,len(node._subnodes)-1)
				node._subnodes=[]
				self.update(idx)
				#self.removeRow()
			menu.addAction('delete').triggered.connect(deleteAll)
		if node.type in (2,4):
			def delete():
				parentidx=idx.model().parent(idx)
				node.model.editor().delete()
				node.parent._subnodes.pop(node.row)
				for i,child in enumerate(node.parent._subnodes[node.row:],node.row):
					child.row=i
				self.rowsRemoved(parentidx, node.row, node.row)
			menu.addAction('delete').triggered.connect(delete)
		menu.exec_(event.globalPos())

	def currentChanged(self, current, previous):
		if self._refModel:
			node=current.internalPointer()
			self._refModel.beginResetModel()
			if node.type in (2,4):
				self._refModel.setRoot(node.model,True)
				self._refModel._widget.setWindowTitle(f'properties of {str(node.model)}')
			else:
				self._refModel.setRoot([])
				self._refModel._widget.setWindowTitle(f'properties')
			self._refModel.endResetModel()
		e=current.internalPointer()
		#self._parent._propertywindow._itemmodel.beginResetModel()
		#self._parent._propertywindow._itemmodel._base=e
		#self._parent._propertywindow._itemmodel.endResetModel()

class DockWidget(QDockWidget):
	def __init__(self, name, widget):
		super().__init__(name)
		self.setObjectName(name)
		self.widget=widget
		self.setWidget(widget)
		self.setAllowedAreas(Qt.AllDockWidgetAreas)

class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()
		settings=QSettings()
		settings.beginGroup('MainWindow')
		self.resize(settings.value('size',QScreen().availableGeometry().size() * 0.7))
		self.move(settings.value('position',QPoint(200, 200)))
		self.setWindowTitle('GoMAT')
		
		#QMenuBar()
		filemenu=self.menuBar().addMenu('File')
		projmenu=self.menuBar().addMenu('Project')
		editprojaction=projmenu.addAction('edit')
		
		#toolbar
		toolbar = QToolBar(self)
		toolbar.setObjectName('toolbar')
		action_save = QAction("save", toolbar)
		action_save.triggered.connect(lambda:autosar.editor().save())
		toolbar.addAction(action_save)
		
		action_showNonExisting = QAction("show non-existing", toolbar)
		action_showNonExisting.setCheckable(True)
		action_showNonExisting.setChecked(settings.value('show non-existing',True,bool))
		#action_showNonExisting.triggered.connect(self.existing_changed)
		toolbar.addAction(action_showNonExisting)
		self.action_showNonExisting=action_showNonExisting

		action_bswMode = QAction("BSW Mode", toolbar)
		action_bswMode.setCheckable(True)
		action_bswMode.setChecked(settings.value('BSW Mode',True,bool))
		#action_bswMode.triggered.connect(self.existing_changed)
		toolbar.addAction(action_bswMode)
		self.action_bswMode=action_bswMode
		
		action_showAttributes = QAction("show attributes", toolbar)
		action_showAttributes.setCheckable(True)
		action_showAttributes.setChecked(settings.value('show attributes',True,bool))
		#action_showAttributes.triggered.connect(self.existing_changed)
		toolbar.addAction(action_showAttributes)
		self.action_showAttributes=action_showAttributes

		self.addToolBar(toolbar)
		
		self.setTabPosition( Qt.TopDockWidgetArea , QTabWidget.North )
		self.setDockOptions( QMainWindow.AllowNestedDocks | QMainWindow.AllowTabbedDocks | QMainWindow.GroupedDragging)
		
		
		datamodel=TreeDataModel(autosar)
		self.mergedModelView=DockWidget('Merged Model view',ModelTree(datamodel))
		self.mergedModelView.widget.hideColumn(1)
		self.addDockWidget(Qt.TopDockWidgetArea, self.mergedModelView)
		
		datamodel=PropertyDataModel()
		datamodel.enableNonExisting=True
		self.propertywindow=DockWidget('properties',ModelTree(datamodel))
		self.addDockWidget(Qt.BottomDockWidgetArea, self.propertywindow)

		basmatmodel=TreeDataModel()
		basmatmodel._root=sorted(list(TreeDataModel.TreeNode(None,idx,e) for idx,e in enumerate(autosar_r4p0.EcucModuleConfigurationValues.instances())), key=lambda e:e.model.shortName.val())
		for e in basmatmodel._root:
			e.type=4
			e.isProperty=False
			e.element=e.model.binding()
		self.bswModelView=DockWidget('BasMAT BSW Configuration view',ModelTree(basmatmodel))
		self.bswModelView.widget.hideColumn(1)
		self.addDockWidget(Qt.TopDockWidgetArea, self.bswModelView)
		
		self.mergedModelView.widget._refModel=datamodel
		self.bswModelView.widget._refModel=datamodel
		datamodel._widget=self.propertywindow

		settings.beginGroup('Merged Model view')
		self.mergedModelView.widget.setColumnWidth(0,settings.value('column0 width',250,int))
		self.mergedModelView.widget.setColumnWidth(1,settings.value('column1 width',250,int))
		self.mergedModelView.widget.setColumnWidth(2,settings.value('column2 width',250,int))
		settings.endGroup()
		settings.beginGroup('BasMAT Model view')
		self.bswModelView.widget.setColumnWidth(0,settings.value('column0 width',250,int))
		self.bswModelView.widget.setColumnWidth(1,settings.value('column1 width',250,int))
		self.bswModelView.widget.setColumnWidth(2,settings.value('column2 width',250,int))
		settings.endGroup()
		settings.beginGroup('properties')
		self.propertywindow.widget.setColumnWidth(0,settings.value('column0 width',250,int))
		self.propertywindow.widget.setColumnWidth(1,settings.value('column1 width',250,int))
		self.propertywindow.widget.setColumnWidth(2,settings.value('column2 width',250,int))
		settings.endGroup()
		
		self.restoreGeometry(settings.value("geometry",self.saveGeometry()))
		self.restoreState(settings.value("windowState",self.saveState()))
		
		settings.endGroup()
		action_showNonExisting.triggered.connect(lambda:self.mergedModelView.widget.datamodel.existing_changed(self.action_showNonExisting.isChecked(),self.mergedModelView.widget))
		self.mergedModelView.widget.datamodel.existing_changed(self.action_showNonExisting.isChecked(),self.mergedModelView.widget)
		action_showNonExisting.triggered.connect(lambda:self.bswModelView.widget.datamodel.existing_changed(self.action_showNonExisting.isChecked(),self.bswModelView.widget))
		self.bswModelView.widget.datamodel.existing_changed(self.action_showNonExisting.isChecked(),self.bswModelView.widget)
		action_bswMode.triggered.connect(lambda:self.mergedModelView.widget.datamodel.bswMode_changed(self.action_bswMode.isChecked(),self.mergedModelView.widget))
		self.mergedModelView.widget.datamodel.bswMode_changed(self.action_bswMode.isChecked(),self.mergedModelView.widget)
		action_showAttributes.triggered.connect(lambda:self.mergedModelView.widget.datamodel.showAttributes_changed(self.action_showAttributes.isChecked(),self.mergedModelView.widget))
		self.mergedModelView.widget.datamodel.showAttributes_changed(self.action_showAttributes.isChecked(),self.mergedModelView.widget)
		action_showAttributes.triggered.connect(lambda:self.bswModelView.widget.datamodel.showAttributes_changed(self.action_showAttributes.isChecked(),self.bswModelView.widget))
		self.bswModelView.widget.datamodel.showAttributes_changed(self.action_showAttributes.isChecked(),self.bswModelView.widget)
		#self.existing_changed()
				
	def closeEvent(self, *args, **kwargs):
		settings=QSettings()
		settings.beginGroup('MainWindow')
		settings.setValue('size',self.size())
		settings.setValue('position',self.pos())
		settings.setValue('show non-existing',self.action_showNonExisting.isChecked())
		settings.setValue('BSW Mode',self.action_bswMode.isChecked())
		settings.setValue('show attributes',self.action_showAttributes.isChecked())

		settings.beginGroup('Merged Model view')
		settings.setValue('size',self.mergedModelView.size())
		settings.setValue('position',self.mergedModelView.pos())
		settings.setValue('column0 width',self.mergedModelView.widget.columnWidth(0))
		settings.setValue('column1 width',self.mergedModelView.widget.columnWidth(1))
		settings.setValue('column2 width',self.mergedModelView.widget.columnWidth(2))
		settings.endGroup()
		settings.beginGroup('BasMAT Model view')
		settings.setValue('size',self.bswModelView.size())
		settings.setValue('position',self.bswModelView.pos())
		settings.setValue('column0 width',self.bswModelView.widget.columnWidth(0))
		settings.setValue('column1 width',self.bswModelView.widget.columnWidth(1))
		settings.setValue('column2 width',self.bswModelView.widget.columnWidth(2))
		settings.endGroup()
		settings.beginGroup('properties')
		settings.setValue('size',self.propertywindow.size())
		settings.setValue('position',self.propertywindow.pos())
		settings.setValue('column0 width',self.propertywindow.widget.columnWidth(0))
		settings.setValue('column1 width',self.propertywindow.widget.columnWidth(1))
		settings.setValue('column2 width',self.propertywindow.widget.columnWidth(2))
		settings.endGroup()
		settings.setValue("geometry",self.saveGeometry())
		settings.setValue("windowState",self.saveState())
		settings.endGroup()

app = QApplication([])

QApplication.setOrganizationName('AutoMAT')
QApplication.setApplicationName('GoMAT')

import ctypes
myappid = 'GoMAT' # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid) # must tell windows that it should use my app icon and not python icon
app.setWindowIcon(QIcon(os.path.join(gomatpath,'automat.svg')))

QSettings.setDefaultFormat(QSettings.IniFormat)
#mainwindow=themain()
#loader = QUiLoader()
#path = os.path.join(os.path.dirname(__file__), "mainwindow.ui")
#ui_file = QFile(path)
#ui_file.open(QFile.ReadOnly)
#mainwindow=loader.load(ui_file)
#ui_file.close()
#mainwindow.treeView.setModel(DataModel())
mainWindow=MainWindow()
#mainwindow.treeView_2.setModel(TreeDataModel())
mainWindow.show()
sys.exit(app.exec_())


# def main(argv):
	#
 # #	window.title('GoMAT: An AUTOSAR model viewer from AutoMAT')
  # # try:
   # # import base64
   # # imgicon = tk.PhotoImage(data=base64.b64decode(loggo))
   # # tree.pack()
   # # window.tk.call('wm', 'iconphoto', window._w, imgicon)
