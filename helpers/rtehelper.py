# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2019 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

from AutoMAT import *

def RunnableApiOpen(runnables):
	res = '#if defined(RTE_RUNNABLEAPI)'
	for runnable in runnables:
		res += f' || \\\n\tdefined(RTE_RUNNABLEAPI_{runnable.name()})'
	return res+'\n'
def RunnableApiClose():
	return '#endif // RTE_RUNNABLEAPI\n'

def TYPE(_type, internalBehavior=None):
	if isinstance(_type,autosar_r4p0.MODE_DECLARATION_GROUP):
		return _VALUE_TYPE(_type, internalBehavior)
	elif isinstance(_type,autosar_r4p0.SW_BASE_TYPE):
		return _BASE_TYPE(_type,internalBehavior)
	type_ref=_type
	while type_ref.CATEGORY.value == 'TYPE_REFERENCE':
		type_ref = type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest
	CATEGORY=type_ref.CATEGORY.value
	if CATEGORY in ('VALUE','BOOLEAN'):
		return _VALUE_TYPE(_type, internalBehavior)
	elif CATEGORY=='ARRAY':
		return _ARRAY_TYPE(_type, internalBehavior)
	elif CATEGORY in ('COM_AXIS','CURVE','MAP'):
		return _CALIB_ARRAY_TYPE(_type, internalBehavior)
	elif CATEGORY == 'DATA_REFERENCE':
		return _PTR_TYPE(_type, internalBehavior)
	else:
		print(f'Error: unsupported type {_type.name()}')


class _TYPE():
	_impl_type=None
	_type=None
	#_int_beh=None
	def isAppl(self):
		return not isinstance(self._type,autosar_r4p0.IMPLEMENTATION_DATA_TYPE)
	def name(self):
		return self._type.name()
	def impl_name(self):
		return self._impl_type.name()

class _VALUE_TYPE(_TYPE):
	_type_base=None
	_compu=ModelList()
	def __init__(self,type_ref, internalBehavior=None):
		self._type=type_ref
		# internalBehaviour only needed for application data types
		if isinstance(type_ref,autosar_r4p0.APPLICATION_PRIMITIVE_DATA_TYPE):
			self._compu=type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest
			if internalBehavior is None:
				assert internalBehavior is not None
			type_ref=internalBehavior.DATA_TYPE_MAPPING_REFS.DATA_TYPE_MAPPING_REF.dest.DATA_TYPE_MAPS.DATA_TYPE_MAP.next(lambda self: self.APPLICATION_DATA_TYPE_REF.dest == type_ref).IMPLEMENTATION_DATA_TYPE_REF.dest
		elif isinstance(type_ref,autosar_r4p0.MODE_DECLARATION_GROUP):
			assert internalBehavior is not None
			type_ref=internalBehavior.DATA_TYPE_MAPPING_REFS.DATA_TYPE_MAPPING_REF.dest.MODE_REQUEST_TYPE_MAPS.MODE_REQUEST_TYPE_MAP.next(lambda self: self.MODE_GROUP_REF.dest == type_ref).IMPLEMENTATION_DATA_TYPE_REF.dest
		else:
			self._compu=ModelList()
		#if type_ref.CATEGORY.value == 'ARRAY' and 
		self._impl_type=type_ref
		while type_ref.CATEGORY.value == 'TYPE_REFERENCE':
			type_ref = type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest
		self._type_base=type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].BASE_TYPE_REF.dest
		#self._int_beh=internalBehavior
	def isArray(self):
		return False
	def isRecord(self):
		return False
	def isPrimitive(self):
		return True
	def isAtomic(self):
		return int(self._type_base.BASE_TYPE_SIZE.value) <= 64
	def base_isSigned(self):
		return self._type_base.BASE_TYPE_ENCODING.value in ('2C','IEEE754')
	def base_isFloat(self):
		return self._type_base.BASE_TYPE_ENCODING.value == 'IEEE754'
	def base_isBoolean(self):
		return self._type_base.BASE_TYPE_ENCODING.value == 'BOOLEAN'
	def base_size(self):
		try:
			return int(self._type_base.BASE_TYPE_SIZE.value)
		except:
			raise
	def scale_val(self,val):
		if self._compu.CATEGORY.value == 'LINEAR':
			val=float(val)
			toF = float(self._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_NUMERATOR.V[1].value.default('1')) / float(self._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_DENOMINATOR.V[0].value.default('1'))
			toO=float(self._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_NUMERATOR.V[0].value.default('0')) / float(self._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_DENOMINATOR.V[0].value.default('1'))
			val=(val+toO)/toF
			if not self.base_isFloat():
				val=round(val)
		else:
			if self.base_isFloat():
				val=float(val.default('0'))
			elif self.base_isBoolean():
				val=val.default('False').lower()
				if val in ('true','false'):
					val=val=='true'
				else:
					val=int(val,0)
			else:
				val=int(val.default('0'),0)
		return val
	def print_val(self,val):
		if self.base_isFloat():
			return str(val)
		elif self.base_isSigned():
			return str(val.__round__())
		elif self.base_isBoolean():
			return 'TRUE' if val else 'FALSE'
		else:
			return str(val.__round__())+'U'
		
	def VALUE2str(self,VALUE_SPEC):
		while not VALUE_SPEC.CONSTANT_REFERENCE.isNone():
			VALUE_SPEC=VALUE_SPEC.CONSTANT_REFERENCE.CONSTANT_REF.dest.VALUE_SPEC
		if not VALUE_SPEC.APPLICATION_VALUE_SPECIFICATION.isNone():
			val=self.scale_val(VALUE_SPEC.APPLICATION_VALUE_SPECIFICATION.SW_VALUE_CONT.SW_VALUES_PHYS.V[0].value)
		elif not VALUE_SPEC.NUMERICAL_VALUE_SPECIFICATION.isNone():
			val=self.scale_val(VALUE_SPEC.NUMERICAL_VALUE_SPECIFICATION.VALUE.value)
		else:
			val=0.0
		return self.print_val(val)
	def isLinear(self):
		if self._compu.isNone():
			return True
		if self._compu.CATEGORY.value == 'LINEAR':
			return True
		if self._compu.CATEGORY.value == 'RATIONAL':
			if(self._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_NUMERATOR.V.__len__() <= 2 and
			   self._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_DENOMINATOR.V.__len__() <= 1):
				return True
		return False
			
	def ScalingNeeded(self,fromT):
		if type(fromT) is not type(self):
			print(f'Error: trying to map datatype {self.name()} to datatype {fromT.name()}')
			return False
		if fromT.isLinear() and self.isLinear():
			fromCoffs=fromT._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0]
			toCoffs=self._compu.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0]
			if(fromCoffs.COMPU_NUMERATOR.V[0].value.default('0') != toCoffs.COMPU_NUMERATOR.V[0].value.default('0') or
			   fromCoffs.COMPU_NUMERATOR.V[1].value.default('1') != toCoffs.COMPU_NUMERATOR.V[1].value.default('1') or
			   fromCoffs.COMPU_DENOMINATOR.V[0].value.default('1') != toCoffs.COMPU_DENOMINATOR.V[0].value.default('1')):
				return True
			else:
				return False
		else:
			#todo
			return False
	def copy(self, tovar, fromT, var):
		return f'{tovar} = {self.Scale(fromT,var)};\n'
	def Scale(self,fromT, var):
		toT = self
		fromC=fromT._compu
		toC=toT._compu
		res=f'({toT.impl_name()})('
		if fromT.isLinear() and toT.isLinear():
			fromF = float(fromC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_NUMERATOR.V[1].value.default('1')) / float(fromC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_DENOMINATOR.V[0].value.default('1'))
			toF = float(toC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_NUMERATOR.V[1].value.default('1')) / float(toC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_DENOMINATOR.V[0].value.default('1'))
			fromO=float(fromC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_NUMERATOR.V[0].value.default('0')) / float(fromC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_DENOMINATOR.V[0].value.default('1'))
			toO=float(toC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_NUMERATOR.V[0].value.default('0')) / float(toC.COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE.COMPU_RATIONAL_COEFFS[0].COMPU_DENOMINATOR.V[0].value.default('1'))
			offset = (fromO-toO)/toF
			if fromT.base_isFloat() or toT.base_isFloat():
				if fromF != toF:
					res+=f'{fromF/toF} * (float){var}'
				else:
					res+=f'(float){var}'
				if offset < 0:
					res+=f' - {-offset}'
				elif offset > 0:
					res+=f' + {offset}'
			else:
				from fractions import Fraction
				maxsize = max(fromT.base_size(),toT.base_size())
				fr = Fraction.from_float(toF/fromF).limit_denominator(2**maxsize)
				offset = float(offset*fr.numerator).__round__()
				if offset != 0 and fr.numerator != 1:
					res += '('
				if fr.denominator == 1:
					res+=f'{var}'
				else:
					import math
					numsize=math.ceil(math.log2(fr.denominator))
					temptype = 'sint' if toT.base_isSigned() else 'uint'
					temptype+=f'{2 ** math.ceil(math.log2(fromT.base_size()+numsize))}'
					res+=f'({temptype}){var} * {fr.denominator}'
				if offset < 0:
					res += f' - {-offset}'
				elif offset > 0:
					res += f' + {offset}'
				if fr.numerator != 1:
					if offset != 0:
						res+=')'
					res += f' / {fr.numerator}'
		else:
			res+=var
		res+=')'
		return res

class _CALIB_ARRAY_TYPE(_VALUE_TYPE):
	_elementType=None
	_size=0
	def __init__(self,type_ref, internalBehavior=None):
		self._type=type_ref
		# internalBehaviour only needed for application data types
		assert isinstance(type_ref,autosar_r4p0.APPLICATION_PRIMITIVE_DATA_TYPE),f'Only primitive appl types may have a category {type_ref.CATEGORY.value}'
		if type_ref.CATEGORY.value=='COM_AXIS':
			self._compu=type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].SW_CALPRM_AXIS_SET.SW_CALPRM_AXIS[0].SW_AXIS_INDIVIDUAL.COMPU_METHOD_REF.dest
		elif type_ref.CATEGORY.value=='CURVE':
			self._compu=type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest
		elif type_ref.CATEGORY.value=='MAP':
			self._compu=type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest
		assert internalBehavior is not None
		type_ref=internalBehavior.DATA_TYPE_MAPPING_REFS.DATA_TYPE_MAPPING_REF.dest.DATA_TYPE_MAPS.DATA_TYPE_MAP.next(lambda self: self.APPLICATION_DATA_TYPE_REF.dest == type_ref).IMPLEMENTATION_DATA_TYPE_REF.dest
		#if type_ref.CATEGORY.value == 'ARRAY' and 
		self._impl_type=type_ref
		while type_ref.CATEGORY.value=='TYPE_REFERENCE':
			type_ref = type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest
		assert type_ref.CATEGORY.value=='ARRAY'
		self._size=int(type_ref.SUB_ELEMENTS.IMPLEMENTATION_DATA_TYPE_ELEMENT[0].ARRAY_SIZE.value)
		self._elementType=TYPE(type_ref.SUB_ELEMENTS.IMPLEMENTATION_DATA_TYPE_ELEMENT[0].SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest)
		self._elementType._compu=self._compu
		self._type_base=self._elementType._type_base
	def isArray(self):
		return True
	def isPrimitive(self):
		return False
	def isAtomic(self):
		return False
	def element_type(self):
		return self._elementType
	def VALUE2str(self,VALUE_SPEC):
		while not VALUE_SPEC.CONSTANT_REFERENCE.isNone():
			VALUE_SPEC=VALUE_SPEC.CONSTANT_REFERENCE.CONSTANT_REF.dest.VALUE_SPEC
		vals=[]
		if not VALUE_SPEC.APPLICATION_VALUE_SPECIFICATION.isNone():
			for physval in VALUE_SPEC.APPLICATION_VALUE_SPECIFICATION.SW_VALUE_CONT.SW_VALUES_PHYS.siblings():
				val=self.scale_val(physval.value)
				vals.append(str(val))
		elif not VALUE_SPEC.NUMERICAL_VALUE_SPECIFICATION.isNone():
			val=self.scale_val(VALUE_SPEC.NUMERICAL_VALUE_SPECIFICATION.VALUE.value)
		else:
			raise AssertionError('Unsupported data type')
		return f'{{{", ".join(vals)}}}'

class _PTR_TYPE(_VALUE_TYPE):
	# todo: implement full support for ptr elements
	_ptr_type=None
	def __init__(self,type_ref, internalBehavior=None):
		swdatadefprops=type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].SW_POINTER_TARGET_PROPS
		if swdatadefprops.TARGET_CATEGORY.value in ('VALUE','BOOLEAN'):
			self._ptr_type=TYPE(swdatadefprops.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].BASE_TYPE_REF.dest,internalBehavior)
		elif swdatadefprops.TARGET_CATEGORY.value=='TYPE_REFERENCE':
			self._ptr_type=TYPE(swdatadefprops.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest,internalBehavior)
		else:
			print('Error: Not supported pointer ref element type')
		self._type=type_ref
		self._impl_type=type_ref

class _BASE_TYPE(_VALUE_TYPE):
	def __init__(self,type_ref, internalBehavior=None):
		self._type=type_ref
		# internalBehaviour only needed for application data types
		self._type_base=type_ref
		self._impl_type=type_ref
	

class _ARRAY_TYPE(_TYPE):
	_elementType=None
	_size=0
	def __init__(self,type_ref, internalBehavior):
		self._type=type_ref
		# internalBehaviour only needed for application data types
		if isinstance(type_ref,autosar_r4p0.APPLICATION_ARRAY_DATA_TYPE):
			#self._compu=type_ref.ELEMENT.TYPE_TREF.dest.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest
			self._elementType=TYPE(type_ref.ELEMENT.TYPE_TREF.dest,internalBehavior)
			self._size=int(type_ref.ELEMENT.MAX_NUMBER_OF_ELEMENTS.value)
			if internalBehavior is not None:
				self._impl_type=internalBehavior.DATA_TYPE_MAPPING_REFS.DATA_TYPE_MAPPING_REF.dest.DATA_TYPE_MAPS.DATA_TYPE_MAP.next(lambda self: self.APPLICATION_DATA_TYPE_REF.dest == type_ref).IMPLEMENTATION_DATA_TYPE_REF.dest
		elif isinstance(type_ref,autosar_r4p0.APPLICATION_PRIMITIVE_DATA_TYPE):
			#self._compu=type_ref.ELEMENT.TYPE_TREF.dest.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest
			self._elementType=TYPE(type_ref.ELEMENT.TYPE_TREF.dest,internalBehavior)
			self._size=int(type_ref.ELEMENT.MAX_NUMBER_OF_ELEMENTS.value)
			if internalBehavior is not None:
				self._impl_type=internalBehavior.DATA_TYPE_MAPPING_REFS.DATA_TYPE_MAPPING_REF.dest.DATA_TYPE_MAPS.DATA_TYPE_MAP.next(lambda self: self.APPLICATION_DATA_TYPE_REF.dest == type_ref).IMPLEMENTATION_DATA_TYPE_REF.dest
		else:
			self._impl_type=type_ref
			while type_ref.CATEGORY.value == 'TYPE_REFERENCE':
				type_ref = type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest
			self._size=int(type_ref.SUB_ELEMENTS.IMPLEMENTATION_DATA_TYPE_ELEMENT[0].ARRAY_SIZE.value)
			self._elementType=TYPE(type_ref.SUB_ELEMENTS.IMPLEMENTATION_DATA_TYPE_ELEMENT[0].SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest)
		#self._type_base=type_ref.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].BASE_TYPE_REF.dest
		#self._int_beh=internalBehavior
	def isArray(self):
		return True
	def isRecord(self):
		return False
	def isPrimitive(self):
		return False
	def isAtomic(self):
		return False
	def element_type(self):
		return self._elementType
		
	def VALUE2str(self,VALUE_SPEC):
		while not VALUE_SPEC.CONSTANT_REFERENCE.isNone():
			VALUE_SPEC=VALUE_SPEC.CONSTANT_REFERENCE.CONSTANT_REF.dest.VALUE_SPEC
		vals=[]
		for val in VALUE_SPEC.ARRAY_VALUE_SPECIFICATION.ELEMENTS.NUMERICAL_VALUE_SPECIFICATION:
			val=self._elementType.scale_val(val.VALUE.value) #todo: instead of looping over num vals, handle each value as a value and convert is liek that
			vals.append(self._elementType.print_val(val))
		return f'{{{", ".join(vals)}}}'
			
	def ScalingNeeded(self,fromT):
		if type(fromT) is not type(self):
			print(f'Error: trying to map datatype {self.name()} to datatype {fromT.name()}')
			return False
		return self._elementType.ScalingNeeded(fromT)
		
	def copy(self, tovar, fromT, var):
		if type(fromT) is not type(self):
			return f'Error: Trying to copy non array type {fromT.name()} to array type {self.name()}'
		res=f'''for(int Rte_i = 0; Rte_i < {self._size}; Rte_i++) {{
			{tovar}[Rte_i] = {self._elementType.Scale(fromT._elementType,var+"[Rte_i]")};
		}}
		'''
		return res
	
def ReferencedObjects(obj):
	import AutoMAT
	def CreateRefs(obj, reflist):
		reflist[obj.type_name()].append(obj)
		if type(obj) is AutoMAT._model_ref:
			CreateRefs(obj.dest, reflist)
		else:
			for sibling in obj.siblings():
				CreateRefs(sibling,reflist)
	reflist = defaultdict(ModelList)
	CreateRefs(obj,reflist)
	return reflist

class OsTask():
	prio=0
	_task=None
	_tasks={}
	def __init__(self, task):
		self._task=task
		self.prio=task.OsTaskPriority.value
	@staticmethod
	def get(task):
		if task in OsTask._tasks:
			return OsTask._tasks[task]
		else:
			t=OsTask(task)
			OsTask._tasks[task]=t
			return t
			
		
		