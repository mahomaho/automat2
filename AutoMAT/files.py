# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

import os as _os

_loaded={}
_loadedabspath={}

class File:
	def __init__(self,file,abspath):
		self._file=file
		self._abspath=abspath
		self._modified=False
	def __repr__(self):
		return self._file
	def __eq__(self,other):
		return self._file==str(other)
	def __ne__(self,other):
		return self._file!=str(other)
	def delete(self):
		_os.remove(self._abspath)
		_loadedabspath.pop(self._abspath)
		_loaded.pop(str(self))
	def readonly(self):
		return not _os.access(self._abspath, _os.W_OK)

def clearmodified(file):
	f=get(file)
	ret=f._modified
	f._modified=False
	return ret
def modified(file):
	get(file)._modified=True

def get(file):
	file=str(file)
	ret=_loaded.get(file,None)
	if ret is not None:
		return ret
	abspath=_os.path.abspath(_os.path.realpath(str(file)))
	ret=_loadedabspath.get(abspath,None)
	if ret is not None:
		return ret
	ret=File(file,abspath)
	_loaded[file]=ret
	_loadedabspath[abspath]=ret
	return ret

def deleteallfiles():
	for file in tuple(_loaded.values()):
		file.delete()