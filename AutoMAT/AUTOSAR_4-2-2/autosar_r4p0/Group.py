
# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.


from ..support.support import *
from . import SimpleTypes
from ..support.groupbase import *
from ..support import complexbase
from collections import defaultdict


class ARElement(complexbase.GroupBase):
	"""An element that can be defined stand-alone, i.e. without being part of another element (except for packages of course)."""
	_references=defaultdict(list)

class ARObject(complexbase.GroupBase):
	"""Implicit base class of all classes in meta-model."""
	checksum=complexbase.Attribute("checksum",SimpleTypes.String,'S',False,"""Checksum calculated by the user's tool environment for an ArObject. May be used in an own tool environment to determine if an ArObject has changed. The checksum has no semantic meaning for an AUTOSAR model and there is no requirement for AUTOSAR tools to manage the checksum.""")
	timestamp=complexbase.Attribute("timestamp",SimpleTypes.DateTime,'T',False,"""Timestamp calculated by the user's tool environment for an ArObject. May be used in an own tool environment to determine the last change of an ArObject. The timestamp has no semantic meaning for an AUTOSAR model and there is no requirement for AUTOSAR tools to manage the timestamp.""")

class ARPackage(complexbase.GroupBase):
	"""AUTOSAR package, allowing to create top level packages to structure the contained ARElements.

ARPackages are open sets. This means that in a file based description system multiple files can be used to partially describe the contents of a package.

This is an extended version of MSR's SW-SYSTEM."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._referenceBase_children=[]
		self._element_children=[]
		self._arPackage_children=[]
		self._variationPoint_child=ModelNone

class AUTOSAR(complexbase.GroupBase):
	"""Root element of an AUTOSAR description, also the root element in corresponding XML documents."""
	def __init__(self):
		super().__init__()
		self._adminData_child=ModelNone
		self._introduction_child=ModelNone
		self._arPackage_children=[]

class AbsoluteTolerance(complexbase.GroupBase):
	"""Maximum allowable deviation"""
	def __init__(self):
		super().__init__()
		self._absolute_child=ModelNone

class AbstractCanCluster(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN, J1939 and CAN Cluster attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._busOffRecovery_child=ModelNone
		self._canFdBaudrate_child=ModelNone

class AbstractCanClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class AbstractCanCommunicationConnector(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN and CAN CommunicationConnector attributes."""
	_references=defaultdict(list)

class AbstractCanCommunicationController(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN and CAN Controller attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._canControllerAttributes_child=[]

class AbstractCanCommunicationControllerAttributes(complexbase.GroupBase):
	"""For the configuration of the CanController parameters two different approaches can be used:
1. Providing exact values which are taken by the ECU developer (CanControllerConfiguration).
2. Providing ranges of values which are taken as requirements and have to be respected by the ECU developer (CanControllerConfigurationRequirements)."""
	def __init__(self):
		super().__init__()
		self._canControllerFdAttributes_child=ModelNone
		self._canControllerFdRequirements_child=ModelNone

class AbstractCanCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class AbstractCanPhysicalChannel(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN and CAN PhysicalChannel attributes."""
	_references=defaultdict(list)

class AbstractEvent(complexbase.GroupBase):
	"""This meta-class represents the abstract ability to model an event that can be taken to implement application software or basic software in AUTOSAR."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._activationReasonRepresentation_child=ModelNone

class AbstractNumericalVariationPoint(complexbase.GroupBase):
	"""This is an abstract NumericalValueVariationPoint. It is introduced to support the case that additional attributes are required for particular purposes."""

class AbstractProvidedPortPrototype(complexbase.GroupBase):
	"""This abstract class provides the ability to become a provided PortPrototype."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._providedComSpec_children=[]

class AbstractRequiredPortPrototype(complexbase.GroupBase):
	"""This abstract class provides the ability to become a required PortPrototype."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._requiredComSpec_children=[]

class AbstractRuleBasedValueSpecification(complexbase.GroupBase):
	"""This represents an abstract base class for all rule-based value specifications."""

class AbstractServiceInstance(complexbase.GroupBase):
	"""Provided and Consumed Ethernet Service Instances that are available at the ApplicationEndpoint."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._routingGroup_children=[]

class AclObjectSet(complexbase.GroupBase):
	"""This meta class represents the ability to denote a set of objects for which roles and rights (access control lists) shall be defined. It basically can define the objects based on

* the nature of objects
* the involved blueprints
* the artifact in which the objects are serialized
* the definition of the object (in a definition - value pattern)
* individual reference objects"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._aclObjectClass_children=[]
		self._aclScope_child=ModelNone
		self._collection_child=ModelNone
		self._derivedFromBlueprint_children=[]
		self._engineeringObject_children=[]
		self._objectDefinition_children=[]
		self._objectDefintion_children=[]
		self._object_children=[]

class AclOperation(complexbase.GroupBase):
	"""This meta class represents the ability to denote a particular operation which may be performed on objects in an AUTOSAR model."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._impliedOperation_children=[]

class AclPermission(complexbase.GroupBase):
	"""This meta class represents the ability to represent permissions granted on objects in an AUTOSAR model."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._aclContext_children=[]
		self._aclObject_children=[]
		self._aclOperation_children=[]
		self._aclRole_children=[]
		self._aclScope_child=ModelNone

class AclRole(complexbase.GroupBase):
	"""This meta class represents the ability to specify a particular role which is used to grant access rights to AUTOSAR model. The purpose of this meta-class is to support the mutual agreements between the involved parties."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ldapUrl_child=ModelNone

class AdminData(complexbase.GroupBase):
	"""AdminData represents the ability to express administrative information for an element. This administration information is to be treated as meta-data such as revision id or state of the file. There are basically four kinds of meta-data

* The language and/or used languages.

* Revision information covering e.g. revision number, state, release date, changes. Note that this information can be given in general as well as related to a particular company.

* Document meta-data specific for a company"""
	def __init__(self):
		super().__init__()
		self._language_child=ModelNone
		self._usedLanguages_child=ModelNone
		self._docRevision_children=[]
		self._sdg_children=[]

class AgeConstraint(complexbase.GroupBase):
	"""The AgeConstraint is used to impose a constraint on an Timing Description Event referenced by the scope.

A minimum and a maximum age can be specified."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maximum_child=ModelNone
		self._minimum_child=ModelNone
		self._scope_child=ModelNone

class AliasNameAssignment(complexbase.GroupBase):
	"""This meta-class represents the ability to associate an alternative name to a flat representations or an Identifiable.

The usage of this name is defined outside of AUTOSAR. For example this name can be used by MCD tools or as a name for component instances in the ECU extract.

Note that flatInstance and identifiable are mutually exclusive."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._label_child=ModelNone
		self._identifiable_child=ModelNone
		self._flatInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class AliasNameSet(complexbase.GroupBase):
	"""This meta-class represents a set of AliasNames. The AliasNameSet can for example be an input to the A2L-Generator. It shall not be used by the RTE generator to generate the MC-Support.

In a given instance of AliasNameSet in the bound system there must be at most one aliasName per FlatInstanceDescriptor."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._aliasName_children=[]

class AnalyzedExecutionTime(complexbase.GroupBase):
	"""AnalyzedExecutionTime provides an analytic method for specifying the best and worst case execution time."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bestCaseExecutionTime_child=ModelNone
		self._worstCaseExecutionTime_child=ModelNone

class Annotation(complexbase.GroupBase):
	"""This is a plain annotation which does not have further formal data."""

class AnyInstanceRef(complexbase.GroupBase):
	"""Describes a reference to any instance in an AUTOSAR model. This is the most generic form of an instance ref. Refer to the superclass notes for more details."""
	def __init__(self):
		super().__init__()
		self._contextElement_children=[]
		self._target_child=ModelNone
		self._variationPoint_child=ModelNone

class ApplicationArrayDataType(complexbase.GroupBase):
	"""An application data type which is an array, each element is of the same application data type."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dynamicArraySizeProfile_child=ModelNone
		self._element_child=ModelNone

class ApplicationArrayElement(complexbase.GroupBase):
	"""Describes the properties of the elements of an application array data type."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._arraySizeHandling_child=ModelNone
		self._arraySizeSemantics_child=ModelNone
		self._maxNumberOfElements_child=ModelNone

class ApplicationCompositeDataType(complexbase.GroupBase):
	"""Abstract base class for all application data types composed of other data types."""
	_references=defaultdict(list)

class ApplicationCompositeDataTypeSubElementRef(complexbase.GroupBase):
	"""This meta-class represents the specialization of SubElementMapping with respect to ApplicationCompositeDataTypes."""
	def __init__(self):
		super().__init__()
		self._applicationCompositeElement_child=ModelNone

class ApplicationCompositeElementDataPrototype(complexbase.GroupBase):
	"""This class represents a data prototype which is aggregated within a composite application data type (record or array). It is introduced to provide a better distinction between target and context in instanceRefs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._type_child=ModelNone

class ApplicationCompositeElementInPortInterfaceInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._rootDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class ApplicationDataType(complexbase.GroupBase):
	"""ApplicationDataType defines a data type from the application point of view. Especially it should be used whenever something \"physical\" is at stake. 

An ApplicationDataType represents a set of values as seen in the application model, such as measurement units. It does not consider implementation details such as bit-size, endianess, etc.

It should be possible to model the application level aspects of a VFB system by using ApplicationDataTypes only."""
	_references=defaultdict(list)

class ApplicationEndpoint(complexbase.GroupBase):
	"""An application endpoint is the endpoint on an Ecu in terms of application addressing (e.g. socket). The application endpoint represents e.g. the listen socket in client-server-based communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._consumedServiceInstance_children=[]
		self._discoveryTechnology_child=ModelNone
		self._networkEndpoint_child=ModelNone
		self._priority_child=ModelNone
		self._providedServiceInstance_children=[]
		self._remotingTechnology_child=ModelNone
		self._serializationTechnology_child=ModelNone
		self._tpConfiguration_child=[]

class ApplicationEntry(complexbase.GroupBase):
	"""Schedule table entry for application messages."""
	def __init__(self):
		super().__init__()
		self._frameTriggering_child=ModelNone

class ApplicationError(complexbase.GroupBase):
	"""This is a user-defined error that is associated with an element of an AUTOSAR interface. It is specific for the particular functionality or service provided by the AUTOSAR software component."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._errorCode_child=ModelNone

class ApplicationErrorMapping(complexbase.GroupBase):
	"""In client server communication, the server may return any value within the application error range.

The ApplicationError is mapped to the responseGroup."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class ApplicationPrimitiveDataType(complexbase.GroupBase):
	"""A primitive data type defines a set of allowed values."""
	_references=defaultdict(list)

class ApplicationRecordDataType(complexbase.GroupBase):
	"""An application data type which can be decomposed into prototypes of other application data types."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._element_children=[]

class ApplicationRecordElement(complexbase.GroupBase):
	"""Describes the properties of one particular element of an application record data type."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class ApplicationRuleBasedValueSpecification(complexbase.GroupBase):
	"""This meta-class represents rule based values for DataPrototypes typed by ApplicationDataTypes (ApplicationArrayDataType or a compound ApplicationPrimitiveDataType which also boils down to an array-nature)."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._swAxisCont_children=[]
		self._swValueCont_child=ModelNone

class ApplicationSwComponentType(complexbase.GroupBase):
	"""The ApplicationSwComponentType is used to represent the application software."""
	_references=defaultdict(list)

class ApplicationValueSpecification(complexbase.GroupBase):
	"""This meta-class represents values for DataPrototypes typed by ApplicationDataTypes (this includes in particular compound primitives). 

For further details refer to ASAM CDF 2.0. This meta-class corresponds to some extent with SW-INSTANCE in ASAM CDF 2.0."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._swAxisCont_children=[]
		self._swValueCont_child=ModelNone

class ArVariableInImplementationDataInstanceRef(complexbase.GroupBase):
	"""This class represents the ability to navigate into a data element inside of an VariableDataPrototype which is typed  by an ImplementationDatatype.

Note that it shall not be used if the target is the VariableDataPrototype itself (e.g. if its a primitive).

Note that this class follows the pattern of an InstanceRef but is not implemented based on the abstract classes because the ImplementationDataType isn't either, especially because ImplementationDataTypeElement isn't derived from AtpPrototype."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._rootVariableDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class ArbitraryEventTriggering(complexbase.GroupBase):
	"""The ArbitraryEventTriggering describes that an event occurs occasionally, singly, irregularly or randomly.

The primary purpose of this event triggering is to abstract event occurrences captured by data acquisition tools (background debugger, trace analyzer, etc.) during system runtime."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._minimumDistance_children=[]
		self._maximumDistance_children=[]
		self._confidenceInterval_children=[]

class Area(complexbase.GroupBase):
	"""This element specifies a region in an image map. Image maps enable authors to specify regions in an object (e.g. a graphic) and to assign a specific activity to each region (e.g. load a document, launch a program etc.).

For more details refer to the specification of HTML."""
	accesskey=complexbase.Attribute("accesskey",SimpleTypes.String,'ACCESSKEY',False,"""This attribute assigns an access key to an element.
An access key is an individual character (e.g. \"B\") within the document character
range. If an access key with an element assigned to it is pressed, the element comes
into focus. The activity performed when an element comes into focus, is dependent on
the element itself""")
	alt=complexbase.Attribute("alt",SimpleTypes.String,'ALT',False,"""This attribute specifies the text to be inserted as an alternative to illustrations, shapes or applets, where these
cannot be displayed by user agents.""")
	class_=complexbase.Attribute("class_",SimpleTypes.String,'CLASS',False,"""Blank separated list of classes""")
	coords=complexbase.Attribute("coords",SimpleTypes.String,'COORDS',False,"""This attribute specifies the position and shape on the screen.
The number of values and their order depend on the geometrical figure defined.""")
	href=complexbase.Attribute("href",SimpleTypes.String,'HREF',False,"""This attribute specifies the memory location of a web resource. It is therefore able to specify a link between the current element and the target element.""")
	nohref=complexbase.Attribute("nohref",SimpleTypes.AreaEnumNohref,'NOHREF',False,"""If this attribute is set, the Area has no associated link.""")
	onblur=complexbase.Attribute("onblur",SimpleTypes.String,'ONBLUR',False,"""The ONBLUR-Event occurs, when focus is switched away from an element. 

A script can be stored in this attribute to be performed in the Event.""")
	onclick=complexbase.Attribute("onclick",SimpleTypes.String,'ONCLICK',False,"""The ONCLICK-Event occurs, if the current element is clicked-on. 

A script can be stored in this attribute to be performed in the Event.""")
	ondblclick=complexbase.Attribute("ondblclick",SimpleTypes.String,'ONDBLCLICK',False,"""The ONCLICK-Event occurs, if the current element is \"double\" clicked-on. 

A script can be stored in this attribute to be performed in the Event.""")
	onfocus=complexbase.Attribute("onfocus",SimpleTypes.String,'ONFOCUS',False,"""The ONFOCUS-Event occurs, if an element comes into focus (e.g., through navigation using the tab button). 

A script can be stored in this attribute to be performed in the Event.""")
	onkeydown=complexbase.Attribute("onkeydown",SimpleTypes.String,'ONKEYDOWN',False,"""The ONKEYDOWN-Event occurs, if a button on the current element is pressed down. 

A script can be stored in this attribute to be performed in the event.""")
	onkeypress=complexbase.Attribute("onkeypress",SimpleTypes.String,'ONKEYPRESS',False,"""The ONKEYPRESS-Event occurs, if a button on the current element is pressed down and released. 

A script can be stored in this attribute to be performed in the Event.""")
	onkeyup=complexbase.Attribute("onkeyup",SimpleTypes.String,'ONKEYUP',False,"""The ONKEYUP-Event occurs, if a button on the current element is released. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousedown=complexbase.Attribute("onmousedown",SimpleTypes.String,'ONMOUSEDOWN',False,"""The ONMOUSEDOWN-Event occurs, if the mouse button used for clicking is held down on the current element. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousemove=complexbase.Attribute("onmousemove",SimpleTypes.String,'ONMOUSEMOVE',False,"""The ONMOUSEMOVE-Event occurs, if the mouse pointer is moved on the current 
element (i.e. it is located on the current element). 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseout=complexbase.Attribute("onmouseout",SimpleTypes.String,'ONMOUSEOUT',False,"""The ONMOUSEOUT-Event occurs, if the mouse pointer is moved from the current element.

A script can be stored in this attribute to be performed in the Event.""")
	onmouseover=complexbase.Attribute("onmouseover",SimpleTypes.String,'ONMOUSEOVER',False,"""The ONMOUSEOVER-Event occurs, if the mouse pointer is moved to the current element
from another location outside it. 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseup=complexbase.Attribute("onmouseup",SimpleTypes.String,'ONMOUSEUP',False,"""The ONMOUSEUP-Event occurs if the mouse button used for clicking is released on the
current element. 

A script can be stored in this attribute to be performed in the Event.""")
	shape=complexbase.Attribute("shape",SimpleTypes.AreaEnumShape,'SHAPE',False,"""The shape of the area. Note that in HTML this is defaulted to RECT.""")
	style=complexbase.Attribute("style",SimpleTypes.String,'STYLE',False,"""Information on the associated style""")
	tabindex=complexbase.Attribute("tabindex",SimpleTypes.String,'TABINDEX',False,"""This attribute specifies the position of the current element in tabbing-order for the corresponding document.

The value must lie between 0 and 32767. The Tabbing Order defines the sequence in which elements are focused on, when the user navigates using the keyboard.""")
	title=complexbase.Attribute("title",SimpleTypes.String,'TITLE',False,"""Title information of the Area element""")

class ArgumentDataPrototype(complexbase.GroupBase):
	"""An argument of an operation, much like a data element, but also carries direction information and is owned by a particular ClientServerOperation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._direction_child=ModelNone
		self._serverArgumentImplPolicy_child=ModelNone
		self._typeBlueprint_children=[]
		self._variationPoint_child=ModelNone

class ArrayValueSpecification(complexbase.GroupBase):
	"""Specifies the values for an array."""
	def __init__(self):
		super().__init__()
		self._element_children=[]

class AssemblySwConnector(complexbase.GroupBase):
	"""AssemblySwConnectors are exclusively used to connect SwComponentPrototypes in the context of a CompositionSwComponentType."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._provider_child=ModelNone
		self._requester_child=ModelNone

class AssignFrameId(complexbase.GroupBase):
	"""Schedule entry for an  Assign Frame Id master request."""
	def __init__(self):
		super().__init__()
		self._assignedFrameTriggering_child=ModelNone

class AssignFrameIdRange(complexbase.GroupBase):
	"""AssignFrameIdRange generates an assign frame PID range request."""
	def __init__(self):
		super().__init__()
		self._framePid_children=[]
		self._startIndex_child=ModelNone

class AssignNad(complexbase.GroupBase):
	"""Schedule entry for an Assign NAD master request."""
	def __init__(self):
		super().__init__()
		self._newNad_child=ModelNone

class AsynchronousServerCallPoint(complexbase.GroupBase):
	"""An AsynchronousServerCallPoint is used for asynchronous invocation of a ClientServerOperation.
IMPORTANT: a ServerCallPoint cannot be used concurrently. Once the client RunnableEntity has made the invocation, the ServerCallPoint cannot be used until the call returns (or an error occurs!) at which point the ServerCallPoint becomes available again."""
	_references=defaultdict(list)

class AsynchronousServerCallResultPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns a AsynchronousServerCallResultPoint it is entitled to get the result of the referenced AsynchronousServerCallPoint.
If it is associated with AsynchronousServerCallReturnsEvent, this RTEEvent notifies the completion of the required ClientServerOperation or a timeout. The occurrence of this event can either unblock a WaitPoint or can lead to the invocation of a RunnableEntity."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._asynchronousServerCallPoint_child=ModelNone
		self._variationPoint_child=ModelNone

class AsynchronousServerCallReturnsEvent(complexbase.GroupBase):
	"""This event is raised when an asynchronous server call is finished."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class AtomicSwComponentType(complexbase.GroupBase):
	"""An atomic software component is atomic in the sense that it cannot be further decomposed and distributed across multiple ECUs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._internalBehavior_children=[]
		self._symbolProps_child=ModelNone

class AtpBlueprint(complexbase.GroupBase):
	"""This meta-class represents the ability to act as a Blueprint. As this class is an abstract one, particular blueprint meta-classes inherit from this one."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._blueprintPolicy_children=[]
		self._shortNamePattern_child=ModelNone

class AtpBlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to express a particular mapping between a blueprint and an element derived from this blueprint.

Particular mappings are defined by specializations of this meta-class."""

class AtpBlueprintable(complexbase.GroupBase):
	"""This meta-class represents the ability to be derived from a Blueprint. As this class is an abstract one, particular blueprintable meta-classes inherit from this one."""
	_references=defaultdict(list)

class AtpClassifier(complexbase.GroupBase):
	"""A classifier classifies M0 instances according to their features. Or: a classifier is something that has instances - an M1 classifier has M0 instances."""
	_references=defaultdict(list)

class AtpDefinition(complexbase.GroupBase):
	"""This abstract meta class represents \"definition\"-elements which identify the respective values. For example the value of a particular system constant is identified by the definition of this system constant."""
	_references=defaultdict(list)

class AtpFeature(complexbase.GroupBase):
	"""Features are properties via which a classifier classifies instances. Or: a classifier has features and every M0 instance of it will have those features."""
	_references=defaultdict(list)

class AtpInstanceRef(complexbase.GroupBase):
	"""An M0 instance of a classifier may be represented as a tree rooted at that instance, where under each node come the sub-trees representing the instances which act as features under that node. 

An instance ref specifies a navigation path from any M0 tree-instance of the base (which is a classifier) to a leaf (which is an instance of the target)."""

class AtpPrototype(complexbase.GroupBase):
	"""A prototype is a typed feature. A prototype in a classifier indicates that instances of that classifier will have a feature, and the structure of that feature is given by the its type. An instance of that type will play the role indicated by the feature in the owning classifier.

A feature is not an instance but an indication of an instance-to-be."""
	_references=defaultdict(list)

class AtpStructureElement(complexbase.GroupBase):
	"""A structure element is both a classifier and a feature. As a feature, its structure is given by the feature it owns as a classifier."""
	_references=defaultdict(list)

class AtpType(complexbase.GroupBase):
	"""A type is a classifier that may serve to type prototypes. It is a reusable classifier."""
	_references=defaultdict(list)

class AttributeValueVariationPoint(complexbase.GroupBase):
	"""This class represents the ability to derive the value of the Attribute from a system constant (by SwSystemconstDependentFormula). It also provides a bindingTime."""
	bindingTime=complexbase.Attribute("bindingTime",SimpleTypes.BindingTimeEnum,'BINDING-TIME',False,"""This is the binding time in which the attribute value needs to be bound.

If this attribute is missing, the attribute is not a variation point. In particular this means that It needs to be a single value according to the type specified in the pure model. It is an error if it is still a formula.""")
	blueprintValue=complexbase.Attribute("blueprintValue",SimpleTypes.String,'BLUEPRINT-VALUE',False,"""This represents a description that documents how the value shall be defined when deriving objects from the blueprint.""")
	sd=complexbase.Attribute("sd",SimpleTypes.String,'SD',False,"""This special data is provided to allow synchronization of Attribute value variation points with variant management systems. The usage is subject of agreement between the involved parties.""")
	shortLabel=complexbase.Attribute("shortLabel",SimpleTypes.PrimitiveIdentifier,'SHORT-LABEL',False,"""This allows to identify the variation point. It is also intended to allow RTE support for CompileTime Variation points.""")

class AutosarDataPrototype(complexbase.GroupBase):
	"""Base class for prototypical roles of an AutosarDataType."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._type_child=ModelNone

class AutosarDataType(complexbase.GroupBase):
	"""Abstract base class for user defined AUTOSAR data types for ECU software."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swDataDefProps_child=ModelNone

class AutosarDataTypeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._AutosarDataType_child=ModelNone
		self._variationPoint_child=ModelNone

class AutosarEngineeringObject(complexbase.GroupBase):
	"""This denotes an engineering object being part of the process. It is a specialization of the abstract class EngineeringObject for usage within AUTOSAR."""

class AutosarOperationArgumentInstance(complexbase.GroupBase):
	"""This class represents a reference to an argument instance. This way it is possible to reference an argument instance in the occurrence expression formula. The argument instance can target to one of the following arguments:

* a whole argument used in an operation of a PortPrototype with ClientServerInterface

* an element inside of a composite argument used in an operation of a PortPrototype with ClientServerInterface"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operationArgumentInstance_child=ModelNone

class AutosarParameterRef(complexbase.GroupBase):
	"""This class represents a reference to a parameter within AUTOSAR which can be one of the following use cases:

localParameter:

* localParameter which is used as whole (e.g.  sharedAxis for curve)

autosarVariable:

* a parameter provided via PortPrototype which is used as whole (e.g. parameterAccess)

* an element inside of a composite local parameter typed by ApplicationDatatype (e.g. sharedAxis for a curve)

* an element inside of a composite parameter provided via Port and typed by ApplicationDatatype (e.g. sharedAxis for a curve)

autosarParameterInImplDatatype:

* an element inside of a composite local parameter typed by ImplementationDatatype

* an element inside of a composite parameter provided via PortPrototype and typed by ImplementationDatatype"""
	def __init__(self):
		super().__init__()
		self._autosarParameter_child=ModelNone
		self._localParameter_child=ModelNone

class AutosarVariableInstance(complexbase.GroupBase):
	"""This class represents a reference to a variable instance within AUTOSAR. This way it is possible to reference a variable instance in the occurrence expression formula. The variable instance can target to one of the following variables:

* a variable provided via a PortPrototype as whole

* an element inside of a composite variable provided via a PortPrototype"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variableInstance_child=ModelNone

class AutosarVariableRef(complexbase.GroupBase):
	"""This class represents a reference to a variable within AUTOSAR which can be one of the following use cases:

localVariable:

* localVariable which is used as whole (e.g. InterRunnableVariable, inputValue for curve)

autosarVariable:

* a variable provided via Port which is used as whole (e.g. dataAccesspoints)

* an element inside of a composite local variable typed by ApplicationDatatype (e.g. inputValue for a curve)

* an element inside of a composite variable provided via Port and typed by ApplicationDatatype (e.g. inputValue for a curve)

autosarVariableInImplDatatype:

* an element inside of a composite local variable typed by ImplementationDatatype  (e.g. nvramData mapping)

* an element inside of a composite variable provided via Port and typed by ImplementationDatatype (e.g. inputValue for a curve)"""
	def __init__(self):
		super().__init__()
		self._autosarVariableInImplDatatype_child=ModelNone
		self._autosarVariable_child=ModelNone
		self._localVariable_child=ModelNone

class BackgroundEvent(complexbase.GroupBase):
	"""This event is used to trigger RunnableEntities that are supposed to be executed in the background."""
	_references=defaultdict(list)

class BaseType(complexbase.GroupBase):
	"""This abstract meta-class represents the ability to specify a platform dependant base type."""
	_references=defaultdict(list)

class BaseTypeDefinition(complexbase.GroupBase):
	"""This meta-class represents the ability to define a basetype."""

class BaseTypeDirectDefinition(complexbase.GroupBase):
	"""This BaseType is defined directly (as opposite to a derived BaseType)"""
	def __init__(self):
		super().__init__()
		self._baseTypeSize_child=ModelNone
		self._maxBaseTypeSize_child=ModelNone
		self._baseTypeEncoding_child=ModelNone
		self._memAlignment_child=ModelNone
		self._byteOrder_child=ModelNone
		self._nativeDeclaration_child=ModelNone

class BlueprintFormula(complexbase.GroupBase):
	"""This class express the extension of the Formula Language to provide formalized blueprint-Value resp. blueprintCondition."""
	def __init__(self):
		super().__init__()
		self._ecucQuery_child=ModelNone
		self._ecuc_child=ModelNone
		self._verbatim_child=ModelNone

class BlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map two an object and its blueprint."""
	def __init__(self):
		super().__init__()
		self._blueprint_child=ModelNone
		self._derivedObject_child=ModelNone

class BlueprintMappingSet(complexbase.GroupBase):
	"""This represents a container of mappings between \"actual\" model elements and the \"blueprint\" that has been taken for their creation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._blueprintMap_children=[]

class BlueprintPolicy(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate whether blueprintable elements will be modifiable or not modifiable."""
	def __init__(self):
		super().__init__()
		self._attributeName_child=ModelNone

class BlueprintPolicyList(complexbase.GroupBase):
	"""The class represents that the related attribute is modifiable during the blueprinting. It applies only to attribute with upper multiplicity greater than 1."""
	def __init__(self):
		super().__init__()
		self._maxNumberOfElements_child=ModelNone
		self._minNumberOfElements_child=ModelNone

class BlueprintPolicyModifiable(complexbase.GroupBase):
	"""The class represents that the related attribute is modifiable during the blueprinting."""
	def __init__(self):
		super().__init__()
		self._blueprintDerivationGuide_child=ModelNone

class BlueprintPolicyNotModifiable(complexbase.GroupBase):
	"""The class represents that the related attribute is not modifiable during the blueprinting."""

class BlueprintPolicySingle(complexbase.GroupBase):
	"""The class represents that the related attribute is modifiable during the blueprinting. It applies only to attribute with upper multiplicity equal 1."""

class BooleanValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Boolean attributes.

Note that this class might be used in the extended meta-model on"""

class Br(complexbase.GroupBase):
	"""This element is the same as function here as in a HTML document i.e. it forces a line break."""

class BswApiOptions(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._enableTakeAddress_child=ModelNone

class BswAsynchronousServerCallPoint(complexbase.GroupBase):
	"""Represents an asynchronous procedure call point via the BSW Scheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._calledEntry_child=ModelNone

class BswAsynchronousServerCallResultPoint(complexbase.GroupBase):
	"""The callback point for an BswAsynchronousServerCallPoint i.e. the point at which the result can be retrieved from the BSW Scheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._asynchronousServerCallPoint_child=ModelNone

class BswAsynchronousServerCallReturnsEvent(complexbase.GroupBase):
	"""This is the \"callback\" event for asynchronous Client-Server-Communication via the BSW Scheduler which is thrown after completion of the asynchronous Client-Server call.

Its eventSource specifies the call point to be used for retrieving the result."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class BswBackgroundEvent(complexbase.GroupBase):
	"""A recurring BswEvent which is used to perform background activities. It is similar to a BswTimingEvent but has no fixed time period and is activated only with low priority."""
	_references=defaultdict(list)

class BswCalledEntity(complexbase.GroupBase):
	"""BSW module entity which is designed to be called from another BSW module or cluster."""
	_references=defaultdict(list)

class BswClientPolicy(complexbase.GroupBase):
	"""The requiredClientServerEntry for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._requiredClientServerEntry_child=ModelNone
		self._variationPoint_child=ModelNone

class BswDataReceivedEvent(complexbase.GroupBase):
	"""This event is thrown on reception of the referenced data via Sender-Receiver-Communication over the BSW Scheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._data_child=ModelNone

class BswDataReceptionPolicy(complexbase.GroupBase):
	"""Specifies the reception policy for the referred data in sender-receiver communication over the BSW Scheduler. To be used for inter-partition and/or inter-core communication."""
	def __init__(self):
		super().__init__()
		self._receivedData_child=ModelNone
		self._variationPoint_child=ModelNone

class BswDataSendPolicy(complexbase.GroupBase):
	"""The data sent over the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._proviedeData_child=ModelNone
		self._variationPoint_child=ModelNone

class BswDebugInfo(complexbase.GroupBase):
	"""Collects the information on the data provided to the AUTOSAR debug module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._localDebugData_children=[]
		self._parameterAccessedForDebug_children=[]
		self._variableAccessedForDebug_children=[]
		self._variationPoint_child=ModelNone

class BswDirectCallPoint(complexbase.GroupBase):
	"""Represents a concrete point in the code from where a BswModuleEntry is called directly, i.e. not via the BSW Scheduler. 

This information can be used to analyze call tree and resource locking scenarios. It is not needed to configure the BSW Scheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._calledEntry_child=ModelNone
		self._calledFromWithinExclusiveArea_child=ModelNone

class BswDistinguishedPartition(complexbase.GroupBase):
	"""Each instance of this meta-class represents an abstract partition in which context the code of the enclosing  BswModuleBehavior can be executed.

The intended use case is to distinguish between several partitions in order to implement different behavior per partition, for example to behave either as a master or satellite in a multicore ECU with shared BSW code."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class BswEvent(complexbase.GroupBase):
	"""Base class of various kinds of events which are used to trigger a BswModuleEntity of this BSW module or cluster. The event is local to the BSW module or cluster. The short name of the meta-class instance is intended as an input to configure the required API of the BSW Scheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._contextLimitation_children=[]
		self._disabledInMode_children=[]
		self._startsOnEvent_child=ModelNone
		self._variationPoint_child=ModelNone

class BswExclusiveAreaPolicy(complexbase.GroupBase):
	"""The ExclusiveArea for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._exclusiveArea_child=ModelNone
		self._variationPoint_child=ModelNone

class BswExternalTriggerOccurredEvent(complexbase.GroupBase):
	"""A BswEvent resulting from a trigger released by another module or cluster."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone

class BswImplementation(complexbase.GroupBase):
	"""Contains the implementation specific information in addition to the generic specification (BswModuleDescription and BswBehavior).
It is possible to have several different BswImplementations referring to the same BswBehavior."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._arReleaseVersion_child=ModelNone
		self._behavior_child=ModelNone
		self._debugInfo_children=[]
		self._preconfiguredConfiguration_children=[]
		self._recommendedConfiguration_children=[]
		self._vendorApiInfix_child=ModelNone
		self._vendorSpecificModuleDef_children=[]

class BswInternalBehavior(complexbase.GroupBase):
	"""Specifies the behavior of a BSW module or a BSW cluster w.r.t. the code entities visible by the BSW Scheduler.
It is possible to have several different BswInternalBehaviors referring to the same BswModuleDescription."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswPerInstanceMemoryPolicy_children=[]
		self._clientPolicy_children=[]
		self._exclusiveAreaPolicy_children=[]
		self._includedDataTypeSet_children=[]
		self._internalTriggeringPointPolicy_children=[]
		self._parameterPolicy_children=[]
		self._releasedTriggerPolicy_children=[]
		self._sendPolicy_children=[]
		self._variationPointProxy_children=[]
		self._internalTriggeringPoint_children=[]
		self._entity_children=[]
		self._event_children=[]
		self._triggerDirectImplementation_children=[]
		self._modeSenderPolicy_children=[]
		self._modeReceiverPolicy_children=[]
		self._serviceDependency_children=[]
		self._perInstanceParameter_children=[]
		self._schedulerNamePrefix_children=[]
		self._receptionPolicy_children=[]
		self._distinguishedPartition_children=[]

class BswInternalTriggerOccurredEvent(complexbase.GroupBase):
	"""A BswEvent, which can happen sporadically. The event is activated by explicit calls from the module to the BSW Scheduler. The main purpose for such an event is to cause a context switch, e.g. from an ISR context into a task context. Activation and switching are handled within the same module or cluster only."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class BswInternalTriggeringPoint(complexbase.GroupBase):
	"""Represents the activation point for one or more BswInternalTriggerOccurredEvents."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swImplPolicy_child=ModelNone
		self._variationPoint_child=ModelNone

class BswInternalTriggeringPointPolicy(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._bswInternalTriggeringPoint_child=ModelNone
		self._variationPoint_child=ModelNone

class BswInternalTriggeringPointRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BswInternalTriggeringPoint_child=ModelNone
		self._variationPoint_child=ModelNone

class BswInterruptEntity(complexbase.GroupBase):
	"""BSW module entity, which is designed to be triggered by an interrupt."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._interruptCategory_child=ModelNone
		self._interruptSource_child=ModelNone

class BswMgrNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Basic Software Manager for one \"user\"."""
	_references=defaultdict(list)

class BswModeManagerErrorEvent(complexbase.GroupBase):
	"""This represents the ability to react on errors occurring during mode handling."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class BswModeReceiverPolicy(complexbase.GroupBase):
	"""Specifies the details for the reception of a mode switch for the referred mode group."""
	def __init__(self):
		super().__init__()
		self._enhancedModeApi_child=ModelNone
		self._requiredModeGroup_child=ModelNone
		self._supportsAsynchronousModeSwitch_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModeSenderPolicy(complexbase.GroupBase):
	"""Specifies the details for the sending of a mode switch for the referred mode group."""
	def __init__(self):
		super().__init__()
		self._ackRequest_child=ModelNone
		self._enhancedModeApi_child=ModelNone
		self._providedModeGroup_child=ModelNone
		self._queueLength_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModeSwitchAckRequest(complexbase.GroupBase):
	"""Requests acknowledgements that a mode switch has been processed  successfully"""
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone

class BswModeSwitchEvent(complexbase.GroupBase):
	"""A BswEvent resulting from a mode switch."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._activation_child=ModelNone
		self._mode_children=[]

class BswModeSwitchedAckEvent(complexbase.GroupBase):
	"""The event is raised after a switch of the referenced mode group has been acknowledged or an error occurs. The referenced mode group must be provided by this module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class BswModuleCallPoint(complexbase.GroupBase):
	"""Represents a point at which a BswModuleEntity handles a procedure call into a BswModuleEntry, either directly or via the BSW Scheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._contextLimitation_children=[]
		self._variationPoint_child=ModelNone

class BswModuleClientServerEntry(complexbase.GroupBase):
	"""This meta-class represents a single API entry into the BSW module or cluster that has the ability to be called in client-server fashion via the BSW Scheduler.

In this regard it is more special than BswModuleEntry and can be seen as a wrapper around the BswModuleEntry to which it refers (property encapsulatedEntry)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._encapsulatedEntry_child=ModelNone
		self._isReentrant_child=ModelNone
		self._isSynchronous_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleDependency(complexbase.GroupBase):
	"""This class collects the dependencies of a BSW module or cluster on a certain other BSW module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._targetModuleId_child=ModelNone
		self._targetModuleRef_children=[]
		self._requiredEntry_children=[]
		self._expectedCallback_children=[]
		self._serviceItem_children=[]
		self._variationPoint_child=ModelNone

class BswModuleDescription(complexbase.GroupBase):
	"""Root element for the description of a single BSW module or BSW cluster.
In case it describes a BSW module, the short name of this element equals the name of the BSW module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._moduleId_child=ModelNone
		self._bswModuleDocumentation_children=[]
		self._providedEntry_children=[]
		self._outgoingCallback_children=[]
		self._bswModuleDependency_children=[]
		self._providedModeGroup_children=[]
		self._requiredModeGroup_children=[]
		self._releasedTrigger_children=[]
		self._requiredTrigger_children=[]
		self._providedClientServerEntry_children=[]
		self._requiredClientServerEntry_children=[]
		self._providedData_children=[]
		self._requiredData_children=[]
		self._internalBehavior_children=[]

class BswModuleDescriptionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BswModuleDescription_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleEntity(complexbase.GroupBase):
	"""Specifies the smallest code fragment which can be described for a BSW module or cluster within AUTOSAR."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessedModeGroup_children=[]
		self._activationPoint_children=[]
		self._callPoint_children=[]
		self._calledEntry_children=[]
		self._dataReceivePoint_children=[]
		self._dataSendPoint_children=[]
		self._implementedEntry_child=ModelNone
		self._issuedTrigger_children=[]
		self._managedModeGroup_children=[]
		self._schedulerNamePrefix_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleEntry(complexbase.GroupBase):
	"""This class represents a single API entry (C-function prototype) into the BSW module or cluster.

The name of the C-function is equal to the short name of this element with one exception: In case of multiple instances of a module on the same CPU, special rules for \"infixes\" apply, see description of class BswImplementation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._serviceId_child=ModelNone
		self._role_child=ModelNone
		self._isReentrant_child=ModelNone
		self._isSynchronous_child=ModelNone
		self._callType_child=ModelNone
		self._executionContext_child=ModelNone
		self._swServiceImplPolicy_child=ModelNone
		self._returnType_child=ModelNone
		self._argument_children=[]

class BswModuleEntryRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BswModuleEntry_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleTiming(complexbase.GroupBase):
	"""A model element used to define timing descriptions and constraints for the BswInternalBehavior of one BSW Module. Thereby, for each BswInternalBehavior a separate timing can be specified.

A constraint defined at this level holds true for all Implementations of that BswInternalBehavior.

TimingDescriptions aggregated by BswModuleTiming are restricted to event chains referring to events which are derived from the class TDEventBswInternalBehavior."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone

class BswOperationInvokedEvent(complexbase.GroupBase):
	"""This event is thrown on operation invocation in Client-Server-Communication via the BSW Scheduler. Its \"entry\" reference provides the BswClientServerEntry that is called subsequently.

Note this event is not needed in case of direct function calls."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._entry_child=ModelNone

class BswParameterPolicy(complexbase.GroupBase):
	"""The perInstanceParameter for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._perInstanceParameter_child=ModelNone
		self._variationPoint_child=ModelNone

class BswPerInstanceMemoryPolicy(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._arTypedPerInstanceMemory_child=ModelNone
		self._variationPoint_child=ModelNone

class BswQueuedDataReceptionPolicy(complexbase.GroupBase):
	"""Reception policy attributes specific for queued receiving."""
	def __init__(self):
		super().__init__()
		self._queueLength_child=ModelNone

class BswReleasedTriggerPolicy(complexbase.GroupBase):
	"""The Trigger for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._releasedTrigger_child=ModelNone
		self._variationPoint_child=ModelNone

class BswSchedulableEntity(complexbase.GroupBase):
	"""BSW module entity, which is designed for control by the BSW Scheduler. It may for example implement a so-called \"main\" function."""
	_references=defaultdict(list)

class BswScheduleEvent(complexbase.GroupBase):
	"""BswEvent that is able to start a BswSchedulabeEntity."""
	_references=defaultdict(list)

class BswSchedulerNamePrefix(complexbase.GroupBase):
	"""A prefix to be used in names of generated code artifacts which make up the interface of a BSW module to the BswScheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class BswServiceDependency(complexbase.GroupBase):
	"""Specialization of ServiceDependency in the context of an BswInternalBehavior. It allows to associate BswModuleEntries and data defined for a BSW module or cluster to a given ServiceNeeds element."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone
		self._assignedData_children=[]
		self._assignedEntryRole_children=[]
		self._serviceNeeds_child=[]
		self._variationPoint_child=ModelNone

class BswServiceDependencyIdent(complexbase.GroupBase):
	"""This meta-class is created to add the ability to become the target of a reference to the non-Referrable BswServiceDependency."""
	_references=defaultdict(list)

class BswSynchronousServerCallPoint(complexbase.GroupBase):
	"""Represents a synchronous procedure call point via the BSW Scheduler."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._calledEntry_child=ModelNone
		self._calledFromWithinExclusiveArea_child=ModelNone

class BswTimingEvent(complexbase.GroupBase):
	"""A recurring BswEvent driven by a time period."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone

class BswTriggerDirectImplementation(complexbase.GroupBase):
	"""Specifies a released trigger to be directly implemented via OS calls, for example in a Complex Driver module."""
	def __init__(self):
		super().__init__()
		self._masteredTrigger_child=ModelNone
		self._task_child=ModelNone
		self._variationPoint_child=ModelNone

class BswVariableAccess(complexbase.GroupBase):
	"""The presence of a BswVariableAccess implies that a BswModuleEntity needs access to a VariableDataPrototype via the BSW Scheduler.

The kind of access is specified by the role in which the class is used."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessedVariable_child=ModelNone
		self._contextLimitation_children=[]
		self._variationPoint_child=ModelNone

class BufferProperties(complexbase.GroupBase):
	"""Configuration of the buffer properties the transformer needs to work."""
	def __init__(self):
		super().__init__()
		self._bufferComputation_child=ModelNone
		self._headerLength_child=ModelNone
		self._inPlace_child=ModelNone

class BuildAction(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a build action."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._predecessorAction_children=[]
		self._followUpAction_children=[]
		self._createdData_children=[]
		self._inputData_children=[]
		self._modifiedData_children=[]
		self._requiredEnvironment_child=ModelNone
		self._variationPoint_child=ModelNone

class BuildActionEntity(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a build action entity which might be specialized to environments as well as to individual build actions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._deliveryArtifact_children=[]
		self._invocation_child=ModelNone

class BuildActionEnvironment(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a build action environment."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._sdg_children=[]
		self._variationPoint_child=ModelNone

class BuildActionInvocator(complexbase.GroupBase):
	"""This meta-class represents the ability to specify the invocation of a task in a build action."""
	def __init__(self):
		super().__init__()
		self._command_child=ModelNone
		self._sdg_children=[]

class BuildActionIoElement(complexbase.GroupBase):
	"""This meta-class represents the ability to specify the input/output entities of a BuildAction."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._sdg_children=[]
		self._ecucDefinition_child=ModelNone
		self._engineeringObject_child=ModelNone
		self._foreignModelReference_child=ModelNone
		self._modelObjectReference_child=ModelNone
		self._role_child=ModelNone

class BuildActionManifest(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a manifest for processing artifacts. An example use case is the processing of ECUC parameter values."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._startAction_children=[]
		self._tearDownAction_children=[]
		self._buildAction_children=[]
		self._buildActionEnvironment_children=[]
		self._dynamicAction_children=[]

class BuildActionManifestRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BuildActionManifest_child=ModelNone
		self._variationPoint_child=ModelNone

class BuildEngineeringObject(complexbase.GroupBase):
	"""This meta-class represents the ability to denote an artifact which is processed within a particular build action."""
	def __init__(self):
		super().__init__()
		self._fileType_child=ModelNone
		self._intendedFilename_child=ModelNone
		self._parentCategory_child=ModelNone
		self._parentShortLabel_child=ModelNone
		self._shortLabelPattern_child=ModelNone
		self._fileTypePattern_child=ModelNone

class BurstPatternEventTriggering(complexbase.GroupBase):
	"""A BurstPatternEventTriggering describes the maximum number of occurrences of the same event in a given time interval.
This is typically used to model a worst case activation scenario."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maxNumberOfOccurrences_child=ModelNone
		self._minimumInterArrivalTime_child=ModelNone
		self._patternJitter_child=ModelNone
		self._patternLength_child=ModelNone
		self._patternPeriod_child=ModelNone
		self._minNumberOfOccurrences_child=ModelNone

class BusspecificNmEcu(complexbase.GroupBase):
	"""Busspecific NmEcu attributes."""

class CalibrationParameterValue(complexbase.GroupBase):
	"""Specifies instance specific calibration parameter values used to initialize the memory objects implementing calibration parameters in the generated RTE code.

RTE generator will use the implInitValue to override the initial values specified for the DataPrototypes of a component type. 

The applInitValue is used to exchange init values with the component vendor not publishing the transformation algorithm between ApplicationDataTypes and ImplementationDataTypes or defining a instance specific initialization of components which are only defined with ApplicationDataTypes.

Note: If both representations of init values are available these need to represent the same content.

Note further that in this case an explicit mapping of ValueSpecification is not implemented because calibration parameters are delivered back after the calibration phase."""
	def __init__(self):
		super().__init__()
		self._applInitValue_child=[]
		self._implInitValue_child=[]
		self._initializedParameter_child=ModelNone
		self._variationPoint_child=ModelNone

class CalibrationParameterValueSet(complexbase.GroupBase):
	"""Specification of a constant that can be part of a package, i.e. it can be defined stand-alone."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._calibrationParameterValue_children=[]

class CanCluster(complexbase.GroupBase):
	"""CAN bus specific cluster attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._CanClusterVariant_children=[]

class CanClusterBusOffRecovery(complexbase.GroupBase):
	"""This element contains the attributes that are used to configure the CAN bus off monitoring / recovery at system level."""
	def __init__(self):
		super().__init__()
		self._borCounterL1ToL2_child=ModelNone
		self._borTimeL1_child=ModelNone
		self._borTimeL2_child=ModelNone
		self._borTimeTxEnsured_child=ModelNone
		self._mainFunctionPeriod_child=ModelNone

class CanClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class CanClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CanCommunicationConnector(complexbase.GroupBase):
	"""CAN bus specific communication connector attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._pncWakeupCanId_child=ModelNone
		self._pncWakeupCanIdExtended_child=ModelNone
		self._pncWakeupCanIdMask_child=ModelNone
		self._pncWakeupDataMask_child=ModelNone
		self._pncWakeupDlc_child=ModelNone

class CanCommunicationController(complexbase.GroupBase):
	"""CAN bus specific communication port attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._CanCommunicationControllerVariant_children=[]

class CanCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class CanCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CanControllerConfiguration(complexbase.GroupBase):
	"""This element is used for the specification of the exact CAN Bit Timing configuration parameter values."""
	def __init__(self):
		super().__init__()
		self._propSeg_child=ModelNone
		self._syncJumpWidth_child=ModelNone
		self._timeSeg1_child=ModelNone
		self._timeSeg2_child=ModelNone

class CanControllerConfigurationRequirements(complexbase.GroupBase):
	"""This element allows the specification of ranges for the CAN Bit Timing configuration parameters. These ranges are taken as requirements and have to be respected by the ECU developer."""
	def __init__(self):
		super().__init__()
		self._maxNumberOfTimeQuantaPerBit_child=ModelNone
		self._maxSamplePoint_child=ModelNone
		self._maxSyncJumpWidth_child=ModelNone
		self._minNumberOfTimeQuantaPerBit_child=ModelNone
		self._minSamplePoint_child=ModelNone
		self._minSyncJumpWidth_child=ModelNone

class CanControllerFdConfiguration(complexbase.GroupBase):
	"""Bit timing related configuration of a CAN controller for payload and CRC of a CAN FD frame."""
	def __init__(self):
		super().__init__()
		self._paddingValue_child=ModelNone
		self._propSeg_child=ModelNone
		self._syncJumpWidth_child=ModelNone
		self._timeSeg1_child=ModelNone
		self._timeSeg2_child=ModelNone
		self._trcvDelayCompensationOffset_child=ModelNone
		self._txBitRateSwitch_child=ModelNone

class CanControllerFdConfigurationRequirements(complexbase.GroupBase):
	"""This element allows the specification of ranges for the CanFD bit timing configuration parameters. These ranges are taken as requirements and shall be respected by the ECU developer."""
	def __init__(self):
		super().__init__()
		self._maxNumberOfTimeQuantaPerBit_child=ModelNone
		self._maxSamplePoint_child=ModelNone
		self._maxSyncJumpWidth_child=ModelNone
		self._maxTrcvDelayCompensationOffset_child=ModelNone
		self._minNumberOfTimeQuantaPerBit_child=ModelNone
		self._minSamplePoint_child=ModelNone
		self._minSyncJumpWidth_child=ModelNone
		self._minTrcvDelayCompensationOffset_child=ModelNone
		self._paddingValue_child=ModelNone
		self._txBitRateSwitch_child=ModelNone

class CanFrame(complexbase.GroupBase):
	"""CAN specific Frame element. This element shall also be used for TTCan."""
	_references=defaultdict(list)

class CanFrameTriggering(complexbase.GroupBase):
	"""CAN specific attributes to the FrameTriggering"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._absolutelyScheduledTiming_children=[]
		self._canAddressingMode_child=ModelNone
		self._canFdFrameSupport_child=ModelNone
		self._canFrameRxBehavior_child=ModelNone
		self._canFrameTxBehavior_child=ModelNone
		self._identifier_child=ModelNone
		self._j1939requestable_child=ModelNone
		self._rxIdentifierRange_child=ModelNone
		self._rxMask_child=ModelNone
		self._txMask_child=ModelNone

class CanNmCluster(complexbase.GroupBase):
	"""Can specific NmCluster attributes"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmBusloadReductionActive_child=ModelNone
		self._nmCarWakeUpBitPosition_child=ModelNone
		self._nmCarWakeUpFilterEnabled_child=ModelNone
		self._nmCarWakeUpFilterNodeId_child=ModelNone
		self._nmCarWakeUpRxEnabled_child=ModelNone
		self._nmCbvPosition_child=ModelNone
		self._nmChannelActive_child=ModelNone
		self._nmImmediateNmCycleTime_child=ModelNone
		self._nmImmediateNmTransmissions_child=ModelNone
		self._nmMessageTimeoutTime_child=ModelNone
		self._nmMsgCycleTime_child=ModelNone
		self._nmNetworkTimeout_child=ModelNone
		self._nmNidPosition_child=ModelNone
		self._nmRemoteSleepIndicationTime_child=ModelNone
		self._nmRepeatMessageTime_child=ModelNone
		self._nmUserDataLength_child=ModelNone
		self._nmWaitBusSleepTime_child=ModelNone

class CanNmClusterCoupling(complexbase.GroupBase):
	"""CAN attributes that are valid for each of the referenced (coupled) CAN clusters."""
	def __init__(self):
		super().__init__()
		self._coupledCluster_children=[]
		self._nmBusloadReductionEnabled_child=ModelNone
		self._nmImmediateRestartEnabled_child=ModelNone

class CanNmEcu(complexbase.GroupBase):
	"""CAN specific attributes."""
	def __init__(self):
		super().__init__()
		self._nmRepeatMsgIndicationEnabled_child=ModelNone

class CanNmNode(complexbase.GroupBase):
	"""CAN specific NM Node attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmCarWakeUpFilterEnabled_child=ModelNone
		self._nmCarWakeUpRxEnabled_child=ModelNone
		self._nmMsgCycleOffset_child=ModelNone
		self._nmMsgReducedTime_child=ModelNone
		self._nmRangeConfig_child=ModelNone

class CanNmRangeConfig(complexbase.GroupBase):
	"""Defines the CANID ranges that are used for Nm. This range definition is redundant to the attribute \"rxIdentifierRange\" of CanFrameTriggering. For backward compatibility reasons this redundancy shall be preserved and both shall be defined.
In future this element will be removed from the model."""
	def __init__(self):
		super().__init__()
		self._lowerCanId_child=ModelNone
		self._upperCanId_child=ModelNone

class CanPhysicalChannel(complexbase.GroupBase):
	"""CAN bus specific physical channel attributes."""
	_references=defaultdict(list)

class CanTpAddress(complexbase.GroupBase):
	"""An ECUs TP address on the referenced channel. This represents the diagnostic Address."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._tpAddress_child=ModelNone
		self._tpAddressExtensionValue_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpChannel(complexbase.GroupBase):
	"""Configuration parameters of the CanTp channel."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._channelId_child=ModelNone
		self._channelMode_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpConfig(complexbase.GroupBase):
	"""This element defines exactly one CAN TP Configuration. 

One CanTpConfig element shall be created for each CAN Network in the System."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpChannel_children=[]
		self._tpConnection_children=[]
		self._tpEcu_children=[]
		self._tpNode_children=[]

class CanTpConnection(complexbase.GroupBase):
	"""A connection identifies the sender and the receiver of this particular communication. The CanTp module routes a Pdu through this connection. 

atpVariation: Derived, because TpNode can vary."""
	def __init__(self):
		super().__init__()
		self._addressingFormat_child=ModelNone
		self._canTpChannel_child=ModelNone
		self._cancellation_child=ModelNone
		self._dataPdu_child=ModelNone
		self._flowControlPdu_child=ModelNone
		self._maxBlockSize_child=ModelNone
		self._multicast_child=ModelNone
		self._paddingActivation_child=ModelNone
		self._receiver_children=[]
		self._taType_child=ModelNone
		self._timeoutBr_child=ModelNone
		self._timeoutBs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._timeoutCs_child=ModelNone
		self._tpSdu_child=ModelNone
		self._transmitCancellation_child=ModelNone
		self._transmitter_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpEcu(complexbase.GroupBase):
	"""ECU specific TP configuration parameters. Each TpEcu element has a reference to exactly one ECUInstance in the topology."""
	def __init__(self):
		super().__init__()
		self._cycleTimeMainFunction_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._connector_child=ModelNone
		self._maxFcWait_child=ModelNone
		self._stMin_child=ModelNone
		self._timeoutAr_child=ModelNone
		self._timeoutAs_child=ModelNone
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class Caption(complexbase.GroupBase):
	"""This meta-class represents the ability to express a caption which is a title, and a shortName."""
	_references=defaultdict(list)

class Chapter(complexbase.GroupBase):
	"""This meta-class represents a chapter of a document. Chapters are the primary structuring element in documentation."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.

Maybe it is a concatenated Identifier, but as of now we leave it as an arbitrary string.""")
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class ChapterContent(complexbase.GroupBase):
	"""This class represents the content which is directly in a chapter. It is basically the same as the one in a Topic but might have additional complex structures (e.g. Synopsis)"""
	def __init__(self):
		super().__init__()
		self._prms_child=ModelNone

class ChapterModel(complexbase.GroupBase):
	"""This is the basic content model of a chapter except the Chapter title. This can be utilized in general chapters as well as in predefined chapters.

A chapter has content on three levels:

1. chapter content

2. topics

3. subchapters"""

class ChapterOrMsrQuery(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a particular chapter or a query returning a chapter."""
	def __init__(self):
		super().__init__()
		self._chapter_children=[]
		self._msrQueryChapter_child=ModelNone

class ClientComSpec(complexbase.GroupBase):
	"""Client-specific communication attributes (RPortPrototype typed by ClientServerInterface)."""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._transformationComSpecProps_children=[]

class ClientIdDefinition(complexbase.GroupBase):
	"""Several clients in one client-ECU can communicate via inter-ECU client-server communication with a server on a different ECU, if a client identifier is used to distinguish the different clients.
The Client Identifier of the transaction handle that is used by the RTE can be defined by this element."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._clientId_child=ModelNone
		self._clientServerOperation_child=ModelNone
		self._variationPoint_child=ModelNone

class ClientIdDefinitionSet(complexbase.GroupBase):
	"""Set of Client Identifiers that are used for inter-ECU client-server communication in the System."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._clientIdDefinition_children=[]

class ClientIdMapping(complexbase.GroupBase):
	"""In case of a server on one ECU with multiple clients on other ECUs, the client server communication shall use different unique COM signals and signal groups for each client to allow the identification of the client associated with each system signal.

The ClientId is mapped to the requestGroup and to the responseGroup."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class ClientIdRange(complexbase.GroupBase):
	"""With this element it is possible to restrict the Client Identifier of the transaction handle that is generated by the client RTE for inter-Ecu Client/Server communication to an allowed range of numerical values."""
	def __init__(self):
		super().__init__()
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone

class ClientServerAnnotation(complexbase.GroupBase):
	"""Annotation to a port regarding a certain Operation."""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone

class ClientServerApplicationErrorMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map ApplicationErrors onto each other."""
	def __init__(self):
		super().__init__()
		self._firstApplicationError_child=ModelNone
		self._secondApplicationError_child=ModelNone

class ClientServerArrayElementMapping(complexbase.GroupBase):
	"""The ApplicationArrayElement may be a primitive one or a composite one. If the element is primitive, it will be mapped to the \"SystemSignal\" (multiplicity 1). If the ArgumentDataPrototype that is referenced by ClientServerCompositeTypeMapping is typed by an ApplicationDataType the reference to the ApplicationArrayElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference to the ImplementationArrayElement shall be used. 

If the element is composite, there will be no mapping to the \"SystemSignal\" (multiplicity 0). In this case the \"ArrayElementMapping\" Element will aggregate the \"TypeMapping\" Element. In that way also the composite datatypes can be mapped to SystemSignals. 

Regardless whether composite or primitive array element is mapped the indexed array element always needs to be specified."""
	def __init__(self):
		super().__init__()
		self._complexTypeMapping_child=[]
		self._indexedArrayElement_child=ModelNone
		self._systemSignal_child=ModelNone

class ClientServerArrayTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is an Array, the \"ArrayTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._arrayElementMapping_children=[]

class ClientServerCompositeTypeMapping(complexbase.GroupBase):
	"""Two mappings exist for the composite data types: \"ArrayTypeMapping\" and \"RecordTypeMapping\".
In both, a primitive datatype will be mapped to a system signal. 

But it is also possible to combine the arrays and the records, so that an \"array\" could be an element of a \"record\" and in the same manner a \"record\" could be an element of an \"array\". Nesting these data types is also possible.

If an element of a composite data type is again a composite one, the \"CompositeTypeMapping\" element will be used one more time (aggregation between the ArrayElementMapping and CompositeTypeMapping or aggregation between the RecordElementMapping and CompositeTypeMapping)."""
	def __init__(self):
		super().__init__()
		self._argument_child=ModelNone

class ClientServerInterface(complexbase.GroupBase):
	"""A client/server interface declares a number of operations that can be invoked on a server by a client."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._possibleError_children=[]

class ClientServerInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of ClientServerOperations in context of two different ClientServerInterfaces."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._errorMapping_children=[]
		self._operationMapping_children=[]

class ClientServerOperation(complexbase.GroupBase):
	"""An operation declared within the scope of a client/server interface."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._argument_children=[]
		self._possibleError_children=[]
		self._variationPoint_child=ModelNone

class ClientServerOperationMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular ClientServerOperations in context of two different ClientServerInterfaces."""
	def __init__(self):
		super().__init__()
		self._argumentMapping_children=[]
		self._firstOperation_child=ModelNone
		self._secondOperation_child=ModelNone

class ClientServerPrimitiveTypeMapping(complexbase.GroupBase):
	"""Mapping of an argument with a primitive datatype to a signal."""
	def __init__(self):
		super().__init__()
		self._argument_child=ModelNone
		self._systemSignal_child=ModelNone

class ClientServerRecordElementMapping(complexbase.GroupBase):
	"""Mapping of a primitive record element to a SystemSignal. If the ArgumentDataPrototype that is referenced by ClientServerCompositeTypeMapping is typed by an ApplicationDataType the reference to the ApplicationRecordElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference to the ImplementationRecordElement shall be used. 

If the element is composite, there will be no mapping (multiplicity 0). In this case the \"RecordElementMapping\" Element will aggregate the \"TypeMapping\" Element. In that way also the composite datatypes can be mapped to SystemSignals. 

Regardless whether composite or primitive record element is mapped the record element always needs to be specified."""
	def __init__(self):
		super().__init__()
		self._applicationRecordElement_child=ModelNone
		self._complexTypeMapping_child=[]
		self._implementationRecordElement_child=ModelNone
		self._systemSignal_child=ModelNone

class ClientServerRecordTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is a Record, the \"RecordTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._recordElementMapping_children=[]

class ClientServerToSignalGroupMapping(complexbase.GroupBase):
	"""This mapping is deprecated and will be removed in future. It is replaced by the ClientServerToSignalMapping.

Old description:
Mapping of client server operation arguments to signals of a signal group. Arguments with a primitive datatype will be mapped via the \"ClientServerPrimitiveTypeMapping\" element. 
Arguments with composite datatypes will be mapped via the \"CompositeTypeMapping\" element."""
	def __init__(self):
		super().__init__()
		self._applicationError_child=ModelNone
		self._clientID_child=ModelNone
		self._compositeTypeMapping_children=[]
		self._emptySignal_child=ModelNone
		self._mappedOperation_child=ModelNone
		self._primitiveTypeMapping_children=[]
		self._requestGroup_child=ModelNone
		self._responseGroup_child=ModelNone
		self._sequenceCounter_child=ModelNone

class ClientServerToSignalMapping(complexbase.GroupBase):
	"""This element maps the ClientServerOperation to call- and return-SystemSignals. The serialization is defined by the referenced SerializationTechnology."""
	def __init__(self):
		super().__init__()
		self._callSignal_child=ModelNone
		self._clientServerOperation_child=ModelNone
		self._lengthClientId_child=ModelNone
		self._lengthSequenceCounter_child=ModelNone
		self._returnSignal_child=ModelNone
		self._serializer_child=ModelNone

class Code(complexbase.GroupBase):
	"""A generic code descriptor. The type of the code (source or object) is defined via the category attribute of the associated engineering object."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._artifactDescriptor_children=[]
		self._callbackHeader_children=[]

class CollectableElement(complexbase.GroupBase):
	"""This meta-class specifies the ability to be part of a specific  AUTOSAR collection of ARPackages or ARElements.

The scope of collection has been extended beyond CollectableElement with Revision 4.0.3. For compatibility reasons the name of this meta Class was not changed."""
	_references=defaultdict(list)

class Collection(complexbase.GroupBase):
	"""This meta-class specifies a collection of elements. A collection can be utilized to express additional aspects for a set of elements. 

Note that Collection is an ARElement. Therefore it is applicable e.g. for EvaluatedVariant, even if this is not obvious.

Usually the category of a Collection is \"SET\". On the other hand, a Collection can also express an arbitrary relationship between elements. This is denoted by the category \"RELATION\" (see also [TPS_GST_00347]). 

In this case the collection represents an association from \"sourceElement\" to \"targetElement\" in the role \"role\"."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._autoCollect_child=ModelNone
		self._elementRole_child=ModelNone
		self._element_children=[]
		self._sourceElement_children=[]
		self._collectedInstance_children=[]
		self._sourceInstance_children=[]

class Colspec(complexbase.GroupBase):
	"""This meta-class represents the ability to specify the properties of a column in a  table."""
	align=complexbase.Attribute("align",SimpleTypes.AlignEnum,'ALIGN',False,"""Specifies how the cell entries shall be horizontally aligned within the specified column.
Default is \"LEFT\"""")
	colname=complexbase.Attribute("colname",SimpleTypes.String,'COLNAME',False,"""Specifies the name of the column.""")
	colnum=complexbase.Attribute("colnum",SimpleTypes.String,'COLNUM',False,"""column number (allows to sort the columns).""")
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates whether a line should be displayed right of this column in the column specification.""")
	colwidth=complexbase.Attribute("colwidth",SimpleTypes.String,'COLWIDTH',False,"""Width of the column.
You can enter absolute values such as 4 cm, 
or relative values marked with * (e.g.,  2* for column widths double those of other columns with 1*).
The unit can be added to the number in the string. Possible units are: cm, mm, px, pt.""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates whether a line should be displayed at the bottom end of the cells of the column defined in the Colspec.""")

class ComMgrUserNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Communication Manager for one \"user\"."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maxCommMode_child=ModelNone

class CommConnectorPort(complexbase.GroupBase):
	"""The Ecu communication relationship defines which signals, Pdus and frames are actually received and transmitted by this ECU. 

For each signal, Pdu or Frame that is transmitted or received and used by the Ecu an association between an ISignalPort, IPduPort or FramePort with the corresponding Triggering shall be created. 
An ISignalPort shall be created only if the corresponding signal is handled by COM (RTE or Signal Gateway). If a Pdu Gateway ECU only routes the Pdu without being interested in the content only a FramePort and an IPduPort needs to be created."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationDirection_child=ModelNone
		self._variationPoint_child=ModelNone

class CommonSignalPath(complexbase.GroupBase):
	"""The CommonSignalPath describes that two or more SwcToSwcSignals and/or SwcToSwcOperationArguments must take the same way (Signal Path) in the topology."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._signal_children=[]

class CommunicationCluster(complexbase.GroupBase):
	"""The CommunicationCluster is the main element to describe the topological connection of communicating ECUs.

A cluster describes the ensemble of ECUs, which are linked by a communication medium of arbitrary topology (bus, star, ring, ...). The nodes within the cluster share the same communication protocol, which may be event-triggered, time-triggered or a combination of both.

A CommunicationCluster aggregates one or more physical channels."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._baudrate_child=ModelNone
		self._physicalChannel_children=[]
		self._protocolName_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._speed_child=ModelNone

class CommunicationClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CommunicationConnector(complexbase.GroupBase):
	"""The connection between the referencing ECU and the referenced channel via the referenced controller.

Connectors are used to describe the bus interfaces of the ECUs and to specify the sending/receiving behavior. 
Each CommunicationConnector has a reference to exactly one communicationController. 

Note: Several CommunicationConnectors can be assigned to one PhysicalChannel in the scope of one ECU Instance."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._commController_child=ModelNone
		self._ecuCommPortInstance_children=[]
		self._pncGatewayType_child=ModelNone

class CommunicationConnectorRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._CommunicationConnector_child=ModelNone
		self._variationPoint_child=ModelNone

class CommunicationController(complexbase.GroupBase):
	"""The communication controller is a dedicated hardware device by means of which hosts are sending frames to and receiving frames from the communication medium."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._wakeUpByControllerSupported_child=ModelNone

class CommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CommunicationControllerMapping(complexbase.GroupBase):
	"""CommunicationControllerMapping specifies the CommunicationPeripheral
hardware (defined in the ECU Resource Template) to realize the specified CommunicationController in a physical topology."""
	def __init__(self):
		super().__init__()
		self._communicationController_child=ModelNone
		self._hwCommunicationController_child=ModelNone

class CommunicationCycle(complexbase.GroupBase):
	"""The communication cycle where the frame is sent."""

class Compiler(complexbase.GroupBase):
	"""Specifies the compiler attributes. In case of source code this specifies requirements how the compiler shall be invoked. In case of object code this documents the used compiler settings."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._options_child=ModelNone
		self._vendor_child=ModelNone
		self._version_child=ModelNone

class ComplexDeviceDriverSwComponentType(complexbase.GroupBase):
	"""The ComplexDeviceDriverSwComponentType is a special AtomicSwComponentType that has direct access to hardware on an ECU and which is therefore linked to a specific ECU or specific hardware. The ComplexDeviceDriverSwComponentType introduces the possibility to link from the software representation to its hardware description provided by the ECU Resource Template."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hardwareElement_children=[]

class ComponentClustering(complexbase.GroupBase):
	"""Constraint that forces the mapping of all referenced SW component instances  to the same ECU, Core, Partition depending on the defined mappingScope attribute.  If mappingScope is not specified then mappingScopeEcu shall be assumed."""
	def __init__(self):
		super().__init__()
		self._clusteredComponent_children=[]
		self._mappingScope_child=ModelNone

class ComponentInCompositionInstanceRef(complexbase.GroupBase):
	"""The ComponentInCompositionInstanceRef points to a concrete SwComponentPrototype within a CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._contextComponent_children=[]
		self._targetComponent_child=ModelNone

class ComponentInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._targetComponent_child=ModelNone

class ComponentSeparation(complexbase.GroupBase):
	"""Constraint that forces the two referenced SW components (called A and B in the following) not to be mapped to the same ECU, Core, Partition depending on the defined mappingScope attribute. If mappingScope is not specified then mappingScopeEcu shall be assumed. 

If a SW component (e.g. A) is a composition, none of the atomic SW components making up the A composition must be mapped together with any of the atomic SW components making up the B composition. Furthermore, A and B must be disjoint."""
	def __init__(self):
		super().__init__()
		self._mappingScope_child=ModelNone
		self._separatedComponent_children=[]

class CompositeNetworkRepresentation(complexbase.GroupBase):
	"""This meta-class is used to define the network representation of leaf elements of composite application data types."""
	def __init__(self):
		super().__init__()
		self._leafElement_child=ModelNone
		self._networkRepresentation_child=ModelNone

class CompositionSwComponentType(complexbase.GroupBase):
	"""A CompositionSwComponentType aggregates SwComponentPrototypes (that in turn are typed by SwComponentTypes) as well as SwConnectors for primarily connecting SwComponentPrototypes among each others and towards the surface of the CompositionSwComponentType. By this means hierarchical structures of software-components can be created."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._component_children=[]
		self._connector_children=[]
		self._constantValueMapping_children=[]
		self._dataTypeMapping_children=[]
		self._instantiationRTEEventProps_children=[]

class Compu(complexbase.GroupBase):
	"""This meta-class represents the ability to express one particular computation."""
	def __init__(self):
		super().__init__()
		self._compuDefaultValue_child=ModelNone

class CompuConst(complexbase.GroupBase):
	"""This meta-class represents the fact that the value of a computation method scale is constant."""

class CompuConstContent(complexbase.GroupBase):
	"""This meta-class represents the fact that the constant value of the computation method can be numerical or textual."""

class CompuConstFormulaContent(complexbase.GroupBase):
	"""This meta-class represents the fact that the constant value of the computation method is represented by a variation point. This difference is due to compatibility with ASAM HDO."""
	def __init__(self):
		super().__init__()
		self._vf_child=ModelNone

class CompuConstNumericContent(complexbase.GroupBase):
	"""This meta-class represents the fact that the constant value of the computation method is a numerical value. It is separated from CompuConstFormulaContent to support compatibility with ASAM HDO."""
	def __init__(self):
		super().__init__()
		self._v_child=ModelNone

class CompuConstTextContent(complexbase.GroupBase):
	"""This meta-class represents the textual content of a scale."""
	def __init__(self):
		super().__init__()
		self._vt_child=ModelNone

class CompuContent(complexbase.GroupBase):
	"""This abstract meta-class represents the various definition means of a computation method."""

class CompuGenericMath(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a generic formula expression."""
	level=complexbase.Attribute("level",SimpleTypes.PrimitiveIdentifier,'LEVEL',False,"""Placeholder to describe an indicator of a language level for the mathematics e.g. INFORMAL, ASAMHDO.
May be refined by particular use-cases.""")

class CompuMethod(complexbase.GroupBase):
	"""This meta-class represents the ability to express the relationship between a physical value and the mathematical representation. 

Note that this is still independent of the technical implementation in data types. It only specifies the formula how the internal value corresponds to its physical pendant."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._displayFormat_child=ModelNone
		self._unit_child=ModelNone
		self._compuInternalToPhys_child=ModelNone
		self._compuPhysToInternal_child=ModelNone

class CompuNominatorDenominator(complexbase.GroupBase):
	"""This class represents the ability to express a polynomial either as Nominator or as Denominator."""
	def __init__(self):
		super().__init__()
		self._v_children=[]

class CompuRationalCoeffs(complexbase.GroupBase):
	"""This meta-class represents the ability to express a rational function by specifying the coefficients of nominator and denominator."""
	def __init__(self):
		super().__init__()
		self._compuNumerator_child=ModelNone
		self._compuDenominator_child=ModelNone

class CompuScale(complexbase.GroupBase):
	"""This meta-class represents the ability to specify one segment of a segmented computation method."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._symbol_child=ModelNone
		self._desc_child=ModelNone
		self._mask_child=ModelNone
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone
		self._compuInverseValue_child=ModelNone
		self._variationPoint_child=ModelNone

class CompuScaleConstantContents(complexbase.GroupBase):
	"""This meta-class represents the fact that a particular scale of the computation method is constant."""
	def __init__(self):
		super().__init__()
		self._compuConst_child=ModelNone

class CompuScaleContents(complexbase.GroupBase):
	"""This abstract meta-class represents the content of one particular scale."""

class CompuScaleRationalFormula(complexbase.GroupBase):
	"""This meta-class represents the fact that the computation in this scale is represented as rational term."""
	def __init__(self):
		super().__init__()
		self._compuRationalCoeffs_child=ModelNone

class CompuScales(complexbase.GroupBase):
	"""This meta-class represents the ability to stepwise express a computation method."""
	def __init__(self):
		super().__init__()
		self._compuScale_children=[]

class ConcretePatternEventTriggering(complexbase.GroupBase):
	"""The ConcretePatternEventTriggering describes the behavior of an event, which occurs following a precisely known pattern."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._patternJitter_child=ModelNone
		self._patternPeriod_child=ModelNone
		self._offset_children=[]
		self._patternLength_child=ModelNone

class ConditionByFormula(complexbase.GroupBase):
	"""This class represents a condition which is computed based on system constants according to the specified expression. The expected result is considered as boolean value.

The result of the expression is interpreted as a condition. 

* \"0\" represents \"false\";  

* a value other than zero is considered \"true\""""
	bindingTime=complexbase.Attribute("bindingTime",SimpleTypes.BindingTimeEnum,'BINDING-TIME',False,"""This attribute specifies the point in time when condition may be evaluated at earliest. At this point in time all referenced system constants shall have a value.""")

class ConditionalChangeNad(complexbase.GroupBase):
	"""Generates an conditional change NAD request. See LIN 2.1 protocol specification for more information."""
	def __init__(self):
		super().__init__()
		self._byte_child=ModelNone
		self._id_child=ModelNone
		self._invert_child=ModelNone
		self._mask_child=ModelNone
		self._newNad_child=ModelNone

class ConfidenceInterval(complexbase.GroupBase):
	"""Additionally to the list of measured distances of event occurrences, a confidence interval can be specified for the expected distance of two consecutive event occurrences with a given probability."""
	def __init__(self):
		super().__init__()
		self._lowerBound_child=ModelNone
		self._propability_child=ModelNone
		self._upperBound_child=ModelNone

class ConsistencyNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to define requirements on the implicit communication behavior."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dpgDoesNotRequireCoherency_children=[]
		self._dpgRequiresCoherency_children=[]
		self._regDoesNotRequireStability_children=[]
		self._regRequiresStability_children=[]
		self._variationPoint_child=ModelNone

class ConsistencyNeedsBlueprintSet(complexbase.GroupBase):
	"""This meta class represents the ability to specify a set of blueprint for ConsistencyNeeds."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._consistencyNeeds_children=[]

class ConstantReference(complexbase.GroupBase):
	"""Instead of defining this value inline, a constant is referenced."""
	def __init__(self):
		super().__init__()
		self._constant_child=ModelNone

class ConstantSpecification(complexbase.GroupBase):
	"""Specification of a constant that can be part of a package, i.e. it can be defined stand-alone."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._valueSpec_child=[]

class ConstantSpecificationMapping(complexbase.GroupBase):
	"""This meta-class is used to create an association of two ConstantSpecifications. One ConstantSpecification is supposed to be defined in the application domain while the other should be defined in the implementation domain.

Hence the ConstantSpecificationMapping needs to be used where a ConstantSpecification defined in one domain needs to be associated to a ConstantSpecification in the other domain.

This information is crucial for the RTE generator."""
	def __init__(self):
		super().__init__()
		self._applConstant_child=ModelNone
		self._implConstant_child=ModelNone

class ConstantSpecificationMappingSet(complexbase.GroupBase):
	"""This meta-class represents the ability to map two ConstantSpecifications to each others. One ConstantSpecification is supposed to be described in the application domain and the other should be described in the implementation domain."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._mapping_children=[]

class ConsumedEventGroup(complexbase.GroupBase):
	"""A Service may have event groups which can be consumed. A service consumer has to subscribe to the corresponding event-group. After the subscription the event consumer takes the role of a server and the event provider that of a client."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._applicationEndpoint_child=ModelNone
		self._eventGroupIdentifier_child=ModelNone
		self._instanceIdentifier_child=ModelNone
		self._priority_child=ModelNone
		self._routingGroup_children=[]
		self._sdClientConfig_child=ModelNone

class ConsumedServiceInstance(complexbase.GroupBase):
	"""Service instances that are consumed by the ECU that is connected via the ApplicationEndpoint to a CommunicationConnector."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._consumedEventGroup_children=[]
		self._providedServiceInstance_child=ModelNone
		self._sdClientConfig_child=ModelNone

class ContainedIPduProps(complexbase.GroupBase):
	"""Defines the aspects of an IPdu which can be collected inside a ContainerIPdu."""
	def __init__(self):
		super().__init__()
		self._collectionSemantics_child=ModelNone
		self._headerIdLongHeader_child=ModelNone
		self._headerIdShortHeader_child=ModelNone
		self._timeout_child=ModelNone
		self._trigger_child=ModelNone

class ContainerIPdu(complexbase.GroupBase):
	"""Allows to collect several IPdus in one ContainerIPdu based on the IPdu.headerId."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._containedPduTriggering_children=[]
		self._containerTimeout_child=ModelNone
		self._containerTrigger_child=ModelNone
		self._headerType_child=ModelNone
		self._rxAcceptContainedIPdu_child=ModelNone
		self._thresholdSize_child=ModelNone

class CouplingElement(complexbase.GroupBase):
	"""A CouplingElement is used to connect EcuInstances to the VLAN of an EthernetCluster. CouplingElements can reach from a simple hub to a complex managed switch or even devices with functionalities in higher layers. A CouplingElement that is not related to an EcuInstance occurs as a dedicated single device."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationCluster_child=ModelNone
		self._couplingPort_children=[]
		self._couplingType_child=ModelNone
		self._ecuInstance_child=ModelNone

class CouplingPort(complexbase.GroupBase):
	"""A CouplingPort is used to connect a CouplingElement with an EcuInstance or two CouplingElements with each other via a CouplingPortConnection. Optionally, the CouplingPort may also have a reference to a macMulticastGroup and a defaultVLAN."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._connectionNegotiationBehavior_child=ModelNone
		self._couplingPortDetails_child=ModelNone
		self._couplingPortSpeed_child=ModelNone
		self._defaultVlan_child=ModelNone
		self._macMulticastAddress_children=[]
		self._physicalLayerType_child=ModelNone
		self._vlanMembership_children=[]
		self._vlanModifier_child=ModelNone

class CouplingPortConnection(complexbase.GroupBase):
	"""Connection between two CouplingPorts (firstPort and secondPort)."""
	def __init__(self):
		super().__init__()
		self._firstPort_child=ModelNone
		self._secondPort_child=ModelNone

class CouplingPortDetails(complexbase.GroupBase):
	"""Defines details of a CouplingPort.

May be used to configure the structures of a switch."""
	def __init__(self):
		super().__init__()
		self._couplingPortStructuralElement_children=[]
		self._ethernetPriorityRegeneration_children=[]
		self._ethernetTrafficClassAssignment_children=[]
		self._lastEgressScheduler_child=ModelNone

class CouplingPortFifo(complexbase.GroupBase):
	"""Defines a Fifo for the CouplingPort egress structure."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._assignedTrafficClass_children=[]

class CouplingPortScheduler(complexbase.GroupBase):
	"""Defines a scheduler for the CouplingPort egress structure."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._portScheduler_child=ModelNone
		self._predecessor_children=[]

class CouplingPortShaper(complexbase.GroupBase):
	"""Defines a shaper for the CouplingPort egress structure."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._idleSlope_child=ModelNone
		self._predecessorFifo_child=ModelNone

class CouplingPortStructuralElement(complexbase.GroupBase):
	"""General class to define structural elements a CouplingPort may consist of."""
	_references=defaultdict(list)

class CouplingPortTrafficClassAssignment(complexbase.GroupBase):
	"""Defines the assignment of Traffic Class to a frame.

Two use-cases are supported:
1. Only one ethernetTrafficClassAssignment is defined and NO priority is given:
--&gt; all frames on this ingress port get assigned the trafficClass.
2. for each ethernetTrafficClass WITH a priority the frames which are coming in with this priority get assigned the trafficClass.

Constraint: 1 and 2 can not be combined for one CouplingPortDetails."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._priority_children=[]
		self._trafficClass_child=ModelNone

class CryptoServiceNeeds(complexbase.GroupBase):
	"""Specifies the needs on the configuration of the CryptoServiceManager for one ConfigID (see Specification AUTOSAR_SWS_CSM.doc). An instance of this class is used to find out which ports of an SWC belong to this ConfigID."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maximumKeyLength_child=ModelNone

class CycleCounter(complexbase.GroupBase):
	"""The communication cycle where the frame is send is described by the attribute \"cycleCounter\"."""
	def __init__(self):
		super().__init__()
		self._CycleCounter_child=ModelNone

class CycleRepetition(complexbase.GroupBase):
	"""The communication cycle where the frame is send is described by the
attributes baseCycle and cycleRepetition."""
	def __init__(self):
		super().__init__()
		self._BaseCycle_child=ModelNone
		self._CycleRepetition_child=ModelNone

class CyclicTiming(complexbase.GroupBase):
	"""Specification of a cyclic sending behavior."""
	def __init__(self):
		super().__init__()
		self._timeOffset_child=ModelNone
		self._timePeriod_child=ModelNone

class DataConstr(complexbase.GroupBase):
	"""This meta-class represents the ability to specify constraints on data."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataConstrRule_children=[]

class DataConstrRule(complexbase.GroupBase):
	"""This meta-class represents the ability to express one specific data constraint rule."""
	def __init__(self):
		super().__init__()
		self._constrLevel_child=ModelNone
		self._physConstrs_child=ModelNone
		self._internalConstrs_child=ModelNone

class DataDumpEntry(complexbase.GroupBase):
	"""This service is reserved for initial configuration of a slave node by the slave node supplier and the format of this message is supplier specific."""
	def __init__(self):
		super().__init__()
		self._byteValue_children=[]

class DataFilter(complexbase.GroupBase):
	"""Base class for data filters. The type of the filter is specified in attribute dataFilterType. Some of the filter types require additional arguments which are specified as attributes of this class."""
	def __init__(self):
		super().__init__()
		self._dataFilterType_child=ModelNone
		self._mask_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone
		self._offset_child=ModelNone
		self._period_child=ModelNone
		self._x_child=ModelNone

class DataInterface(complexbase.GroupBase):
	"""The purpose of this meta-class is to act as an abstract base class for subclasses that share the semantics of being concerned about data (as opposed to e.g. operations)."""
	_references=defaultdict(list)

class DataMapping(complexbase.GroupBase):
	"""Mapping of port elements (data elements and parameters) to frames and signals."""
	def __init__(self):
		super().__init__()
		self._communicationDirection_child=ModelNone
		self._eventGroup_children=[]
		self._eventHandler_children=[]
		self._introduction_child=ModelNone
		self._serviceInstance_children=[]
		self._variationPoint_child=ModelNone

class DataPrototype(complexbase.GroupBase):
	"""Base class for prototypical roles of any data type."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swDataDefProps_child=ModelNone

class DataPrototypeGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to define a collection of DataPrototypes that are subject to the formal definition of implicit communication behavior. The definition of the collection can be nested."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataPrototypeGroup_children=[]
		self._implicitDataAccess_children=[]
		self._variationPoint_child=ModelNone

class DataPrototypeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class DataPrototypeMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular VariableDataPrototypes, ParameterDataPrototypes or ArgumentDataPrototypes with unequal names and/or unequal semantic (resolution or range) in context of two different SenderReceiverInterface, NvDataInterface or ParameterInterface or Operations.

If the semantic is unequal following rules apply:
The textTableMapping is only applicable if the referred DataPrototypes are typed by AutosarDataType referring to CompuMethods of category TEXTTABLE, SCALE_LINEAR_AND_TEXTTABLE or BITFIELD_TEXTTABLE.

In the case that the  DataPrototypes are typed by AutosarDataType either referring to CompuMethods of category LINEAR, IDENTICAL or referring to no CompuMethod (which is similar as  IDENTICAL) the linear conversion factor is calculated out of the factorSiToUnit and offsetSiToUnit attributes of the referred Units and the CompuRationalCoeffs of a compuInternalToPhys of the referred CompuMethods."""
	def __init__(self):
		super().__init__()
		self._firstDataPrototype_child=ModelNone
		self._firstToSecondDataTransformation_child=ModelNone
		self._secondDataPrototype_child=ModelNone
		self._subElementMapping_children=[]
		self._textTableMapping_children=[]

class DataReceiveErrorEvent(complexbase.GroupBase):
	"""This event is raised by the RTE when the Com layer detects and notifies an error concerning the reception of the referenced data element."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._data_child=ModelNone

class DataReceivedEvent(complexbase.GroupBase):
	"""The event is raised when the referenced data elements are received."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._data_child=ModelNone

class DataSendCompletedEvent(complexbase.GroupBase):
	"""The event is raised when the referenced data elements have been sent or an error occurs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class DataTransformation(complexbase.GroupBase):
	"""A DataTransformation represents a transformer chain. It is an ordered list of transformers."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._executeDespiteDataUnavailability_child=ModelNone
		self._transformerChain_children=[]
		self._variationPoint_child=ModelNone

class DataTransformationRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DataTransformation_child=ModelNone
		self._variationPoint_child=ModelNone

class DataTransformationSet(complexbase.GroupBase):
	"""This element is the system wide container of DataTransformations which represent transformer chains."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataTransformation_children=[]
		self._transformationTechnology_children=[]

class DataTypeMap(complexbase.GroupBase):
	"""This class represents the relationship between ApplicationDataType and its implementing ImplementationDataType."""
	def __init__(self):
		super().__init__()
		self._applicationDataType_child=ModelNone
		self._implementationDataType_child=ModelNone

class DataTypeMappingSet(complexbase.GroupBase):
	"""This class represents a list of mappings between ApplicationDataTypes and ImplementationDataTypes. In addition, it can contain mappings between ImplementationDataTypes and ModeDeclarationGroups."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataTypeMap_children=[]
		self._modeRequestTypeMap_children=[]

class DataWriteCompletedEvent(complexbase.GroupBase):
	"""This event is raised if an implicit write access was successful or an error occurred."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class DcmIPdu(complexbase.GroupBase):
	"""Represents the IPdus handled by Dcm."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagPduType_child=ModelNone

class DefItem(complexbase.GroupBase):
	"""This represents an entry in a definition list. The defined item is specified using shortName and longName."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax must be defined by the applied help system respectively help system generator.""")
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._def__child=ModelNone
		self._variationPoint_child=ModelNone

class DefList(complexbase.GroupBase):
	"""This meta-class represents the ability to express a list of definitions. Note that a definition list might be rendered similar to a labeled list but has a particular semantics to denote definitions."""
	def __init__(self):
		super().__init__()
		self._defItem_children=[]
		self._variationPoint_child=ModelNone

class DefaultValueElement(complexbase.GroupBase):
	"""The default value consists of a number of elements. Each element is one byte long and the number of elements is specified by SduLength."""
	def __init__(self):
		super().__init__()
		self._elementByteValue_child=ModelNone
		self._elementPosition_child=ModelNone

class DelegatedPortAnnotation(complexbase.GroupBase):
	"""Annotation to a \"delegated port\" to specify the Signal Fan In or Signal Fan Out inside the CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._signalFan_child=ModelNone

class DelegationSwConnector(complexbase.GroupBase):
	"""A delegation connector delegates one inner PortPrototype  (a port of a component that is used inside the composition) to a outer PortPrototype of compatible type that belongs directly to the composition (a port that is owned by the composition)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._innerPort_child=[]
		self._outerPort_child=ModelNone

class DependencyOnArtifact(complexbase.GroupBase):
	"""Dependency on the existence of another artifact, e.g. a library."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._artifactDescriptor_child=ModelNone
		self._usage_children=[]
		self._variationPoint_child=ModelNone

class Describable(complexbase.GroupBase):
	"""This meta-class represents the ability to add a descriptive documentation to non identifiable elements."""
	def __init__(self):
		super().__init__()
		self._desc_child=ModelNone
		self._category_child=ModelNone
		self._introduction_child=ModelNone
		self._adminData_child=ModelNone

class DhcpServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of DHCP servers that are running on the network endpoint. It is possible that an Ipv4DhcpServer and an Ipv6DhcpServer run on the same Ecu."""
	def __init__(self):
		super().__init__()
		self._ipv4DhcpServerConfiguration_child=ModelNone
		self._ipv6DhcpServerConfiguration_child=ModelNone

class DiagEventDebounceAlgorithm(complexbase.GroupBase):
	"""This class represents the ability to specify the pre-debounce algorithm which is selected and/or required by the particular monitor.

This class inherits from Identifiable in order to allow further documentation of the expected or implemented debouncing and to use the category for the identification of the expected / implemented debouncing."""
	_references=defaultdict(list)

class DiagEventDebounceCounterBased(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate that the counter-based debounce algorithm shall be used by the DEM for this diagnostic monitor.

This is related to set the ECUC choice container DemDebounceAlgorithmClass to DemDebounceCounterBased."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._counterDecrementStepSize_child=ModelNone
		self._counterFailedThreshold_child=ModelNone
		self._counterIncrementStepSize_child=ModelNone
		self._counterJumpDown_child=ModelNone
		self._counterJumpDownValue_child=ModelNone
		self._counterJumpUp_child=ModelNone
		self._counterJumpUpValue_child=ModelNone
		self._counterPassedThreshold_child=ModelNone

class DiagEventDebounceMonitorInternal(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate that the pre-debounce algorithm shall be used by the DEM for this diagnostic monitor.

This is related to setting the ECUC choice container DemDebounceAlgorithmClass to DemDebounceMonitorInternal.

If the FaultDetectionAlogrithm is already known to be implemented by a specific BswModuleEntry the reference bswModuleEntry points to the function specification.

If the FaultDetectionCounter value is accessible at a PortPrototype this PortPrototype shall be referenced by an assignedPort."""
	_references=defaultdict(list)

class DiagEventDebounceTimeBased(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate that the time-based pre-debounce algorithm shall be used by the DEM for this diagnostic monitor.

This is related to set the ECUC choice container DemDebounceAlgorithmClass to DemDebounceTimeBase."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._timeFailedThreshold_child=ModelNone
		self._timePassedThreshold_child=ModelNone

class DiagnosticAbstractDataIdentifier(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for the modeling of a diagnostic data identifier (DID)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone

class DiagnosticAccessPermission(complexbase.GroupBase):
	"""This represents the specification of whether a given service can be accessed according to the existence of meta-classes referenced by a particular DiagnosticAccessPermission.

In other words, this meta-class acts as a mapping element between several (otherwise unrelated) pieces of information that are put into context for the purpose of checking for access rights."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticSession_children=[]
		self._securityLevel_children=[]

class DiagnosticAging(complexbase.GroupBase):
	"""Defines the aging algorithm."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._agingCycle_children=[]
		self._threshold_child=ModelNone

class DiagnosticCapabilityElement(complexbase.GroupBase):
	"""This class identifies the capability to provide generic information about diagnostic capabilities"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._audience_children=[]
		self._diagRequirement_child=ModelNone
		self._securityAccessLevel_child=ModelNone

class DiagnosticClearDiagnosticInformation(complexbase.GroupBase):
	"""This represents an instance of the \"Clear Diagnostic Information\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._clearDiagnosticInformationClass_child=ModelNone

class DiagnosticClearDiagnosticInformationClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Clear Diagnostic Information\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticComControl(complexbase.GroupBase):
	"""This represents an instance of the \"Communication Control\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._comControlClass_child=ModelNone
		self._customSubFunctionNumber_child=ModelNone

class DiagnosticComControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Communication Control\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._allChannels_children=[]
		self._specificChannel_children=[]
		self._subNodeChannel_children=[]

class DiagnosticComControlSpecificChannel(complexbase.GroupBase):
	"""This represents the ability to add further attributes to the definition of a specific channel that is subject to the diagnostic service \"communication control\"."""
	def __init__(self):
		super().__init__()
		self._specificChannel_child=ModelNone
		self._subnetNumber_child=ModelNone

class DiagnosticComControlSubNodeChannel(complexbase.GroupBase):
	"""This represents the ability to add further attributes to the definition of a specific sub-node channel that is subject to the diagnostic service \"communication control\"."""
	def __init__(self):
		super().__init__()
		self._subNodeChannel_child=ModelNone
		self._subNodeNumber_child=ModelNone

class DiagnosticCommonElement(complexbase.GroupBase):
	"""This meta-class represents a common base class for all diagnostic elements. It does not contribute any specific functionality other than the ability to become the target of a reference."""
	_references=defaultdict(list)

class DiagnosticCommonElementRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticCommonElement_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticCommonProps(complexbase.GroupBase):
	"""This meta-class aggregates a number of common properties that are shared among a diagnostic extract."""
	def __init__(self):
		super().__init__()
		self._DiagnosticCommonPropsVariant_children=[]
		self._agingRequiresTestedCycle_child=ModelNone
		self._clearDtcLimitation_child=ModelNone
		self._debounceAlgorithmProps_children=[]
		self._defaultEndianness_child=ModelNone
		self._dtcStatusAvailabilityMask_child=ModelNone
		self._environmentDataCapture_child=ModelNone
		self._eventDisplacementStrategy_child=ModelNone
		self._maxNumberOfEventEntries_child=ModelNone
		self._maxNumberOfRequestCorrectlyReceivedResponsePending_child=ModelNone
		self._memoryEntryStorageTrigger_child=ModelNone
		self._occurrenceCounterProcessing_child=ModelNone
		self._resetConfirmedBitOnOverflow_child=ModelNone
		self._responseOnAllRequestSids_child=ModelNone
		self._responseOnSecondDeclinedRequest_child=ModelNone
		self._securityDelayTimeOnBoot_child=ModelNone
		self._statusBitHandlingTestFailedSinceLastClear_child=ModelNone
		self._statusBitStorageTestFailed_child=ModelNone
		self._typeOfDtcSupported_child=ModelNone
		self._typeOfFreezeFrameRecordNumeration_child=ModelNone

class DiagnosticCommonPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class DiagnosticCommonPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class DiagnosticCommunicationManagerNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (DCM) which are not related to a particular item (e.g. a PID or DiagnosticRoutineNeeds). The main use case is the mapping of service ports to the DCM which are not related to a particular item."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._serviceRequestCallbackType_child=ModelNone

class DiagnosticCondition(complexbase.GroupBase):
	"""Abstract element for StorageConditions and EnableConditions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initValue_child=ModelNone

class DiagnosticConditionGroup(complexbase.GroupBase):
	"""Abstract element for StorageConditionGroups and EnableConditionGroups."""
	_references=defaultdict(list)

class DiagnosticConnectedIndicator(complexbase.GroupBase):
	"""Description of indicators that are defined per DiagnosticEvent."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone
		self._healingCycle_child=ModelNone
		self._indicator_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticConnection(complexbase.GroupBase):
	"""DiagnosticConncection that is used to describe the relationship between several TP connections."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._functionalRequest_children=[]
		self._periodicResponseUudt_children=[]
		self._physicalRequest_child=ModelNone
		self._responseOnEvent_child=ModelNone
		self._response_child=ModelNone

class DiagnosticConnectionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticConnection_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticContributionSet(complexbase.GroupBase):
	""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._commonProperties_child=ModelNone
		self._ecuInstance_children=[]
		self._element_children=[]
		self._serviceTable_children=[]

class DiagnosticControlDTCSetting(complexbase.GroupBase):
	"""This represents an instance of the \"Control DTC Setting\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dtcSettingClass_child=ModelNone
		self._dtcSettingParameter_child=ModelNone

class DiagnosticControlDTCSettingClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Control DTC Setting\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._controlOptionRecordPresent_child=ModelNone

class DiagnosticCustomServiceClass(complexbase.GroupBase):
	"""This represents the ability to define a custom diagnostic service class and assign an ID to it. Further configuration is not foreseen from the point of view of the diagnostic extract and consequently needs to be done on the level of ECUC."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._customServiceId_child=ModelNone

class DiagnosticDataByIdentifier(complexbase.GroupBase):
	"""This represents an abstract base class for all diagnostic services that access data by identifier."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone

class DiagnosticDataChangeTrigger(complexbase.GroupBase):
	"""This represents the ability to define a trigger based on the change of a given DiagnosticDataIdentifier."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone

class DiagnosticDataElement(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a concrete piece of data to be taken into account for diagnostic purposes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._arraySizeSemantics_child=ModelNone
		self._maxNumberOfElements_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticDataIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to model a diagnostic data identifier (DID) that is fully specified regarding the payload at configuration-time."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._representsVin_child=ModelNone

class DiagnosticDataIdentifierSet(complexbase.GroupBase):
	"""This represents the ability to define a list of DiagnosticDataIdentifiers that can be reused in different contexts."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataIdentifier_children=[]

class DiagnosticDataTransfer(complexbase.GroupBase):
	"""This represents an instance of the \"Data Transfer\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataTransferClass_child=ModelNone

class DiagnosticDataTransferClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Data Transfer\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticDebounceAlgorithmProps(complexbase.GroupBase):
	"""Defines properties for the debounce algorithm class."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._debounceAlgorithm_child=[]
		self._debounceBehavior_child=ModelNone
		self._debounceCounterStorage_child=ModelNone

class DiagnosticDemProvidedDataMapping(complexbase.GroupBase):
	"""This represents the ability to define the nature of a data access for a DiagnsoticDataElement in the Dem."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._dataProvider_child=ModelNone

class DiagnosticDtcChangeTrigger(complexbase.GroupBase):
	"""This represents the ability to define a trigger that executes on the change of any DiagnosticTroubleCode."""

class DiagnosticDynamicDataIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic data identifier (DID) at run-time."""
	_references=defaultdict(list)

class DiagnosticDynamicallyDefineDataIdentifier(complexbase.GroupBase):
	"""This represents an instance of the \"Dynamically Define Data Identifier\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone
		self._dynamicallyDefineDataIdentifierClass_child=ModelNone
		self._maxSourceElement_child=ModelNone

class DiagnosticDynamicallyDefineDataIdentifierClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Dynamically Define Data Identifier\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._checkPerSourceId_child=ModelNone
		self._configurationHandling_child=ModelNone

class DiagnosticEcuProps(complexbase.GroupBase):
	"""This meta-class is defined to gather diagnostic-related properties that apply in the scope of an entire ECU."""
	def __init__(self):
		super().__init__()
		self._isObdRelevant_child=ModelNone
		self._sendRespPendOnTransToBoot_child=ModelNone

class DiagnosticEcuReset(complexbase.GroupBase):
	"""This represents an instance of the \"ECU Reset\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._customSubFunctionNumber_child=ModelNone
		self._ecuResetClass_child=ModelNone
		self._respondToReset_child=ModelNone

class DiagnosticEcuResetClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Ecu Reset\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticEnableCondition(complexbase.GroupBase):
	"""Specification of an enable condition."""
	_references=defaultdict(list)

class DiagnosticEnableConditionGroup(complexbase.GroupBase):
	"""Enable condition group which includes one or several enable conditions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._enableCondition_children=[]

class DiagnosticEnableConditionNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide the capability to set an enable condition."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initialStatus_child=ModelNone

class DiagnosticEnableConditionPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports with DiagnosticEnableConditionNeeds the DiagnosticEnableCondition is mapped."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._enableCondition_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticEnableConditionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticEnableCondition_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticEvent(complexbase.GroupBase):
	"""This element is used to configure DiagnosticEvents."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._agingAllowed_child=ModelNone
		self._clearEventBehavior_child=ModelNone
		self._connectedIndicator_children=[]
		self._eventClearAllowed_child=ModelNone
		self._eventFailureCycleCounterThreshold_child=ModelNone
		self._eventKind_child=ModelNone
		self._prestorageFreezeFrame_child=ModelNone

class DiagnosticEventInfoNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component interested to get information regarding specific DTCs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dtcKind_child=ModelNone
		self._dtcNumber_child=ModelNone
		self._obdDtcNumber_child=ModelNone
		self._udsDtcNumber_child=ModelNone

class DiagnosticEventManagerNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Event Manager (DEM) which are not related to a particular item."""
	_references=defaultdict(list)

class DiagnosticEventNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Diagnostic Event Manager for one diagnostic event. Its shortName can be regarded as a symbol identifying the diagnostic event from the viewpoint of the component or module which owns this element.

In case the diagnostic event specifies a production error, the shortName shall be the name of the production error."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._considerPtoStatus_child=ModelNone
		self._deferringFid_children=[]
		self._diagEventDebounceAlgorithm_child=[]
		self._dtcKind_child=ModelNone
		self._dtcNumber_child=ModelNone
		self._inhibitingFid_child=ModelNone
		self._inhibitingSecondaryFid_children=[]
		self._obdDtcNumber_child=ModelNone
		self._reportBehavior_child=ModelNone
		self._udsDtcNumber_child=ModelNone

class DiagnosticEventPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports with DiagnosticEventNeeds the DiagnosticEvent is mapped."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswServiceDependency_child=ModelNone
		self._diagnosticEvent_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticEventToDebounceAlgorithmMapping(complexbase.GroupBase):
	"""Defines which Debounce Algorithm is applicable for a DiagnosticEvent."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._debounceAlgorithm_child=ModelNone
		self._diagnosticEvent_child=ModelNone

class DiagnosticEventToEnableConditionGroupMapping(complexbase.GroupBase):
	"""Defines which EnableConditionGroup is applicable for a DiagnosticEvent."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._enableConditionGroup_child=ModelNone

class DiagnosticEventToOperationCycleMapping(complexbase.GroupBase):
	"""Defines which OperationCycle is applicable for a DiagnosticEvent."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._operationCycle_child=ModelNone

class DiagnosticEventToStorageConditionGroupMapping(complexbase.GroupBase):
	"""Defines which StorageConditionGroup is applicable for a DiagnosticEvent."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._storageConditionGroup_child=ModelNone

class DiagnosticEventToTroubleCodeUdsMapping(complexbase.GroupBase):
	"""Defines which UDS Diagnostic Trouble Code is applicable for a DiagnosticEvent."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._troubleCodeUds_child=ModelNone

class DiagnosticEventWindow(complexbase.GroupBase):
	"""This represents the ability to define the characteristics of the applicable event window"""
	def __init__(self):
		super().__init__()
		self._eventWindowTime_child=ModelNone
		self._storageStateEvaluation_child=ModelNone

class DiagnosticExtendedDataRecord(complexbase.GroupBase):
	"""Description of an extended data record."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._recordElement_children=[]
		self._recordNumber_child=ModelNone
		self._trigger_child=ModelNone
		self._update_child=ModelNone

class DiagnosticExtendedDataRecordRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticExtendedDataRecord_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticFreezeFrame(complexbase.GroupBase):
	"""This element describes combinations of DIDs for a non OBD relevant freeze frame."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._recordNumber_child=ModelNone
		self._trigger_child=ModelNone
		self._update_child=ModelNone

class DiagnosticFreezeFrameRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticFreezeFrame_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticIOControl(complexbase.GroupBase):
	"""This represents an instance of the \"I/O Control\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone
		self._freezeCurrentState_child=ModelNone
		self._ioControlClass_child=ModelNone
		self._resetToDefault_child=ModelNone
		self._shortTermAdjustment_child=ModelNone

class DiagnosticIndicator(complexbase.GroupBase):
	"""Definition of an indicator."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._healingCycleCounterThreshold_child=ModelNone
		self._type_child=ModelNone

class DiagnosticIoControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"IO Control\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticIoControlNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (DCM) which are not related to a particular item (e.g. a PID). The main use case is the mapping of service ports to the DCM which are not related to a particular item."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._currentValue_child=ModelNone
		self._didNumber_child=ModelNone
		self._freezeCurrentStateSupported_child=ModelNone
		self._resetToDefaultSupported_child=ModelNone
		self._shortTermAdjustmentSupported_child=ModelNone

class DiagnosticMapping(complexbase.GroupBase):
	"""Abstract element for different kinds of diagnostic mappings."""
	_references=defaultdict(list)

class DiagnosticMemoryAddressableRangeAccess(complexbase.GroupBase):
	"""This abstract base class"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._memoryRange_children=[]

class DiagnosticMemoryByAddress(complexbase.GroupBase):
	"""This represents an abstract base class for diagnostic services that deal with accessing memory by address."""
	_references=defaultdict(list)

class DiagnosticMemoryDestination(complexbase.GroupBase):
	"""This abstract meta-class represents a possible memory destination for a diagnostic event."""
	_references=defaultdict(list)

class DiagnosticMemoryDestinationMirror(complexbase.GroupBase):
	"""This represents a mirror memory for a diagnostic event."""
	_references=defaultdict(list)

class DiagnosticMemoryDestinationPrimary(complexbase.GroupBase):
	"""This represents a primary memory for a diagnostic event."""
	_references=defaultdict(list)

class DiagnosticMemoryDestinationUserDefined(complexbase.GroupBase):
	"""This represents a user-defined memory for a diagnostic event."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._memoryId_child=ModelNone

class DiagnosticMemoryIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to define memory properties from the diagnostics point of view."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone
		self._id_child=ModelNone
		self._memoryHighAddress_child=ModelNone
		self._memoryLowAddress_child=ModelNone

class DiagnosticOperationCycle(complexbase.GroupBase):
	"""Definition of an operation cycle that is the base of the event qualifying and for Dem scheduling."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._automaticEnd_child=ModelNone
		self._cycleAutostart_child=ModelNone
		self._cycleStatusStorage_child=ModelNone
		self._type_child=ModelNone

class DiagnosticOperationCycleNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide information regarding the operation cycle management to the Dem module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operationCycle_child=ModelNone
		self._operationCycleAutomaticEnd_child=ModelNone
		self._operationCycleAutostart_child=ModelNone

class DiagnosticOperationCyclePortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports with DiagnosticOperationCycleNeeds the DiagnosticOperationCycle is mapped."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operationCycle_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticOperationCycleRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticOperationCycle_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticParameter(complexbase.GroupBase):
	"""This meta-class represents the ability to describe information relevant for the execution of a specific diagnostic service, i.e. it can be taken to parameterize the service."""
	def __init__(self):
		super().__init__()
		self._bitOffset_child=ModelNone
		self._dataElement_children=[]
		self._variationPoint_child=ModelNone

class DiagnosticPeriodicRate(complexbase.GroupBase):
	"""This represents the ability to define a periodic rate for the specification of the \"read data by periodic ID\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone

class DiagnosticReadDTCInformation(complexbase.GroupBase):
	"""This represents an instance of the \"Read DTC Information\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._readDTCInformationClass_child=ModelNone

class DiagnosticReadDTCInformationClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"ReadDTCInformation\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticReadDataByIdentifier(complexbase.GroupBase):
	"""This represents an instance of the \"Read Data by Identifier\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._readClass_child=ModelNone

class DiagnosticReadDataByIdentifierClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Read Data by Identifier\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maxDidToRead_child=ModelNone

class DiagnosticReadDataByPeriodicID(complexbase.GroupBase):
	"""This represents an instance of the \"Read Data by periodic Identifier\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone
		self._readDataClass_child=ModelNone

class DiagnosticReadDataByPeriodicIDClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Read Data by periodic Identifier\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maxPeriodicDidToRead_child=ModelNone
		self._periodicRate_children=[]
		self._schedulerMaxNumber_child=ModelNone

class DiagnosticReadMemoryByAddress(complexbase.GroupBase):
	"""This represents an instance of the \"Read Memory by Address\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._readClass_child=ModelNone

class DiagnosticReadMemoryByAddressClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Read Memory by Address\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticRequestDownload(complexbase.GroupBase):
	"""This represents an instance of the \"Request Download\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._requestDownloadClass_child=ModelNone

class DiagnosticRequestDownloadClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Request Download\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticRequestFileTransfer(complexbase.GroupBase):
	"""This diagnostic service instance implements the UDS service 0x38."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._requestFileTransferClass_child=ModelNone

class DiagnosticRequestFileTransferClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Request File transfer\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticRequestRoutineResults(complexbase.GroupBase):
	"""This meta-class represents the ability to define the result of a diagnostic routine execution."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._response_children=[]

class DiagnosticRequestUpload(complexbase.GroupBase):
	"""This represents an instance of the \"Request Upload\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._requestUploadClass_child=ModelNone

class DiagnosticRequestUploadClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Request Upload\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticResponseOnEvent(complexbase.GroupBase):
	"""This represents an instance of the \"Response on Event\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._event_children=[]
		self._eventWindow_children=[]
		self._responseOnEventClass_child=ModelNone

class DiagnosticResponseOnEventClass(complexbase.GroupBase):
	"""This represents the ability to define common properties for alle instances of the \"Response on Event\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._interMessageTime_child=ModelNone

class DiagnosticResponseOnEventTrigger(complexbase.GroupBase):
	"""This represents the ability to further specify the events that are associated with the execution of the diagnostic service."""
	def __init__(self):
		super().__init__()
		self._initialEventStatus_child=ModelNone

class DiagnosticRoutine(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic routine."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._requestResult_child=ModelNone
		self._start_child=ModelNone
		self._stop_child=ModelNone

class DiagnosticRoutineControl(complexbase.GroupBase):
	"""This represents an instance of the \"Routine Control\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._routineControlClass_child=ModelNone
		self._routine_child=ModelNone

class DiagnosticRoutineControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Routine Control\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticRoutineNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (DCM) which are not related to a particular item (e.g. a PID). The main use case is the mapping of service ports to the DCM which are not related to a particular item."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagRoutineType_child=ModelNone
		self._ridNumber_child=ModelNone

class DiagnosticSecurityAccess(complexbase.GroupBase):
	"""This represents an instance of the \"Security Access\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._requestSeedId_child=ModelNone
		self._securityAccessClass_child=ModelNone
		self._securityLevel_child=ModelNone

class DiagnosticSecurityAccessClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Security Access\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticSecurityLevel(complexbase.GroupBase):
	"""This meta-class represents the ability to define a security level considered for diagnostic purposes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessDataRecordSize_child=ModelNone
		self._keySize_child=ModelNone
		self._numFailedSecurityAccess_child=ModelNone
		self._securityDelayTime_child=ModelNone
		self._seedSize_child=ModelNone

class DiagnosticServiceClass(complexbase.GroupBase):
	"""This meta-class provides the ability to define common properties that are shared among all instances of sub-classes of DiagnosticServiceInstance."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone
		self._accessPermissionValidity_child=ModelNone

class DiagnosticServiceDataMapping(complexbase.GroupBase):
	"""This represents the ability to define a mapping of a diagnostic service to a software-component. This kind of service mapping is applicable for the usage of SenderReceiverInterfaces."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticDataElement_child=ModelNone
		self._mappedDataElement_child=ModelNone

class DiagnosticServiceInstance(complexbase.GroupBase):
	"""This represents a concrete instance of a diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone

class DiagnosticServiceSwMapping(complexbase.GroupBase):
	"""This represents the ability to define a mapping of a diagnostic service to a software-component. This kind of service mapping is applicable for the usage of ClientServerInterfaces."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticDataElement_child=ModelNone
		self._mappedBswServiceDependency_child=ModelNone
		self._mappedFlatSwcServiceDependency_child=ModelNone
		self._mappedSwcServiceDependency_child=ModelNone
		self._serviceInstance_child=ModelNone

class DiagnosticServiceTable(complexbase.GroupBase):
	""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticConnection_children=[]
		self._ecuInstance_child=ModelNone
		self._protocolKind_child=ModelNone
		self._serviceInstance_children=[]

class DiagnosticServiceTableRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticServiceTable_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticSession(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic session."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._jumpToBootLoader_child=ModelNone
		self._p2ServerMax_child=ModelNone
		self._p2StarServerMax_child=ModelNone

class DiagnosticSessionControl(complexbase.GroupBase):
	"""This represents an instance of the \"Session Control\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticSession_child=ModelNone
		self._sessionControlClass_child=ModelNone

class DiagnosticSessionControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Session Control\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticStartRoutine(complexbase.GroupBase):
	"""This represents the ability to start a diagnostic routine."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._request_children=[]
		self._response_children=[]

class DiagnosticStopRoutine(complexbase.GroupBase):
	"""This represents the ability to stop a diagnostic routine."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._request_children=[]
		self._response_children=[]

class DiagnosticStorageCondition(complexbase.GroupBase):
	"""Specification of a storage condition."""
	_references=defaultdict(list)

class DiagnosticStorageConditionGroup(complexbase.GroupBase):
	"""Storage condition group which includes one or several storage conditions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._storageCondition_children=[]

class DiagnosticStorageConditionNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide the capability to set a storage condition."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initialStatus_child=ModelNone

class DiagnosticStorageConditionPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports with DiagnosticStorageConditionNeeds the DiagnosticStorageCondition is mapped. 

DiagnosticStorageConditionNeeds"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticStorageCondition_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticStorageConditionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticStorageCondition_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticSwMapping(complexbase.GroupBase):
	"""This represents the ability to define a mapping between a diagnostic information (at this point there is no way to become more specific about the semantics) to a software-component."""
	_references=defaultdict(list)

class DiagnosticTransferExit(complexbase.GroupBase):
	"""This represents an instance of the \"Transfer Exit\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._transferExitClass_child=ModelNone

class DiagnosticTransferExitClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Transfer Exit\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticTroubleCode(complexbase.GroupBase):
	"""A diagnostic trouble code defines a unique identifier that is shown to the diagnostic tester."""
	_references=defaultdict(list)

class DiagnosticTroubleCodeGroup(complexbase.GroupBase):
	"""The diagnostic trouble code group defines the DTCs belonging together and thereby forming a group."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dtc_children=[]
		self._groupNumber_child=ModelNone

class DiagnosticTroubleCodeJ1939(complexbase.GroupBase):
	""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._j1939DtcValue_child=ModelNone

class DiagnosticTroubleCodeObd(complexbase.GroupBase):
	"""This element is used to define OBD-relevant DTCs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._considerPtoStatus_child=ModelNone
		self._eventObdReadinessGroup_child=ModelNone
		self._obdDTCValue_child=ModelNone

class DiagnosticTroubleCodeProps(complexbase.GroupBase):
	"""This element defines common Dtc properties that can be reused by different non OBD-relevant DTCs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._aging_child=ModelNone
		self._environmentCaptureToReporting_child=ModelNone
		self._extendedDataRecord_children=[]
		self._fdcThresholdStorageValue_child=ModelNone
		self._freezeFrame_children=[]
		self._freezeFrameContent_child=ModelNone
		self._immediateNvDataStorage_child=ModelNone
		self._maxNumberFreezeFrameRecords_child=ModelNone
		self._memoryDestination_children=[]
		self._priority_child=ModelNone
		self._significance_child=ModelNone

class DiagnosticTroubleCodeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticTroubleCode_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticTroubleCodeUds(complexbase.GroupBase):
	"""This element is used to describe non OBD-relevant DTCs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dtcProps_child=ModelNone
		self._functionalUnit_child=ModelNone
		self._severity_child=ModelNone
		self._udsDtcValue_child=ModelNone

class DiagnosticValueNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (DCM) which are not related to a particular item (e.g. a PID). The main use case is the mapping of service ports to the DCM which are not related to a particular item.

In the case of using a sender receiver communicated value, the related value shall be taken via assignedData in the role \"signalBasedDiagnostics\".

In case of using a client/server communicated value, the related value shall be communicated via the port referenced by asssignedPort. The details of this communication (e.g. appropriate naming conventions) are specified in the related software specifications (SWS)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataLength_child=ModelNone
		self._diagnosticValueAccess_child=ModelNone
		self._didNumber_child=ModelNone
		self._fixedLength_child=ModelNone
		self._processingStyle_child=ModelNone

class DiagnosticWriteDataByIdentifier(complexbase.GroupBase):
	"""This represents an instance of the \"Write Data by Identifier\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._writeClass_child=ModelNone

class DiagnosticWriteDataByIdentifierClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Write Data by Identifier\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticWriteMemoryByAddress(complexbase.GroupBase):
	"""This represents an instance of the \"Write Memory by Address\" diagnostic service."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._writeClass_child=ModelNone

class DiagnosticWriteMemoryByAddressClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Write Memory by Address\" diagnostic service."""
	_references=defaultdict(list)

class DiagnosticsCommunicationSecurityNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to verify the access to security level via diagnostic services."""
	_references=defaultdict(list)

class DiscoveryTechnology(complexbase.GroupBase):
	"""This element is deprecated and will be removed in future. This information is replaced by the runtimePortConfiguration and runtimeIpAddressConfiguration attributes in the SocketConnection.

Old description:
Discovery technology information."""
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._version_child=ModelNone

class DltUserNeeds(complexbase.GroupBase):
	"""Specifies the needs on the configuration of the Diagnostic Log and Trace module for one SessionId. This class currently contains no attributes. An instance of this class is used to find out which ports of an SWC belong to this SessionId in order to group the request and response ports of the same SessionId. The actual SessionId value is stored in the PortDefinedArgumentValue of the respective port specification."""
	_references=defaultdict(list)

class DoIpActivationLineNeeds(complexbase.GroupBase):
	"""A DoIP entity needs to be informed when an external tester is attached or activated. The DoIpActivationServiceNeeds specifies the trigger for such an event. Examples would be a Pdu via a regular communication bus, a PWM signal, or an I/O. For details please refer to the ISO 13400."""
	_references=defaultdict(list)

class DoIpEntity(complexbase.GroupBase):
	"""ECU providing this infrastructure service is a DoIP-Entity."""
	def __init__(self):
		super().__init__()
		self._doIpEntityRole_child=ModelNone

class DoIpGidNeeds(complexbase.GroupBase):
	"""The DoIpGidNeeds indicates that the software-component owning this ServiceNeeds is providing the GID number either after a GID Synchronisation or by other means like e.g. flashed EEPROM parameter. This need can be used independent from DoIpGidSynchronizationNeeds and is necessary if the GID can not be provided out of the DoIP configuration options."""
	_references=defaultdict(list)

class DoIpGidSynchronizationNeeds(complexbase.GroupBase):
	"""The DoIpGidSynchronizationNeeds indicates that the software-component owning this ServiceNeeds is triggered by the DoIP entity to start a synchronization of the GID (Group Identification) on the DoIP service 0x0001, 0x0002, 0x0003 or before announcement via service 0x0004 according to ISO 13400-2:2012 if necessary. Note that this need is only relevant for DoIP synchronization masters."""
	_references=defaultdict(list)

class DoIpLogicAddress(complexbase.GroupBase):
	"""The logical DoIP address."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._address_child=ModelNone

class DoIpPowerModeStatusNeeds(complexbase.GroupBase):
	"""The DoIpPowerModeStatusNeeds indicates that the software-component owning this ServiceNeeds is providing the PowerModeStatus for the DoIP service 0x4003 according to ISO 13400-2:2012."""
	_references=defaultdict(list)

class DoIpRoutingActivationAuthenticationNeeds(complexbase.GroupBase):
	"""DoIPRoutingActivationAuthenticationNeeds indicates that the software-component owning this ServiceNeeds will have an authentication required for a DoIP routing activation service (0x0005) according to ISO 13400-2:2012."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataLengthRequest_child=ModelNone
		self._dataLengthResponse_child=ModelNone
		self._routingActivationType_child=ModelNone

class DoIpRoutingActivationConfirmationNeeds(complexbase.GroupBase):
	"""DoIpRoutingActivationConfirmationNeeds indicates that the software-component that owns this ServiceNeeds will have a confirmation required for a DoIP routing activation service (0x0005) according to ISO 13400-2:2012."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataLengthRequest_child=ModelNone
		self._dataLengthResponse_child=ModelNone
		self._routingActivationType_child=ModelNone

class DoIpServiceNeeds(complexbase.GroupBase):
	"""This represents an abstract base class for ServiceNeeds related to DoIP."""
	_references=defaultdict(list)

class DoIpTpConfig(complexbase.GroupBase):
	"""This element defines exactly one DoIp Configuration."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._doIpLogicAddress_children=[]
		self._tpConnection_children=[]

class DoIpTpConnection(complexbase.GroupBase):
	"""A connection identifies the sender and the receiver of this particular communication. The DoIp module routes a tpSdu through this connection."""
	def __init__(self):
		super().__init__()
		self._doIpSourceAddress_child=ModelNone
		self._doIpTargetAddress_child=ModelNone
		self._tpSdu_child=ModelNone

class DocRevision(complexbase.GroupBase):
	"""This meta-class represents the ability to maintain information which relates to revision management of documents or objects."""
	def __init__(self):
		super().__init__()
		self._revisionLabel_child=ModelNone
		self._revisionLabelP1_child=ModelNone
		self._revisionLabelP2_child=ModelNone
		self._state_child=ModelNone
		self._issuedBy_child=ModelNone
		self._date_child=ModelNone
		self._modification_children=[]

class DocumentViewSelectable(complexbase.GroupBase):
	"""This meta-class represents the ability to be dedicated to a particular audience or document view."""
	si=complexbase.Attribute("si",SimpleTypes.NameTokens,'SI',False,"""This attribute allows to denote a semantic information which is used to identify documentation objects to be selected in customizable document views. It shall be defined in agreement between the involved parties.""")
	view=complexbase.Attribute("view",SimpleTypes.ViewTokens,'VIEW',False,"""This attribute lists the document views in which the object shall appear. If it is missing, the object appears in all document views.""")

class Documentation(complexbase.GroupBase):
	"""This meta-class represents the ability to handle a so called standalone documentation. Standalone means, that such a documentation is not embedded in another ARElement or identifiable object.  The standalone documentation is an entity of its own which denotes its context by reference to other objects and instances."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._context_children=[]
		self._documentationContent_child=ModelNone

class DocumentationBlock(complexbase.GroupBase):
	"""This class represents a documentation block. It is made of basic text structure elements which can be displayed in a table cell."""
	def __init__(self):
		super().__init__()
		self._msrQueryP2_child=ModelNone
		self._p_children=[]
		self._verbatim_children=[]
		self._list_children=[]
		self._defList_children=[]
		self._labeledList_children=[]
		self._formula_children=[]
		self._figure_children=[]
		self._note_children=[]
		self._trace_children=[]
		self._structuredReq_children=[]

class DocumentationContext(complexbase.GroupBase):
	"""This class represents the ability to denote a context of a so called standalone  documentation. Note that this is an <<atpMixed>>. The contents needs to be considered as ordered."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._feature_child=ModelNone
		self._identifiable_child=ModelNone

class DtcStatusChangeNotificationNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component interested to get information regarding any DTC status change."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dtcFormatType_child=ModelNone

class DynamicPart(complexbase.GroupBase):
	"""Dynamic part of a multiplexed I-Pdu. 
Reserved space which is used to transport varying SignalIPdus at the same position, controlled by the corresponding selectorFieldCode."""
	def __init__(self):
		super().__init__()
		self._dynamicPartAlternative_children=[]
		self._variationPoint_child=ModelNone

class DynamicPartAlternative(complexbase.GroupBase):
	"""One of the Com IPdu alternatives that are transmitted in the Dynamic Part of the MultiplexedIPdu. The selectorFieldCode
specifies which Com IPdu is contained in the DynamicPart within a
certain transmission of a multiplexed PDU."""
	def __init__(self):
		super().__init__()
		self._iPdu_child=ModelNone
		self._initialDynamicPart_child=ModelNone
		self._selectorFieldCode_child=ModelNone

class ECUMapping(complexbase.GroupBase):
	"""ECUMapping allows to assign an ECU hardware type (defined in the ECU Resource Template) to an ECUInstance used in a physical topology."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._commControllerMapping_children=[]
		self._ecuInstance_child=ModelNone
		self._ecu_child=ModelNone
		self._hwPortMapping_children=[]
		self._variationPoint_child=ModelNone

class EOCEventRef(complexbase.GroupBase):
	"""This is used to define a reference to an RTE or BSW Event."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswModuleInstance_child=ModelNone
		self._component_child=ModelNone
		self._event_child=ModelNone
		self._successor_children=[]

class EOCExecutableEntityRef(complexbase.GroupBase):
	"""This is used to define a reference to an ExecutableEntity

If the ExecutionOrderConstraint is defined on VFB, System or ECU level, a reference to the SwComponentPrototype, via the ComponentInCompositionInstanceRef, the referenced ExecutableEntity belongs to, must be provided as context information."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswModuleInstance_child=ModelNone
		self._component_child=ModelNone
		self._executable_child=ModelNone
		self._successor_children=[]

class EOCExecutableEntityRefAbstract(complexbase.GroupBase):
	"""This is the abstractions for Execution Order Constraint Executable Entity References (leaves) and Execution Order Constraint Executable Entity Reference Groups (composites)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._directSuccessor_children=[]

class EOCExecutableEntityRefGroup(complexbase.GroupBase):
	"""This is used to specify a group (composite) consisting of Execution Order Constraint Executable Entity References (leaves) and/or further Execution Order Constraint Executable Entity Reference Groups (composite)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maxCycles_child=ModelNone
		self._maxSlots_child=ModelNone
		self._nestedElement_children=[]
		self._successor_children=[]
		self._triggeringEvent_child=ModelNone

class EcuAbstractionSwComponentType(complexbase.GroupBase):
	"""The ECUAbstraction is a special AtomicSwComponentType that resides between a software-component that wants to access ECU periphery and the Microcontroller Abstraction. The EcuAbstractionSwComponentType introduces the possibility to link from the software representation to its hardware description provided by the ECU Resource Template."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hardwareElement_children=[]

class EcuInstance(complexbase.GroupBase):
	"""ECUInstances are used to define the ECUs used in the topology. The type of the ECU is defined by a reference to an ECU specified with the ECU resource description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._associatedComIPduGroup_children=[]
		self._associatedPdurIPduGroup_children=[]
		self._canTpAddress_children=[]
		self._clientIdRange_child=ModelNone
		self._comConfigurationGwTimeBase_child=ModelNone
		self._comConfigurationRxTimeBase_child=ModelNone
		self._comConfigurationTxTimeBase_child=ModelNone
		self._comEnableMDTForCyclicTransmission_child=ModelNone
		self._commController_children=[]
		self._connector_children=[]
		self._diagnosticAddress_child=ModelNone
		self._diagnosticProps_child=ModelNone
		self._partition_children=[]
		self._pnResetTime_child=ModelNone
		self._pncPrepareSleepTimer_child=ModelNone
		self._sleepModeSupported_child=ModelNone
		self._tpAddress_children=[]
		self._wakeUpOverBusSupported_child=ModelNone

class EcuPartition(complexbase.GroupBase):
	"""Partitions are used as error containment regions. They permit the grouping of SWCs and resources and allow to describe recovery policies individually for each partition. Partitions can be terminated or restarted during run-time as a result of a detected error."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._execInUserMode_child=ModelNone

class EcuResourceEstimation(complexbase.GroupBase):
	"""Resource estimations for RTE and BSW of a single ECU instance."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._bswResourceEstimation_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._rteResourceEstimation_child=ModelNone
		self._swCompToEcuMapping_children=[]
		self._variationPoint_child=ModelNone

class EcuStateMgrUserNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the ECU State  Manager for one \"user\". This class currently contains no attributes. Its name can be regarded as a symbol identifying the user  from the viewpoint of the component or module which owns this class."""
	_references=defaultdict(list)

class EcuTiming(complexbase.GroupBase):
	"""A model element used to define timing descriptions and constraints within the scope of one ECU configuration.

TimingDescriptions aggregated by EcuTiming are allowed to use all events derived from the class TimingDescriptionEvent."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ecuConfiguration_child=ModelNone

class EcucAbstractConfigurationClass(complexbase.GroupBase):
	"""Specifies the ValueConfigurationClass of a parameter/reference or the MultiplicityConfigurationClass of a parameter/reference or a container for each ConfigurationVariant of the EcucModuleDef."""
	def __init__(self):
		super().__init__()
		self._configClass_child=ModelNone
		self._configVariant_child=ModelNone

class EcucAbstractReferenceDef(complexbase.GroupBase):
	"""Common class to gather the attributes for the definition of references."""
	_references=defaultdict(list)

class EcucAbstractReferenceValue(complexbase.GroupBase):
	"""Abstract class to be used as common parent for all reference values in the ECU Configuration Description."""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._annotation_children=[]
		self._variationPoint_child=ModelNone

class EcucAbstractStringParamDef(complexbase.GroupBase):
	"""Abstract class that is used to collect the common properties for StringParamDefs, LinkerSymbolDef, FunctionNameDef and MultilineStringParamDefs. 

atpVariation: [RS_ECUC_0083]"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._maxLength_child=ModelNone
		self._minLength_child=ModelNone
		self._regularExpression_child=ModelNone

class EcucAbstractStringParamDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucAddInfoParamDef(complexbase.GroupBase):
	"""Configuration Parameter Definition for the specification of formatted text in the ECU Configuration Parameter Description."""
	_references=defaultdict(list)

class EcucAddInfoParamValue(complexbase.GroupBase):
	"""This parameter corresponds to EcucAddInfoParamDef."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucBooleanParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Boolean. Allowed values are true and false."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone

class EcucChoiceContainerDef(complexbase.GroupBase):
	"""Used to define configuration containers that provide a choice between several EcucParamConfContainerDef. But in the actual ECU Configuration Values only one instance from the choice list will be present."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._choice_children=[]

class EcucChoiceReferenceDef(complexbase.GroupBase):
	"""Specify alternative references where in the ECU Configuration description only one of the specified references will actually be used."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destination_children=[]

class EcucCommonAttributes(complexbase.GroupBase):
	"""Attributes used by Configuration Parameters as well as References."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._configurationClassAffection_child=ModelNone
		self._implementationConfigClass_children=[]
		self._multiplicityConfigClass_children=[]
		self._origin_child=ModelNone
		self._postBuildVariantMultiplicity_child=ModelNone
		self._postBuildVariantValue_child=ModelNone
		self._requiresIndex_child=ModelNone
		self._valueConfigClass_children=[]

class EcucConditionFormula(complexbase.GroupBase):
	"""This formula must yield a boolean expression depending on ecuc queries. Note that the EcucConditionFormula is a mixed string. Therefore, the properties have the upper multiplicity 1."""
	def __init__(self):
		super().__init__()
		self._ecucQuery_child=ModelNone
		self._ecucQueryString_child=ModelNone

class EcucConditionSpecification(complexbase.GroupBase):
	"""Allows to define existence dependencies based on the value of  parameter values."""
	def __init__(self):
		super().__init__()
		self._conditionFormula_child=ModelNone
		self._ecucQuery_children=[]
		self._informalFormula_child=ModelNone

class EcucConfigurationClassAffection(complexbase.GroupBase):
	"""Specifies in the \"VendorSpecificModuleDefinition\" whether changes on this parameter do affect other parameters in a later configuration step.

This element is removed from the specifications and shall not be used."""
	def __init__(self):
		super().__init__()
		self._affected_children=[]
		self._affectionKind_child=ModelNone

class EcucContainerDef(complexbase.GroupBase):
	"""Base class used to gather common attributes of configuration container definitions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destinationUri_children=[]
		self._multiplicityConfigClass_children=[]
		self._postBuildChangeable_child=ModelNone
		self._postBuildVariantMultiplicity_child=ModelNone
		self._requiresIndex_child=ModelNone

class EcucContainerValue(complexbase.GroupBase):
	"""Represents a Container definition in the ECU Configuration Description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._parameterValue_children=[]
		self._referenceValue_children=[]
		self._subContainer_children=[]
		self._variationPoint_child=ModelNone

class EcucDefinitionCollection(complexbase.GroupBase):
	"""This represents the anchor point of an ECU Configuration Parameter Definition within the AUTOSAR templates structure."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._module_children=[]

class EcucDefinitionElement(complexbase.GroupBase):
	"""Common class used to express the commonalities of configuration parameters, references and containers.
If not stated otherwise the default multiplicity is exactly one mandatory occurrence of the specified element."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._relatedTraceItem_child=ModelNone
		self._ecucValidationCond_children=[]
		self._ecucCond_child=ModelNone
		self._lowerMultiplicity_child=ModelNone
		self._upperMultiplicity_child=ModelNone
		self._upperMultiplicityInfinite_child=ModelNone
		self._scope_child=ModelNone
		self._validationConds=[]

class EcucDerivationSpecification(complexbase.GroupBase):
	"""Allows to define configuration items that are calculated based on the value of
- other parameter values
- elements (attributes/classes) defined in other AUTOSAR templates such as System template and SW component template"""
	def __init__(self):
		super().__init__()
		self._calculationFormula_child=ModelNone
		self._ecucQuery_children=[]
		self._informalFormula_child=ModelNone

class EcucDestinationUriDef(complexbase.GroupBase):
	"""Description of an EcucDestinationUriDef that is used as target of EcucUriReferenceDefs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destinationUriPolicy_child=ModelNone

class EcucDestinationUriDefSet(complexbase.GroupBase):
	"""This class represents a list of EcucDestinationUriDefs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destinationUriDef_children=[]

class EcucDestinationUriPolicy(complexbase.GroupBase):
	"""The EcucDestinationUriPolicy describes the EcucContainerDef that will be targeted by EcucUriReferenceDefs. The type of the description is dependent of the destinationUriNestingContract attribute."""
	def __init__(self):
		super().__init__()
		self._container_children=[]
		self._destinationUriNestingContract_child=ModelNone
		self._parameter_children=[]
		self._reference_children=[]

class EcucEnumerationLiteralDef(complexbase.GroupBase):
	"""Configuration parameter type for enumeration literals definition."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ecucCond_child=ModelNone
		self._origin_child=ModelNone

class EcucEnumerationParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Enumeration."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._literal_children=[]

class EcucFloatParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Float."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone

class EcucForeignReferenceDef(complexbase.GroupBase):
	"""Specify a reference to an XML description of an entity described in another AUTOSAR template."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destinationType_child=ModelNone

class EcucFunctionNameDef(complexbase.GroupBase):
	"""Configuration parameter type for Function Names like those used to specify callback functions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._EcucFunctionNameDefVariant_children=[]

class EcucFunctionNameDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucFunctionNameDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucImplementationConfigurationClass(complexbase.GroupBase):
	"""Specifies which ConfigurationClass this parameter has in the individual ConfigurationVariants.

This element is removed from the specifications and therefore it shall not be used."""
	def __init__(self):
		super().__init__()
		self._configClass_child=ModelNone
		self._configVariant_child=ModelNone

class EcucIndexableValue(complexbase.GroupBase):
	"""Used to support the specification of ordering of parameter values."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone

class EcucInstanceReferenceDef(complexbase.GroupBase):
	"""Specify a reference to an XML description of an entity described in another AUTOSAR template using the INSTANCE REFERENCE semantics."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destinationContext_child=ModelNone
		self._destinationType_child=ModelNone

class EcucInstanceReferenceValue(complexbase.GroupBase):
	"""InstanceReference representation in the ECU Configuration."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucIntegerParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Integer."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone

class EcucLinkerSymbolDef(complexbase.GroupBase):
	"""Configuration parameter type for Linker Symbol Names like those used to specify memory locations of variables and constants."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._EcucLinkerSymbolDefVariant_children=[]

class EcucLinkerSymbolDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucLinkerSymbolDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucModuleConfigurationValues(complexbase.GroupBase):
	"""Head of the configuration of one Module. A Module can be a BSW module as well as the RTE and ECU Infrastructure.

As part of the BSW module description, the EcucModuleConfigurationValues element has two different roles:

The recommendedConfiguration contains parameter values recommended by the BSW module vendor. 

The preconfiguredConfiguration contains values for those parameters which are fixed by the implementation and cannot be changed.

These two EcucModuleConfigurationValues are used when the base EcucModuleConfigurationValues (as part of the base ECU configuration) is created to fill parameters with initial values."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._ecucDefEdition_child=ModelNone
		self._implementationConfigVariant_child=ModelNone
		self._moduleDescription_child=ModelNone
		self._container_children=[]

class EcucModuleConfigurationValuesRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._EcucModuleConfigurationValues_child=ModelNone
		self._variationPoint_child=ModelNone

class EcucModuleDef(complexbase.GroupBase):
	"""Used as the top-level element for configuration definition for Software Modules, including BSW and RTE as well as ECU Infrastructure."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._apiServicePrefix_child=ModelNone
		self._postBuildVariantSupport_child=ModelNone
		self._refinedModuleDef_child=ModelNone
		self._supportedConfigVariant_children=[]
		self._container_children=[]

class EcucMultilineStringParamDef(complexbase.GroupBase):
	"""Configuration parameter type for multiline Strings (including \"carriage return\")."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._EcucMultilineStringParamDefVariant_children=[]

class EcucMultilineStringParamDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucMultilineStringParamDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucMultiplicityConfigurationClass(complexbase.GroupBase):
	"""Specifies the MultiplicityConfigurationClass of a parameter/reference or a container for each ConfigurationVariant of the EcucModuleDef."""

class EcucNumericalParamValue(complexbase.GroupBase):
	"""Holding the value which is subject to variant handling."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucParamConfContainerDef(complexbase.GroupBase):
	"""Used to define configuration containers that can hierarchically contain other containers and/or parameter definitions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._multipleConfigurationContainer_child=ModelNone
		self._parameter_children=[]
		self._reference_children=[]
		self._subContainer_children=[]

class EcucParameterDef(complexbase.GroupBase):
	"""Abstract class used to define the similarities of all ECU Configuration Parameter types defined as subclasses."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._derivation_child=ModelNone
		self._symbolicNameValue_child=ModelNone
		self._withAuto_child=ModelNone

class EcucParameterDerivationFormula(complexbase.GroupBase):
	"""This formula is intended to specify how an ecu parameter can be derived from other information in the Autosar Templates."""
	def __init__(self):
		super().__init__()
		self._ecucQuery_child=ModelNone
		self._ecucQueryString_child=ModelNone

class EcucParameterValue(complexbase.GroupBase):
	"""Common class to all types of configuration values."""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._annotation_children=[]
		self._isAutoValue_child=ModelNone
		self._variationPoint_child=ModelNone

class EcucQuery(complexbase.GroupBase):
	"""Defines a query to the ECUC Description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ecucQueryExpression_child=ModelNone

class EcucQueryExpression(complexbase.GroupBase):
	"""Defines a query expression to the ECUC Description and output the result as an numerical value. Due to the \"mixedString\" nature of the formula there can be several EcuQueryExpressions used."""
	def __init__(self):
		super().__init__()
		self._configElementDefGlobal_child=ModelNone
		self._configElementDefLocal_child=ModelNone

class EcucReferenceDef(complexbase.GroupBase):
	"""Specify references within the ECU Configuration Description between parameter containers."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destination_child=ModelNone

class EcucReferenceValue(complexbase.GroupBase):
	"""Used to represent a configuration value that has a parameter definition of type EcucAbstractReferenceDef(used for all of its specializations excluding EcucInstanceReferenceDef)."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucStringParamDef(complexbase.GroupBase):
	"""Configuration parameter type for String."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._EcucStringParamDefVariant_children=[]

class EcucStringParamDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucStringParamDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucSymbolicNameReferenceDef(complexbase.GroupBase):
	"""This meta-class specifies that the implementation of the reference is done using a symbolic name defined by the referenced Container's shortName."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destination_child=ModelNone

class EcucTextualParamValue(complexbase.GroupBase):
	"""Holding a value which is not subject to variation."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucUriReferenceDef(complexbase.GroupBase):
	"""Definition of reference with a destination that is specified via a destinationUri. With such a reference it is possible to define a reference to a EcucContainerDef in a different module independent from the concrete definition of the target container."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._destinationUri_child=ModelNone

class EcucValidationCondition(complexbase.GroupBase):
	"""Validation condition to perform a formula calculation based on EcucQueries."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ecucQuery_children=[]
		self._validationFormula_child=ModelNone

class EcucValueCollection(complexbase.GroupBase):
	"""This represents the anchor point of the ECU configuration description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ecuExtract_child=ModelNone
		self._ecucValue_children=[]

class EcucValueConfigurationClass(complexbase.GroupBase):
	"""Specifies the ValueConfigurationClass of a parameter/reference for each ConfigurationVariant of the EcucModuleDef."""

class EmphasisText(complexbase.GroupBase):
	"""This is an emphasized text. As a compromise it contains some rendering oriented attributes such as color and font."""
	color=complexbase.Attribute("color",SimpleTypes.String,'COLOR',False,"""This allows to recommend a color of the emphasis. It is specified bases on 6 digits RGB hex-code.""")
	font=complexbase.Attribute("font",SimpleTypes.EEnumFont,'FONT',False,"""This specifies the font style in which the emphasized text shall be rendered.""")
	type=complexbase.Attribute("type",SimpleTypes.EEnum,'TYPE',False,"""Indicates how the text may be emphasized. Note that this is only a proposal which can be overridden or ignored by particular formatting engines. Default is BOLD.""")
	def __init__(self):
		super().__init__()
		self._sub_child=ModelNone
		self._sup_child=ModelNone
		self._tt_child=ModelNone

class EmptySignalMapping(complexbase.GroupBase):
	"""If no actual data is configured for a client server communication the element EmptySignalMapping shall be used. 
An EmptySignalMapping shall only reference a SystemSignal that is referenced by an ISignal with length equal to zero. In this case there shall be an \"update-bit\" configured. The EmptySignal can be mapped to the response group or to request group."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class EndToEndDescription(complexbase.GroupBase):
	"""This meta-class contains information about end-to-end protection. The set of applicable attributes depends on the actual value of the category attribute of EndToEndProtection."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._dataId_children=[]
		self._dataIdMode_child=ModelNone
		self._dataLength_child=ModelNone
		self._maxDeltaCounterInit_child=ModelNone
		self._crcOffset_child=ModelNone
		self._counterOffset_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._dataIdNibbleOffset_child=ModelNone

class EndToEndProtection(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a particular end to end protection."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._endToEndProfile_child=ModelNone
		self._endToEndProtectionISignalIPdu_children=[]
		self._endToEndProtectionVariablePrototype_children=[]
		self._variationPoint_child=ModelNone

class EndToEndProtectionISignalIPdu(complexbase.GroupBase):
	"""It is possible to protect the inter-ECU data exchange of safety-related ISignalGroups at the level of COM IPdus using protection mechanisms provided by E2E Library. For each ISignalGroup to be protected, a separate EndToEndProtectionISignalIPdu element shall be created within the EndToEndProtectionSet. 

The EndToEndProtectionISignalIPdu element refers to the ISignalGroup that is to be protected and to the ISignalIPdu that transmits the protected ISignalGroup. The information how the referenced ISignalGroup shall be protected (through which E2E Profile and with which E2E settings) is defined in the EndToEndDescription element."""
	def __init__(self):
		super().__init__()
		self._dataOffset_child=ModelNone
		self._iSignalGroup_child=ModelNone
		self._iSignalIPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class EndToEndProtectionSet(complexbase.GroupBase):
	"""This represents a container for collection EndToEndProtectionInformation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._endToEndProtection_children=[]

class EndToEndProtectionVariablePrototype(complexbase.GroupBase):
	"""It is possible to protect the data exchanged between software components. For this purpose, for each communication to be protected,  the user defines a separate EndToEndProtection (specifying a set of protection settings) and refers to a variableDataPrototype in the role of sender and to one or many variableDataPrototypes in the role of receiver. For details, see EndToEnd Library."""
	def __init__(self):
		super().__init__()
		self._receiver_children=[]
		self._sender_child=ModelNone
		self._shortLabel_child=ModelNone
		self._variationPoint_child=ModelNone

class EndToEndTransformationComSpecProps(complexbase.GroupBase):
	"""The class EndToEndTransformationIComSpecProps specifies port specific
configuration properties for EndToEnd transformer attributes."""
	def __init__(self):
		super().__init__()
		self._disableEndToEndCheck_child=ModelNone
		self._maxDeltaCounter_child=ModelNone
		self._maxErrorStateInit_child=ModelNone
		self._maxErrorStateInvalid_child=ModelNone
		self._maxErrorStateValid_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._minOkStateInit_child=ModelNone
		self._minOkStateInvalid_child=ModelNone
		self._minOkStateValid_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._windowSize_child=ModelNone

class EndToEndTransformationDescription(complexbase.GroupBase):
	"""EndToEndTransformationDescription holds these attributes which are profile specific and have the same value for all E2E transformers."""
	def __init__(self):
		super().__init__()
		self._counterOffset_child=ModelNone
		self._crcOffset_child=ModelNone
		self._dataIdMode_child=ModelNone
		self._dataIdNibbleOffset_child=ModelNone
		self._maxDeltaCounter_child=ModelNone
		self._maxErrorStateInit_child=ModelNone
		self._maxErrorStateInvalid_child=ModelNone
		self._maxErrorStateValid_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._minOkStateInit_child=ModelNone
		self._minOkStateInvalid_child=ModelNone
		self._minOkStateValid_child=ModelNone
		self._offset_child=ModelNone
		self._profileBehavior_child=ModelNone
		self._profileName_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._upperHeaderBitsToShift_child=ModelNone
		self._windowSize_child=ModelNone

class EndToEndTransformationISignalProps(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._EndToEndTransformationISignalPropsVariant_children=[]
		self._dataId_children=[]
		self._dataLength_child=ModelNone
		self._maxDataLength_child=ModelNone
		self._minDataLength_child=ModelNone

class EndToEndTransformationISignalPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EndToEndTransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EngineeringObject(complexbase.GroupBase):
	"""This class specifies an engineering object. Usually such an object is represented by a file artifact. The properties of engineering object are such that the artifact can be found  by querying an ASAM catalog file.

The engineering object is uniquely identified by domain+category+shortLabel+revisionLabel."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._revisionLabel_children=[]
		self._domain_child=ModelNone

class Entry(complexbase.GroupBase):
	"""This represents one particular table cell."""
	align=complexbase.Attribute("align",SimpleTypes.AlignEnum,'ALIGN',False,"""Specifies how the cell ENTRY shall be horizontally aligned.  Default is \"LEFT\"""")
	colname=complexbase.Attribute("colname",SimpleTypes.String,'COLNAME',False,"""Indicate the name of the column, where the entry should appear.""")
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates whether a line should be displayed end of this entry.""")
	morerows=complexbase.Attribute("morerows",SimpleTypes.String,'MOREROWS',False,"""Number of additional rows. Default is \"0\"""")
	nameend=complexbase.Attribute("nameend",SimpleTypes.String,'NAMEEND',False,"""When an entry spans multiple column this is the name of the last column.""")
	namest=complexbase.Attribute("namest",SimpleTypes.String,'NAMEST',False,"""When an entry spans multiple column this is the name of the first column.""")
	rotate=complexbase.Attribute("rotate",SimpleTypes.String,'ROTATE',False,"""Indicates if the cellcontent shall be rotated. Default is 0; 1 would rotate the contents 90 degree counterclockwise. This attribute is defined by OASIS.""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates whether a line should be displayed at the bottom end of the cell.""")
	spanname=complexbase.Attribute("spanname",SimpleTypes.String,'SPANNAME',False,"""Capture the name of entry merging multiple columns.""")
	valign=complexbase.Attribute("valign",SimpleTypes.ValignEnum,'VALIGN',False,"""Indicates how the content of the cell shall be aligned. Default is inherited from row or tbody, otherwise \"TOP\"""")
	def __init__(self):
		super().__init__()
		self._bgcolor_child=ModelNone

class EthernetCluster(complexbase.GroupBase):
	"""Ethernet-specific cluster attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._EthernetClusterVariant_children=[]
		self._couplingPortConnection_children=[]
		self._macMulticastGroup_children=[]

class EthernetClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EthernetClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EthernetCommunicationConnector(complexbase.GroupBase):
	"""Ethernet specific attributes to the CommunicationConnector."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ipV6PathMtuEnabled_child=ModelNone
		self._ipV6PathMtuTimeout_child=ModelNone
		self._maximumTransmissionUnit_child=ModelNone
		self._networkEndpoint_children=[]
		self._pncFilterDataMask_child=ModelNone

class EthernetCommunicationController(complexbase.GroupBase):
	"""Ethernet specific communication port attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._EthernetCommunicationControllerVariant_children=[]
		self._couplingPort_children=[]
		self._macUnicastAddress_child=ModelNone
		self._maximumReceiveBufferLength_child=ModelNone
		self._maximumTransmissionUnit_child=ModelNone
		self._maximumTransmitBufferLength_child=ModelNone

class EthernetCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EthernetCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EthernetFrame(complexbase.GroupBase):
	"""Ethernet specific attributes to the Frame"""
	_references=defaultdict(list)

class EthernetFrameTriggering(complexbase.GroupBase):
	"""Ethernet specific Frame element."""
	_references=defaultdict(list)

class EthernetPhysicalChannel(complexbase.GroupBase):
	"""The EthernetPhysicalChannel represents a VLAN or an untagged channel. 
An untagged channel is modeled as an EthernetPhysicalChannel without an aggregated VLAN."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._networkEndpoint_children=[]
		self._soAdConfig_child=ModelNone
		self._vlan_child=ModelNone

class EthernetPriorityRegeneration(complexbase.GroupBase):
	"""Defines a priority regeneration where the ingressPriority is replaced by regeneratedPriority.

The ethernetPriorityRegeneration is optional in case no priority regeneration shall be performed.

In case a ethernetPriorityRegeneration is defined it shall have 8 mappings, one for each priority."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ingressPriority_child=ModelNone
		self._regeneratedPriority_child=ModelNone

class EvaluatedVariantSet(complexbase.GroupBase):
	"""This meta class represents the ability to express if a set of ARElements is able to support one or more particular variants.

In other words, for a given set of evaluatedElements this meta class represents a table of evaluated variants, where each PredefinedVariant represents one column. In this column each descendant swSystemconstantValue resp. postbuildVariantCriterionValue represents one entry.

In a graphical representation each swSystemconstantValueSet / postBuildVariantCriterionValueSet could be used as an intermediate headline in the table column.

If the approvalStatus is \"APPROVED\" it expresses that the collection of CollectableElements is known be valid for the given evaluatedVariants.

Note that the EvaluatedVariantSet is a CollectableElement. This allows to establish a hierarchy of EvaluatedVariantSets."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._approvalStatus_child=ModelNone
		self._evaluatedElement_children=[]
		self._evaluatedVariant_children=[]

class EventControlledTiming(complexbase.GroupBase):
	"""Specification of a event driven sending behavior. The PDU is sent n
(numberOfRepeat + 1) times separated by the repetitionPeriod. If numberOfRepeats
= 0, then the Pdu is sent just once."""
	def __init__(self):
		super().__init__()
		self._numberOfRepetitions_child=ModelNone
		self._repetitionPeriod_child=ModelNone

class EventHandler(complexbase.GroupBase):
	"""Configures the outbound application endpoint a server uses to call a clients callback. Only required if the source TpPort is not dynamically assigned. If a consumed event group is referenced the configuration is only valid for this relation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._applicationEndpoint_child=ModelNone
		self._consumedEventGroup_children=[]
		self._multicastThreshold_child=ModelNone
		self._routingGroup_children=[]
		self._sdServerConfig_child=ModelNone

class EventTriggeringConstraint(complexbase.GroupBase):
	"""Describes the occurrence behavior of the referenced timing event.

The occurrence behavior can only be determined when a mapping from the timing events to the implementation can be obtained. However, such an occurrence behavior can also be described by the modeler as an assumption or as a requirement about the occurrence of the event."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._event_child=ModelNone

class ExclusiveArea(complexbase.GroupBase):
	"""Prevents an executable entity running in the area from being preempted."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class ExclusiveAreaNestingOrder(complexbase.GroupBase):
	"""This meta-class represents the ability to define a nesting order of ExclusiveAreas. A nesting order (that may occur in the executable code) is formally defined to be able to analyze the resource locking behavior."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._exclusiveArea_children=[]
		self._variationPoint_child=ModelNone

class ExecutableEntity(complexbase.GroupBase):
	"""Abstraction of executable code."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._activationReason_children=[]
		self._canEnterExclusiveArea_children=[]
		self._exclusiveAreaNestingOrder_children=[]
		self._minimumStartInterval_child=ModelNone
		self._reentrancyLevel_child=ModelNone
		self._runsInsideExclusiveArea_children=[]
		self._swAddrMethod_child=ModelNone

class ExecutableEntityActivationReason(complexbase.GroupBase):
	"""This meta-class represents the ability to define the reason for the activation of the enclosing ExecutableEntity."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bitPosition_child=ModelNone

class ExecutionOrderConstraint(complexbase.GroupBase):
	"""This constraint is used to restrict the order of execution for a set of ExecutableEntities. The ExecutionOrderConstraint can be used in any timing view.

The various scopes for ExecutionOrderConstraint are described below. Generally, each ExecutionOrderConstraint has a scope of software components and can reference all executable entities available in the corresponding internal behavior (RunnableEntity and BswModuleEntity) either directly or by the events activating respectively starting them (RteEvent and BswEvent).

On VFB level an ExecutionOrderConstraint can be specified for RunnableEntities part of the composition hierarchy referenced by the VfbTiming. The ExecutionOrderConstraint is aggregated by the VfbTiming.

On SW-C level an ExecutionOrderConstraint can be specified for RunnableEntities part of the InternalBehavior referenced by the SwcTiming. The ExecutionOrderConstraint is aggregated by the SwcTiming.

On System level an ExecutionOrderConstraint can be specified for RunnableEntities part of the composition hierarchy of the system referenced by the SystemTiming. The ExecutionOrderConstraint is aggregated by the SystemTiming.

On BSW Module level, an ExectionOrderConstraint can be specified for BswModuleEntities part of an BswInternalBehavior referenced by the BswModuleTiming. The ExecutionOrderConstraint is aggregated by the BswModuleTiming.

On ECU level an ExecutionOrderConstraint can be specified for all ExecutableEntities and Events available via the EcucValueCollection, covering ECU Extract and BSW Module Configuration, referenced by the EcuTiming. The ExecutionOrderConstraint is aggregated by the EcuTiming."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._baseComposition_child=ModelNone
		self._executionOrderConstraintType_child=ModelNone
		self._isEvent_child=ModelNone
		self._orderedElement_children=[]
		self._permitMultipleReferencesToEE_child=ModelNone

class ExecutionTime(complexbase.GroupBase):
	"""Base class for several means how to describe the ExecutionTime of software. The required context information is provided through this class."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._exclusiveArea_child=ModelNone
		self._executableEntity_child=ModelNone
		self._hardwareConfiguration_child=ModelNone
		self._hwElement_child=ModelNone
		self._includedLibrary_children=[]
		self._memorySectionLocation_children=[]
		self._softwareContext_child=ModelNone
		self._variationPoint_child=ModelNone

class ExecutionTimeConstraint(complexbase.GroupBase):
	"""An ExecutionTimeConstraint is used to specify the execution time of the referenced ExecutableEntity in the referenced component. A minimum and maximum execution time can be defined.

Two types of execution time semantics can be used. The desired semantics can be set by the attribute executionTimeType:
<ul>
	<li>The \"net\" execution time is the time used to execute the ExecutableEntity without interruption and without external calls.</li>
	<li>The \"gross\" execution time is the time used to execute the ExecutableEntity without interruption including external calls to other entities.</li>
</ul>

The time to execute the ExecutableEntity including interruptions by other entities and including external calls is commonly called \"response time\". The TimingExtensions provide the concept of event chains and latency constraints for that purpose. An event chain from the start of the entity to the termination of the entity with according latency constraint represents a response time constraint for that executable entity."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone
		self._executable_child=ModelNone
		self._executionTimeType_child=ModelNone
		self._maximum_child=ModelNone
		self._minimum_child=ModelNone

class ExternalTriggerOccurredEvent(complexbase.GroupBase):
	"""The event is raised when the referenced trigger have been occurred."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone

class ExternalTriggeringPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns an ExternalTriggeringPoint it is entitled to raise an ExternalTriggerOccurredEvent."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone
		self._trigger_child=[]
		self._variationPoint_child=ModelNone

class ExternalTriggeringPointIdent(complexbase.GroupBase):
	"""This meta-class has been created to introduce the ability to become referenced into the meta-class ExternalTriggeringPoint without breaking backwards compatibility."""
	_references=defaultdict(list)

class FMAttributeDef(complexbase.GroupBase):
	"""This metaclass represents the ability to define attributes for a feature."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone

class FMAttributeValue(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._value_child=ModelNone

class FMConditionByFeaturesAndAttributes(complexbase.GroupBase):
	"""A boolean expression that has the syntax of the AUTOSAR formula language but uses only references to features or feature attributes (not system constants) as operands."""

class FMConditionByFeaturesAndSwSystemconsts(complexbase.GroupBase):
	"""A boolean expression that has the syntax of the AUTOSAR formula language and may use references to features or system constants as operands."""

class FMFeature(complexbase.GroupBase):
	"""A FMFeature describes an essential characteristic of a product. Each FMFeature is contained in exactly one FMFeatureModel."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._attributeDef_children=[]
		self._decomposition_children=[]
		self._maximumIntendedBindingTime_child=ModelNone
		self._minimumIntendedBindingTime_child=ModelNone
		self._relation_children=[]
		self._restriction_children=[]

class FMFeatureDecomposition(complexbase.GroupBase):
	"""A FMFeatureDecomposition describes dependencies between a list of
features and their parent feature (i.e., the FMFeature that
aggregates the FMFeatureDecomposition). The kind of dependency is
defined by the attribute category."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._feature_children=[]
		self._max_child=ModelNone
		self._min_child=ModelNone

class FMFeatureMap(complexbase.GroupBase):
	"""A FMFeatureMap associates FMFeatures with variation points in the
AUTOSAR model. To do this, it defines value sets for system
constants and postbuild variant criterions that shall be chosen 
whenever a certain combination of features (and system constants) is encountered."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._mapping_children=[]

class FMFeatureMapAssertion(complexbase.GroupBase):
	"""Defines a boolean expression which must evaluate to true for this mapping to become active. The expression is a formula that is based on features and system constants, and is defined by fmSyscond."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._fmSyscond_child=ModelNone

class FMFeatureMapCondition(complexbase.GroupBase):
	"""Defines a condition which needs to be fulfilled for this mapping to become active. The condition is implemented as formula that is based on features and attributes and is defined by fmCond."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._fmCond_child=ModelNone

class FMFeatureMapElement(complexbase.GroupBase):
	"""Defines value sets for system constants and postbuild variant
criterions that shall be chosen whenever a certain combination
of features (and system constants) is encountered."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._assertion_children=[]
		self._condition_children=[]
		self._postBuildVariantCriterionValueSet_children=[]
		self._swSystemconstantValueSet_children=[]

class FMFeatureModel(complexbase.GroupBase):
	"""A Feature model describes the features of a product line and their dependencies. Feature models are an optional part of an AUTOSAR model."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._feature_children=[]
		self._root_child=ModelNone

class FMFeatureRelation(complexbase.GroupBase):
	"""Defines relations for FMFeatures, for example dependencies on
other FMFeatures, or conflicts with other FMFeatures. A FMFeature
can only be part of a FMFeatureSelectionSet if all its
relations are fulfilled."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._feature_children=[]

class FMFeatureRestriction(complexbase.GroupBase):
	"""Defines restrictions for FMFeatures. A FMFeature can only be part
of a FMFeatureSelectionSet if at least one of its restrictions evaluate to true."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._restriction_child=ModelNone

class FMFeatureSelection(complexbase.GroupBase):
	"""A FMFeatureSelection represents the state of a particular
FMFeature within a FMFeatureSelectionSet."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._feature_child=ModelNone
		self._state_child=ModelNone
		self._minimumSelectedBindingTime_child=ModelNone
		self._maximumSelectedBindingTime_child=ModelNone
		self._attributeValue_children=[]

class FMFeatureSelectionSet(complexbase.GroupBase):
	"""A FMFeatureSelectionSet is a set of FMFeatures that describes a specific product."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._featureModel_children=[]
		self._include_children=[]
		self._selection_children=[]

class FMFormulaByFeaturesAndAttributes(complexbase.GroupBase):
	"""An expression that has the syntax of the AUTOSAR formula language but uses only references to features or feature attributes (not system constants) as operands."""
	def __init__(self):
		super().__init__()
		self._attribute_child=ModelNone
		self._feature_child=ModelNone

class FMFormulaByFeaturesAndSwSystemconsts(complexbase.GroupBase):
	"""An expression that has the syntax of the AUTOSAR formula language and may use references to features or system constants as operands."""
	def __init__(self):
		super().__init__()
		self._feature_child=ModelNone

class FibexElement(complexbase.GroupBase):
	"""ASAM FIBEX elements specifying Communication and Topology."""
	_references=defaultdict(list)

class FibexElementRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._FibexElement_child=ModelNone
		self._variationPoint_child=ModelNone

class FlatInstanceDescriptor(complexbase.GroupBase):
	"""Represents exactly one node (e.g. a component instance or data element) of the instance tree of a software system. The purpose of this element is to map the various nested representations of this instance to a flat representation and assign a unique name (shortName) to it.

Use cases: 
* Specify unique names of measurable data to be used by MCD tools
* Specify unique names of calibration data to be used by MCD tool
* Specify a unique name for an instance of a component prototype in the ECU extract of the system description

Note that in addition it is possible to assign alias names via AliasNameAssignment."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._upstreamReference_child=ModelNone
		self._ecuExtractReference_child=ModelNone
		self._variationPoint_child=ModelNone

class FlatMap(complexbase.GroupBase):
	"""Contains a flat list of references to software objects. This list is used to identify instances and to resolve name conflicts. The scope is given by the RootSwCompositionPrototype for which it is used, i.e. it can be applied to a system, system extract or ECU-extract.

An instance of FlatMap may also be used in a preliminary context, e.g. in the scope of a software component before integration into a system. In this case it is not referred by a RootSwCompositionPrototype."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._instance_children=[]

class FlexrayAbsolutelyScheduledTiming(complexbase.GroupBase):
	"""Each frame in FlexRay is identified by its slot id and communication cycle. A description is provided by the usage of AbsolutelyScheduledTiming. 

In the static segment a frame can be sent multiple times within one communication cycle. For describing this case multiple AbsolutelyScheduledTimings have to be used. The main use case would be that a frame is sent twice within one communication cycle."""
	def __init__(self):
		super().__init__()
		self._communicationCycle_child=[]
		self._slotID_child=ModelNone

class FlexrayArTpChannel(complexbase.GroupBase):
	"""A channel is a group of connections sharing several properties.

The FlexRay AutosarTransport Layer supports several channels. These channels can work concurrently, thus each of them requires its own state machine and management data structures and its own PDU-IDs."""
	def __init__(self):
		super().__init__()
		self._ackType_child=ModelNone
		self._cancellation_child=ModelNone
		self._extendedAddressing_child=ModelNone
		self._flowControlPdu_child=ModelNone
		self._maxAr_child=ModelNone
		self._maxAs_child=ModelNone
		self._maxBs_child=ModelNone
		self._maxBufferRequest_child=ModelNone
		self._maxFcWait_child=ModelNone
		self._maxFrIf_child=ModelNone
		self._maxRetries_child=ModelNone
		self._maximumMessageLength_child=ModelNone
		self._minimumMulticastSeperationTime_child=ModelNone
		self._minimumSeparationTime_child=ModelNone
		self._multicastSegmentation_child=ModelNone
		self._nPdu_children=[]
		self._pduPool_children=[]
		self._timeBr_child=ModelNone
		self._timeBuffer_child=ModelNone
		self._timeCs_child=ModelNone
		self._timeFrIf_child=ModelNone
		self._timeoutAr_child=ModelNone
		self._timeoutAs_child=ModelNone
		self._timeoutBs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._tpConnection_children=[]
		self._transmitCancellation_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayArTpConfig(complexbase.GroupBase):
	"""This element defines exactly one FlexRay Autosar TP Configuration. 

One FlexrayArTpConfig element shall be created for each FlexRay Network in the System that uses FlexRay Autosar TP."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpChannel_children=[]
		self._tpNode_children=[]

class FlexrayArTpConnection(complexbase.GroupBase):
	"""A connection within a channel identifies the sender and the receiver of this particular communication.

The FlexRay Autosar Tp module routes a Pdu through this connection."""
	def __init__(self):
		super().__init__()
		self._connectionPrioPdus_child=ModelNone
		self._directTpSdu_child=ModelNone
		self._flowControlPdu_child=ModelNone
		self._multicast_child=ModelNone
		self._reversedTpSdu_child=ModelNone
		self._source_child=ModelNone
		self._target_children=[]
		self._transmitPdu_children=[]

class FlexrayArTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._connector_children=[]
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayCluster(complexbase.GroupBase):
	"""FlexRay specific attributes to the physicalCluster"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._FlexrayClusterVariant_children=[]
		self._actionPointOffset_child=ModelNone
		self._bit_child=ModelNone
		self._casRxLowMax_child=ModelNone
		self._coldStartAttempts_child=ModelNone
		self._cycle_child=ModelNone
		self._cycleCountMax_child=ModelNone
		self._detectNitError_child=ModelNone
		self._dynamicSlotIdlePhase_child=ModelNone
		self._ignoreAfterTx_child=ModelNone
		self._listenNoise_child=ModelNone
		self._macroPerCycle_child=ModelNone
		self._macrotickDuration_child=ModelNone
		self._maxWithoutClockCorrectionFatal_child=ModelNone
		self._maxWithoutClockCorrectionPassive_child=ModelNone
		self._minislotActionPointOffset_child=ModelNone
		self._minislotDuration_child=ModelNone
		self._networkIdleTime_child=ModelNone
		self._networkManagementVectorLength_child=ModelNone
		self._numberOfMinislots_child=ModelNone
		self._numberOfStaticSlots_child=ModelNone
		self._offsetCorrectionStart_child=ModelNone
		self._payloadLengthStatic_child=ModelNone
		self._safetyMargin_child=ModelNone
		self._sampleClockPeriod_child=ModelNone
		self._staticSlotDuration_child=ModelNone
		self._symbolWindow_child=ModelNone
		self._symbolWindowActionPointOffset_child=ModelNone
		self._syncFrameIdCountMax_child=ModelNone
		self._tranceiverStandbyDelay_child=ModelNone
		self._transmissionStartSequenceDuration_child=ModelNone
		self._wakeupRxIdle_child=ModelNone
		self._wakeupRxLow_child=ModelNone
		self._wakeupRxWindow_child=ModelNone
		self._wakeupTxActive_child=ModelNone
		self._wakeupTxIdle_child=ModelNone

class FlexrayClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class FlexrayClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class FlexrayCommunicationConnector(complexbase.GroupBase):
	"""FlexRay specific attributes to the CommunicationConnector"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmReadySleepTime_child=ModelNone
		self._pncFilterDataMask_child=ModelNone
		self._wakeUpChannel_child=ModelNone

class FlexrayCommunicationController(complexbase.GroupBase):
	"""FlexRay bus specific communication port attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._FlexrayCommunicationControllerVariant_children=[]
		self._acceptedStartupRange_child=ModelNone
		self._allowHaltDueToClock_child=ModelNone
		self._allowPassiveToActive_child=ModelNone
		self._clusterDriftDamping_child=ModelNone
		self._decodingCorrection_child=ModelNone
		self._delayCompensationA_child=ModelNone
		self._delayCompensationB_child=ModelNone
		self._externOffsetCorrection_child=ModelNone
		self._externRateCorrection_child=ModelNone
		self._externalSync_child=ModelNone
		self._fallBackInternal_child=ModelNone
		self._flexrayFifo_children=[]
		self._keySlotID_child=ModelNone
		self._keySlotOnlyEnabled_child=ModelNone
		self._keySlotUsedForStartUp_child=ModelNone
		self._keySlotUsedForSync_child=ModelNone
		self._latestTX_child=ModelNone
		self._listenTimeout_child=ModelNone
		self._macroInitialOffsetA_child=ModelNone
		self._macroInitialOffsetB_child=ModelNone
		self._maximumDynamicPayloadLength_child=ModelNone
		self._microInitialOffsetA_child=ModelNone
		self._microInitialOffsetB_child=ModelNone
		self._microPerCycle_child=ModelNone
		self._microtickDuration_child=ModelNone
		self._nmVectorEarlyUpdate_child=ModelNone
		self._offsetCorrectionOut_child=ModelNone
		self._rateCorrectionOut_child=ModelNone
		self._samplesPerMicrotick_child=ModelNone
		self._secondKeySlotId_child=ModelNone
		self._twoKeySlotMode_child=ModelNone
		self._wakeUpPattern_child=ModelNone

class FlexrayCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class FlexrayCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class FlexrayFifoConfiguration(complexbase.GroupBase):
	"""One First In First Out (FIFO) queued receive structure, defining the admittance criteria to the FIFO, and mandating the ability to admit messages into the FIFO based on Message Id filtering criteria."""
	def __init__(self):
		super().__init__()
		self._admitWithoutMessageId_child=ModelNone
		self._baseCycle_child=ModelNone
		self._channel_child=ModelNone
		self._cycleRepetition_child=ModelNone
		self._fifoDepth_child=ModelNone
		self._fifoRange_children=[]
		self._msgIdMask_child=ModelNone
		self._msgIdMatch_child=ModelNone

class FlexrayFifoRange(complexbase.GroupBase):
	"""FIFO Frame Id range acceptance criteria."""
	def __init__(self):
		super().__init__()
		self._rangeMax_child=ModelNone
		self._rangeMin_child=ModelNone

class FlexrayFrame(complexbase.GroupBase):
	"""FlexRay specific Frame element."""
	_references=defaultdict(list)

class FlexrayFrameTriggering(complexbase.GroupBase):
	"""FlexRay specific attributes to the FrameTriggering"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._absolutelyScheduledTiming_children=[]
		self._allowDynamicLSduLength_child=ModelNone
		self._messageId_child=ModelNone
		self._payloadPreambleIndicator_child=ModelNone

class FlexrayNmCluster(complexbase.GroupBase):
	"""FlexRay specific NM cluster attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmCarWakeUpBitPosition_child=ModelNone
		self._nmCarWakeUpFilterEnabled_child=ModelNone
		self._nmCarWakeUpFilterNodeId_child=ModelNone
		self._nmCarWakeUpRxEnabled_child=ModelNone
		self._nmControlBitVectorActive_child=ModelNone
		self._nmDataCycle_child=ModelNone
		self._nmDataEnabled_child=ModelNone
		self._nmDetectionLock_child=ModelNone
		self._nmMainFunctionPeriod_child=ModelNone
		self._nmMessageTimeoutTime_child=ModelNone
		self._nmReadySleepCount_child=ModelNone
		self._nmRemoteSleepIndicationTime_child=ModelNone
		self._nmRepeatMessageBitActive_child=ModelNone
		self._nmRepeatMessageTime_child=ModelNone
		self._nmRepetitionCycle_child=ModelNone
		self._nmVotingCycle_child=ModelNone

class FlexrayNmClusterCoupling(complexbase.GroupBase):
	"""FlexRay attributes that are valid for each of the referenced (coupled) FlexRay clusters."""
	def __init__(self):
		super().__init__()
		self._coupledCluster_children=[]
		self._nmControlBitVectorEnabled_child=ModelNone
		self._nmDataDisabled_child=ModelNone
		self._nmScheduleVariant_child=ModelNone

class FlexrayNmEcu(complexbase.GroupBase):
	"""FlexRay specific attributes."""
	def __init__(self):
		super().__init__()
		self._nmHwVoteEnabled_child=ModelNone
		self._nmMainFunctionAcrossFrCycle_child=ModelNone
		self._nmRepeatMessageBitEnable_child=ModelNone

class FlexrayNmNode(complexbase.GroupBase):
	"""FlexRay specific NM Node attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmInstanceId_child=ModelNone

class FlexrayPhysicalChannel(complexbase.GroupBase):
	"""FlexRay specific attributes to the physicalChannel"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._channelName_child=ModelNone

class FlexrayTpConfig(complexbase.GroupBase):
	"""This element defines exactly one FlexRay ISO TP Configuration. 

One FlexRayTpConfig element shall be created for each FlexRay Network in the System that uses FlexRay Iso Tp."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._pduPool_children=[]
		self._tpAddress_children=[]
		self._tpConnection_children=[]
		self._tpConnectionControl_children=[]
		self._tpEcu_children=[]
		self._tpNode_children=[]

class FlexrayTpConnection(complexbase.GroupBase):
	"""A connection identifies the sender and the receiver of this particular communication. The FlexRayTp module routes a Pdu through this connection. 

In a System Description the references to the PduPools are mandatory. In an ECU Extract these references can be optional:
On unicast connections these references are always mandatory.
On multicast the txPduPool is mandatory on the sender side. The rxPduPool is mandatory on the receiver side. On Gateway ECUs both references are mandatory."""
	def __init__(self):
		super().__init__()
		self._bandwidthLimitation_child=ModelNone
		self._directTpSdu_child=ModelNone
		self._multicast_child=ModelNone
		self._receiver_children=[]
		self._reversedTpSdu_child=ModelNone
		self._rxPduPool_child=ModelNone
		self._tpConnectionControl_child=ModelNone
		self._transmitter_child=ModelNone
		self._txPduPool_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpConnectionControl(complexbase.GroupBase):
	"""Configuration parameters to control a FlexRay TP connection."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ackType_child=ModelNone
		self._maxAr_child=ModelNone
		self._maxAs_child=ModelNone
		self._maxBufferSize_child=ModelNone
		self._maxFcWait_child=ModelNone
		self._maxFrIf_child=ModelNone
		self._maxNumberOfNpduPerCycle_child=ModelNone
		self._maxRetries_child=ModelNone
		self._separationCycleExponent_child=ModelNone
		self._timeBr_child=ModelNone
		self._timeBuffer_child=ModelNone
		self._timeCs_child=ModelNone
		self._timeFrIf_child=ModelNone
		self._timeoutAr_child=ModelNone
		self._timeoutAs_child=ModelNone
		self._timeoutBr_child=ModelNone
		self._timeoutBs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._timeoutCs_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpEcu(complexbase.GroupBase):
	"""ECU specific TP configuration parameters. Each TpEcu element has a reference to exactly one ECUInstance in the topology."""
	def __init__(self):
		super().__init__()
		self._cancellation_child=ModelNone
		self._cycleTimeMainFunction_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._fullDuplexEnabled_child=ModelNone
		self._transmitCancellation_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._connector_children=[]
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpPduPool(complexbase.GroupBase):
	"""FlexrayTpPduPool is a set of N-PDUs which are defined for FrTp sending or receiving purpose."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nPdu_children=[]
		self._variationPoint_child=ModelNone

class FloatValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Float attributes.

Note that this class might be used in the extended meta-model only"""

class ForbiddenSignalPath(complexbase.GroupBase):
	"""The ForbiddenSignalPath describes the physical channels which an element must not take in the topology. Such a signal path can be a constraint for the communication matrix,  because such a path has an effect on the frame generation and the frame path."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._physicalChannel_children=[]
		self._signal_children=[]

class ForeignModelReference(complexbase.GroupBase):
	"""This meta-class represents the ability to express a late binding reference to a foreign model element. The foreign model element can be from every model. Even if it is modeled according to the association representation, it is not limited to refer to AUTOSAR model elements."""
	base=complexbase.Attribute("base",SimpleTypes.NameToken,'BASE',False,"""This establishes the reference base.""")
	dest=complexbase.Attribute("dest",SimpleTypes.NameToken,'DEST',False,"""This attribute represents the class of the referenced model element. It is a String, since the model element can be in any model. Therefore we cannot have any assumption here.""")
	def __init__(self):
		super().__init__()
		self._ref_child=ModelNone

class FormulaExpression(complexbase.GroupBase):
	"""This class represents the syntax of the formula language. The class is modeled as an abstract class in order to be specialized into particular use cases. For each use case the referable objects might be specified in the specialization."""

class Frame(complexbase.GroupBase):
	"""Data frame which is sent over a communication medium. This element describes the pure Layout of a frame sent on a channel."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._frameLength_child=ModelNone
		self._pduToFrameMapping_children=[]

class FrameMapping(complexbase.GroupBase):
	"""The entire source frame is mapped as it is onto the target frame (what in general is only possible inside of a common platform). In this case source and target frame should be the identical object.

Each pair consists in a SOURCE and a TARGET referencing to a FrameTriggering.

The Frame Mapping is not supported by the Autosar BSW. The existence is optional and has been incorporated into the System Template mainly for compatibility in order to allow interchange between FIBEX and AUTOSAR descriptions."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._sourceFrame_child=ModelNone
		self._targetFrame_child=ModelNone
		self._variationPoint_child=ModelNone

class FramePid(complexbase.GroupBase):
	"""Frame_PIDs that are included in the request. The \"pid\" attribute describes the value and the \"index\" attribute the position of the frame_PID in the request."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone
		self._pid_child=ModelNone

class FramePort(complexbase.GroupBase):
	"""Connectors reception or send port on the referenced channel referenced by a FrameTriggering."""
	_references=defaultdict(list)

class FrameTriggering(complexbase.GroupBase):
	"""The FrameTriggering describes the instance of a frame sent on a channel and defines the manner of triggering (timing information) and identification of a frame on the channel, on which it is sent.

For the same frame, if FrameTriggerings exist on more than one channel of the same cluster the fan-out/in is handled by the Bus interface."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._framePort_children=[]
		self._frame_child=ModelNone
		self._pduTriggering_children=[]
		self._variationPoint_child=ModelNone

class FreeFormat(complexbase.GroupBase):
	"""Representing freely defined data."""
	def __init__(self):
		super().__init__()
		self._byteValue_children=[]

class FreeFormatEntry(complexbase.GroupBase):
	"""FreeFormat transmits a fixed master request frame with the eight data bytes provided. This may for instance be used to issue user specific fixed frames."""

class FunctionInhibitionNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Function Inhibition Manager for one Function Identifier (FID). This class currently contains no attributes. Its name can be regarded as a symbol identifying the FID  from the viewpoint of the component or module which owns this class."""
	_references=defaultdict(list)

class Gateway(complexbase.GroupBase):
	"""A gateway is an ECU that is connected to two or more clusters (channels, but not redundant), and performs a frame, Pdu or signal mapping between them."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ecu_child=ModelNone
		self._frameMapping_children=[]
		self._iPduMapping_children=[]
		self._signalMapping_children=[]

class GeneralAnnotation(complexbase.GroupBase):
	"""This class represents textual comments (called annotations) which relate to the object in which it is aggregated. These annotations are intended for use during the development process for transferring information from one step of the development process to the next one. 

The approach is similar to the \"yellow pads\" ...

This abstract class can be specialized in order to add some further formal properties."""
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone
		self._annotationOrigin_child=ModelNone
		self._annotationText_child=ModelNone

class GeneralParameter(complexbase.GroupBase):
	"""This represents a parameter in general e.g. an entry in a data sheet."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._prmChar_children=[]

class GeneralPurposeIPdu(complexbase.GroupBase):
	"""This element is used for AUTOSAR Pdus without attributes that are routed by the PduR. Please note that the category name of such Pdus is standardized in the AUTOSAR System Template."""
	_references=defaultdict(list)

class GeneralPurposePdu(complexbase.GroupBase):
	"""This element is used for AUTOSAR Pdus without additional attributes that are routed by a bus interface. Please note that the category name of such Pdus is standardized in the AUTOSAR System Template."""
	_references=defaultdict(list)

class GenericEthernetFrame(complexbase.GroupBase):
	"""This element is used for EthernetFrames without additional attributes that are routed by the EthIf."""
	_references=defaultdict(list)

class GenericModelReference(complexbase.GroupBase):
	"""This meta-class represents the ability to express a late binding reference to a model element. The model element can be from every model. Even if it is modeled according to the association representation, it is not limited to refer to AUTOSAR model elements."""
	base=complexbase.Attribute("base",SimpleTypes.NameToken,'BASE',False,"""This establishes the reference base.""")
	dest=complexbase.Attribute("dest",SimpleTypes.NameToken,'DEST',False,"""This attribute represents the class of the referenced model element. It is a String, since the model element can be in any model. Therefore we cannot have any assumption here.""")
	def __init__(self):
		super().__init__()
		self._ref_child=ModelNone

class GenericTp(complexbase.GroupBase):
	"""Content Model for a generic transport protocol."""
	def __init__(self):
		super().__init__()
		self._tpAddress_child=ModelNone
		self._tpTechnology_child=ModelNone

class GlobalTimeCanMaster(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeMaster for the CAN communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._crcSecured_child=ModelNone
		self._followUpOffset_child=ModelNone
		self._syncConfirmationTimeout_child=ModelNone

class GlobalTimeCanSlave(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeSlave for the CAN communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._crcValidated_child=ModelNone
		self._sequenceCounterJumpWidth_child=ModelNone

class GlobalTimeDomain(complexbase.GroupBase):
	"""This represents the ability to define a global time domain."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationCluster_children=[]
		self._domainId_child=ModelNone
		self._followUpTimeoutValue_child=ModelNone
		self._gateway_children=[]
		self._globalTimePdu_child=ModelNone
		self._master_child=[]
		self._offsetTimeDomain_child=ModelNone
		self._slave_children=[]
		self._subDomain_children=[]
		self._syncLossThreshold_child=ModelNone
		self._syncLossTimeout_child=ModelNone

class GlobalTimeEthMaster(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeMaster for Ethernet communication."""
	_references=defaultdict(list)

class GlobalTimeEthSlave(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeSlave for Ethernet communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._timeHardwareCorrectionThreshold_child=ModelNone

class GlobalTimeFrMaster(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeMaster for Flexray communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._crcSecured_child=ModelNone

class GlobalTimeFrSlave(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeSlave for Flexray communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._crcValidated_child=ModelNone
		self._sequenceCounterJumpWidth_child=ModelNone

class GlobalTimeGateway(complexbase.GroupBase):
	"""This represents the ability to define a time gateway for establishing a global time domain over several communication clusters."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._host_child=ModelNone
		self._master_child=ModelNone
		self._slave_child=ModelNone

class GlobalTimeMaster(complexbase.GroupBase):
	"""This represents the generic concept of a global time master."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone
		self._isSystemWideGlobalTimeMaster_child=ModelNone
		self._syncPeriod_child=ModelNone

class GlobalTimeSlave(complexbase.GroupBase):
	"""This represents the generic concept of a global time slave."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone

class Graphic(complexbase.GroupBase):
	"""This class represents an artifact containing the image to be inserted in the document"""
	editHeight=complexbase.Attribute("editHeight",SimpleTypes.String,'EDIT-HEIGHT',False,"""Specifies the height of the graphic when it is displayed in an editor. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	editWidth=complexbase.Attribute("editWidth",SimpleTypes.String,'EDIT-WIDTH',False,"""Specifies the width of the graphic when it is displayed in an editor. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	editfit=complexbase.Attribute("editfit",SimpleTypes.GraphicFitEnum,'EDITFIT',False,"""Specifies how the graphic shall be displayed in an editor. If the attribute is missing,""")
	editscale=complexbase.Attribute("editscale",SimpleTypes.String,'EDITSCALE',False,"""Set the proportional scale when displayed in an editor.""")
	filename=complexbase.Attribute("filename",SimpleTypes.String,'FILENAME',False,"""Name of the file that should be displayed. This attribute is supported in ASAM FSX and kept in AUTOSAR in order to support cut and paste.""")
	fit=complexbase.Attribute("fit",SimpleTypes.GraphicFitEnum,'FIT',False,"""It determines the way in which the graphic should be inserted.

Enter the attribute value \"AS-IS\" , to insert a graphic in its original dimensions.

The graphic is adapted, if it is too big for the space for which it was intended. 
Default is \"AS-IS\"""")
	generator=complexbase.Attribute("generator",SimpleTypes.NameToken,'GENERATOR',False,"""This attribute specifies the generator which is used to generate the image. 

Use case is that when editing a documentation, a figure (to be delivered by the  modeling tool) is inserted by the authoring tool as reference (this is the role  of graphic). But the real figure maybe injected during document processing. To  be able to recognize this situation, this attribute can be applied.""")
	height=complexbase.Attribute("height",SimpleTypes.String,'HEIGHT',False,"""Define the displayed height of the figure. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	htmlFit=complexbase.Attribute("htmlFit",SimpleTypes.GraphicFitEnum,'HTML-FIT',False,"""How to fit the graphic in an online media. Default is AS-IS.""")
	htmlHeight=complexbase.Attribute("htmlHeight",SimpleTypes.String,'HTML-HEIGHT',False,"""Specifies the height of the graphic when it is displayed online. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	htmlScale=complexbase.Attribute("htmlScale",SimpleTypes.String,'HTML-SCALE',False,"""Set the proportional scale when displayed online.""")
	htmlWidth=complexbase.Attribute("htmlWidth",SimpleTypes.String,'HTML-WIDTH',False,"""Specifies the width of the graphic when it is displayed online. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	notation=complexbase.Attribute("notation",SimpleTypes.GraphicNotationEnum,'NOTATION',False,"""This attribute captures the format used to represent the graphic.""")
	scale=complexbase.Attribute("scale",SimpleTypes.String,'SCALE',False,"""In this element the dimensions of the graphic can be altered proportionally.""")
	width=complexbase.Attribute("width",SimpleTypes.String,'WIDTH',False,"""Define the displayed width of the figure. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")

class HardwareConfiguration(complexbase.GroupBase):
	"""Describes in which mode the hardware is operating while needing this resource consumption."""
	def __init__(self):
		super().__init__()
		self._additionalInformation_child=ModelNone
		self._processorMode_child=ModelNone
		self._processorSpeed_child=ModelNone

class HeapUsage(complexbase.GroupBase):
	"""Describes the heap memory usage of a SW-Component."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hardwareConfiguration_child=ModelNone
		self._hwElement_child=ModelNone
		self._softwareContext_child=ModelNone
		self._variationPoint_child=ModelNone

class HttpTp(complexbase.GroupBase):
	"""Http over TCP as transport protocol."""
	def __init__(self):
		super().__init__()
		self._contentType_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._requestMethod_child=ModelNone
		self._tcpTpConfig_child=ModelNone
		self._uri_child=ModelNone

class HwAttributeDef(complexbase.GroupBase):
	"""This metaclass represents the ability to define a particular hardware attribute.

The category of this element defines the type of the attributeValue. If the category is Enumeration the hwAttributeEnumerationLiterals specify the available literals."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hwAttributeLiteral_children=[]
		self._isRequired_child=ModelNone
		self._unit_child=ModelNone

class HwAttributeLiteralDef(complexbase.GroupBase):
	"""One available EnumerationLiteral of the Enumeration definition. Only applicable if the category of the HwAttributeDef equals Enumeration."""
	_references=defaultdict(list)

class HwAttributeValue(complexbase.GroupBase):
	"""This metaclass represents the ability to assign a hardware attribute value. Note that v and vt are mutually exclusive."""
	def __init__(self):
		super().__init__()
		self._annotation_child=ModelNone
		self._hwAttributeDef_child=ModelNone
		self._v_child=ModelNone
		self._vt_child=ModelNone
		self._variationPoint_child=ModelNone

class HwCategory(complexbase.GroupBase):
	"""This metaclass represents the ability to declare hardware categories and its particular attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hwAttributeDef_children=[]

class HwDescriptionEntity(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a hardware entity."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hwType_child=ModelNone
		self._hwCategory_children=[]
		self._hwAttributeValue_children=[]

class HwElement(complexbase.GroupBase):
	"""This represents the ability to describe Hardware Elements on an instance level. The particular types of hardware are distinguished by the category. This category determines the applicable attributes. The possible categories and attributes are defined in HwCategory."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nestedElement_children=[]
		self._hwPinGroup_children=[]
		self._hwElementConnection_children=[]

class HwElementConnector(complexbase.GroupBase):
	"""This meta-class represents the ability to connect two hardware elements. 
The details of the connection can be refined by hwPinGroupConnection."""
	def __init__(self):
		super().__init__()
		self._hwElement_children=[]
		self._hwPinGroupConnection_children=[]
		self._hwPinConnection_children=[]
		self._variationPoint_child=ModelNone

class HwElementRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._HwElement_child=ModelNone
		self._variationPoint_child=ModelNone

class HwPin(complexbase.GroupBase):
	"""This meta-class represents the possibility to describe a hardware pin."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._pinNumber_child=ModelNone
		self._variationPoint_child=ModelNone

class HwPinConnector(complexbase.GroupBase):
	"""This meta-class represents the ability to connect two pins."""
	def __init__(self):
		super().__init__()
		self._hwPin_children=[]
		self._variationPoint_child=ModelNone

class HwPinGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to describe groups of pins which are used to connect hardware elements. This group acts as a bundle of pins. Thereby they allow to describe high level connections. Pin groups can even be nested."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hwPinGroupContent_child=ModelNone
		self._variationPoint_child=ModelNone

class HwPinGroupConnector(complexbase.GroupBase):
	"""This meta-class represents the ability to connect two pin groups."""
	def __init__(self):
		super().__init__()
		self._hwPinConnection_children=[]
		self._hwPinGroup_children=[]
		self._variationPoint_child=ModelNone

class HwPinGroupContent(complexbase.GroupBase):
	"""This meta-class specifies a mixture of hwPins and hwPinGroups."""
	def __init__(self):
		super().__init__()
		self._hwPin_children=[]
		self._hwPinGroup_children=[]

class HwPortMapping(complexbase.GroupBase):
	"""HWPortMapping specifies the hwCommunicationPort (defined in the ECU Resource Template) to realize the specified CommunicationConnector in a physical topology."""
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone
		self._hwCommunicationPort_child=ModelNone

class HwType(complexbase.GroupBase):
	"""This represents the ability to describe Hardware types on an abstract level. The particular types of hardware are distinguished by the category. This category determines the applicable attributes. The possible categories and attributes are defined in HwCategory."""
	_references=defaultdict(list)

class IPdu(complexbase.GroupBase):
	"""The IPdu (Interaction Layer Protocol Data Unit) element is used to sum up all Pdus that are routed by the PduR."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._containedIPduProps_child=ModelNone

class IPduMapping(complexbase.GroupBase):
	"""Arranges those IPdus that are transferred by the gateway from one channel to the other in pairs and defines the mapping between them."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._pdurTpChunkSize_child=ModelNone
		self._sourceIPdu_child=ModelNone
		self._targetIPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class IPduPort(complexbase.GroupBase):
	"""Connectors reception or send port on the referenced channel referenced by a PduTriggering."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iPduSignalProcessing_child=ModelNone
		self._keyId_child=ModelNone
		self._rxSecurityVerification_child=ModelNone
		self._timestampRxAcceptanceWindow_child=ModelNone

class IPduTiming(complexbase.GroupBase):
	"""AUTOSAR COM provides the possibility to define two different TRANSMISSION MODES for each IPdu.

The Transmission Mode of an IPdu that is valid at a specific point in time is selected using the values of the signals that are mapped to this IPdu. For each IPdu a Transmission Mode Selector is defined. The Transmission Mode Selector is calculated by evaluating the conditions for a subset of signals  (class TransmissionModeCondition in the System Template).

The Transmission Mode Selector is defined to be true, if at least one Condition evaluates to true and is defined to be false, if all Conditions evaluate to false."""
	def __init__(self):
		super().__init__()
		self._minimumDelay_child=ModelNone
		self._transmissionModeDeclaration_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignal(complexbase.GroupBase):
	"""Signal of the Interaction Layer. The RTE supports a \"signal fan-out\" where the same System Signal is sent in different SignalIPdus to multiple receivers. 

To support the RTE \"signal fan-out\" each SignalIPdu  contains ISignals. If the same System Signal is to be mapped into several SignalIPdus there is one ISignal needed for each ISignalToIPduMapping. 

ISignals describe the Interface between the Precompile configured RTE and the potentially Postbuild configured Com Stack (see ECUC Parameter Mapping). 

In case of the SystemSignalGroup an ISignal must be created for each SystemSignal contained in the SystemSignalGroup."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataTransformation_children=[]
		self._dataTypePolicy_child=ModelNone
		self._iSignalProps_child=ModelNone
		self._initValue_child=[]
		self._length_child=ModelNone
		self._networkRepresentationProps_child=ModelNone
		self._systemSignal_child=ModelNone
		self._transformationISignalProps_children=[]

class ISignalGroup(complexbase.GroupBase):
	"""SignalGroup of the Interaction Layer. The RTE supports a \"signal fan-out\" where the same System Signal Group is sent in different SignalIPdus to multiple receivers.   

An ISignalGroup refers to a set of ISignals that shall always be kept together. A ISignalGroup represents a COM Signal Group.  

Therefore it is recommended to put the ISignalGroup in the same Package as ISignals (see atp.recommendedPackage)"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._comBasedSignalGroupTransformation_children=[]
		self._iSignal_children=[]
		self._systemSignalGroup_child=ModelNone
		self._transformationISignalProps_children=[]

class ISignalIPdu(complexbase.GroupBase):
	"""Represents the IPdus handled by Com. The ISignalIPdu assembled and disassembled in AUTOSAR COM consists of one or more signals.
In case no multiplexing is performed this IPdu is routed to/from the Interface Layer.

A maximum of one dynamic length signal per IPdu is allowed."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iPduTimingSpecification_children=[]
		self._iSignalToPduMapping_children=[]
		self._pduCounter_children=[]
		self._pduReplication_children=[]
		self._unusedBitPattern_child=ModelNone

class ISignalIPduGroup(complexbase.GroupBase):
	"""The AUTOSAR COM Layer is able to start and to stop sending and receiving  configurable groups of I-Pdus during runtime. An ISignalIPduGroup contains either ISignalIPdus or ISignalIPduGroups."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationDirection_child=ModelNone
		self._communicationMode_child=ModelNone
		self._containedISignalIPduGroup_children=[]
		self._iSignalIPdu_children=[]
		self._nmPdu_children=[]

class ISignalIPduRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ISignalIPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalMapping(complexbase.GroupBase):
	"""Arranges those signals (or SignalGroups) that are transferred by the gateway from one channel to the other in pairs and defines the mapping between them.
Each pair consists in a source and a target referencing to a ISignalTriggering."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._sourceSignal_child=ModelNone
		self._targetSignal_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalPort(complexbase.GroupBase):
	"""Connectors reception or send port on the referenced channel referenced by an ISignalTriggering. 
If different timeouts or DataFilters for ISignals need to be specified several ISignalPorts may be created."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataFilter_child=ModelNone
		self._timeout_child=ModelNone

class ISignalProps(complexbase.GroupBase):
	"""Additional ISignal properties that may be stored in different files."""
	def __init__(self):
		super().__init__()
		self._handleOutOfRange_child=ModelNone

class ISignalToIPduMapping(complexbase.GroupBase):
	"""An ISignalToIPduMapping describes the mapping of ISignals to ISignalIPdus and defines the position of the ISignal within an ISignalIPdu."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iSignalGroup_child=ModelNone
		self._iSignal_child=ModelNone
		self._packingByteOrder_child=ModelNone
		self._startPosition_child=ModelNone
		self._transferProperty_child=ModelNone
		self._updateIndicationBitPosition_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalTriggering(complexbase.GroupBase):
	"""A ISignalTriggering allows an assignment of ISignals to physical channels."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iSignalGroup_child=ModelNone
		self._iSignalPort_children=[]
		self._iSignal_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalTriggeringRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ISignalTriggering_child=ModelNone
		self._variationPoint_child=ModelNone

class IdentCaption(complexbase.GroupBase):
	"""This meta-class represents the caption. This allows having some meta classes optionally identifiable."""
	_references=defaultdict(list)

class Identifiable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (within the namespace borders). In addition to this, Identifiables are objects  which contribute significantly to the overall structure of an AUTOSAR description. In particular, Identifiables might contain Identifiables."""
	uuid=complexbase.Attribute("uuid",SimpleTypes.String,'UUID',False,"""The purpose of this attribute is to provide a globally unique identifier for an instance of a meta-class. The values of this attribute should be globally unique strings prefixed by the type of identifier.  For example, to include a
DCE UUID as defined by The Open Group, the UUID would be preceded by \"DCE:\". The values of this attribute may be used to support merging of different AUTOSAR models. 
The form of the UUID (Universally Unique Identifier) is taken from a standard defined by the Open Group (was Open Software Foundation). This standard is widely used, including by Microsoft for COM (GUIDs) and by many companies for DCE, which is based on CORBA. The method for generating these 128-bit IDs is published in the standard and the effectiveness and uniqueness of the IDs is not in practice disputed.
If the id namespace is omitted, DCE is assumed. 
An example is \"DCE:2fac1234-31f8-11b4-a222-08002b34c003\".
The uuid attribute has no semantic meaning for an AUTOSAR model and there is no requirement for AUTOSAR tools to manage the timestamp.""")
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._desc_child=ModelNone
		self._category_child=ModelNone
		self._adminData_child=ModelNone
		self._introduction_child=ModelNone
		self._annotation_children=[]

class Ieee1722Tp(complexbase.GroupBase):
	"""Content Model for IEEE 1722 configuration."""
	def __init__(self):
		super().__init__()
		self._relativeRepresentationTime_child=ModelNone
		self._streamIdentifier_child=ModelNone
		self._subType_child=ModelNone
		self._version_child=ModelNone

class Ieee1722TpEthernetFrame(complexbase.GroupBase):
	"""Ieee1722Tp Ethernet Frame"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._relativeRepresentationTime_child=ModelNone
		self._streamIdentifier_child=ModelNone
		self._subType_child=ModelNone
		self._version_child=ModelNone

class Implementation(complexbase.GroupBase):
	"""Description of an implementation a single software component or module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._buildActionManifest_children=[]
		self._codeDescriptor_children=[]
		self._compiler_children=[]
		self._generatedArtifact_children=[]
		self._hwElement_children=[]
		self._linker_children=[]
		self._mcSupport_child=ModelNone
		self._programmingLanguage_child=ModelNone
		self._requiredArtifact_children=[]
		self._requiredGeneratorTool_children=[]
		self._resourceConsumption_child=ModelNone
		self._swVersion_child=ModelNone
		self._swcBswMapping_child=ModelNone
		self._usedCodeGenerator_child=ModelNone
		self._vendorId_child=ModelNone

class ImplementationDataType(complexbase.GroupBase):
	"""Describes a reusable data type on the implementation level. This will typically correspond to a typedef in C-code."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dynamicArraySizeProfile_child=ModelNone
		self._subElement_children=[]
		self._symbolProps_child=ModelNone
		self._typeEmitter_child=ModelNone

class ImplementationDataTypeElement(complexbase.GroupBase):
	"""Declares a data object which is locally aggregated. Such an element can only be used within the scope where it is aggregated. 

This element either consists of further subElements or it is further defined via its swDataDefProps.

There are several use cases within the system of ImplementationDataTypes fur such a local declaration:
* It can represent the elements of an array, defining the element type and array size
* It can represent an element of a struct, defining its type
* It can be the local declaration of a debug element."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._arraySize_child=ModelNone
		self._arraySizeHandling_child=ModelNone
		self._arraySizeSemantics_child=ModelNone
		self._subElement_children=[]
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class ImplementationDataTypeSubElementRef(complexbase.GroupBase):
	"""This meta-class represents the specialization of SubElementMapping with respect to ImplementationDataTypes."""
	def __init__(self):
		super().__init__()
		self._implementationDataTypeElement_child=ModelNone

class ImplementationElementInParameterInstanceRef(complexbase.GroupBase):
	"""Describes a reference to a particular ImplementationDataTypeElement instance in the context of a given ParameterDataPrototype.
Thus it refers to a particular element in the implementation description of a software data structure.

Use Case:
The RTE generator publishes its generated structure of calibration parameters in its BSW module description using the \"constantMemory\" role of ParameterDataPrototypes. Each ParameterDataPrototype describes a group of single calibration parameters. In order to point to these single parameters, this \"instance ref\" is needed.

Note that this class follows the pattern of an InstanceRef but is not implemented based on the abstract classes because the ImplementationDataType isn't either, especially because ImplementationDataTypeElement isn't derived from AtpPrototype."""
	def __init__(self):
		super().__init__()
		self._context_child=ModelNone
		self._target_child=ModelNone

class ImplementationProps(complexbase.GroupBase):
	"""Defines a symbol to be used as (depending on the concrete case) either a complete replacement or a prefix when generating code artifacts."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._symbol_child=ModelNone

class IncludedDataTypeSet(complexbase.GroupBase):
	"""An includedDataTypeSet declares that a set of AutosarDataType is used by a basic software module or a software component for its implementation and the AutosarDataType becomes part of the contract. 

This information is required if the AutosarDataType is not used for any DataPrototype owned by this software component or if the enumeration literals, lowerLimit and upperLimit constants shall be generated with a literalPrefix.

The optional literalPrefix is used to add a common prefix on enumeration literals, lowerLimit and upperLimit constants created by the RTE."""
	def __init__(self):
		super().__init__()
		self._dataType_children=[]
		self._literalPrefix_child=ModelNone

class IncludedModeDeclarationGroupSet(complexbase.GroupBase):
	"""An IncludedModeDeclarationGroupSet declares that a set of ModeDeclarationGroups used by the software component for its implementation and consequently these ModeDeclarationGroups become part of the contract."""
	def __init__(self):
		super().__init__()
		self._modeDeclarationGroup_children=[]
		self._prefix_child=ModelNone

class IndentSample(complexbase.GroupBase):
	"""This represents the ability to specify indentation of a labeled list by providing a sample content. This content can be measured by the rendering system in order to determine the width of indentation."""
	itemLabelPos=complexbase.Attribute("itemLabelPos",SimpleTypes.ItemLabelPosEnum,'ITEM-LABEL-POS',False,"""The position of the label in case the label is too long. The default is  \"NO-NEWLINE\"""")
	def __init__(self):
		super().__init__()
		self._l2_children=[]

class IndexEntry(complexbase.GroupBase):
	"""This class represents an index entry."""
	def __init__(self):
		super().__init__()
		self._sup_child=ModelNone
		self._sub_child=ModelNone

class IndexedArrayElement(complexbase.GroupBase):
	"""This element represents exactly one indexed element in the array.  Either the applicationArrayElement or implementationArrayElement reference shall be used."""
	def __init__(self):
		super().__init__()
		self._applicationArrayElement_child=ModelNone
		self._implementationArrayElement_child=ModelNone
		self._index_child=ModelNone

class InfrastructureServices(complexbase.GroupBase):
	"""Defines the network infrastructure services provided or consumed."""
	def __init__(self):
		super().__init__()
		self._dhcpServerConfiguration_child=ModelNone
		self._doIpEntity_child=ModelNone
		self._timeSynchronization_child=ModelNone

class InitEvent(complexbase.GroupBase):
	"""This RTEEvent is supposed to be used for initialization purposes, i.e. for starting and restarting a partition. It is not guaranteed that all RunnableEntities referenced by this InitEvent are executed before the 'regular' RunnableEntities are executed for the first time. The execution order depends on the task mapping."""
	_references=defaultdict(list)

class InitialSdDelayConfig(complexbase.GroupBase):
	"""This element is used to configure the offer behavior of the server and the find behavior on the client."""
	def __init__(self):
		super().__init__()
		self._initialDelayMaxValue_child=ModelNone
		self._initialDelayMinValue_child=ModelNone
		self._initialRepetitionsBaseDelay_child=ModelNone
		self._initialRepetitionsMax_child=ModelNone

class InnerDataPrototypeGroupInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a nested DataPrototypeGroup"""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._targetDataPrototypeGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class InnerPortGroupInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._context_children=[]
		self._target_child=ModelNone

class InnerRunnableEntityGroupInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a nested RunnableEntityGroup."""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._targetRunnableEntityGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class InstanceEventInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponentPrototype_children=[]
		self._targetEvent_child=ModelNone

class InstantiationDataDefProps(complexbase.GroupBase):
	"""This is a general class allowing to apply additional SwDataDefProps to particular instantiations of a DataPrototype.

Typically the accessibility and further information like alias names for a  particular data is modeled on the level of DataPrototypes (especially  VariableDataPrototypes, ParameterDataPrototypes). But due to the recursive  structure of the meta-model concerning data types (a composite (data) type consists  out of data prototypes) a part of the MCD information is described in the data  type (in case of ApplicationCompositeDataType).

This is a strong restriction in the reuse of data typed because the data type should be re-used  for different VariableDataPrototypes and ParameterDataPrototypes to guarantee type  compatibility on C-implementation level (e.g. data of a Port is stored in PIM  or a ParameterDataPrototype used as ROM Block and shall be typed by the same data type as NVRAM Block).

This class overcomes such a restriction if applied properly."""
	def __init__(self):
		super().__init__()
		self._parameterInstance_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variableInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class InstantiationRTEEventProps(complexbase.GroupBase):
	"""This meta class represents the ability to refine the properties of RTEEvents for particular instances of a software component."""
	def __init__(self):
		super().__init__()
		self._refinedEvent_child=ModelNone
		self._shortLabel_child=ModelNone
		self._variationPoint_child=ModelNone

class InstantiationTimingEventProps(complexbase.GroupBase):
	"""This meta class represents the ability to refine a timing event for particular instances of a software component. This supports then an instance specific timing."""
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone

class IntegerValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Integer attributes.

Note that this class might be used in the extended meta-model only."""

class InternalBehavior(complexbase.GroupBase):
	"""Common base class (abstract) for the internal behavior of both software components and basic software modules/clusters."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._constantMemory_children=[]
		self._constantValueMapping_children=[]
		self._dataTypeMapping_children=[]
		self._exclusiveArea_children=[]
		self._exclusiveAreaNestingOrder_children=[]
		self._staticMemory_children=[]

class InternalConstrs(complexbase.GroupBase):
	"""This meta-class represents the ability to express internal constraints."""
	def __init__(self):
		super().__init__()
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone
		self._scaleConstr_children=[]
		self._maxGradient_child=ModelNone
		self._maxDiff_child=ModelNone
		self._monotony_child=ModelNone

class InternalTriggerOccurredEvent(complexbase.GroupBase):
	"""The event is raised when the referenced internal trigger have been occurred."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class InternalTriggeringPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns a InternalTriggeringPoint it is entitled to trigger the execution of RunnableEntities of the corresponding software-component."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swImplPolicy_child=ModelNone
		self._variationPoint_child=ModelNone

class InterpolationRoutine(complexbase.GroupBase):
	"""This represents an interpolation routine taken to evaluate the contents of a curve or map against a specific input value."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._isDefault_child=ModelNone
		self._interpolationRoutine_child=ModelNone

class InterpolationRoutineMapping(complexbase.GroupBase):
	"""This meta-class provides a mapping between one record layout and its matching interpolation routines. This allows to formally specify the semantics of the interpolation routines. 

The use case is such that the curves/Maps define an interpolation method. This mapping table specifies which interpolation routine implements methods for a particular record layout. Using this information, the implementer of a software-component can select the appropriate interpolation routine."""
	def __init__(self):
		super().__init__()
		self._interpolationRoutine_children=[]
		self._swRecordLayout_child=ModelNone

class InterpolationRoutineMappingSet(complexbase.GroupBase):
	"""This meta-class specifies a set of interpolation routine mappings."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._interpolationRoutineMapping_children=[]

class InvalidationPolicy(complexbase.GroupBase):
	"""Specifies whether the component can actively invalidate a particular dataElement.

If no invalidationPolicy points to a dataElement this is considered to yield the identical result as if the handleInvalid attribute was set to dontInvalidate."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._handleInvalid_child=ModelNone

class IoHwAbstractionServerAnnotation(complexbase.GroupBase):
	"""The IoHwAbstractionServerAnnotation will only be used from a sensor- or an actuator component while interacting with the IoHwAbstraction layer.

Note that the \"server\" in the name of this meta-class is not meant to restrict the usage to ClientServerInterfaces."""
	def __init__(self):
		super().__init__()
		self._age_child=ModelNone
		self._argument_child=ModelNone
		self._bswResolution_child=ModelNone
		self._dataElement_child=ModelNone
		self._failureMonitoring_child=ModelNone
		self._filteringDebouncing_child=ModelNone
		self._pulseTest_child=ModelNone
		self._trigger_child=ModelNone

class Ipv4Configuration(complexbase.GroupBase):
	"""Internet Protocol version 4 (IPv4) configuration."""
	def __init__(self):
		super().__init__()
		self._assignmentPriority_child=ModelNone
		self._defaultGateway_child=ModelNone
		self._dnsServerAddress_children=[]
		self._ipAddressKeepBehavior_child=ModelNone
		self._ipv4Address_child=ModelNone
		self._ipv4AddressSource_child=ModelNone
		self._networkMask_child=ModelNone
		self._ttl_child=ModelNone

class Ipv4DhcpServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of a IPv4 DHCP server that runs on the network endpoint."""
	def __init__(self):
		super().__init__()
		self._addressRangeLowerBound_child=ModelNone
		self._addressRangeUpperBound_child=ModelNone
		self._defaultGateway_child=ModelNone
		self._defaultLeaseTime_child=ModelNone
		self._dnsServerAddress_children=[]
		self._networkMask_child=ModelNone

class Ipv6Configuration(complexbase.GroupBase):
	"""Internet Protocol version 6 (IPv6) configuration."""
	def __init__(self):
		super().__init__()
		self._assignmentPriority_child=ModelNone
		self._defaultRouter_child=ModelNone
		self._dnsServerAddress_children=[]
		self._hopCount_child=ModelNone
		self._ipAddressKeepBehavior_child=ModelNone
		self._ipAddressPrefixLength_child=ModelNone
		self._ipv6Address_child=ModelNone
		self._ipv6AddressSource_child=ModelNone

class Ipv6DhcpServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of a IPv6 DHCP server that runs on the network endpoint."""
	def __init__(self):
		super().__init__()
		self._addressRangeLowerBound_child=ModelNone
		self._addressRangeUpperBound_child=ModelNone
		self._defaultGateway_child=ModelNone
		self._defaultLeaseTime_child=ModelNone
		self._dnsServerAddress_children=[]
		self._networkMask_child=ModelNone

class Item(complexbase.GroupBase):
	"""This meta-class represents one particular item in a list."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class J1939Cluster(complexbase.GroupBase):
	"""J1939 specific cluster attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._J1939ClusterVariant_children=[]

class J1939ClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class J1939ClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class J1939DcmIPdu(complexbase.GroupBase):
	"""Represents the IPdus handled by J1939Dcm."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._diagnosticMessageType_child=ModelNone

class J1939NmCluster(complexbase.GroupBase):
	"""J1939 specific NmCluster attributes"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._addressClaimEnabled_child=ModelNone

class J1939NmEcu(complexbase.GroupBase):
	"""J1939 NmEcu specific attributes."""

class J1939NmNode(complexbase.GroupBase):
	"""J1939 specific NM Node attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nodeName_child=ModelNone

class J1939NodeName(complexbase.GroupBase):
	"""This element contains attributes to configure the J1939NmNode NAME."""
	def __init__(self):
		super().__init__()
		self._arbitraryAddressCapable_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._function_child=ModelNone
		self._functionInstance_child=ModelNone
		self._identitiyNumber_child=ModelNone
		self._industryGroup_child=ModelNone
		self._manufacturerCode_child=ModelNone
		self._vehicleSystem_child=ModelNone
		self._vehicleSystemInstance_child=ModelNone

class J1939TpConfig(complexbase.GroupBase):
	"""This element defines exactly one J1939 TP Configuration. 

One J1939TpConfig element shall be created for each J1939 Network in the System."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpConnection_children=[]
		self._tpNode_children=[]

class J1939TpConnection(complexbase.GroupBase):
	"""A J1939TpConnection represents an internal path for the transmission or reception of a Pdu via J1939Tp and describes the the sender and the receiver of this particular communication. The J1939Tp module routes a Pdu (J1939 PGN) through the connection."""
	def __init__(self):
		super().__init__()
		self._broadcast_child=ModelNone
		self._bufferRatio_child=ModelNone
		self._cancellation_child=ModelNone
		self._dataPdu_child=ModelNone
		self._directPdu_child=ModelNone
		self._dynamicBs_child=ModelNone
		self._flowControlPdu_children=[]
		self._maxBs_child=ModelNone
		self._maxExpBs_child=ModelNone
		self._receiver_children=[]
		self._retry_child=ModelNone
		self._tpPg_children=[]
		self._tpSdu_children=[]
		self._transmitter_child=ModelNone
		self._variationPoint_child=ModelNone

class J1939TpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._connector_child=ModelNone
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class J1939TpPg(complexbase.GroupBase):
	"""A J1939TpPg represents one J1939 message (parameter group, PG) identified by the PGN (parameter group number) that can be received or transmitted via J1939Tp."""
	def __init__(self):
		super().__init__()
		self._directPdu_child=ModelNone
		self._pgn_child=ModelNone
		self._requestable_child=ModelNone
		self._sdu_children=[]
		self._tpSdu_child=ModelNone

class Keyword(complexbase.GroupBase):
	"""This meta-class represents the ability to predefine keywords which may subsequently be used to construct names following a given naming convention, e.g. the AUTOSAR naming conventions.

Note that such names is not only shortName. It could be symbol, or even longName. Application of keywords is not limited to particular names."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._abbrName_child=ModelNone
		self._classification_children=[]

class KeywordSet(complexbase.GroupBase):
	"""This meta--class represents the ability to collect a set of predefined keywords."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._keyword_children=[]

class LGraphic(complexbase.GroupBase):
	"""This meta-class represents the figure in one particular language."""
	def __init__(self):
		super().__init__()
		self._graphic_child=ModelNone
		self._map_child=ModelNone

class LLongName(complexbase.GroupBase):
	"""MixedContentForLongNames  in one particular language. The language is denoted in the attribute l."""

class LOverviewParagraph(complexbase.GroupBase):
	"""MixedContentForOverviewParagraph in one particular language. The language is denoted in the attribute l."""

class LParagraph(complexbase.GroupBase):
	"""This is the text for a paragraph in one particular language. The language is denoted in the attribute l."""

class LPlainText(complexbase.GroupBase):
	"""This represents plain string in one particular language. The language is denoted in the attribute l."""

class LVerbatim(complexbase.GroupBase):
	"""MixedContentForVerbatim in one particular language. The language is denoted in the attribute l."""

class LabeledItem(complexbase.GroupBase):
	"""this represents an item of a labeled list."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax must be defined by the applied help system respectively help system generator.""")
	def __init__(self):
		super().__init__()
		self._itemLabel_child=ModelNone
		self._variationPoint_child=ModelNone

class LabeledList(complexbase.GroupBase):
	"""This meta-class represents a labeled list, in which items have a label and a content. The policy how to render such items is specified in the labeled list."""
	def __init__(self):
		super().__init__()
		self._indentSample_child=ModelNone
		self._labeledItem_children=[]
		self._variationPoint_child=ModelNone

class LanguageSpecific(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a particular language for which an object is applicable."""
	l=complexbase.Attribute("l",SimpleTypes.LEnum,'L',True,"""'This attribute denotes the language in which the language specific document entity is given. Note that \"FOR-ALL\" means, that the entity is applicable to all languages. It is language neutral.

It follows ISO 639-1:2002 and is specified in upper case.""")

class LatencyTimingConstraint(complexbase.GroupBase):
	"""This constraint type restricts the time duration between the occurrence of the stimulus and the occurrence of the corresponding response of that chain.

Two latency constraint types are of interest for automotive systems. These are the age of a certain response and the reaction to a certain stimulus.

In contrast to OffsetTimingConstraint, a causal dependency between the stimulus and response event of the associated event chain is required."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._latencyConstraintType_child=ModelNone
		self._scope_child=ModelNone
		self._minimum_child=ModelNone
		self._maximum_child=ModelNone
		self._nominal_child=ModelNone

class LifeCycleInfo(complexbase.GroupBase):
	"""LifeCycleInfo describes the life cycle state of an element together with additional information like what to use instead"""
	def __init__(self):
		super().__init__()
		self._lcObject_child=ModelNone
		self._lcState_child=ModelNone
		self._periodBegin_child=ModelNone
		self._periodEnd_child=ModelNone
		self._remark_child=ModelNone
		self._useInstead_children=[]

class LifeCycleInfoSet(complexbase.GroupBase):
	"""This meta class represents the ability to attach a life cycle  information to a particular set of elements.

The information can be defined for a particular period. This supports the definition of transition plans.

If no period is specified, the life cycle state applies forever."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defaultLcState_child=ModelNone
		self._defaultPeriodBegin_child=ModelNone
		self._defaultPeriodEnd_child=ModelNone
		self._lifeCycleInfo_children=[]
		self._usedLifeCycleStateDefinitionGroup_child=ModelNone

class LifeCyclePeriod(complexbase.GroupBase):
	"""This meta class represents the ability to specify a point of time within a specified period, e.g. the starting or end point, in which a specific life cycle state is valid/applies to."""
	def __init__(self):
		super().__init__()
		self._date_child=ModelNone
		self._arReleaseVersion_child=ModelNone
		self._productRelease_child=ModelNone

class LifeCycleState(complexbase.GroupBase):
	"""This meta class represents one particular state in the LifeCycle."""
	_references=defaultdict(list)

class LifeCycleStateDefinitionGroup(complexbase.GroupBase):
	"""This meta class represents the ability to define the states and properties of one particular life cycle."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._lcState_children=[]

class LimitValueVariationPoint(complexbase.GroupBase):
	"""This class represents the ability to express a numerical limit.  Note that this is in fact a NumericalValuationPoint but has the additional attribute intervalType.

Note that the xml.name is \"LIMIT\" for backward compatibility reasons."""
	intervalType=complexbase.Attribute("intervalType",SimpleTypes.IntervalTypeEnum,'INTERVAL-TYPE',False,"""This specifies the type of the interval. If the attribute is missing the interval shall be considered as \"CLOSED\".""")

class LinCluster(complexbase.GroupBase):
	"""LIN specific attributes"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._LinClusterVariant_children=[]

class LinClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class LinClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinCommunicationConnector(complexbase.GroupBase):
	"""LIN bus specific communication connector attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initialNad_child=ModelNone
		self._linConfigurableFrame_children=[]
		self._linOrderedConfigurableFrame_children=[]

class LinCommunicationController(complexbase.GroupBase):
	"""LIN bus specific communication controller attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._protocolVersion_child=ModelNone

class LinCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinConfigurableFrame(complexbase.GroupBase):
	"""Assignment of messageIds to Frames. 
This element shall be used for the LIN 2.0 Assign-Frame command."""
	def __init__(self):
		super().__init__()
		self._frame_child=ModelNone
		self._messageId_child=ModelNone

class LinConfigurationEntry(complexbase.GroupBase):
	"""A ScheduleTableEntry which contains LIN specific assignments."""
	def __init__(self):
		super().__init__()
		self._assignedController_child=ModelNone
		self._assignedLinSlaveConfig_child=ModelNone

class LinErrorResponse(complexbase.GroupBase):
	"""Each slave node shall publish a one bit signal, named response_error, to the master node in one of its transmitted unconditional frames. The response_error signal shall be set whenever a frame (except for event triggered frame responses) that is transmitted or received by the slave node contains an error in the frame response. The response_error signal shall be cleared when the unconditional frame containing the response_error signal is successfully transmitted."""
	def __init__(self):
		super().__init__()
		self._frameTriggering_child=ModelNone
		self._responseErrorPosition_child=ModelNone

class LinEventTriggeredFrame(complexbase.GroupBase):
	"""An event triggered frame is used as a placeholder to allow multiple slave nodes to provide its response.

The header of an event triggered frame is transmitted when a frame slot allocated to the event triggered frame is processed. The publisher of an associated unconditional frame shall only transmit the response if at least one of the signals carried in its unconditional frame is updated. The LIN Master discovers and purges collisions with the collisionResolvingScheduleTable. 

The event controlled frame shall not contain any Pdus."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._collisionResolvingSchedule_child=ModelNone
		self._linUnconditionalFrame_children=[]

class LinFrame(complexbase.GroupBase):
	"""Lin specific Frame element."""
	_references=defaultdict(list)

class LinFrameTriggering(complexbase.GroupBase):
	"""LIN specific attributes to the FrameTriggering"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._identifier_child=ModelNone
		self._linChecksum_child=ModelNone

class LinMaster(complexbase.GroupBase):
	"""Describing the properties of the refering ecu as a LIN master."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._LinMasterVariant_children=[]
		self._linSlave_children=[]
		self._timeBase_child=ModelNone
		self._timeBaseJitter_child=ModelNone

class LinMasterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class LinMasterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinOrderedConfigurableFrame(complexbase.GroupBase):
	"""With the assignment of the index to a frame a mapping of Pids to Frames is possible. This element shall be used for the LIN 2.1 Assign-Frame-PID-Range command."""
	def __init__(self):
		super().__init__()
		self._frame_child=ModelNone
		self._index_child=ModelNone

class LinPhysicalChannel(complexbase.GroupBase):
	"""LIN specific attributes to the physicalChannel"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._scheduleTable_children=[]

class LinScheduleTable(complexbase.GroupBase):
	"""The master task (in the master node) transmits frame headers based on a schedule table. The schedule table specifies the identifiers for each header and the interval between the start of a frame and the start of the following frame."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._resumePosition_child=ModelNone
		self._runMode_child=ModelNone
		self._tableEntry_children=[]
		self._variationPoint_child=ModelNone

class LinSlave(complexbase.GroupBase):
	"""Describing the properties of the referring ecu as a LIN slave."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._LinSlaveVariant_children=[]
		self._configuredNad_child=ModelNone
		self._functionId_child=ModelNone
		self._linErrorResponse_child=ModelNone
		self._supplierId_child=ModelNone
		self._variantId_child=ModelNone

class LinSlaveConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class LinSlaveConfig(complexbase.GroupBase):
	"""Node attributes of LIN slaves that are handled by the LinMaster. 

In the System Description LIN slaves may be described as non AUTOSAR ECUs (linSlaveEcu reference). But in an Ecu Extract of the LinMaster the LinSlaveEcus will not be available. The information that is described here is necessary in the ECU Extract for the configuration of the LinMaster. 

The values of attributes of LinSlaveConfig and LinSlave shall be identical for each LinSlaveConfig that points to a LinSlave. Please note that this causes redundancy which is intended to support flexible development methodology."""
	def __init__(self):
		super().__init__()
		self._configuredNad_child=ModelNone
		self._functionId_child=ModelNone
		self._ident_child=ModelNone
		self._linErrorResponse_child=ModelNone
		self._linSlaveEcu_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._supplierId_child=ModelNone
		self._variantId_child=ModelNone

class LinSlaveConfigIdent(complexbase.GroupBase):
	"""This meta-class is created to add the ability to become the target of a reference to the non-Referrable LinSlaveConfig."""
	_references=defaultdict(list)

class LinSlaveContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinSporadicFrame(complexbase.GroupBase):
	"""A sporadic frame is a group of unconditional frames that share the same frame slot. The sporadic frame shall not contain any Pdus."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._substitutedFrame_children=[]

class LinTpConfig(complexbase.GroupBase):
	"""This element defines exactly one Lin TP Configuration. 

One LinTpConfig element shall be created for each Lin Network in the System."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpConnection_children=[]
		self._tpNode_children=[]

class LinTpConnection(complexbase.GroupBase):
	"""A LinTP channel represents an internal path for the transmission or reception of a Pdu via LinTp and describes the the sender and the receiver of this particular communication. 

LinTp supports (per Lin Cluster) the configuration of one Rx Tp-SDU and one Tx Tp-SDU per NAD the LinMaster uses to address one or more of its Lin Slaves. To support this an arbitrary number of LinTpConnections shall be described."""
	def __init__(self):
		super().__init__()
		self._dataPdu_child=ModelNone
		self._dropNotRequestedNad_child=ModelNone
		self._flowControl_child=ModelNone
		self._linTpNSdu_child=ModelNone
		self._multicast_child=ModelNone
		self._receiver_children=[]
		self._timeoutAs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._timeoutCs_child=ModelNone
		self._transmitter_child=ModelNone
		self._variationPoint_child=ModelNone

class LinTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._connector_child=ModelNone
		self._maxNumberOfRespPendingFrames_child=ModelNone
		self._p2Max_child=ModelNone
		self._p2Timing_child=ModelNone
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class LinUnconditionalFrame(complexbase.GroupBase):
	"""Unconditional frames carry signals. The master sends a frame header in a scheduled frame slot and the designated slave node fills the frame with data."""
	_references=defaultdict(list)

class Linker(complexbase.GroupBase):
	"""Specifies the linker attributes used to describe how the linker shall be invoked."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._options_child=ModelNone
		self._vendor_child=ModelNone
		self._version_child=ModelNone

class List(complexbase.GroupBase):
	"""This meta-class represents the ability to express a list. The kind of list is specified in the attribute."""
	type=complexbase.Attribute("type",SimpleTypes.ListEnum,'TYPE',False,"""The type of the list. Default is \"UNNUMBER\"""")
	def __init__(self):
		super().__init__()
		self._item_children=[]
		self._variationPoint_child=ModelNone

class LogicAddress(complexbase.GroupBase):
	"""The logical DoIP address. This element shall only be used for DoIP (Diagnosis over IP)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._address_child=ModelNone
		self._variationPoint_child=ModelNone

class MacMulticastConfiguration(complexbase.GroupBase):
	"""References a per cluster globally defined MAC-Multicast-Group."""
	def __init__(self):
		super().__init__()
		self._macMulticastGroup_child=ModelNone

class MacMulticastGroup(complexbase.GroupBase):
	"""Per EthernetCluster globally defined MacMulticastGroup. One sender can handle many receivers simultaneously if the receivers have all the same macMulticastAddress. The addresses need to be unique for the particular EthernetCluster."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._macMulticastAddress_child=ModelNone

class Map(complexbase.GroupBase):
	"""Image maps enable authors to specify regions of an image or object and assign a specific action to each region (e.g., retrieve a document, run a program, etc.) When the region is activated by the user, the action is executed.

The class follows the html approach and is intended to support interactive documents."""
	class_=complexbase.Attribute("class_",SimpleTypes.String,'CLASS',False,"""This attribute assigns a class name or set of class names to an element. Any number of elements may be assigned the same class name or set of class names. Multiple class names must be separated by white space characters. Class names are typically used to apply CSS formatting rules to an element.""")
	name=complexbase.Attribute("name",SimpleTypes.NameToken,'NAME',False,"""This attribute assigns a name to the image map in the MAP element. This name can be used to be referenced in an HTML image through the attribute USEMAP. Although this is not actually necessary in the
MSR model, it was inserted in order to support the MAPs which were created for HTML.""")
	onclick=complexbase.Attribute("onclick",SimpleTypes.String,'ONCLICK',False,"""The ONCLICK-Event occurs, if the current element is clicked on. A script can be stored in
this attribute to be performed in the Event.""")
	ondblclick=complexbase.Attribute("ondblclick",SimpleTypes.String,'ONDBLCLICK',False,"""The ONDBLCLICK-Event occurs, if the current Event is \"double\" clicked-on. 
A script can be stored in this attribute to be performed in the Event.""")
	onkeydown=complexbase.Attribute("onkeydown",SimpleTypes.String,'ONKEYDOWN',False,"""The ONKEYDOWN-Event occurs, if a button on the current element is pressed down. 

A script can be stored in this attribute to be performed in the event.""")
	onkeypress=complexbase.Attribute("onkeypress",SimpleTypes.String,'ONKEYPRESS',False,"""The ONKEYPRESS-Event occurs, if a button on the current element is pressed down and released. 

A script can be stored in this attribute to be performed in the Event.""")
	onkeyup=complexbase.Attribute("onkeyup",SimpleTypes.String,'ONKEYUP',False,"""The ONKEYUP-Event occurs, if a button on the current element is released. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousedown=complexbase.Attribute("onmousedown",SimpleTypes.String,'ONMOUSEDOWN',False,"""The ONMOUSEDOWN-Event occurs, if the mouse button used for clicking is held down on the current element. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousemove=complexbase.Attribute("onmousemove",SimpleTypes.String,'ONMOUSEMOVE',False,"""The ONMOUSEMOVE-Event occurs, if the mouse pointer is moved on the current 
element (i.e. it is located on the current element). 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseout=complexbase.Attribute("onmouseout",SimpleTypes.String,'ONMOUSEOUT',False,"""The ONMOUSEOUT-Event occurs, if the mouse pointer is moved from the current element.

A script can be stored in this attribute to be performed in the Event.""")
	onmouseover=complexbase.Attribute("onmouseover",SimpleTypes.String,'ONMOUSEOVER',False,"""The ONMOUSEOVER-Event occurs, if the mouse pointer is moved to the current element
from another location outside it. 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseup=complexbase.Attribute("onmouseup",SimpleTypes.String,'ONMOUSEUP',False,"""The ONMOUSEUP-Event occurs if the mouse button used for clicking is released on the
current element. 

A script can be stored in this attribute to be performed in the Event.""")
	style=complexbase.Attribute("style",SimpleTypes.String,'STYLE',False,"""This attribute specifies formatting style information for the current element. The content of this attribute is called inline CSS. The style attribute is deprecated (considered outdated), because it fuses together content and formatting.""")
	title=complexbase.Attribute("title",SimpleTypes.String,'TITLE',False,"""This attribute offers advisory information. Some Web browsers will display this information as tooltips. Authoring tools may make this information available to users as additional information about the element.""")
	def __init__(self):
		super().__init__()
		self._area_children=[]

class MappingConstraint(complexbase.GroupBase):
	"""Different constraints that may be used to limit the mapping of SW components to applicable ECUs, Partitions or Cores depending on the mappingScope attribute."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._variationPoint_child=ModelNone

class McDataAccessDetails(complexbase.GroupBase):
	"""This meta-class allows to attach detailed information about the usage of a data buffer by the RTE to a corresponding McDataInstance.

Use Case:  Direct memory access to RTE internal buffers for rapid prototyping. In case of implicit communication, the various task local buffers need to be identified in relation to RTE events and variable access points.

Note that the SwComponentPrototype, the RunnableEntity and the VariableDataPrototype are implicitly given be the referred instances of RTEEvent and VariableAccess."""
	def __init__(self):
		super().__init__()
		self._rteEvent_children=[]
		self._variableAccess_children=[]

class McDataInstance(complexbase.GroupBase):
	"""Describes the specific properties of one data instance in order to support measurement and/or calibration of this data instance.

The most important attributes are:
* Its shortName is copied from the ECU Flat map (if applicable) and will be used as identifier and for display by the MC system.
* The category is copied from the corresponding data type (ApplicationDataType if defined, otherwise ImplementationDataType) as far as applicable.
* The symbol is the one used in the programming language. It will be used to find out the actual memory address by the final generation tool with the help of linker generated information.

It is assumed that in the M1 model this part and all the aggregated and referred elements (with the exception of the Flat Map and the references from ImplementationElementInParameterInstanceRef and McAccessDetails) are completely generated from \"upstream\" information. This means, that even if an element like e.g. a CompuMethod is only used via reference here, it will be copied into the M1 artifact which holds the complete McSupportData for a given Implementation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._arraySize_child=ModelNone
		self._displayIdentifier_child=ModelNone
		self._flatMapEntry_child=ModelNone
		self._instanceInMemory_child=ModelNone
		self._mcDataAccessDetails_child=ModelNone
		self._mcDataAssignment_children=[]
		self._resultingProperties_child=ModelNone
		self._role_child=ModelNone
		self._subElement_children=[]
		self._symbol_child=ModelNone
		self._variationPoint_child=ModelNone

class McFunction(complexbase.GroupBase):
	"""Represents a functional element to be used as input to support measurement and calibration. It is used to
* assign calibration parameters to a logical function
* assign measurement variables to a logical function
* structure functions hierarchically"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._defCalprmSet_child=ModelNone
		self._refCalprmSet_child=ModelNone
		self._inMeasurementSet_child=ModelNone
		self._outMeasurmentSet_child=ModelNone
		self._locMeasurementSet_child=ModelNone
		self._outMeasurementSet_child=ModelNone
		self._subFunction_children=[]

class McFunctionDataRefSet(complexbase.GroupBase):
	"""Refers to a set of data assigned to an McFunction in a particular role. The data are given
* either by entries in a FlatMap
* or by data instances that are part of MC support data.
These two possibilities are exclusive within a given McFunctionDataRefSet. Which one to use depends on the process and tool environment. 

The set is subject to variability because the same functional model may be used with various representation of the data."""
	def __init__(self):
		super().__init__()
		self._McFunctionDataRefSetVariant_children=[]
		self._flatMapEntry_children=[]
		self._mcDataInstance_children=[]

class McFunctionDataRefSetConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class McFunctionDataRefSetContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class McParameterElementGroup(complexbase.GroupBase):
	"""Denotes a group of calibration parameters which are handled by the RTE as one data structure."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._ramLocation_child=ModelNone
		self._romLocation_child=ModelNone

class McSupportData(complexbase.GroupBase):
	"""Root element for all measurement and calibration support data related to one Implementation artifact on an ECU. There shall be one such element related to the RTE implementation (if it owns MC data) and a separate one for each module or component, which owns private MC data."""
	def __init__(self):
		super().__init__()
		self._emulationSupport_children=[]
		self._mcParameterInstance_children=[]
		self._mcVariableInstance_children=[]
		self._measurableSystemConstantValues_children=[]

class McSwEmulationMethodSupport(complexbase.GroupBase):
	"""This denotes the method used by the RTE to handle the calibration data. It is published by the RTE generator and can be used e.g. to generate the corresponding emulation method in a Complex Driver.

According to the actual method given by the category attribute,  not all attributes are always needed:

* double pointered method: only baseReference is mandatory
* single pointered method: only referenceTable is mandatory
* initRam method: only elementGroup(s) are mandatory

Note: For single/double pointered method the group locations are implicitly accessed via the reference table and their location can be found from the initial values in the M1 model of the respective pointers. Therefore, the description of elementGroups is not needed in these cases.  Likewise, for double pointered method the reference table description can be accessed via the M1 model under baseReference."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._baseReference_child=ModelNone
		self._elementGroup_children=[]
		self._referenceTable_child=ModelNone
		self._variationPoint_child=ModelNone

class MeasuredExecutionTime(complexbase.GroupBase):
	"""Specifies the ExecutionTime which has been gathered using measurement means."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maximumExecutionTime_child=ModelNone
		self._minimumExecutionTime_child=ModelNone
		self._nominalExecutionTime_child=ModelNone

class MeasuredHeapUsage(complexbase.GroupBase):
	"""The heap usage has been measured."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._averageMemoryConsumption_child=ModelNone
		self._maximumMemoryConsumption_child=ModelNone
		self._minimumMemoryConsumption_child=ModelNone
		self._testPattern_child=ModelNone

class MeasuredStackUsage(complexbase.GroupBase):
	"""The stack usage has been measured."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._averageMemoryConsumption_child=ModelNone
		self._maximumMemoryConsumption_child=ModelNone
		self._minimumMemoryConsumption_child=ModelNone
		self._testPattern_child=ModelNone

class MemorySection(complexbase.GroupBase):
	"""Provides a description of an abstract memory section used in the Implementation for code or data. It shall be declared by the Implementation Description of the module or component, which actually allocates the memory in its code. This means in case of data prototypes which are allocated by the RTE, that the generated Implementation Description of the RTE shall contain the corresponding MemorySections.

The attribute \"symbol\" (if symbol is missing: \"shortName\") defines the module or component specific section name used in the code. For details see the document \"Specification of Memory Mapping\".
Typically the section name is build according the pattern:

<SwAddrMethod shortName>[_<further specialization nominator>][_<alignment>] 
where
* '''[<SwAddrMethod shortName>]''' is the shortName of the referenced SwAddrMethod
* '''[_<further specialization nominator>]''' is an optional infix to indicate the specialization in the case that several MemorySections for different purpose of the same Implementation Description referring to the same or equally named SwAddrMethods. 
* '''[_<alignment>]''' is the alignment attributes value and is only applicable in the case that the memoryAllocationKeywordPolicy value of the referenced SwAddrMethod is set to addrMethodShortNameAndAlignment 

MemorySection used to Implement the code of RunnableEntitys and BswSchedulableEntitys shall have a symbol (if missing: shortName) identical to the referred SwAddrMethod to conform to the generated RTE header files.   

In addition to the section name described above, a prefix is used in the corresponding macro code in order to define a name space. This prefix is by default given by the shortName of the BswModuleDescription resp. the SwComponentType. It can be superseded by  the prefix attribute."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._executableEntity_children=[]
		self._memClassSymbol_child=ModelNone
		self._option_children=[]
		self._prefix_child=ModelNone
		self._size_child=ModelNone
		self._swAddrmethod_child=ModelNone
		self._symbol_child=ModelNone
		self._variationPoint_child=ModelNone

class MemorySectionLocation(complexbase.GroupBase):
	"""Specifies in which hardware ProvidedMemorySegment the softwareMemorySection is located."""
	def __init__(self):
		super().__init__()
		self._providedMemory_child=ModelNone
		self._softwareMemorySection_child=ModelNone

class MixedContentForLongName(complexbase.GroupBase):
	"""This is the model for titles and long-names. It allows some emphasis and index entries but no reference target (which is provided by the identifiable in question). It is intended that the content model can also be rendered as plain text.

The abstract class can be used for single language as well as for multi language elements."""
	def __init__(self):
		super().__init__()
		self._tt_child=ModelNone
		self._e_child=ModelNone
		self._sup_child=ModelNone
		self._sub_child=ModelNone
		self._ie_child=ModelNone

class MixedContentForOverviewParagraph(complexbase.GroupBase):
	"""This is the text model of a restricted paragraph item within a documentation. Such restricted paragraphs are used mainly for overview items, e.g. desc."""
	def __init__(self):
		super().__init__()
		self._br_child=ModelNone
		self._ft_child=ModelNone
		self._trace_child=ModelNone
		self._tt_child=ModelNone
		self._xref_child=ModelNone
		self._xrefTarget_child=ModelNone
		self._e_child=ModelNone
		self._sup_child=ModelNone
		self._sub_child=ModelNone
		self._ie_child=ModelNone

class MixedContentForParagraph(complexbase.GroupBase):
	"""This mainly represents the text model of a full blown paragraph within a documentation."""
	def __init__(self):
		super().__init__()
		self._ft_child=ModelNone
		self._trace_child=ModelNone
		self._tt_child=ModelNone
		self._br_child=ModelNone
		self._xref_child=ModelNone
		self._xrefTarget_child=ModelNone
		self._e_child=ModelNone
		self._sup_child=ModelNone
		self._sub_child=ModelNone
		self._ie_child=ModelNone
		self._std_child=ModelNone
		self._xdoc_child=ModelNone
		self._xfile_child=ModelNone

class MixedContentForPlainText(complexbase.GroupBase):
	"""This represents a plain text which conceptually is handled as mixed contents. It is modeled as such for symmetry reasons."""

class MixedContentForUnitNames(complexbase.GroupBase):
	"""This is the text model for items with subscript and superscripts such as measurement unit designations. It is intended, that such models can easily be transcribed to a plain text model either by using appropriate characters or by transcribing like m^2."""
	def __init__(self):
		super().__init__()
		self._sup_child=ModelNone
		self._sub_child=ModelNone

class MixedContentForVerbatim(complexbase.GroupBase):
	"""This is the text model for preformatted (verbatim) text. It mainly consists of attributes which do not change the length on rendering.

This class represents multilingual verbatim. Verbatim, sometimes called preformatted text, means that white-space is maintained. When verbatim is rendered in PDF or Online media, it is rendered using a monospaced font while white-space is obeyed. Blanks are rendered as well as newline characters.

Even if there are inline elements, the length of the data must not be influenced by formatting."""
	def __init__(self):
		super().__init__()
		self._tt_child=ModelNone
		self._e_child=ModelNone
		self._xref_child=ModelNone
		self._br_child=ModelNone

class MlFigure(complexbase.GroupBase):
	"""This metaclass represents the ability to embed a figure."""
	frame=complexbase.Attribute("frame",SimpleTypes.FrameEnum,'FRAME',False,"""Used to defined the frame line around a figure. It can assume the following values:

* TOP - Border at the top of the figure
* BOTTOM - Border at the bottom of the figure
* TOPBOT - Borders at the top and bottom of  the figure
* ALL - Borders all around the figure
* SIDES - Borders at the sides of the figure
* NONE - No borders around the figure""")
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax must be defined by the applied help system respectively help system generator.""")
	pgwide=complexbase.Attribute("pgwide",SimpleTypes.PgwideEnum,'PGWIDE',False,"""Used to indicate wether the figure should take the complete page width (value = \"pgwide\") or not (value = \"noPgwide\").""")
	def __init__(self):
		super().__init__()
		self._figureCaption_child=ModelNone
		self._verbatim_child=ModelNone
		self._variationPoint_child=ModelNone

class MlFormula(complexbase.GroupBase):
	"""This meta-class represents the ability to express a formula in a documentation. The formula can be expressed by various means. If more than one representation is available, they need to be consistent. The rendering system can use the representation which is most appropriate."""
	def __init__(self):
		super().__init__()
		self._formulaCaption_child=ModelNone
		self._verbatim_child=ModelNone
		self._texMath_child=ModelNone
		self._genericMath_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeAccessPoint(complexbase.GroupBase):
	"""A ModeAccessPoint is required by a RunnableEntity owned by a Mode Manager or Mode User.
Its semantics implies the ability to access the current mode (provided by the RTE)  of a ModeDeclarationGroupPrototype's ModeDeclarationGroup."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone
		self._modeGroup_child=[]
		self._variationPoint_child=ModelNone

class ModeAccessPointIdent(complexbase.GroupBase):
	"""This meta-class has been created to introduce the ability to become referenced into the meta-class ModeAccessPoint without breaking backwards compatibility."""
	_references=defaultdict(list)

class ModeDeclaration(complexbase.GroupBase):
	"""Declaration of one Mode. The name and semantics of a specific mode is not defined in the meta-model."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeDeclarationGroup(complexbase.GroupBase):
	"""A collection of Mode Declarations. Also, the initial mode is explicitly identified."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initialMode_child=ModelNone
		self._modeDeclaration_children=[]
		self._modeManagerErrorBehavior_child=ModelNone
		self._modeTransition_children=[]
		self._modeUserErrorBehavior_child=ModelNone
		self._onTransitionValue_child=ModelNone

class ModeDeclarationGroupPrototype(complexbase.GroupBase):
	"""The ModeDeclarationGroupPrototype specifies a set of Modes (ModeDeclarationGroup) which is provided or required in the given context."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swCalibrationAccess_child=ModelNone
		self._type_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeDeclarationGroupPrototypeMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular ModeDeclarationGroupPrototypes (in the given context) that are unequally named and/or require a reference to a ModeDeclarationMappingSet in order to become compatible by definition of ModeDeclarationMappings."""
	def __init__(self):
		super().__init__()
		self._firstModeGroup_child=ModelNone
		self._modeDeclarationMappingSet_child=ModelNone
		self._secondModeGroup_child=ModelNone

class ModeDeclarationGroupPrototypeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ModeDeclarationGroupPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeDeclarationMapping(complexbase.GroupBase):
	"""This meta-class implements a concrete mapping of two ModeDeclarations."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._firstMode_children=[]
		self._secondMode_child=ModelNone

class ModeDeclarationMappingSet(complexbase.GroupBase):
	"""This meta-class implements a container for ModeDeclarationGroupMappings"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._modeDeclarationMapping_children=[]

class ModeDrivenTransmissionModeCondition(complexbase.GroupBase):
	"""The condition defined by this class evaluates to true if one of the referenced modeDeclarations (OR associated) is active. All referenced modeDeclarations shall be from the same ModeDeclarationGroup.

The condition is used to define which TransmissionMode shall be activated using Com_SwitchIpduTxMode."""
	def __init__(self):
		super().__init__()
		self._modeDeclaration_children=[]

class ModeErrorBehavior(complexbase.GroupBase):
	"""This represents the ability to define the error behavior in the context of mode handling."""
	def __init__(self):
		super().__init__()
		self._defaultMode_child=ModelNone
		self._errorReactionPolicy_child=ModelNone

class ModeGroupInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class ModeInBswModuleDescriptionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextModeDeclarationGroup_child=ModelNone
		self._targetMode_child=ModelNone

class ModeInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of ModeDeclarationGroupPrototypes in context of two different ModeInterfaces."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._modeMapping_child=ModelNone

class ModePortAnnotation(complexbase.GroupBase):
	"""Annotation to a port used for calibration regarding a certain ModeDeclarationGroupPrototype."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class ModeRequestTypeMap(complexbase.GroupBase):
	"""Specifies a mapping between a ModeDeclarationGroup and an ImplementationDataType. This ImplementationDataType shall be used to implement the ModeDeclarationGroup."""
	def __init__(self):
		super().__init__()
		self._implementationDataType_child=ModelNone
		self._modeGroup_child=ModelNone

class ModeSwitchInterface(complexbase.GroupBase):
	"""A mode switch interface declares a ModeDeclarationGroupPrototype to be sent and received."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class ModeSwitchPoint(complexbase.GroupBase):
	"""A ModeSwitchPoint is required by a RunnableEntity owned a Mode Manager. Its semantics implies the ability to initiate a mode switch."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeSwitchReceiverComSpec(complexbase.GroupBase):
	"""Communication attributes of RPortPrototypes with respect to mode communication"""
	def __init__(self):
		super().__init__()
		self._enhancedModeApi_child=ModelNone
		self._modeGroup_child=ModelNone
		self._supportsAsynchronousModeSwitch_child=ModelNone

class ModeSwitchSenderComSpec(complexbase.GroupBase):
	"""Communication attributes of PPortPrototypes with respect to mode communication"""
	def __init__(self):
		super().__init__()
		self._enhancedModeApi_child=ModelNone
		self._modeGroup_child=ModelNone
		self._modeSwitchedAck_child=ModelNone
		self._queueLength_child=ModelNone

class ModeSwitchedAckEvent(complexbase.GroupBase):
	"""The event is raised when the referenced modes have been received or an error occurs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class ModeSwitchedAckRequest(complexbase.GroupBase):
	"""Requests acknowledgements that a mode switch has been proceeded successfully"""
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone

class ModeTransition(complexbase.GroupBase):
	"""This meta-class represents the ability to describe possible ModeTransitions in the context of a ModeDeclarationGroup."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._enteredMode_child=ModelNone
		self._exitedMode_child=ModelNone

class Modification(complexbase.GroupBase):
	"""This meta-class represents the ability  to record what has changed in a document in comparison to its predecessor."""
	def __init__(self):
		super().__init__()
		self._change_child=ModelNone
		self._reason_child=ModelNone

class MsrQueryArg(complexbase.GroupBase):
	"""This represents an argument to the query. Note that the arguments are not standardized and therefore subject to mutual agreement."""
	si=complexbase.Attribute("si",SimpleTypes.NameToken,'SI',False,"""This denotes the name of the query argument (semantic information)""")
	def __init__(self):
		super().__init__()
		self._arg_child=ModelNone

class MsrQueryChapter(complexbase.GroupBase):
	"""This meta-class represents the ability to express a query which yields a set of chapters as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultChapter_child=ModelNone

class MsrQueryP1(complexbase.GroupBase):
	"""This meta-class represents the ability to express a query which yields the content of a topic as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultP1_child=ModelNone

class MsrQueryP2(complexbase.GroupBase):
	"""This meta-class represents the ability to express a query which yields the content of a DocumentationBlock as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultP2_child=ModelNone

class MsrQueryProps(complexbase.GroupBase):
	"""This metaclass represents the ability to specificy a query which yields some documentation text. The qualities of the result are determined by the context in which the query is used."""
	def __init__(self):
		super().__init__()
		self._msrQueryName_child=ModelNone
		self._msrQueryArg_children=[]
		self._comment_child=ModelNone

class MsrQueryResultChapter(complexbase.GroupBase):
	"""This metaclass represents the result of an msrquery which is a set of chapters."""
	def __init__(self):
		super().__init__()
		self._chapter_children=[]

class MsrQueryResultTopic1(complexbase.GroupBase):
	"""This metaclass represents the ability to express the result of a query which is a set of topics."""
	def __init__(self):
		super().__init__()
		self._topic1_children=[]

class MsrQueryTopic1(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a query which yields a set of topics as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultTopic1_child=ModelNone

class MultiLanguageOverviewParagraph(complexbase.GroupBase):
	"""This is the content of a multilingual paragraph in an overview item."""
	def __init__(self):
		super().__init__()
		self._l2_children=[]

class MultiLanguageParagraph(complexbase.GroupBase):
	"""This is the content model of a multilingual paragraph in a documentation."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax must be defined by the applied help system respectively help system generator.""")
	def __init__(self):
		super().__init__()
		self._l1_children=[]
		self._variationPoint_child=ModelNone

class MultiLanguagePlainText(complexbase.GroupBase):
	"""This is a multilingual plaint Text.It is intended to be rendered as a paragraph."""
	def __init__(self):
		super().__init__()
		self._l10_children=[]

class MultiLanguageVerbatim(complexbase.GroupBase):
	"""This class represents multilingual Verbatim. Verbatim means, that white-space is maintained. When Verbatim is rendered in PDF or Online media, white-space is obeyed. Blanks are rendered as well as newline characters."""
	allowBreak=complexbase.Attribute("allowBreak",SimpleTypes.NameToken,'ALLOW-BREAK',False,"""This indicates if the verbatim text might be split on multiple pages. Default is \"1\".""")
	float=complexbase.Attribute("float",SimpleTypes.FloatEnum,'FLOAT',False,"""Indicate whether it is allowed to break the element. The following values are allowed:""")
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax must be defined by the applied help system respectively help system generator.""")
	pgwide=complexbase.Attribute("pgwide",SimpleTypes.PgwideEnum,'PGWIDE',False,"""Used to indicate wether the figure should take the complete page width (value = \"pgwide\") or not (value = \"noPgwide\").""")
	def __init__(self):
		super().__init__()
		self._l5_children=[]
		self._variationPoint_child=ModelNone

class MultidimensionalTime(complexbase.GroupBase):
	"""This is used to specify a multidimensional time value based on ASAM CSE codes. It is specified by a code which defined the basis of the time and a scaling factor which finally determines the time value.

If for example the cseCode is 100 and the cseCodeFactor is 360, it represents 360 angular degrees.
If the cseCode is 2 and the cseCodeFactor is 50 it represents 50 microseconds"""
	def __init__(self):
		super().__init__()
		self._cseCode_child=ModelNone
		self._cseCodeFactor_child=ModelNone

class MultilanguageLongName(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a long name which acts in the role of a headline. It is intended for human readers. Per language it should be around max 80 characters."""
	def __init__(self):
		super().__init__()
		self._l4_children=[]

class MultilanguageReferrable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (while adhering to namespace borders). They also may have a longName. But they are not considered to contribute substantially to the overall structure of an AUTOSAR description. In particular it does not contain other Referrables."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._longName_child=ModelNone

class MultiplexedIPdu(complexbase.GroupBase):
	"""A MultiplexedPdu (i.e. NOT a COM I-PDU) contains a DynamicPart, an optional StaticPart and a selectorField. In case of multiplexing this IPdu is routed between the Pdu Multiplexer and the Interface Layer. 

A multiplexer is used to define variable parts within an IPdu that may carry different signals. The receivers of such a IPdu can determine which signalPdus are transmitted by evaluating the selector field, which carries a unique selector code for each sub-part."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dynamicPart_children=[]
		self._selectorFieldByteOrder_child=ModelNone
		self._selectorFieldLength_child=ModelNone
		self._selectorFieldStartPosition_child=ModelNone
		self._staticPart_children=[]
		self._triggerMode_child=ModelNone
		self._unusedBitPattern_child=ModelNone

class MultiplexedPart(complexbase.GroupBase):
	"""The StaticPart and the DynamicPart have common properties. Both can be separated in multiple segments within the multiplexed PDU."""
	def __init__(self):
		super().__init__()
		self._segmentPosition_children=[]

class NPdu(complexbase.GroupBase):
	"""This is a Pdu of the Transport Layer.
The main purpose of the TP Layer is to segment and reassemble IPdus."""
	_references=defaultdict(list)

class NetworkEndpoint(complexbase.GroupBase):
	"""The network endpoint defines the network addressing (e.g. IP-Address or MAC multicast address)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._fullyQualifiedDomainName_child=ModelNone
		self._infrastructureServices_child=ModelNone
		self._networkEndpointAddress_children=[]
		self._priority_child=ModelNone

class NetworkEndpointAddress(complexbase.GroupBase):
	"""To build a valid network endpoint address there has to be either one MAC multicast group reference or an ipv4 configuration or an ipv6 configuration."""

class NmCluster(complexbase.GroupBase):
	"""Set of NM nodes coordinated with use of the NM algorithm."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationCluster_child=ModelNone
		self._nmChannelId_child=ModelNone
		self._nmChannelSleepMaster_child=ModelNone
		self._nmNode_children=[]
		self._nmSynchronizingNetwork_child=ModelNone
		self._variationPoint_child=ModelNone

class NmClusterCoupling(complexbase.GroupBase):
	"""Attributes that are valid for each of the referenced (coupled) clusters."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class NmConfig(complexbase.GroupBase):
	"""Contains the all configuration elements for AUTOSAR Nm."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmCluster_children=[]
		self._nmClusterCoupling_children=[]
		self._nmIfEcu_children=[]

class NmCoordinator(complexbase.GroupBase):
	"""A NM coordinator is an ECU, which is connected to at least two busses, and where the requirement exists that shutdown of NM of at least two of these busses (also referred to as coordinated busses) has to be performed synchronously."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone
		self._nmActiveCoordinator_child=ModelNone
		self._nmCoordSyncSupport_child=ModelNone
		self._nmGlobalCoordinatorTime_child=ModelNone
		self._nmNode_children=[]
		self._nmShutdownDelayTimer_child=ModelNone

class NmEcu(complexbase.GroupBase):
	"""ECU on which NM is running."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._busDependentNmEcu_children=[]
		self._busSpecificNmEcu_child=[]
		self._ecuInstance_child=ModelNone
		self._nmBusSynchronizationEnabled_child=ModelNone
		self._nmComControlEnabled_child=ModelNone
		self._nmCoordinator_child=ModelNone
		self._nmCycletimeMainFunction_child=ModelNone
		self._nmMultipleChannelsEnabled_child=ModelNone
		self._nmNodeDetectionEnabled_child=ModelNone
		self._nmNodeIdEnabled_child=ModelNone
		self._nmPassiveModeEnabled_child=ModelNone
		self._nmPduRxIndicationEnabled_child=ModelNone
		self._nmRemoteSleepIndEnabled_child=ModelNone
		self._nmRepeatMsgIndEnabled_child=ModelNone
		self._nmStateChangeIndEnabled_child=ModelNone
		self._nmUserDataEnabled_child=ModelNone
		self._variationPoint_child=ModelNone

class NmNode(complexbase.GroupBase):
	"""The linking of NmEcus to NmClusters is realized via the NmNodes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._controller_child=ModelNone
		self._nmCoordCluster_child=ModelNone
		self._nmCoordinatorRole_child=ModelNone
		self._nmIfEcu_child=ModelNone
		self._nmNodeId_child=ModelNone
		self._nmPassiveModeEnabled_child=ModelNone
		self._rxNmPdu_children=[]
		self._txNmPdu_children=[]
		self._variationPoint_child=ModelNone

class NmPdu(complexbase.GroupBase):
	"""Network Management Pdu"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iSignalToIPduMapping_children=[]
		self._nmDataInformation_child=ModelNone
		self._nmVoteInformation_child=ModelNone
		self._unusedBitPattern_child=ModelNone

class NmPduRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._NmPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class NonqueuedReceiverComSpec(complexbase.GroupBase):
	"""Communication attributes specific to non-queued receiving."""
	def __init__(self):
		super().__init__()
		self._aliveTimeout_child=ModelNone
		self._enableUpdate_child=ModelNone
		self._filter_child=ModelNone
		self._handleDataStatus_child=ModelNone
		self._handleNeverReceived_child=ModelNone
		self._handleTimeoutType_child=ModelNone
		self._initValue_child=[]

class NonqueuedSenderComSpec(complexbase.GroupBase):
	"""Communication attributes for non-queued sender/receiver communication (sender side)"""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]

class Note(complexbase.GroupBase):
	"""This represents a note in a documentation, which may be used to highlight specific issues such as hints or caution notes.

N.B., Documentation notes can be nested recursively, even if this is not really intended. In case of nested notes e.g. the note icon of inner notes might be omitted while rendering the note."""
	noteType=complexbase.Attribute("noteType",SimpleTypes.NoteTypeEnum,'NOTE-TYPE',False,"""Type of the Note. Default is \"HINT\"""")
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone
		self._variationPoint_child=ModelNone

class NumericalOrText(complexbase.GroupBase):
	"""This meta-class represents the ability to yield either a numerical or a string. A typical use case is that two or more instances of this meta-class are aggregated with a VariationPoint where some instances yield strings while other instances yield numerical depending on the resolution of the binding expression."""
	def __init__(self):
		super().__init__()
		self._vf_child=ModelNone
		self._vt_child=ModelNone
		self._variationPoint_child=ModelNone

class NumericalRuleBasedValueSpecification(complexbase.GroupBase):
	"""This meta-class is used to support a rule-based initialization approach for data types with an array-nature (ImplementationDataType of category ARRAY)."""
	def __init__(self):
		super().__init__()
		self._ruleBasedValues_child=ModelNone

class NumericalValueSpecification(complexbase.GroupBase):
	"""A numerical  ValueSpecification which is intended to be assigned to a Primitive data element.
Note that the numerical value is a variant, it can be computed by a formula."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class NumericalValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Numerical attributes.

Note that this class might be used in the extended meta-model only."""

class NvBlockDataMapping(complexbase.GroupBase):
	"""Defines the mapping between the VariableDataPrototypes in the NvBlockComponents ports and the VariableDataPrototypes of the RAM Block.

The data types of the referenced VariableDataPrototypes in the ports and the referenced sub-element (inside a CompositeDataType) of the VariableDataPrototype representing the RAM Block shall be compatible."""
	def __init__(self):
		super().__init__()
		self._nvRamBlockElement_child=ModelNone
		self._readNvData_child=ModelNone
		self._writtenNvData_child=ModelNone
		self._writtenReadNvData_child=ModelNone
		self._variationPoint_child=ModelNone

class NvBlockDescriptor(complexbase.GroupBase):
	"""Specifies the properties of exactly on NVRAM Block."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._clientServerPort_children=[]
		self._constantValueMapping_children=[]
		self._dataTypeMapping_children=[]
		self._instantiationDataDefProps_children=[]
		self._nvBlockDataMapping_children=[]
		self._nvBlockNeeds_child=ModelNone
		self._ramBlock_child=ModelNone
		self._romBlock_child=ModelNone
		self._supportDirtyFlag_child=ModelNone
		self._timingEvent_child=ModelNone
		self._variationPoint_child=ModelNone

class NvBlockNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of a single NVRAM Block."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._calcRamBlockCrc_child=ModelNone
		self._checkStaticBlockId_child=ModelNone
		self._cyclicWritingPeriod_child=ModelNone
		self._nDataSets_child=ModelNone
		self._nRomBlocks_child=ModelNone
		self._ramBlockStatusControl_child=ModelNone
		self._readonly_child=ModelNone
		self._reliability_child=ModelNone
		self._resistantToChangedSw_child=ModelNone
		self._restoreAtStart_child=ModelNone
		self._storeAtShutdown_child=ModelNone
		self._storeCyclic_child=ModelNone
		self._storeEmergency_child=ModelNone
		self._storeImmediate_child=ModelNone
		self._useAutoValidationAtShutDown_child=ModelNone
		self._useCRCCompMechanism_child=ModelNone
		self._writeOnlyOnce_child=ModelNone
		self._writeVerification_child=ModelNone
		self._writingFrequency_child=ModelNone
		self._writingPriority_child=ModelNone

class NvBlockSwComponentType(complexbase.GroupBase):
	"""The NvBlockSwComponentType defines non volatile data which data can be shared between SwComponentPrototypes. The non volatile data of the NvBlockSwComponentType are accessible via provided and required ports."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nvBlockDescriptor_children=[]

class NvDataInterface(complexbase.GroupBase):
	"""A non volatile data interface declares a number of VariableDataPrototypes to be exchanged between non volatile block components and atomic software components."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nvData_children=[]

class NvDataPortAnnotation(complexbase.GroupBase):
	"""Annotation to a port regarding a certain VariableDataPrototype."""
	def __init__(self):
		super().__init__()
		self._variable_child=ModelNone

class NvProvideComSpec(complexbase.GroupBase):
	"""Communication attributes of PPortPrototypes with respect to Nv data communication on the provided side."""
	def __init__(self):
		super().__init__()
		self._ramBlockInitValue_child=[]
		self._romBlockInitValue_child=[]
		self._variable_child=ModelNone

class NvRequireComSpec(complexbase.GroupBase):
	"""Communication attributes of RPortPrototypes with respect to Nv data communication on the required side."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._variable_child=ModelNone

class ObdControlServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Service 08 (request control of on-board system) in relation to a particular test-Identifier (TID) supported by this component or module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._testId_child=ModelNone

class ObdInfoServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a given InfoType (OBD Service 09) which is supported by this component or module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataLength_child=ModelNone
		self._infoType_child=ModelNone

class ObdMonitorServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a particular on-board monitoring test supported by this component or module. (OBD Service 06)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._onBoardMonitorId_child=ModelNone
		self._testId_child=ModelNone
		self._unitAndScalingId_child=ModelNone

class ObdPidServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a particular PID (parameter identifier) which is supported by this component or module.

In case of using a client/server communicated value, the related value shall be communicated via the port referenced by asssignedPort. The details of this communication (e.g. appropriate naming conventions) are specified in the related software specifications (SWS)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataLength_child=ModelNone
		self._parameterId_child=ModelNone
		self._standard_child=ModelNone

class ObdRatioServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a particular \"ratio monitoring\" which is supported by this component or module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._connectionType_child=ModelNone
		self._iumprGroup_child=ModelNone
		self._rateBasedMonitoredEvent_child=ModelNone
		self._usedFid_child=ModelNone
		self._usedSecondaryFid_children=[]

class OffsetTimingConstraint(complexbase.GroupBase):
	"""Bounds the time offset between the occurrence of two timing events, without requiring a direct functional dependency between the source and the target.

If the target event occurs, it is expected to occur earliest with the minimum, and latest with the maximum offset relatively after the occurrence of the source event. Note: not every source event occurrence must be followed by a target event occurrence.

In contrast to LatencyTimingConstraint, there must not necessarily be a causal dependency between the source and target event."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._source_child=ModelNone
		self._target_child=ModelNone
		self._minimum_child=ModelNone
		self._maximum_child=ModelNone

class OperationArgumentInComponentInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._base_child=ModelNone
		self._contextComponent_children=[]
		self._contextPortPrototype_child=ModelNone
		self._contextOperation_child=ModelNone
		self._rootArgumentDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class OperationInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class OperationInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._targetOperation_child=ModelNone

class OperationInvokedEvent(complexbase.GroupBase):
	"""The OperationInvokedEvent references the ClientServerOperation invoked by the client."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone

class OrderedMaster(complexbase.GroupBase):
	"""Element in the network endpoint list."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone
		self._timeSyncServer_child=ModelNone

class PModeGroupInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPPort_child=ModelNone
		self._targetModeGroup_child=ModelNone

class POperationInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPPort_child=ModelNone
		self._targetProvidedOperation_child=ModelNone

class PPortComSpec(complexbase.GroupBase):
	"""Communication attributes of a provided PortPrototype. This class will contain attributes that are valid for all kinds of provide ports, independent of client-server or sender-receiver communication patterns."""

class PPortInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_child=ModelNone
		self._targetPPort_child=ModelNone

class PPortPrototype(complexbase.GroupBase):
	"""Component port providing a certain port interface."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._providedInterface_child=ModelNone

class PRPortPrototype(complexbase.GroupBase):
	"""This kind of PortPrototype can take the role of both a required and a provided PortPrototype."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._providedRequiredInterface_child=ModelNone

class PTriggerInAtomicSwcTypeInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPPort_child=ModelNone
		self._targetTrigger_child=ModelNone

class PackageableElement(complexbase.GroupBase):
	"""This meta-class specifies the ability to be a member of an AUTOSAR package."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class Paginateable(complexbase.GroupBase):
	"""This meta-class represents the ability to control the pagination policy when creating documents."""
	break_=complexbase.Attribute("break_",SimpleTypes.ChapterEnumBreak,'BREAK',False,"""This attributes allows to specify a forced page break.""")
	keepWithPrevious=complexbase.Attribute("keepWithPrevious",SimpleTypes.KeepWithPreviousEnum,'KEEP-WITH-PREVIOUS',False,"""This attribute denotes the pagination policy. In particular it defines if the containing text block shall be kept together with the previous block.""")

class ParameterAccess(complexbase.GroupBase):
	"""The presence of a ParameterAccess implies that a RunnableEntity needs access to a ParameterDataPrototype."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessedParameter_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class ParameterDataPrototype(complexbase.GroupBase):
	"""A parameter element used for parameter interface and internal behavior, supporting signal like parameter and characteristic value communication patterns and parameter and characteristic value definition."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._variationPoint_child=ModelNone

class ParameterInAtomicSWCTypeInstanceRef(complexbase.GroupBase):
	"""This class implements an instance reference which can be applied for variables as well as for parameters."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._rootParameterDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class ParameterInterface(complexbase.GroupBase):
	"""A parameter interface declares a number of parameter and characteristic values to be exchanged between parameter components and software components."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._parameter_children=[]

class ParameterPortAnnotation(complexbase.GroupBase):
	"""Annotation to a port used for calibration regarding a certain ParameterDataPrototype."""
	def __init__(self):
		super().__init__()
		self._parameter_child=ModelNone

class ParameterProvideComSpec(complexbase.GroupBase):
	"""\"Communication\" specification that applies to parameters on the provided side of a connection."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._parameter_child=ModelNone

class ParameterRequireComSpec(complexbase.GroupBase):
	"""\"Communication\" specification that applies to parameters on the required side of a connection."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._parameter_child=ModelNone

class ParameterSwComponentType(complexbase.GroupBase):
	"""The ParameterSwComponentType defines parameters and characteristic values accessible via provided Ports. The provided values are the same for all connected SwComponentPrototypes"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._constantMapping_children=[]
		self._dataTypeMapping_children=[]
		self._instantiationDataDefProps_children=[]

class PassThroughSwConnector(complexbase.GroupBase):
	"""This kind of SwConnector can be used inside a CompositionSwComponentType to connect two delegation PortPrototypes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._providedOuterPort_child=ModelNone
		self._requiredOuterPort_child=ModelNone

class Pdu(complexbase.GroupBase):
	"""Collection of all Pdus that can be routed through a bus interface."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._length_child=ModelNone
		self._metaDataLength_child=ModelNone

class PduMappingDefaultValue(complexbase.GroupBase):
	"""Default Value which will be distributed if no I-Pdu has been received since last sending."""
	def __init__(self):
		super().__init__()
		self._defaultValueElement_children=[]

class PduToFrameMapping(complexbase.GroupBase):
	"""A PduToFrameMapping defines the composition of Pdus in each frame."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._packingByteOrder_child=ModelNone
		self._pdu_child=ModelNone
		self._startPosition_child=ModelNone
		self._updateIndicationBitPosition_child=ModelNone
		self._variationPoint_child=ModelNone

class PduTriggering(complexbase.GroupBase):
	"""The PduTriggering describes on which channel the IPdu is transmitted.
The Pdu routing by the PduR is only allowed for subclasses of IPdu.

Depending on its relation to entities such channels and clusters it can be unambiguously deduced whether a fan-out is handled by the Pdu router or the Bus Interface. 

If the fan-out is specified between different clusters it shall be handled by the Pdu Router. If the fan-out is specified between different channels of the same cluster it shall be handled by the Bus Interface."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iPduPort_children=[]
		self._iPdu_child=ModelNone
		self._iSignalTriggering_children=[]
		self._triggerIPduSendCondition_children=[]
		self._variationPoint_child=ModelNone

class PduTriggeringRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._PduTriggering_child=ModelNone
		self._variationPoint_child=ModelNone

class PdurIPduGroup(complexbase.GroupBase):
	"""The AUTOSAR PduR will enable and disable the sending of configurable groups of IPdus during runtime according to the AUTOSAR PduR specification."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationMode_child=ModelNone
		self._iPdu_children=[]

class PerInstanceMemory(complexbase.GroupBase):
	"""Defines a 'C' typed memory-block that needs to be available for each instance of the SW-component.  This is typically only useful if supportsMultipleInstantiation is set to \"true\" or if the software-component defines NVRAM access via permanent blocks."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initValue_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._type_child=ModelNone
		self._typeDefinition_child=ModelNone
		self._variationPoint_child=ModelNone

class PerInstanceMemorySize(complexbase.GroupBase):
	"""Resources needed by the allocation of PerInstanceMemory for each SWC instance. Note that these resources are not covered by an ObjectFileSection, because they are supposed to be allocated by the RTE."""
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._perInstanceMemory_child=ModelNone
		self._size_child=ModelNone
		self._variationPoint_child=ModelNone

class PeriodicEventTriggering(complexbase.GroupBase):
	"""The PeriodicEventTriggering describes the behavior of an event with a strict periodic occurrence pattern, given by the period attribute.

Additionally, it is possible to soften the strictness of the periodic occurrence behavior by specifying a jitter, so that there can be a deviation from the period up to the size of the jitter."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._minimumInterArrivalTime_child=ModelNone
		self._jitter_child=ModelNone
		self._period_child=ModelNone

class PermissibleSignalPath(complexbase.GroupBase):
	"""The PermissibleSignalPath describes the way a data element shall take in the topology. The path is described by ordered references to PhysicalChannels. 

If more than one PermissibleSignalPath is defined for the same
signal/operation attributes, any of them can be chosen. Such a signal path can be a constraint for the communication matrix . This path describes that one data element should take path A (e.g. 1. CAN channel, 2. LIN channel) 
and not path B (1. CAN channel, FlexRay channel A). 

This has an effect on the frame generation and the frame path."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._physicalChannel_children=[]
		self._signal_children=[]

class PhysConstrs(complexbase.GroupBase):
	"""This meta-class represents the ability to express physical constraints. Therefore it has (in opposite to InternalConstrs) a reference to a Unit."""
	def __init__(self):
		super().__init__()
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone
		self._scaleConstr_children=[]
		self._maxGradient_child=ModelNone
		self._maxDiff_child=ModelNone
		self._monotony_child=ModelNone
		self._unit_child=ModelNone

class PhysicalChannel(complexbase.GroupBase):
	"""A physical channel is the transmission medium that is used to send and receive information between communicating ECUs. Each CommunicationCluster has at least one physical channel. Bus systems like CAN and LIN only have exactly one PhysicalChannel. A FlexRay cluster may have more than one PhysicalChannels that may be used in parallel for redundant communication.

An ECU is part of a cluster if it contains at least one controller that is connected to at least one channel of the cluster."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._commConnector_children=[]
		self._frameTriggering_children=[]
		self._iSignalTriggering_children=[]
		self._pduTriggering_children=[]
		self._variationPoint_child=ModelNone

class PhysicalDimension(complexbase.GroupBase):
	"""This class represents a physical dimension.
If the physical dimension of two units is identical, then a conversion between them is possible. The conversion between units is related to the definition of the physical dimension.

Note that the equivalence of the exponents does not per se define the convertibility. For example Energy and Torque share the same exponents (Nm).

Please note further the value of an exponent does not necessarily have to be an integer number. It is also possible that the value yields a rational number, e.g. to compute the square root of a given physical quantity. In this case the exponent value would be a rational number where the numerator value is 1 and the denominator value is 2."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._lengthExp_child=ModelNone
		self._massExp_child=ModelNone
		self._timeExp_child=ModelNone
		self._currentExp_child=ModelNone
		self._temperatureExp_child=ModelNone
		self._molarAmountExp_child=ModelNone
		self._luminousIntensityExp_child=ModelNone

class PhysicalDimensionMapping(complexbase.GroupBase):
	"""This class represents a specific mapping between two PhysicalDimensions."""
	def __init__(self):
		super().__init__()
		self._firstPhysicalDimension_child=ModelNone
		self._secondPhysicalDimension_child=ModelNone

class PhysicalDimensionMappingSet(complexbase.GroupBase):
	"""This class represents a container for a list of mappings between PhysicalDimensions."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._physicalDimensionMapping_children=[]

class PncMapping(complexbase.GroupBase):
	"""Describes a mapping between one or several Virtual Function Clusters onto Partial Network Clusters. A Virtual Function Cluster is realized by a PortGroup. A Partial Network Cluster is realized by one or more IPduGroups."""
	def __init__(self):
		super().__init__()
		self._pncGroup_children=[]
		self._pncIdentifier_child=ModelNone
		self._shortLabel_child=ModelNone
		self._vfc_children=[]
		self._wakeupFrame_children=[]
		self._variationPoint_child=ModelNone

class PortAPIOption(complexbase.GroupBase):
	"""Options how to generate the signatures of calls for an AtomicSwComponentType in order to communicate over a PortPrototype (for calls into a RunnableEntity as well as for calls from a RunnableEntity to the PortPrototype)."""
	def __init__(self):
		super().__init__()
		self._enableTakeAddress_child=ModelNone
		self._errorHandling_child=ModelNone
		self._indirectAPI_child=ModelNone
		self._portArgValue_children=[]
		self._port_child=ModelNone
		self._variationPoint_child=ModelNone

class PortDefinedArgumentValue(complexbase.GroupBase):
	"""A PortDefinedArgumentValue is passed to a RunnableEntity dealing with the ClientServerOperations provided by a given PortPrototype. Note that this is restricted to PPortPrototypes of a ClientServerInterface."""
	def __init__(self):
		super().__init__()
		self._value_child=[]
		self._valueType_child=ModelNone

class PortGroup(complexbase.GroupBase):
	"""Group of ports which share a common functionality, e.g. need specific network resources. This information shall be available on the VFB level in order to delegate it properly via compositions. When propagated into the ECU extract, this information is used as input for the configuration of Services like the Communication Manager. A PortGroup is defined locally in a component (which can be a composition) and refers to the \"outer\" ports belonging to the group as well as to the \"inner\" groups which propagate  this group into the components which are part of a composition. A PortGroup within an atomic SWC cannot be linked to inner groups."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._innerGroup_children=[]
		self._outerPort_children=[]
		self._variationPoint_child=ModelNone

class PortGroupInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._target_child=ModelNone

class PortInCompositionTypeInstanceRef(complexbase.GroupBase):
	""

class PortInterface(complexbase.GroupBase):
	"""Abstract base class for an interface that is either provided or required by a port of a  software component."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._isService_child=ModelNone
		self._serviceKind_child=ModelNone

class PortInterfaceBlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map two PortInterfaces of which one acts as the blueprint for the other."""
	def __init__(self):
		super().__init__()
		self._portInterfaceBlueprint_child=ModelNone
		self._derivedPortInterface_child=ModelNone

class PortInterfaceMapping(complexbase.GroupBase):
	"""Specifies one PortInterfaceMapping to support the connection of Ports typed by two different PortInterfaces with PortInterface elements having unequal names and/or unequal semantic (resolution or range)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class PortInterfaceMappingSet(complexbase.GroupBase):
	"""Specifies a set of (one or more) PortInterfaceMappings."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._portInterfaceMapping_children=[]

class PortPrototype(complexbase.GroupBase):
	"""Base class for the ports of an AUTOSAR software component.

The aggregation of PortPrototypes is subject to variability with the purpose to support the conditional existence of ports."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._clientServerAnnotation_children=[]
		self._delegatedPortAnnotation_child=ModelNone
		self._ioHwAbstractionServerAnnotation_children=[]
		self._modePortAnnotation_children=[]
		self._nvDataPortAnnotation_children=[]
		self._parameterPortAnnotation_children=[]
		self._senderReceiverAnnotation_children=[]
		self._triggerPortAnnotation_children=[]
		self._variationPoint_child=ModelNone

class PortPrototypeBlueprint(complexbase.GroupBase):
	"""This meta-class represents the ability to express a blueprint of a PortPrototype by referring to a particular PortInterface. This blueprint can then be used as a guidance to create particular PortPrototypes which are defined according to this blueprint. By this it is possible to standardize application interfaces without the need to also standardize software-components with PortPrototypes typed by the standardized PortInterfaces."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initValue_children=[]
		self._interface_child=ModelNone
		self._providedComSpec_children=[]
		self._requiredComSpec_children=[]

class PortPrototypeBlueprintInitValue(complexbase.GroupBase):
	"""This meta-class represents the ability to express init values in PortPrototypeBlueprints. These init values act as a kind of blueprint from which for example proper ComSpecs can be derived."""
	def __init__(self):
		super().__init__()
		self._dataPrototype_child=ModelNone
		self._value_child=[]

class PortPrototypeBlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a PortPrototypeBlueprint to a PortProtoype of which one acts as the blueprint for the other."""
	def __init__(self):
		super().__init__()
		self._portPrototypeBlueprint_child=ModelNone
		self._derivedPortPrototype_child=ModelNone

class PortPrototypeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._PortPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class PositiveIntegerValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for positive Integer attributes.

Note that this class might be used in the extended meta-model only."""

class PostBuildVariantCondition(complexbase.GroupBase):
	"""This class specifies the value which must be assigned to a particular variant criterion in order to bind the variation point. If multiple criterion/value pairs are specified, they shall all match to bind the variation point.

In other words binding can be represented by

  (criterion1 == value1) &amp;&amp; (condition2 == value2) ..."""
	def __init__(self):
		super().__init__()
		self._matchingCriterion_child=ModelNone
		self._value_child=ModelNone

class PostBuildVariantCriterion(complexbase.GroupBase):
	"""This class specifies one particular PostBuildVariantSelector."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._compuMethod_child=ModelNone

class PostBuildVariantCriterionValue(complexbase.GroupBase):
	"""This class specifies a the value which must be assigned to a particular variant criterion in order to bind the variation point. If multiple criterion/value pairs are specified, they all must must match to bind the variation point."""
	def __init__(self):
		super().__init__()
		self._variantCriterion_child=ModelNone
		self._value_child=ModelNone
		self._annotation_children=[]

class PostBuildVariantCriterionValueSet(complexbase.GroupBase):
	"""This meta-class represents the ability to denote one set of postBuildVariantCriterionValues."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._postBuildVariantCriterionValue_children=[]

class PredefinedChapter(complexbase.GroupBase):
	"""This represents a predefined chapter."""

class PredefinedVariant(complexbase.GroupBase):
	"""This specifies one predefined variant. It is characterized by the union of all system constant values and post-build variant criterion values aggregated within all referenced system constant value sets and post build variant criterion value sets plus the value sets of the included variants."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._includedVariant_children=[]
		self._postBuildVariantCriterionValueSet_children=[]
		self._swSystemconstantValueSet_children=[]

class PrmChar(complexbase.GroupBase):
	"""This metaclass represents the ability to express the characteristics of one particular parameter. It can be exressed as numerical or as text parameter (provided as subclasses of PrmCharContents)"""
	def __init__(self):
		super().__init__()
		self._cond_child=ModelNone
		self._remark_child=ModelNone

class PrmCharAbsTol(complexbase.GroupBase):
	"""The parameter is specified as ablolute value with a tolerance."""
	def __init__(self):
		super().__init__()
		self._abs_child=ModelNone
		self._tol_child=ModelNone

class PrmCharContents(complexbase.GroupBase):
	"""This is the contents of the parameter."""

class PrmCharMinTypMax(complexbase.GroupBase):
	"""This metaclass represents the characteristics of a parameter as minimal, typical maximum value."""
	def __init__(self):
		super().__init__()
		self._min_child=ModelNone
		self._typ_child=ModelNone
		self._max_child=ModelNone

class PrmCharNumericalContents(complexbase.GroupBase):
	"""This metaclass represents the fact that it is a numerical parameter."""
	def __init__(self):
		super().__init__()
		self._prmUnit_child=ModelNone

class PrmCharNumericalValue(complexbase.GroupBase):
	"""This metaclass represents a numercial parameter characteristics."""

class PrmCharTextualContents(complexbase.GroupBase):
	"""This metaclass represents the fact that it is a textual parameter."""
	def __init__(self):
		super().__init__()
		self._text_child=ModelNone

class Prms(complexbase.GroupBase):
	"""This metaclass represents the ability to specify a parameter table. It can be used e.g. to specify parameter tables in a data sheet."""
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone
		self._prm_children=[]

class ProvidedServiceInstance(complexbase.GroupBase):
	"""Service instances that are provided by the ECU that is connected via the ApplicationEndpoint to a CommunicationConnector."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._EventHandler_children=[]
		self._instanceIdentifier_child=ModelNone
		self._priority_child=ModelNone
		self._sdServerConfig_child=ModelNone
		self._serviceIdentifier_child=ModelNone

class QueuedReceiverComSpec(complexbase.GroupBase):
	"""Communication attributes specific to queued receiving."""
	def __init__(self):
		super().__init__()
		self._queueLength_child=ModelNone

class QueuedSenderComSpec(complexbase.GroupBase):
	"""Communication attributes specific to distribution of  events (PPortPrototype, SenderReceiverInterface and dataElement carries an \"event\")."""

class RModeGroupInAtomicSWCInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetModeGroup_child=ModelNone

class RModeInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPort_child=ModelNone
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class ROperationInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetRequiredOperation_child=ModelNone

class RPortComSpec(complexbase.GroupBase):
	"""Communication attributes of a required PortPrototype. This class will contain attributes that are valid for all kinds of require-ports, independent of client-server or sender-receiver communication patterns."""

class RPortInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_child=ModelNone
		self._targetRPort_child=ModelNone

class RPortPrototype(complexbase.GroupBase):
	"""Component port requiring a certain port interface."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._requiredInterface_child=ModelNone

class RTEEvent(complexbase.GroupBase):
	"""Abstract base class for all RTE-related events"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._disabledMode_children=[]
		self._startOnEvent_child=ModelNone
		self._variationPoint_child=ModelNone

class RTriggerInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetTrigger_child=ModelNone

class RVariableInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetDataElement_child=ModelNone

class RapidPrototypingScenario(complexbase.GroupBase):
	"""This meta class provides the ability to describe a Rapid Prototyping Scenario. Such a Rapid Prototyping Scenario consist out of two main aspects, the description of the byPassPoints and the relation to an rptHook."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._hostSystem_child=ModelNone
		self._rptContainer_children=[]
		self._rptSystem_child=ModelNone

class ReceiverAnnotation(complexbase.GroupBase):
	"""Annotation of a receiver port, specifying properties of data elements that don't affect communication or generation of the RTE. The given attributes are requirements on the required data."""
	def __init__(self):
		super().__init__()
		self._signalAge_child=ModelNone

class ReceiverComSpec(complexbase.GroupBase):
	"""Receiver-specific communication attributes (RPortPrototype typed by SenderReceiverInterface)."""
	def __init__(self):
		super().__init__()
		self._compositeNetworkRepresentation_children=[]
		self._dataElement_child=ModelNone
		self._externalReplacement_child=ModelNone
		self._handleOutOfRange_child=ModelNone
		self._handleOutOfRangeStatus_child=ModelNone
		self._maxDeltaCounterInit_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._networkRepresentation_child=ModelNone
		self._replaceWith_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._transformationComSpecProps_children=[]
		self._usesEndToEndProtection_child=ModelNone

class RecordValueSpecification(complexbase.GroupBase):
	"""Specifies the values for a record."""
	def __init__(self):
		super().__init__()
		self._field_children=[]

class ReferenceBase(complexbase.GroupBase):
	"""This meta-class establishes a basis for relative references. Reference bases are identified by the shortLabel which must be unique in the current package."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._isDefault_child=ModelNone
		self._isGlobal_child=ModelNone
		self._baseIsThisPackage_child=ModelNone
		self._globalInPackage_children=[]
		self._globalElement_children=[]
		self._package_child=ModelNone

class ReferenceValueSpecification(complexbase.GroupBase):
	"""Specifies a reference to a data prototype to be used as an initial value for a pointer in the software."""
	def __init__(self):
		super().__init__()
		self._referenceValue_child=ModelNone

class Referrable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (while adhering to namespace borders)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._shortName_child=ModelNone
		self._shortNameFragment_children=[]

class ReferrableRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._Referrable_child=ModelNone
		self._variationPoint_child=ModelNone

class RelativeTolerance(complexbase.GroupBase):
	"""Maximum allowable deviation"""
	def __init__(self):
		super().__init__()
		self._relative_child=ModelNone

class RemotingTechnology(complexbase.GroupBase):
	"""This element is deprecated and will be removed in future. 

Old description: Defines the used remoting Technology."""
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._version_child=ModelNone

class RequestResponseDelay(complexbase.GroupBase):
	"""Time to wait before answering the query."""
	def __init__(self):
		super().__init__()
		self._maxValue_child=ModelNone
		self._minValue_child=ModelNone

class ResourceConsumption(complexbase.GroupBase):
	"""Description of consumed resources by one implementation of a software."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._executionTime_children=[]
		self._heapUsage_children=[]
		self._memorySection_children=[]
		self._sectionNamePrefix_children=[]
		self._stackUsage_children=[]

class RoleBasedBswModuleEntryAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular BswModuleEntry (usually a configurable callback). 

With this assignment, the role of the callback is mapped to a specific ServiceNeeds element, so that a tool is able to create appropriate configuration values for the module that implements the AUTOSAR Service."""
	def __init__(self):
		super().__init__()
		self._assignedEntry_child=ModelNone
		self._role_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedDataAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular data object in the SwcInternalBehavior of a software component (or in the BswModuleBehavior of a module or cluster) in the context of an AUTOSAR Service.

With this assignment, the role of the data can be mapped to a specific ServiceNeeds element, so that a tool is able to create the correct access."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._usedDataElement_child=ModelNone
		self._usedParameterElement_child=ModelNone
		self._usedPim_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedDataTypeAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular data type of a software component (or in the BswModuleBehavior of a module or cluster) in the context of an AUTOSAR Service.

With this assignment, the role of the data type can be mapped to a specific ServiceNeeds element, so that a tool is able to create the correct access."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._usedImplementationDataType_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedMcDataAssignment(complexbase.GroupBase):
	"""This meta-class allows to define links that specify logical relationships between single McDataInstances. The details on the existence and semantics of such links are not standardized.

Possible Use Case: 
Rapid Prototyping solutions in which additional communication buffers and switches are implemented  in the RTE that allow to switch between the usage of the original and the bypass buffers. The different buffers and the switch can be represented by McDataInstances (in order to be accessed by MC tools) which have relationships to each other."""
	def __init__(self):
		super().__init__()
		self._mcDataInstance_children=[]
		self._role_child=ModelNone

class RoleBasedPortAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular service port (RPortPrototype or PPortPrototype) of an AtomicSwComponentType. With this assignment, the role of the service port can be mapped to a specific ServiceNeeds element, so that a tool is able to create the correct connector."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._role_child=ModelNone
		self._variationPoint_child=ModelNone

class RootSwCompositionPrototype(complexbase.GroupBase):
	"""The RootSwCompositionPrototype represents the top-level-composition of software components within a given System. According to the use case of the System, this may for example be the a more or less complete VFB description, the software of a System Extract or the software of a flat ECU Extract with only atomic SWCs. 

Therefore the RootSwComposition will only occasionally contain all atomic software components that are used in a complete VFB System. The OEM is primarily interested in the required functionality and the interfaces defining the integration of the Software Component into the System. The internal structure of such a component contains often substantial intellectual property of a supplier. Therefore a top-level software composition will often contain empty compositions which represent subsystems. 

The contained SwComponentPrototypes are fully specified by their SwComponentTypes (including PortPrototypes, PortInterfaces, VariableDataPrototypes, SwcInternalBehavior etc.), and their ports are interconnected using SwConnectorPrototypes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._calibrationParameterValueSet_children=[]
		self._flatMap_child=ModelNone
		self._softwareComposition_child=ModelNone
		self._variationPoint_child=ModelNone

class RoughEstimateHeapUsage(complexbase.GroupBase):
	"""Rough estimation of the heap usage."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class RoughEstimateOfExecutionTime(complexbase.GroupBase):
	"""Provides a description of a rough estimate on the ExecutionTime."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._additionalInformation_child=ModelNone
		self._estimatedExecutionTime_child=ModelNone

class RoughEstimateStackUsage(complexbase.GroupBase):
	"""Rough estimation of the stack usage."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class Row(complexbase.GroupBase):
	"""This meta-class represents the ability to express one row in a table."""
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates if by default a line should be displayed below the row.""")
	valign=complexbase.Attribute("valign",SimpleTypes.ValignEnum,'VALIGN',False,"""Indicates how the cells in the rows shall be aligned. Default is inherited from tbody, otherwise it is \"TOP\"""")
	def __init__(self):
		super().__init__()
		self._entry_children=[]
		self._variationPoint_child=ModelNone

class RptContainer(complexbase.GroupBase):
	"""This meta class defines a byPassPoint and the relation to a rptHook.

Additionally it may contain further rptContainers if the byPassPoint is not atomic. For example a byPassPoint refereing to a RunnableEntity may contain rptContainers referring to the data access points of the RunnableEntity. 

The RptContainer structure on M1 shall follow the M1 structure of the Software Component Descriptions. The category attribute denotes which level of the Software Component Description is annotated."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._byPassPoint_children=[]
		self._rptContainer_children=[]
		self._rptHook_children=[]
		self._variationPoint_child=ModelNone

class RptHook(complexbase.GroupBase):
	"""This meta class provide the ability to describe a rapid prototyping hook. This can either be described by an other AUTOSAR system with the category RPT_SYSTEM or as a non AUTOSAR software."""
	def __init__(self):
		super().__init__()
		self._codeLabel_child=ModelNone
		self._mcdIdentifier_child=ModelNone
		self._rptArHook_child=ModelNone
		self._sdg_children=[]
		self._variationPoint_child=ModelNone

class RteEventInEcuInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootComposition_child=ModelNone
		self._contextAtomicComponent_child=ModelNone
		self._targetRteEvent_child=ModelNone

class RtpTp(complexbase.GroupBase):
	"""RTP over UDP or over TCP as transport protocol."""
	def __init__(self):
		super().__init__()
		self._ssrc_child=ModelNone
		self._tcpUdpConfig_child=[]

class RuleArguments(complexbase.GroupBase):
	"""This represents the arguments for a rule-based value specification."""
	def __init__(self):
		super().__init__()
		self._v_child=ModelNone
		self._vf_child=ModelNone
		self._vt_child=ModelNone
		self._vtf_children=[]
		self._variationPoint_child=ModelNone

class RuleBasedAxisCont(complexbase.GroupBase):
	"""This represents the values for the axis of a compound primitive (curve, map).

For standard and fix axes,  SwAxisCont contains the values of the axis directly. 

The axis values of SwAxisCont with the category CURVE_AXIS, COM_AXIS, RES_AXIS are for display only. For editing and processing, only the values in the related GroupAxis are binding."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._unit_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swAxisIndex_child=ModelNone
		self._ruleBasedValues_child=ModelNone

class RuleBasedValueCont(complexbase.GroupBase):
	"""This represents the values of a compound primitive (CURVE, MAP, CUBOID, CUBE_4, CUBE_5, VAL_BLK) or an array."""
	def __init__(self):
		super().__init__()
		self._unit_child=ModelNone
		self._swArraysize_child=ModelNone
		self._ruleBasedValues_child=ModelNone

class RuleBasedValueSpecification(complexbase.GroupBase):
	"""This meta-class is used to support a rule-based initialization approach for data types with an array-nature (ApplicationArrayDataType and ImplementationDataType of category ARRAY) or a compound ApplicationPrimitiveDataType (which also boils down to an array-nature)."""
	def __init__(self):
		super().__init__()
		self._rule_child=ModelNone
		self._arguments_children=[]
		self._maxSizeToFill_child=ModelNone

class RunnableEntity(complexbase.GroupBase):
	"""A RunnableEntity represents the smallest code-fragment that is provided by an AtomicSwComponentType and are executed under control of the RTE. RunnableEntities are for instance set up to respond to data reception or operation invocation on a server."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._argument_children=[]
		self._asynchronousServerCallResultPoint_children=[]
		self._canBeInvokedConcurrently_child=ModelNone
		self._dataReadAccess_children=[]
		self._dataReceivePointByArgument_children=[]
		self._dataReceivePointByValue_children=[]
		self._dataSendPoint_children=[]
		self._dataWriteAccess_children=[]
		self._externalTriggeringPoint_children=[]
		self._internalTriggeringPoint_children=[]
		self._modeAccessPoint_children=[]
		self._modeSwitchPoint_children=[]
		self._parameterAccess_children=[]
		self._readLocalVariable_children=[]
		self._serverCallPoint_children=[]
		self._symbol_child=ModelNone
		self._waitPoint_children=[]
		self._writtenLocalVariable_children=[]
		self._variationPoint_child=ModelNone

class RunnableEntityArgument(complexbase.GroupBase):
	"""This meta-class represents the ability to provide specific information regarding the arguments to a RunnableEntity."""
	def __init__(self):
		super().__init__()
		self._symbol_child=ModelNone

class RunnableEntityGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to define a collection of RunnableEntities. The collection can be nested."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._runnableEntityGroup_children=[]
		self._runnableEntity_children=[]
		self._variationPoint_child=ModelNone

class RunnableEntityInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a RunnableEntity in the context of a CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._targetRunnableEntity_child=ModelNone
		self._variationPoint_child=ModelNone

class RxIdentifierRange(complexbase.GroupBase):
	"""Optional definition of a CanId range to reduce the effort of specifying every possible FrameTriggering within the defined Id range during reception. All frames received within a range are mapped to the same Pdu that is passed to a upper layer module (e.g. Nm, CDD, PduR)."""
	def __init__(self):
		super().__init__()
		self._lowerCanId_child=ModelNone
		self._upperCanId_child=ModelNone

class SOMEIPTransformationDescription(complexbase.GroupBase):
	"""The SOMEIPTransformationDescription is used to specify SOME/IP transformer specific attributes."""
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._byteOrder_child=ModelNone
		self._interfaceVersion_child=ModelNone

class SOMEIPTransformationISignalProps(complexbase.GroupBase):
	"""The class SOMEIPTransformationISignalProps specifies ISignal specific configuration properties for SOME/IP transformer attributes."""
	def __init__(self):
		super().__init__()
		self._SOMEIPTransformationISignalPropsVariant_children=[]
		self._interfaceVersion_child=ModelNone
		self._messageType_child=ModelNone
		self._sessionHandlingSR_child=ModelNone
		self._sizeOfArrayLengthFields_child=ModelNone
		self._sizeOfStructLengthFields_child=ModelNone
		self._sizeOfUnionLengthFields_child=ModelNone

class SOMEIPTransformationISignalPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class SOMEIPTransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class SaveConfigurationEntry(complexbase.GroupBase):
	"""This service is used to notify a slave node to store its configuration."""

class ScaleConstr(complexbase.GroupBase):
	"""This meta-class represents the ability to specify constraints as a list of intervals (called scales)."""
	validity=complexbase.Attribute("validity",SimpleTypes.ScaleConstrValidityEnum,'VALIDITY',False,"""Specifies if the values defined by the scales are considered to be valid. If the attribute is missing then the default value is \"VALID\".""")
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._desc_child=ModelNone
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone

class ScheduleTableEntry(complexbase.GroupBase):
	"""Table entry in a LinScheduleTable. Specifies what will be done in the frame slot."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._delay_child=ModelNone
		self._positionInTable_child=ModelNone

class Sd(complexbase.GroupBase):
	"""This class represents a primitive element in a special data group."""
	gid=complexbase.Attribute("gid",SimpleTypes.NameToken,'GID',False,"""This attributes specifies an identifier. Gid comes from the SGML/XML-Term \"Generic Identifier\" which is the element name in XML. The role of this attribute is the same as the name of an XML - element.""")
	xmlSpace=complexbase.Attribute("xmlSpace",SimpleTypes.String,'space',False,"""This attribute is used to signal an intention that in that element, white space should be preserved by applications. It is defined according to xml:space as declared by W3C.""")
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class SdClientConfig(complexbase.GroupBase):
	"""Client configuration for Service-Discovery."""
	def __init__(self):
		super().__init__()
		self._capabilityRecord_children=[]
		self._clientServiceMajorVersion_child=ModelNone
		self._clientServiceMinorVersion_child=ModelNone
		self._initialFindBehavior_child=ModelNone
		self._requestResponseDelay_child=ModelNone
		self._ttl_child=ModelNone

class SdServerConfig(complexbase.GroupBase):
	"""Server configuration for Service-Discovery."""
	def __init__(self):
		super().__init__()
		self._capabilityRecord_children=[]
		self._initialOfferBehavior_child=ModelNone
		self._offerCyclicDelay_child=ModelNone
		self._requestResponseDelay_child=ModelNone
		self._serverServiceMajorVersion_child=ModelNone
		self._serverServiceMinorVersion_child=ModelNone
		self._ttl_child=ModelNone

class Sdf(complexbase.GroupBase):
	"""This class represents a numerical value in a special data group which may be subject to variability."""
	gid=complexbase.Attribute("gid",SimpleTypes.NameToken,'GID',False,"""This attributes specifies an identifier. Gid comes from the SGML/XML-Term \"Generic Identifier\" which is the element name in XML. The role of this attribute is the same as the name of an XML - element.""")
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class Sdg(complexbase.GroupBase):
	"""Sdg (SpecialDataGroup) is a generic model which can be used to keep arbitrary information which is not explicitly modeled in the meta-model. 

Sdg can have various contents as defined by sdgContentsType. Special Data should only be used moderately since all elements should be defined in the meta-model. 

Thereby SDG should be considered as a temporary solution when no explicit model is available. If an sdgCaption is available, it is possible to establish a reference to the sdg structure."""
	gid=complexbase.Attribute("gid",SimpleTypes.NameToken,'GID',False,"""This attributes specifies an identifier. Gid comes from the SGML/XML-Term \"Generic Identifier\" which is the element name in XML. The role of this attribute is the same as the name of an XML - element.""")
	def __init__(self):
		super().__init__()
		self._sdgCaption_child=ModelNone
		self._sdgCaptionRef_child=ModelNone
		self._variationPoint_child=ModelNone

class SdgCaption(complexbase.GroupBase):
	"""This meta-class represents the caption of a special data group. This allows to have some parts of special data as identifiable."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._desc_child=ModelNone

class SdgContents(complexbase.GroupBase):
	"""This meta-class represents the possible contents of a special data group. It can be an arbitrary mix of references, of primitive special data and nested special data groups."""
	def __init__(self):
		super().__init__()
		self._sdx_child=ModelNone
		self._sdxf_children=[]
		self._sd_child=ModelNone
		self._sdg_children=[]
		self._sdf_child=ModelNone

class SectionNamePrefix(complexbase.GroupBase):
	"""A prefix to be used for generated code artifacts defining a memory section name in the source code of the using module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._implementedIn_child=ModelNone
		self._variationPoint_child=ModelNone

class SecureCommunicationProps(complexbase.GroupBase):
	"""Properties used to configure SecuredIPdus."""
	def __init__(self):
		super().__init__()
		self._authAlgorithm_child=ModelNone
		self._authInfoTxLength_child=ModelNone
		self._authenticationRetries_child=ModelNone
		self._dataId_child=ModelNone
		self._freshnessCounterSyncAttempts_child=ModelNone
		self._freshnessTimestampTimePeriodFactor_child=ModelNone
		self._freshnessValueId_child=ModelNone
		self._freshnessValueLength_child=ModelNone
		self._freshnessValueTxLength_child=ModelNone
		self._secondaryFreshnessValueId_child=ModelNone
		self._useFreshnessTimestamp_child=ModelNone

class SecuredIPdu(complexbase.GroupBase):
	"""IPdu that contains payload of an Authentic IPdu supplemented by additional Authentication Information (Freshness Counter and an Authenticator)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._payload_child=ModelNone
		self._secureCommunicationProps_child=ModelNone

class SegmentPosition(complexbase.GroupBase):
	"""The StaticPart and the DynamicPart can be
separated in multiple segments within the multiplexed PDU. 

The ISignalIPdus are copied bit by bit into the MultiplexedIPdu. If the space of the first segment is 5 bits large than the first 5 bits of the ISignalIPdu are copied into this first segment and so on."""
	def __init__(self):
		super().__init__()
		self._segmentByteOrder_child=ModelNone
		self._segmentLength_child=ModelNone
		self._segmentPosition_child=ModelNone

class SenderAnnotation(complexbase.GroupBase):
	"""Annotation of a sender port, specifying properties of data elements that don't affect communication or generation of the RTE."""

class SenderComSpec(complexbase.GroupBase):
	"""Communication attributes for a sender port (PPortPrototype typed by SenderReceiverInterface)."""
	def __init__(self):
		super().__init__()
		self._compositeNetworkRepresentation_children=[]
		self._dataElement_child=ModelNone
		self._handleOutOfRange_child=ModelNone
		self._networkRepresentation_child=ModelNone
		self._transmissionAcknowledge_child=ModelNone
		self._usesEndToEndProtection_child=ModelNone

class SenderRecArrayElementMapping(complexbase.GroupBase):
	"""The SenderRecArrayElement may be a primitive one or a composite one. If the element is primitive, it will be mapped to the SystemSignal (multiplicity 1). If the VariableDataPrototype that is referenced by SenderReceiverToSignalGroupMapping is typed by an ApplicationDataType the reference to the ApplicationArrayElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference to the ImplementationArrayElement shall be used. 

If the element is composite, there will be no mapping to the SystemSignal (multiplicity 0). In this case the ArrayElementMapping element will aggregate the TypeMapping element. In that way also the composite datatypes can be mapped to SystemSignals. 

Regardless whether composite or primitive array element is mapped the indexed element always needs to be specified."""
	def __init__(self):
		super().__init__()
		self._complexTypeMapping_child=[]
		self._indexedArrayElement_child=ModelNone
		self._systemSignal_child=ModelNone

class SenderRecArrayTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is an Array, the \"ArrayTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._arrayElementMapping_children=[]

class SenderRecCompositeTypeMapping(complexbase.GroupBase):
	"""Two mappings exist for the composite data types:
\"ArrayTypeMapping\" and \"RecordTypeMapping\".
In both, a primitive datatype will be mapped to a system signal. 

But it is also possible to combine the arrays and the records, so that an \"array\" could be an element of a \"record\" and in the same manner a \"record\" could be an element of an \"array\". Nesting these data types is also possible.

If an element of a composite data type is again a composite one, the \"CompositeTypeMapping\" element will be used one more time (aggregation between the ArrayElementMapping and CompositeTypeMapping or aggregation between the RecordElementMapping and CompositeTypeMapping)."""

class SenderRecRecordElementMapping(complexbase.GroupBase):
	"""Mapping of a primitive record element to a SystemSignal. If the VariableDataPrototype that is referenced by SenderReceiverToSignalGroupMapping is typed by an ApplicationDataType the reference applicationRecordElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference implementationRecordElement shall be used. Either the implementationRecordElement or applicationRecordElement reference shall be used. 

If the element is composite, there will be no mapping to the SystemSignal (multiplicity 0). In this case the RecordElementMapping element will aggregate the complexTypeMapping element. In that way also the composite datatypes can be mapped to SystemSignals."""
	def __init__(self):
		super().__init__()
		self._applicationRecordElement_child=ModelNone
		self._complexTypeMapping_child=[]
		self._implementationRecordElement_child=ModelNone
		self._systemSignal_child=ModelNone

class SenderRecRecordTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is a Record, the \"RecordTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._recordElementMapping_children=[]

class SenderReceiverAnnotation(complexbase.GroupBase):
	"""Annotation of the data elements in a port that realizes a sender/receiver interface."""
	def __init__(self):
		super().__init__()
		self._computed_child=ModelNone
		self._dataElement_child=ModelNone
		self._limitKind_child=ModelNone
		self._processingKind_child=ModelNone

class SenderReceiverCompositeElementToSignalMapping(complexbase.GroupBase):
	"""Mapping of an Variable Data Prototype which is aggregated within a composite datatype to a SystemSignal (only one element of the composite data type is mapped)."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._systemSignal_child=ModelNone
		self._typeMapping_child=[]

class SenderReceiverInterface(complexbase.GroupBase):
	"""A sender/receiver interface declares a number of data elements to be sent and received."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._invalidationPolicy_children=[]

class SenderReceiverToSignalGroupMapping(complexbase.GroupBase):
	"""Mapping of a sender receiver communication data element with a composite datatype to a signal group."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._signalGroup_child=ModelNone
		self._typeMapping_child=[]

class SenderReceiverToSignalMapping(complexbase.GroupBase):
	"""Mapping of a sender receiver communication data element with a primitive datatype to a signal."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._systemSignal_child=ModelNone

class SensorActuatorSwComponentType(complexbase.GroupBase):
	"""The SensorActuatorSwComponentType introduces the possibility to link from the software representation of a sensor/actuator to its hardware description provided by the ECU Resource Template."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._sensorActuator_child=ModelNone

class SeparateSignalPath(complexbase.GroupBase):
	"""The SeparateSignalPath describes that two SwcToSwcSignals and/or SwcToSwcOperationArguments must not take the same way (Signal Path) in the topology (e.g. Redundancy). 
This means that the signals are not allowed to share 
even a single physical channel in their path."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._signal_children=[]

class SequenceCounterMapping(complexbase.GroupBase):
	"""The purpose of sequence counters is to map a response to the correct request of a known client.

The SequenceCounter is mapped to the requestGroup and to the responseGroup."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class SerializationTechnology(complexbase.GroupBase):
	"""<font color=\"#0f0f0f\">This element is deprecated and will be removed in future. This information is replaced by the TransformationTechnology element.</font>
<font color=\"#0f0f0f\">
</font><font color=\"#0f0f0f\">Old description: Identifies the used serialization technology. The corresponding serialization plugin has to be provided on each affected ECU by the integrator. </font>"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._protocol_child=ModelNone
		self._version_child=ModelNone

class ServerCallPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns a ServerCallPoint it is entitled to invoke a particular ClientServerOperation of a specific RPortPrototype of the corresponding AtomicSwComponentType"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._timeout_child=ModelNone
		self._variationPoint_child=ModelNone

class ServerComSpec(complexbase.GroupBase):
	"""Communication attributes for a server port (PPortPrototype and ClientServerInterface)."""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._queueLength_child=ModelNone
		self._transformationComSpecProps_children=[]

class ServiceDependency(complexbase.GroupBase):
	"""Collects all dependencies of a software module or component on an  AUTOSAR Service related to a specific item (e.g. an NVRAM Block, a diagnostic event etc.). It defines the quality of service (ServiceNeeds) of this item as well as (optionally) references to additional elements.

This information is required for tools in order to generate the related basic software configuration and ServiceSwComponentTypes."""
	def __init__(self):
		super().__init__()
		self._assignedDataType_children=[]
		self._symbolicNameProps_child=ModelNone

class ServiceNeeds(complexbase.GroupBase):
	"""This expresses the abstract needs that a Software Component or Basic Software Module has on the configuration of an AUTOSAR Service to which it will be connected. \"Abstract needs\" means that the model abstracts from the Configuration Parameters of the underlying Basic Software."""
	_references=defaultdict(list)

class ServiceProxySwComponentType(complexbase.GroupBase):
	"""This class provides the ability to express a software-component which provides access to an internal service for remote ECUs. It acts as a proxy for the service providing access to the service.

An important use case is the request of vehicle mode switches: Such requests can be communicated via sender-receiver interfaces across ECU boundaries, but the mode manager being responsible to perform the mode switches is an AUTOSAR Service which is located in the Basic Software and is not visible in the VFB view. To handle this situation, a ServiceProxySwComponentType will act as proxy for the mode manager. It will have R-Ports to be connected with the mode requestors on VFB level and Service-Ports to be connected with the local mode manager at ECU integration time.

Apart from the semantics, a ServiceProxySwComponentType has these specific properties:
* A prototype of it can be mapped to more than one ECUs in the system description.
* Exactly one additional instance of it will be created in the ECU-Extract per ECU to which the prototype has been mapped.
* For remote communication, it can have only R-Ports with sender-receiver interfaces and 1:n semantics.
* There shall be no connectors between two prototypes of any ServiceProxySwComponentType."""
	_references=defaultdict(list)

class ServiceSwComponentType(complexbase.GroupBase):
	"""ServiceSwComponentType is used for configuring services for a given ECU. Instances of this class are only to be created in ECU Configuration phase for the specific purpose of the service configuration."""
	_references=defaultdict(list)

class ShortNameFragment(complexbase.GroupBase):
	"""This class describes how the Referrable.shortName is composed of several shortNameFragments."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._fragment_child=ModelNone

class SignalIPduCounter(complexbase.GroupBase):
	"""A PduCounter is included in a predefined set of Pdus and used to ensure that a sequence of Pdus is maintained. The counter is incremented when a Pdu is transmitted. The receivers check if the received Pdu is the next one in sequence."""
	def __init__(self):
		super().__init__()
		self._pduCounterSize_child=ModelNone
		self._pduCounterStartPosition_child=ModelNone
		self._pduCounterThreshold_child=ModelNone
		self._variationPoint_child=ModelNone

class SignalIPduReplication(complexbase.GroupBase):
	"""PduReplication is a form of redundancy where the data content of one ISignalIPdu (source) is transmitted inside a set of replica ISignalIPdus. These ISignalIPdus (copies) have different Pdu IDs, identical PduCounters, identical data content and are transmitted with the same frequency."""
	def __init__(self):
		super().__init__()
		self._pduReplicationVoting_child=ModelNone
		self._replicaPdus_children=[]
		self._variationPoint_child=ModelNone

class SignalPathConstraint(complexbase.GroupBase):
	"""Additional guidelines for the System Generator, which specific way a signal between two Software Components should take in the network without defining in which frame and with which timing it is transmitted."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._variationPoint_child=ModelNone

class SimulatedExecutionTime(complexbase.GroupBase):
	"""Specifies the ExecutionTime which has been gathered using simulation means."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._maximumExecutionTime_child=ModelNone
		self._minimumExecutionTime_child=ModelNone
		self._nominalExecutionTime_child=ModelNone

class SingleLanguageLongName(complexbase.GroupBase):
	"""SingleLanguageLongName"""

class SingleLanguageReferrable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (while adhering to namespace borders). They also may have a longName but in one language only. 

Specializations of this class only occur as inline elements in one particular language. Therefore they aggregate

But they are not considered to contribute substantially to the overall structure of an AUTOSAR description. In particular it does not contain other Referrables."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._longName1_child=ModelNone

class SingleLanguageUnitNames(complexbase.GroupBase):
	"""This represents the ability to express a display name."""

class SoAdConfig(complexbase.GroupBase):
	"""SoAd Configuration for one specific Physical Channel."""
	def __init__(self):
		super().__init__()
		self._connection_children=[]
		self._connectionBundle_children=[]
		self._logicAddress_children=[]
		self._socketAddress_children=[]

class SoAdRoutingGroup(complexbase.GroupBase):
	"""Routing of Pdus in the SoAd can be activated or deactivated. The ShortName of this element shall contain the RoutingGroupId."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventGroupControlType_child=ModelNone

class SocketAddress(complexbase.GroupBase):
	"""This meta-class represents the ability represent a socket address towards the rest of the meta-model. The actual semantics of the represented socket address, however, is contributed by aggregation of ApplicationEndpoint."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._applicationEndpoint_child=ModelNone
		self._connector_child=ModelNone
		self._ipAddress_child=ModelNone
		self._multicastConnector_children=[]
		self._portAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketConnection(complexbase.GroupBase):
	"""The SoAd serves as a (De)Multiplexer between different PDU sources and the TCP/IP stack."""
	def __init__(self):
		super().__init__()
		self._autosarConnector_child=ModelNone
		self._clientIpAddrFromConnectionRequest_child=ModelNone
		self._clientPortFromConnectionRequest_child=ModelNone
		self._clientPort_child=ModelNone
		self._doIpSourceAddress_child=ModelNone
		self._doIpTargetAddress_child=ModelNone
		self._ident_child=ModelNone
		self._localPort_child=ModelNone
		self._nPdu_child=ModelNone
		self._pdu_children=[]
		self._pduCollectionMaxBufferSize_child=ModelNone
		self._pduCollectionTimeout_child=ModelNone
		self._remotePort_child=ModelNone
		self._runtimeIpAddressConfiguration_child=ModelNone
		self._runtimePortConfiguration_child=ModelNone
		self._shortLabel_child=ModelNone
		self._socketProtocol_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketConnectionBundle(complexbase.GroupBase):
	"""This elements groups SocketConnections, i.e. specifies socket connections belonging to the bundle and describes properties which are common for all socket connections in the bundle."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bundledConnection_children=[]
		self._pdu_children=[]
		self._serverPort_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketConnectionIpduIdentifier(complexbase.GroupBase):
	"""An Identifier is required in case of one port per ECU communication where multiple Pdus are transmitted over the same connection. If only one IPdu is transmitted over the connetion this attribute can be ignored."""
	def __init__(self):
		super().__init__()
		self._headerId_child=ModelNone
		self._pduCollectionPduTimeout_child=ModelNone
		self._pduCollectionTrigger_child=ModelNone
		self._pdu_child=ModelNone
		self._pduTriggering_child=ModelNone
		self._routingGroup_children=[]

class SoftwareContext(complexbase.GroupBase):
	"""Specifies the context of the software for this resource consumption."""
	def __init__(self):
		super().__init__()
		self._input_child=ModelNone
		self._state_child=ModelNone

class SporadicEventTriggering(complexbase.GroupBase):
	"""The SporadicEventTriggering describes the behavior of an event which occurs occasionally or singly."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._minimumInterArrivalTime_child=ModelNone
		self._maximumInterArrivalTime_child=ModelNone
		self._jitter_child=ModelNone
		self._period_child=ModelNone

class StackUsage(complexbase.GroupBase):
	"""Describes the stack memory usage of a software."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._executableEntity_child=ModelNone
		self._hardwareConfiguration_child=ModelNone
		self._hwElement_child=ModelNone
		self._softwareContext_child=ModelNone
		self._variationPoint_child=ModelNone

class StaticPart(complexbase.GroupBase):
	"""Some parts/signals of the I-PDU may be the same regardless of the selector field. Such a part is called static part. The static part is optional."""
	def __init__(self):
		super().__init__()
		self._iPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class Std(complexbase.GroupBase):
	"""This represents a reference to external standards."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._subtitle_child=ModelNone
		self._state_child=ModelNone
		self._date_child=ModelNone
		self._url_child=ModelNone
		self._position_child=ModelNone

class StructuredReq(complexbase.GroupBase):
	"""This represents a structured requirement. This is intended for a case where specific requirements for features are collected.

Note that this can be rendered as a labeled list."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._date_child=ModelNone
		self._issuedBy_child=ModelNone
		self._type_child=ModelNone
		self._importance_child=ModelNone
		self._description_child=ModelNone
		self._rationale_child=ModelNone
		self._dependencies_child=ModelNone
		self._useCase_child=ModelNone
		self._conflicts_child=ModelNone
		self._supportingMaterial_child=ModelNone
		self._remark_child=ModelNone
		self._testedItem_children=[]
		self._variationPoint_child=ModelNone

class SubElementMapping(complexbase.GroupBase):
	"""This meta-class allows for the definition of mappings of elements of a composite data type."""
	def __init__(self):
		super().__init__()
		self._firstElement_children=[]
		self._secondElement_children=[]
		self._textTableMapping_children=[]

class SubElementRef(complexbase.GroupBase):
	"""This meta-class provides the ability to reference elements of composite data type."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class SupervisedEntityNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Watchdog Manager for one specific Supervised Entity."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._activateAtStart_child=ModelNone
		self._enableDeactivation_child=ModelNone
		self._expectedAliveCycle_child=ModelNone
		self._maxAliveCycle_child=ModelNone
		self._minAliveCycle_child=ModelNone
		self._toleratedFailedCycles_child=ModelNone

class SwAddrMethod(complexbase.GroupBase):
	"""Used to assign a common addressing method, e.g. common memory section, to data or code objects. These objects could actually live in different modules or components."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._memoryAllocationKeywordPolicy_child=ModelNone
		self._option_children=[]
		self._sectionInitializationPolicy_child=ModelNone
		self._sectionType_child=ModelNone

class SwAxisCont(complexbase.GroupBase):
	"""This represents the values for the axis of a compound primitive (curve, map).

For standard and fix axes,  SwAxisCont contains the values of the axis directly. 

The axis values of SwAxisCont with the category CURVE_AXIS, COM_AXIS, RES_AXIS are for display only. For editing and processing, only the values in the related GroupAxis are binding."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._unit_child=ModelNone
		self._unitDisplayName_child=ModelNone
		self._swAxisIndex_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swValuesPhys_child=ModelNone

class SwAxisGeneric(complexbase.GroupBase):
	"""This meta-class defines a generic axis. In a generic axis the axispoints points are calculated in the ECU. 

The ECU is equipped with a fixed calculation algorithm. Parameters for the algorithm can be stored in the data component of the ECU. Therefore these parameters are specified in the data declaration, not in the calibration data."""
	def __init__(self):
		super().__init__()
		self._swAxisType_child=ModelNone
		self._swNumberOfAxisPoints_child=ModelNone
		self._swGenericAxisParam_children=[]

class SwAxisGrouped(complexbase.GroupBase):
	"""An SwAxisGrouped is an axis which is shared between multiple calibration parameters."""
	def __init__(self):
		super().__init__()
		self._sharedAxisType_child=ModelNone
		self._swAxisIndex_child=ModelNone

class SwAxisIndividual(complexbase.GroupBase):
	"""This meta-class describes an axis integrated into a parameter (field etc.). The integration makes this individual to each parameter. The so-called grouped axis represents the counterpart to this. It is conceived as an independent parameter (see class SwAxisGrouped)."""
	def __init__(self):
		super().__init__()
		self._inputVariableType_child=ModelNone
		self._swVariableRef_children=[]
		self._compuMethod_child=ModelNone
		self._unit_child=ModelNone
		self._swMaxAxisPoints_child=ModelNone
		self._swMinAxisPoints_child=ModelNone
		self._dataConstr_child=ModelNone
		self._swAxisGeneric_child=ModelNone

class SwAxisType(complexbase.GroupBase):
	"""This meta-class represents a specific axis calculation strategy. No formal specification is given, due to the fact that it is possible to use arbitrary algorithms for calculating axis-points. 

Instead, the algorithm is described verbally but the parameters are specified formally with respect to their names and constraints. As a result, SwAxisType mainly reserves appropriate keywords."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swGenericAxisDesc_child=ModelNone
		self._swGenericAxisParamType_children=[]

class SwBaseType(complexbase.GroupBase):
	"""This meta-class represents a base type used within ECU software."""
	_references=defaultdict(list)

class SwBitRepresentation(complexbase.GroupBase):
	"""Description of the structure of a bit variable: Comprises of the bitPosition in a memory object (e.g. swHostVariable, which stands parallel to swBitRepresentation) and the numberOfBits . In this way, interrelated memory areas can be described. Non-related memory areas are not supported."""
	def __init__(self):
		super().__init__()
		self._bitPosition_child=ModelNone
		self._numberOfBits_child=ModelNone

class SwCalprmAxis(complexbase.GroupBase):
	"""This element specifies an individual input parameter axis (abscissa)."""
	def __init__(self):
		super().__init__()
		self._swAxisIndex_child=ModelNone
		self._category_child=ModelNone
		self._swCalibrationAccess_child=ModelNone
		self._displayFormat_child=ModelNone
		self._baseType_child=ModelNone

class SwCalprmAxisSet(complexbase.GroupBase):
	"""This element specifies the input parameter axes (abscissas) of parameters (and variables, if these are used adaptively)."""
	def __init__(self):
		super().__init__()
		self._swCalprmAxis_children=[]

class SwCalprmAxisTypeProps(complexbase.GroupBase):
	"""Base class for the type of the calibration axis. This provides the particular model of the specialization. If the specialization would be the directly from SwCalPrmAxis, the sequence of common properties and the specializes ones would be different."""

class SwCalprmRefProxy(complexbase.GroupBase):
	"""Wrapper class for different kinds of references to a calibration parameter."""
	def __init__(self):
		super().__init__()
		self._arParameter_child=ModelNone
		self._mcDataInstance_child=ModelNone

class SwComponentDocumentation(complexbase.GroupBase):
	"""This class specifies the ability to write dedicated documentation to a component type according to ASAM FSX."""
	def __init__(self):
		super().__init__()
		self._swFeatureDef_child=ModelNone
		self._swFeatureDesc_child=ModelNone
		self._swTestDesc_child=ModelNone
		self._swCalibrationNotes_child=ModelNone
		self._swMaintenanceNotes_child=ModelNone
		self._swDiagnosticsNotes_child=ModelNone
		self._swCarbDoc_child=ModelNone
		self._chapter_children=[]
		self._variationPoint_child=ModelNone

class SwComponentPrototype(complexbase.GroupBase):
	"""Role of a software component within a composition."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._type_child=ModelNone
		self._variationPoint_child=ModelNone

class SwComponentType(complexbase.GroupBase):
	"""Base class for AUTOSAR software components."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swComponentDocumentation_children=[]
		self._consistencyNeeds_children=[]
		self._port_children=[]
		self._portGroup_children=[]
		self._unitGroup_children=[]

class SwConnector(complexbase.GroupBase):
	"""The base class for connectors between ports. Connectors have to be identifiable to allow references from the system constraint template."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._mapping_child=ModelNone
		self._variationPoint_child=ModelNone

class SwDataDefProps(complexbase.GroupBase):
	"""This class is a collection of properties relevant for data objects under various aspects. One could consider this class as a \"pattern of inheritance by aggregation\". The properties can be applied to all objects of all classes in which SwDataDefProps is aggregated.

Note that not all of the attributes or associated elements are useful all of the time. Hence, the process definition (e.g. expressed with an OCL or a Document Control Instance MSR-DCI) has the task of implementing limitations.

SwDataDefProps covers various aspects:

* Structure of the data element for calibration use cases: is it a single value, a curve, or a map, but also the recordLayouts which specify how such elements are mapped/converted to the DataTypes in the programming language (or in AUTOSAR). This is mainly expressed by properties like swRecordLayout and swCalprmAxisSet 

* Implementation aspects, mainly expressed by swImplPolicy, swVariableAccessImplPolicy, swAddrMethod, swPointerTagetProps, baseType, implementationDataType and additionalNativeTypeQualifier

* Access policy for the MCD system, mainly expressed by swCalibrationAccess 

* Semantics of the data element, mainly expressed by compuMethod and/or unit, dataConstr, invalidValue

* Code generation policy provided by swRecordLayout"""
	def __init__(self):
		super().__init__()
		self._SwDataDefPropsVariant_children=[]
		self._stepSize_child=ModelNone
		self._annotation_children=[]
		self._swAddrMethod_child=ModelNone
		self._swAlignment_child=ModelNone
		self._baseType_child=ModelNone
		self._swBitRepresentation_child=ModelNone
		self._swCalibrationAccess_child=ModelNone
		self._swValueBlockSize_child=ModelNone
		self._swCalprmAxisSet_child=ModelNone
		self._swTextProps_child=ModelNone
		self._swComparisonVariable_children=[]
		self._compuMethod_child=ModelNone
		self._dataConstr_child=ModelNone
		self._swDataDependency_child=ModelNone
		self._displayFormat_child=ModelNone
		self._implementationDataType_child=ModelNone
		self._swHostVariable_child=ModelNone
		self._swImplPolicy_child=ModelNone
		self._additionalNativeTypeQualifier_child=ModelNone
		self._swIntendedResolution_child=ModelNone
		self._swInterpolationMethod_child=ModelNone
		self._invalidValue_child=[]
		self._mcFunction_child=ModelNone
		self._swIsVirtual_child=ModelNone
		self._swPointerTargetProps_child=ModelNone
		self._swRecordLayout_child=ModelNone
		self._swRefreshTiming_child=ModelNone
		self._unit_child=ModelNone
		self._valueAxisDataType_child=ModelNone

class SwDataDefPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class SwDataDefPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class SwDataDependency(complexbase.GroupBase):
	"""This element describes the interdependencies of data objects, e.g. variables and parameters. 

Use cases:
* Calculate the value of a calibration parameter (by the MCD system) from the value(s) of other calibration parameters.
* Virtual data - that means the data object is not directly in the ecu and this property describes how the \"virtual variable\" can be computed from the real ones (by the MCD system)."""
	def __init__(self):
		super().__init__()
		self._swDataDependencyFormula_child=ModelNone
		self._swDataDependencyArgs_child=ModelNone

class SwDataDependencyArgs(complexbase.GroupBase):
	"""This element specifies the elements used in a SwDataDependency."""

class SwGenericAxisParam(complexbase.GroupBase):
	"""This meta-class describes a specific parameter of a generic axis. The name of the parameter is defined through a reference to a parameter type defined on a corresponding axis type.

The value of the parameter is given here in case that it is not changeable during calibration. Example is shift / offset in a fixed axis."""
	def __init__(self):
		super().__init__()
		self._swGenericAxisParamType_child=ModelNone
		self._vf_children=[]

class SwGenericAxisParamType(complexbase.GroupBase):
	"""This meta-class describes a generic axis parameter type, namely:

* Plausibility checks can be specified via dataConstr.

* Textual description (desc), as a formal description is not of any use, due to the large variety of possibilities.

* If this parameter contains structures, these can be simulated through the recursive use of SwGenericAxisParamTypes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataConstr_child=ModelNone

class SwPointerTargetProps(complexbase.GroupBase):
	"""This element defines, that the data object (which is specified by the aggregating element) contains a reference to another data object or to a function in the CPU code. This corresponds to a pointer in the C-language.

The attributes of this element describe the category and the detailed properties of the target which is either a data description or a function signature."""
	def __init__(self):
		super().__init__()
		self._targetCategory_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._functionPointerSignature_child=ModelNone

class SwRecordLayout(complexbase.GroupBase):
	"""Defines how the data objects (variables, calibration parameters etc.) are to be stored in the ECU memory. As an example, this definition specifies the sequence of axis points in the ECU memory. Iterations through axis values are stored within the sub-elements swRecordLayoutGroup."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swRecordLayoutGroup_child=ModelNone

class SwRecordLayoutGroup(complexbase.GroupBase):
	"""Specifies how a record layout is set up. Using SwRecordLayoutGroup it recursively models iterations through axis values. The subelement swRecordLayoutGroupContentType may reference other SwRecordLayouts, SwRecordLayoutVs and SwRecordLayoutGroups for the modeled record layout."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._desc_child=ModelNone
		self._swRecordLayoutGroupAxis_child=ModelNone
		self._swRecordLayoutGroupIndex_child=ModelNone
		self._swGenericAxisParamType_child=ModelNone
		self._swRecordLayoutGroupFrom_child=ModelNone
		self._swRecordLayoutGroupTo_child=ModelNone
		self._swRecordLayoutGroupStep_child=ModelNone
		self._swRecordLayoutComponent_child=ModelNone

class SwRecordLayoutGroupContent(complexbase.GroupBase):
	"""This is the contents of a RecordLayout which is inserted for every iteration. Note that since this is atpMixed, multiple properties can be inserted for each iteration."""
	def __init__(self):
		super().__init__()
		self._swRecordLayout_child=ModelNone
		self._swRecordLayoutV_child=ModelNone
		self._swRecordLayoutGroup_child=ModelNone

class SwRecordLayoutV(complexbase.GroupBase):
	"""This element specifies which values are stored for the current SwRecordLayoutGroup. If no baseType is present, the SwBaseType referenced initially in the parent SwRecordLayoutGroup is valid. The specification of swRecordLayoutVAxis gives the axis of the values which shall be stored in accordance with the current record layout SwRecordLayoutGroup. In swRecordLayoutVProp one can specify the information which shall be stored."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._desc_child=ModelNone
		self._baseType_child=ModelNone
		self._swRecordLayoutVAxis_child=ModelNone
		self._swRecordLayoutVProp_child=ModelNone
		self._swRecordLayoutVIndex_child=ModelNone
		self._swGenericAxisParamType_child=ModelNone
		self._swRecordLayoutVFixValue_child=ModelNone

class SwServiceArg(complexbase.GroupBase):
	"""Specifies the properties of a data object exchanged during the call of an SwService, e.g. an argument or a return value.

The SwServiceArg can also be used in the argument list of a C-macro. For this purpose the category shall be set to \"MACRO\". A reference to implementationDataType can optional be added if the actual argument has an implementationDataType."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._direction_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class SwSystemconst(complexbase.GroupBase):
	"""This element defines a system constant which serves an input to 
select a particular variation point. In particular a system constant serves as an operand of the binding function (swSyscond) in a Variation point.

Note that the binding process can only happen  if a value was assigned to to the referenced system constants."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swDataDefProps_child=ModelNone

class SwSystemconstDependentFormula(complexbase.GroupBase):
	"""This class represents an expression depending on system constants."""
	def __init__(self):
		super().__init__()
		self._syscString_child=ModelNone
		self._sysc_child=ModelNone

class SwSystemconstValue(complexbase.GroupBase):
	"""This meta-class assigns a particular value to a system constant."""
	def __init__(self):
		super().__init__()
		self._swSystemconst_child=ModelNone
		self._value_child=ModelNone
		self._annotation_children=[]

class SwSystemconstantValueSet(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a set of system constant values."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swSystemconstantValue_children=[]

class SwTextProps(complexbase.GroupBase):
	"""This meta-class expresses particular properties applicable to strings in variables or calibration parameters."""
	def __init__(self):
		super().__init__()
		self._arraySizeSemantics_child=ModelNone
		self._swMaxTextSize_child=ModelNone
		self._baseType_child=ModelNone
		self._swFillCharacter_child=ModelNone

class SwValueCont(complexbase.GroupBase):
	"""This metaclass represents the content of one particular SwInstance."""
	def __init__(self):
		super().__init__()
		self._unit_child=ModelNone
		self._unitDisplayName_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swValuesPhys_child=ModelNone

class SwValues(complexbase.GroupBase):
	"""This meta-class represents a list of values. These values can either be the input values of a curve (abscissa values) or the associated values (ordinate values). 

In case of multidimensional structures, the values are ordered such that the lowest index runs the fastest. In particular for maps and cuboids etc. the resulting long value list can be subsectioned using ValueGroup. But the processing needs to be done as if vg is not there.

Note that numerical values and textual values should not be mixed."""
	def __init__(self):
		super().__init__()
		self._vtf_children=[]
		self._vf_child=ModelNone
		self._vt_child=ModelNone
		self._v_child=ModelNone
		self._vg_child=ModelNone

class SwVariableRefProxy(complexbase.GroupBase):
	"""Proxy class for several kinds of references to a variable."""
	def __init__(self):
		super().__init__()
		self._autosarVariable_child=ModelNone
		self._mcDataInstanceVar_child=ModelNone

class SwcBswMapping(complexbase.GroupBase):
	"""Maps an SwcInternalBehavior to an BswInternalBehavior. This is required to coordinate the API generation and the scheduling for AUTOSAR Service Components, ECU Abstraction Components and Complex Driver Components by the RTE and the BSW scheduling mechanisms."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswBehavior_child=ModelNone
		self._runnableMapping_children=[]
		self._swcBehavior_child=ModelNone
		self._synchronizedModeGroup_children=[]
		self._synchronizedTrigger_children=[]

class SwcBswRunnableMapping(complexbase.GroupBase):
	"""Maps a BswModuleEntity to a RunnableEntity if it is implemented as part of a BSW module (in the case of an AUTOSAR Service, a Complex Driver or an ECU Abstraction). The mapping can be used by a tool to find relevant information on the behavior, e.g. whether the bswEntity shall be running in interrupt context."""
	def __init__(self):
		super().__init__()
		self._bswEntity_child=ModelNone
		self._swcRunnable_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcBswSynchronizedModeGroupPrototype(complexbase.GroupBase):
	"""Synchronizes a mode group provided by a component via a port with a mode group provided by a BSW module or cluster."""
	def __init__(self):
		super().__init__()
		self._bswModeGroup_child=ModelNone
		self._swcModeGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcBswSynchronizedTrigger(complexbase.GroupBase):
	"""Synchronizes a Trigger provided by a component via a port with a Trigger provided by a BSW module or cluster."""
	def __init__(self):
		super().__init__()
		self._bswTrigger_child=ModelNone
		self._swcTrigger_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcImplementation(complexbase.GroupBase):
	"""This meta-class represents a specialization of the general Implementation meta-class with respect to the usage in application software."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone
		self._perInstanceMemorySize_children=[]
		self._requiredRTEVendor_child=ModelNone

class SwcInternalBehavior(complexbase.GroupBase):
	"""The SwcInternalBehavior of an AtomicSwComponentType describes the relevant aspects of the software-component with respect to the RTE, i.e. the RunnableEntities and the RTEEvents they respond to."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._arTypedPerInstanceMemory_children=[]
		self._event_children=[]
		self._explicitInterRunnableVariable_children=[]
		self._handleTerminationAndRestart_child=ModelNone
		self._implicitInterRunnableVariable_children=[]
		self._includedDataTypeSet_children=[]
		self._includedModeDeclarationGroupSet_children=[]
		self._instantiationDataDefProps_children=[]
		self._perInstanceMemory_children=[]
		self._perInstanceParameter_children=[]
		self._portAPIOption_children=[]
		self._runnable_children=[]
		self._serviceDependency_children=[]
		self._sharedParameter_children=[]
		self._supportsMultipleInstantiation_child=ModelNone
		self._variationPointProxy_children=[]
		self._variationPoint_child=ModelNone

class SwcModeManagerErrorEvent(complexbase.GroupBase):
	"""This represents the ability to react on errors occurring during mode handling."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class SwcModeSwitchEvent(complexbase.GroupBase):
	"""This event is raised upon a received mode change."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._activation_child=ModelNone
		self._mode_children=[]

class SwcServiceDependency(complexbase.GroupBase):
	"""Specialization of ServiceDependency in the context of an SwcInternalBehavior. It allows to associate ports, port groups and (in special cases) data defined for an atomic software component  to a given ServiceNeeds element."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._assignedData_children=[]
		self._assignedPort_children=[]
		self._representedPortGroup_child=ModelNone
		self._serviceNeeds_child=[]
		self._variationPoint_child=ModelNone

class SwcServiceDependencyInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._base_child=ModelNone
		self._rootContext_child=ModelNone
		self._contextSwComponentPrototype_children=[]
		self._targetSwcServiceDependency_child=ModelNone

class SwcTiming(complexbase.GroupBase):
	"""The SwcTiming is used to describe the timing of an atomic software component.

TimingDescriptions aggregated by SwcTiming are restricted to event chains referring to events which are derived from the classes TDEventVfb and TDEventSwcInternalBehavior."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone
		self._component_child=ModelNone

class SwcToEcuMapping(complexbase.GroupBase):
	"""Map software components to a specific ECU Instance and optionally to a processing unit and to an EcuPartition. For each combination of ECUInstance and the optional ProcessingUnit and the optional EcuPartition and the optional SensorActuator only one SwcToEcuMapping shall be used."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._component_children=[]
		self._controlledHwElement_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._partition_child=ModelNone
		self._processingUnit_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcToEcuMappingConstraint(complexbase.GroupBase):
	"""The System Constraint Description has to describe dedicated and exclusive mapping of SW-Cs to one or more ECUs. Dedicated mapping means that the SW-C can only be mapped to the ECUs it is dedicated to. Exclusive Mapping means that the SW-C cannot be mapped to the ECUs it is excluded from."""
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone
		self._ecuInstance_children=[]
		self._swcToEcuMappingConstraintType_child=ModelNone

class SwcToImplMapping(complexbase.GroupBase):
	"""Map instances of an AtomicSwComponentType to a specific Implementation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._componentImplementation_child=ModelNone
		self._component_children=[]
		self._variationPoint_child=ModelNone

class SwcToSwcOperationArguments(complexbase.GroupBase):
	"""The SwcToSwcOperationArguments describes the information (client server operation arguments, plus the operation identification, if required) that are exchanged between two SW Components from exactly one client to one server, or from one server back to one client. The direction attribute defines which direction is described. If direction == IN, all arguments sent from the client to the server are described by the SwcToSwcOperationArguments, in direction == OUT, it's the arguments sent back from server to client."""
	def __init__(self):
		super().__init__()
		self._direction_child=ModelNone
		self._operation_children=[]

class SwcToSwcSignal(complexbase.GroupBase):
	"""The SwcToSwcSignal describes the information (data element) that is exchanged between two SW Components. On the SWC Level it is possible that a SW Component sends one data element from one P-Port to two different SW Components (1:n Communication). The SwcToSwcSignal describes exactly the information which is exchanged between one P-Port of a SW Component and one R-Port of another SW Component."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]

class SymbolProps(complexbase.GroupBase):
	"""This meta-class represents the ability to attach with the symbol attribute a symbolic name that is conform to C language requirements to another meta-class, e.g. AtomicSwComponentType, that is a potential subject to a name clash on the level of RTE source code."""
	_references=defaultdict(list)

class SymbolicNameProps(complexbase.GroupBase):
	"""This meta-class can be taken to contribute to the creation of symbolic name values."""
	_references=defaultdict(list)

class SyncTimeBaseMgrUserNeeds(complexbase.GroupBase):
	"""Specifies the needs on the configuration of the Synchronized Time-base Manager for one time-base. This class currently contains no attributes. An instance of this class is used to find out which ports of a software-component belong to this time-base in order to group the request and response ports of the same time-base. The actual time-base value is stored in the PortDefinedArgumentValue of the respective port specification."""
	_references=defaultdict(list)

class SynchronizationTimingConstraint(complexbase.GroupBase):
	"""This constraint is used to restrict the timing behavior of different, but correlated events or event chains, with regard to synchronization.

Thereby, in case of imposing a synchronization timing constraint on events or event chains the following two scenarios are supported:

1) [synchronizationConstraintType=responseSynchronization] Events: An arbitrary number of correlated events which play the role of responses shall occur synchronously with respect to a predefined tolerance. Event Chains: An arbitrary number of correlated event chains with a common stimulus, but different responses, where the responses shall occur synchronously with respect to a predefined tolerance.

2) [synchronizationConstraintType=stimulusSynchronization] Events: An arbitrary number of correlated events which play the role of stimuli shall occur synchronously with respect to a predefined tolerance. Event Chains: An arbitrary number of correlated event chains with a common response, but different stimuli, where the stimuli shall occur synchronously with respect to a predefined tolerance.

In case of imposing a synchronization timing constraint on events the following two scenarios are supported:

1) [eventOccurrenceKind=singleOccurrence] Any of the events shall occur only once in the given time interval.

2) [eventOccurrenceKind=multipleOccurrences] Any of the events may occur more than once in the given time interval. In other words multiple occurrences of an event within the given time interval are permitted."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._eventOccurrenceKind_child=ModelNone
		self._scopeEvent_children=[]
		self._scope_children=[]
		self._synchronizationConstraintType_child=ModelNone
		self._tolerance_child=ModelNone

class SynchronousServerCallPoint(complexbase.GroupBase):
	"""This means that the RunnableEntity is supposed to perform a  blocking wait for a response from the server."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._calledFromWithinExclusiveArea_child=ModelNone

class System(complexbase.GroupBase):
	"""The top level element of the System Description.
The System description defines five major elements: Topology, Software, Communication, Mapping and Mapping Constraints.

The System element directly aggregates the elements describing the Software, Mapping and Mapping Constraints; it contains a reference to an ASAM FIBEX description specifying Communication and Topology."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._systemDocumentation_children=[]
		self._clientIdDefinitionSet_children=[]
		self._containerIPduHeaderByteOrder_child=ModelNone
		self._ecuExtractVersion_child=ModelNone
		self._fibexElement_children=[]
		self._mapping_children=[]
		self._pncVectorLength_child=ModelNone
		self._pncVectorOffset_child=ModelNone
		self._rootSoftwareComposition_children=[]
		self._systemVersion_child=ModelNone

class SystemMapping(complexbase.GroupBase):
	"""The system mapping aggregates all mapping aspects (mapping of SW components to ECUs, mapping of data elements to signals, and mapping constraints)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataMapping_children=[]
		self._ecuResourceMapping_children=[]
		self._mappingConstraint_children=[]
		self._pncMapping_children=[]
		self._resourceEstimation_children=[]
		self._signalPathConstraint_children=[]
		self._swImplMapping_children=[]
		self._swMapping_children=[]
		self._variationPoint_child=ModelNone

class SystemSignal(complexbase.GroupBase):
	"""The system signal represents the communication system's view of data exchanged between SW components which reside on different ECUs. The system signals allow to represent this communication in a flattened structure, with exactly one system signal defined for each data element prototype sent and received by connected SW component instances."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dynamicLength_child=ModelNone
		self._physicalProps_child=ModelNone

class SystemSignalGroup(complexbase.GroupBase):
	"""A signal group refers to a set of signals that must always be kept together. A signal group is used to guarantee the atomic transfer of AUTOSAR composite data types.  

The SystemSignalGroup defines a signal grouping on VFB level. On cluster level the Signal grouping is described by the ISignalGroup element."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._systemSignal_children=[]
		self._transformingSystemSignal_child=ModelNone

class SystemTiming(complexbase.GroupBase):
	"""A model element used to refine timing descriptions and constraints (from a VfbTiming) at System level, utilizing information about topology, software deployment, and signal mapping described in the System Template.

TimingDescriptions aggregated by SystemTiming are restricted to events which are derived from the class TDEventVfb, TDEventSwcInternalBehavior and TDEventCom."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._system_child=ModelNone

class TDEventBsw(complexbase.GroupBase):
	"""This is used to describe timing events related to BSW modules."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswModuleDescription_child=ModelNone

class TDEventBswInternalBehavior(complexbase.GroupBase):
	"""This is used to describe timing events related to the BswInternalBehavior of a BSW module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswModuleEntity_child=ModelNone
		self._tdEventBswInternalBehaviorType_child=ModelNone

class TDEventBswModeDeclaration(complexbase.GroupBase):
	"""This is used to describe timing events related to the mode communication on BSW level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._entryModeDeclaration_child=ModelNone
		self._exitModeDeclaration_child=ModelNone
		self._modeDeclaration_child=ModelNone
		self._tdEventBswModeDeclarationType_child=ModelNone

class TDEventBswModule(complexbase.GroupBase):
	"""This is used to describe timing events related to the interaction between BSW modules."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bswModuleEntry_child=ModelNone
		self._tdEventBswModuleType_child=ModelNone

class TDEventCom(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events related to communication including the physical layer."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ecuInstance_child=ModelNone

class TDEventComplex(complexbase.GroupBase):
	"""This is used to describe complex timing events.

The context of a complex timing event either is described informally, e.g. using the documentation block, or is described formally by the associated TDEventOccurrenceExpression."""
	_references=defaultdict(list)

class TDEventCycleStart(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events related to a point in time where a communication cycle starts.

Via the attribute \"cycleRepetition\", a filtered view to the cycle start can be defined."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._cycleRepetition_child=ModelNone

class TDEventFrClusterCycleStart(complexbase.GroupBase):
	"""This is used to describe the timing event related to a point in time where a communication cycle starts on a FlexRay cluster."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._frCluster_child=ModelNone

class TDEventFrame(complexbase.GroupBase):
	"""This is used to describe timing events related to the exchange of frames between the communication controller and the bus specific (FlexRay / CAN / LIN) Interface BSW module."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._frame_child=ModelNone
		self._physicalChannel_child=ModelNone
		self._tdEventType_child=ModelNone

class TDEventIPdu(complexbase.GroupBase):
	"""This is used to describe timing events related to the exchange of I-PDUs between the bus specific (FlexRay / CAN / LIN) Interface BSW module and COM."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iPdu_child=ModelNone
		self._physicalChannel_child=ModelNone
		self._tdEventType_child=ModelNone

class TDEventISignal(complexbase.GroupBase):
	"""This is used to describe timing events related to the exchange of I-Signals between COM and RTE."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._iSignal_child=ModelNone
		self._physicalChannel_child=ModelNone
		self._tdEventType_child=ModelNone

class TDEventModeDeclaration(complexbase.GroupBase):
	"""This is used to describe timing events related to mode switch communication at VFB level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._entryModeDeclaration_child=ModelNone
		self._exitModeDeclaration_child=ModelNone
		self._modeDeclaration_child=ModelNone
		self._tdEventModeDeclarationType_child=ModelNone

class TDEventOccurrenceExpression(complexbase.GroupBase):
	"""This is used to specify a filter on the occurrences of TimingDescriptionEvents by means of a TDEventOccurrenceExpressionFormula.
Filter criteria can be variable and argument values, i.e. the timing event only occurs for specific values, as well as the temporal characteristics of the occurrences of arbitrary timing events."""
	def __init__(self):
		super().__init__()
		self._argument_children=[]
		self._formula_child=ModelNone
		self._variable_children=[]

class TDEventOccurrenceExpressionFormula(complexbase.GroupBase):
	"""This is an extension of the FormulaExpression for the AUTOSAR Timing Extensions.

A TDEventOccurrenceExpressionFormula provides the means to express the temporal characteristics of timing event occurrences in correlation with specific variable and argument values.

The formal definition of the extended functions (ExtUnaryFunctions) is described in detail in the AUTOSAR Timing Extensions."""
	def __init__(self):
		super().__init__()
		self._argument_child=ModelNone
		self._event_child=ModelNone
		self._variable_child=ModelNone

class TDEventOperation(complexbase.GroupBase):
	"""This is used to describe timing events related to client-server communication at VFB level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._tdEventOperationType_child=ModelNone

class TDEventSwc(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events at Software Component (SW-C) level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone

class TDEventSwcInternalBehavior(complexbase.GroupBase):
	"""This is used to describe timing events related to the SwcInternalBehavior of an AtomicSwComponentType."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._runnable_child=ModelNone
		self._tdEventSwcInternalBehaviorType_child=ModelNone
		self._variableAccess_child=ModelNone

class TDEventSwcInternalBehaviorReference(complexbase.GroupBase):
	"""This is used to reference timing description events related to the Software Component (SW-C) view which are specified in other timing views."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._referencedTDEventSwc_child=ModelNone

class TDEventTTCanCycleStart(complexbase.GroupBase):
	"""This is used to describe the timing event related to a point in time where a communication cycle starts on a TTCAN cluster."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._ttCanCluster_child=ModelNone

class TDEventTrigger(complexbase.GroupBase):
	"""This is used to describe timing events related to triggers at VFB level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._tdEventTriggerType_child=ModelNone
		self._trigger_child=ModelNone

class TDEventVariableDataPrototype(complexbase.GroupBase):
	"""This is used to describe timing events related to sender-receiver communication at VFB level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._tdEventVariableDataPrototypeType_child=ModelNone

class TDEventVfb(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events at Virtual Function Bus (VFB) level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone

class TDEventVfbPort(complexbase.GroupBase):
	"""This is the abstract parent class to describe specific timing event types at Virtual Function Bus (VFB) level."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._isExternal_child=ModelNone
		self._portPrototypeBlueprint_child=ModelNone
		self._port_child=ModelNone

class TDEventVfbReference(complexbase.GroupBase):
	"""This is used to reference timing description events related to the Virtual Function Bus (VFB) view which are specified in other timing views."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._referencedTDEventVfb_child=ModelNone

class Table(complexbase.GroupBase):
	"""This class implements an exchange table according to OASIS Technical Resolution TR 9503:1995.

http://www.oasis-open.org/specs/a503.htm"""
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates if by default a line should be drawn between the columns of this table.""")
	float=complexbase.Attribute("float",SimpleTypes.FloatEnum,'FLOAT',False,"""Indicate whether it is allowed to break the element.""")
	frame=complexbase.Attribute("frame",SimpleTypes.FrameEnum,'FRAME',False,"""Used to defined the frame line around a table.""")
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax must be defined by the applied help system respectively help system generator.""")
	orient=complexbase.Attribute("orient",SimpleTypes.OrientEnum,'ORIENT',False,"""Indicate whether a table should be represented as landscape or portrait. 
- land : landscape
- port : portrait""")
	pgwide=complexbase.Attribute("pgwide",SimpleTypes.NameToken,'PGWIDE',False,"""Used to indicate wether the figure should take the complete page width (value = \"pgwide\") or not (value = \"noPgwide\").""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates if by default  a line should be drawn at the bottom of table rows.""")
	tabstyle=complexbase.Attribute("tabstyle",SimpleTypes.NameToken,'TABSTYLE',False,"""Indicates an external table style.""")
	def __init__(self):
		super().__init__()
		self._tableCaption_child=ModelNone
		self._tgroup_children=[]
		self._variationPoint_child=ModelNone

class TagWithOptionalValue(complexbase.GroupBase):
	"""A tagged value is a combination of a tag (key) and a value that gives supplementary information that is attached to a model element. Please note that keys without a value are allowed."""
	def __init__(self):
		super().__init__()
		self._key_child=ModelNone
		self._value_child=ModelNone

class TargetIPduRef(complexbase.GroupBase):
	"""Target destination of the referencing mapping."""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._targetIPdu_child=ModelNone

class Tbody(complexbase.GroupBase):
	"""This meta-class represents a part within a table group. Such a part can be the table head, the table body or the table foot."""
	valign=complexbase.Attribute("valign",SimpleTypes.ValignEnum,'VALIGN',False,"""Indicates how the cells in the rows shall be aligned. Default is inherited from tbody, otherwise it is \"TOP\"""")
	def __init__(self):
		super().__init__()
		self._row_children=[]

class TcpTp(complexbase.GroupBase):
	"""Content Model for TCP configuration."""
	def __init__(self):
		super().__init__()
		self._keepAliveInterval_child=ModelNone
		self._keepAliveProbesMax_child=ModelNone
		self._keepAliveTime_child=ModelNone
		self._keepAlives_child=ModelNone
		self._naglesAlgorithm_child=ModelNone
		self._tcpTpPort_child=ModelNone

class TcpUdpConfig(complexbase.GroupBase):
	"""Tcp or Udp Transport Protocol Configuration."""

class TextTableMapping(complexbase.GroupBase):
	"""Defines the mapping of two DataPrototypes typed by AutosarDataTypes that refer to CompuMethods of category TEXTTABLE, SCALE_LINEAR_AND_TEXTTABLE or BITFIELD_TEXTTABLE."""
	def __init__(self):
		super().__init__()
		self._bitfieldTextTableMaskFirst_child=ModelNone
		self._bitfieldTextTableMaskSecond_child=ModelNone
		self._identicalMapping_child=ModelNone
		self._mappingDirection_child=ModelNone
		self._valuePair_children=[]

class TextTableValuePair(complexbase.GroupBase):
	"""Defines a pair of text values which are translated into each other."""
	def __init__(self):
		super().__init__()
		self._firstValue_child=ModelNone
		self._secondValue_child=ModelNone

class TextValueSpecification(complexbase.GroupBase):
	"""The purpose of TextValueSpecification is to define the labels that correspond to enumeration values."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class Tgroup(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a table section."""
	align=complexbase.Attribute("align",SimpleTypes.AlignEnum,'ALIGN',False,"""Specifies how the cell entries shall be horizontally aligned within the specified TGROUP.
Default is \"LEFT\"""")
	cols=complexbase.Attribute("cols",SimpleTypes.Integer,'COLS',False,"""This attribute represents the number of columns in the table.""")
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates if by default a line shall be drawn between the columns of this table group.""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates if by default a line shall be drawn at the bottom of the rows in this table group.""")
	def __init__(self):
		super().__init__()
		self._colspec_children=[]
		self._thead_child=ModelNone
		self._tfoot_child=ModelNone
		self._tbody_child=ModelNone

class TimeRangeType(complexbase.GroupBase):
	"""The timeRange can be specified with the value attribute. Optionally a tolerance can be defined."""
	def __init__(self):
		super().__init__()
		self._tolerance_child=[]
		self._value_child=ModelNone

class TimeRangeTypeTolerance(complexbase.GroupBase):
	"""Maximum allowable deviation"""

class TimeSyncClientConfiguration(complexbase.GroupBase):
	"""Defines the configuration of the time synchronisation client."""
	def __init__(self):
		super().__init__()
		self._orderedMaster_children=[]
		self._timeSyncTechnology_child=ModelNone

class TimeSyncServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of the time synchronisation server."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._priority_child=ModelNone
		self._syncInterval_child=ModelNone
		self._timeSyncServerIdentifier_child=ModelNone
		self._timeSyncTechnology_child=ModelNone

class TimeSynchronization(complexbase.GroupBase):
	"""Defines the servers / clients in a time synchronised network."""
	def __init__(self):
		super().__init__()
		self._timeSyncClient_child=ModelNone
		self._timeSyncServer_child=ModelNone

class TimingConstraint(complexbase.GroupBase):
	"""The abstract parent class of different timing constraints supported by the Timing extension.

A concrete timing constraint is used to bound the timing behavior of the model elements in its scope."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TimingDescription(complexbase.GroupBase):
	"""The abstract parent class of the model elements that are used to define the scope of a timing constraint."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TimingDescriptionEvent(complexbase.GroupBase):
	"""A timing event is the abstract representation of a specific system behavior -- that can be observed at runtime -- in the AUTOSAR specification. Timing events are used to define the scope for timing constraints. Depending on the specific scope, the view on the system, and the level of abstraction different types of events are defined.

In order to avoid confusion with existing event descriptions in the AUTOSAR templates the timing specific event types use the prefix TD."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._occurrenceExpression_child=ModelNone

class TimingDescriptionEventChain(complexbase.GroupBase):
	"""An event chain describes the causal order for a set of functionally dependent timing events. Each event chain has a well defined stimulus and response, which describe its start and end point. Furthermore, it can be hierarchically decomposed into an arbitrary number of sub-chains, so called ''event chain segments''."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._stimulus_child=ModelNone
		self._response_child=ModelNone
		self._segment_children=[]

class TimingEvent(complexbase.GroupBase):
	"""TimingEvent references the RunnableEntity that need to be started in response to the TimingEvent"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone

class TimingExtension(complexbase.GroupBase):
	"""The abstract parent class of the different template specific timing extensions.

Depending on the specific timing extension (VfbTiming, SwcTiming, SystemTiming, BswModuleTiming, EcuTiming) the timing descriptions and timing constraints, that can be used to specify the timing behavior, are restricted."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._timingDescription_children=[]
		self._timingGuarantee_children=[]
		self._timingRequirement_children=[]

class Topic1(complexbase.GroupBase):
	"""This meta-class represents a topic of a documentation. Topics are similar to chapters but they cannot be nested. 

They also do not appear in the table of content. Topics can be used to produce intermediate headlines thus structuring a chapter internally."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax must be defined by the applied help system respectively help system generator.""")
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TopicContent(complexbase.GroupBase):
	"""This meta-class represents the content of a topic. It is mainly a documentation block, but can also be a table."""
	def __init__(self):
		super().__init__()
		self._table_children=[]

class TopicContentOrMsrQuery(complexbase.GroupBase):
	"""This meta-class represents a topic or a topic content which is generated using queries."""
	def __init__(self):
		super().__init__()
		self._msrQueryP1_child=ModelNone

class TopicOrMsrQuery(complexbase.GroupBase):
	"""This class provides the alternative of a Topic with an MsrQuery which delivers a topic."""
	def __init__(self):
		super().__init__()
		self._topic1_children=[]
		self._msrQueryTopic1_child=ModelNone

class TpAddress(complexbase.GroupBase):
	"""An ECUs TP address on the referenced channel. This represents the diagnostic Address."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class TpConfig(complexbase.GroupBase):
	"""Contains all configuration elements for AUTOSAR TP."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._communicationCluster_child=ModelNone

class TpConnection(complexbase.GroupBase):
	"""TpConnection Base Class."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone

class TpConnectionIdent(complexbase.GroupBase):
	"""This meta-class is created to add the ability to become the target of a reference to the non-Referrable TpConnection."""
	_references=defaultdict(list)

class TpPort(complexbase.GroupBase):
	"""Dynamic or direct assignment of a PortNumber."""
	def __init__(self):
		super().__init__()
		self._dynamicallyAssigned_child=ModelNone
		self._portNumber_child=ModelNone

class TraceReferrable(complexbase.GroupBase):
	"""This meta class is intended to add the category to the subclasses of Traceable. 

Even if the model seems to be a bit awkward, it ensures backwards compatibility of the schema.

This approach allows to have subclasses of Traceable which are either Identifiable or only Referrable while still maintaining the consistent sequence of shortName, longName, category."""
	_references=defaultdict(list)

class Traceable(complexbase.GroupBase):
	"""This meta class represents the ability to be subject to tracing within an AUTOSAR model.

Note that it is expected that its subclasses inherit either from MultilanguageReferrable or from Identifiable. Nevertheless it also inherits from MultilanguageReferrable in order to provide a common reference target for all Traceables."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._trace_children=[]

class TraceableText(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a traceable text item such as requirements etc.

The following approach appliles:

* '''shortName''' represents the tag for tracing
* '''longName''' represents the head line
* '''category''' represents the kind of the tagged text"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TransformationComSpecProps(complexbase.GroupBase):
	"""TransformationComSpecProps holds all the attributes for transformers that are port specific."""

class TransformationDescription(complexbase.GroupBase):
	"""The TransformationDescription is the abstract class that can be used by specific transformers to add transformer specific properties."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TransformationISignalProps(complexbase.GroupBase):
	"""TransformationISignalProps holds all the attributes for the different TransformationTechnologies that are ISignal specific."""
	def __init__(self):
		super().__init__()
		self._csErrorReaction_child=ModelNone
		self._transformer_child=ModelNone

class TransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class TransformationTechnology(complexbase.GroupBase):
	"""A TransformationTechnology is a transformer inside a transformer chain."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._bufferProperties_child=ModelNone
		self._needsOriginalData_child=ModelNone
		self._protocol_child=ModelNone
		self._transformationDescription_children=[]
		self._transformerClass_child=ModelNone
		self._version_child=ModelNone
		self._variationPoint_child=ModelNone

class TransformerHardErrorEvent(complexbase.GroupBase):
	"""The event is raised when data are received which should trigger a Client/Server operation or an external trigger but during transformation of the data a hard transformer error occurred."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._trigger_child=ModelNone

class TransmissionAcknowledgementRequest(complexbase.GroupBase):
	"""Requests transmission acknowledgement that data has been sent successfully. Success/failure is reported via a SendPoint of a RunnableEntity."""
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone

class TransmissionModeCondition(complexbase.GroupBase):
	"""Possibility to attach a condition to each signal within an I-PDU.

If at least one condition evaluates to true, TRANSMISSION MODE True shall be used for this I-Pdu. In all other cases, the TRANSMISSION MODE FALSE shall be used."""
	def __init__(self):
		super().__init__()
		self._dataFilter_child=ModelNone
		self._iSignalInIPdu_child=ModelNone

class TransmissionModeDeclaration(complexbase.GroupBase):
	"""AUTOSAR COM provides the possibility to define two different TRANSMISSION MODES (True and False) for each I-PDU.

As TransmissionMode selector the signal content can be evaluated via transmissionModeCondition (implemented directly in the COM module) or mode conditions can be defined with the modeDrivenTrueCondition or modeDrivenFalseCondition (evaluated by BswM and invoking Com_SwitchIpduTxMode COM API). If modeDrivenTrueCondition and modeDrivenFalseCondition are defined they shall never evaluate to true both at the same time. 

The mixing of Transmission Mode Switch via API and signal value is not allowed."""
	def __init__(self):
		super().__init__()
		self._modeDrivenFalseCondition_children=[]
		self._modeDrivenTrueCondition_children=[]
		self._transmissionModeCondition_children=[]
		self._transmissionModeFalseTiming_child=ModelNone
		self._transmissionModeTrueTiming_child=ModelNone

class TransmissionModeTiming(complexbase.GroupBase):
	"""If the COM Transmission Mode is false the timing is aggregated by the TransmissionModeTiming element in the role of transmissionModeFalseTiming. If the COM Transmission Mode is true the timing is aggregated by the TransmissionModeTiming element in the role of transmissionModeTrueTiming.  

COM supports the following Transmission Modes: 
Periodic (Cyclic Timing)
Direct /n-times (EventControlledTiming)
Mixed (Cyclic and EventControlledTiming are assigned)
None (no timing is assigned)"""
	def __init__(self):
		super().__init__()
		self._cyclicTiming_child=ModelNone
		self._eventControlledTiming_child=ModelNone

class TransportProtocolConfiguration(complexbase.GroupBase):
	"""Transport Protocol configuration."""

class Trigger(complexbase.GroupBase):
	"""A trigger which is provided (i.e. released) or required (i.e. used to activate something) in the given context."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._swImplPolicy_child=ModelNone
		self._triggerPeriod_child=ModelNone
		self._variationPoint_child=ModelNone

class TriggerIPduSendCondition(complexbase.GroupBase):
	"""The condition defined by this class evaluates to true if one of the referenced modeDeclarations (OR associated) is active. All referenced modeDeclarations shall be from the same ModeDeclarationGroup.

The condition is used to define when the Pdu is triggered with the Com_TriggerIPDUSend API call."""
	def __init__(self):
		super().__init__()
		self._modeDeclaration_children=[]

class TriggerInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class TriggerInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._targetTrigger_child=ModelNone

class TriggerInterface(complexbase.GroupBase):
	"""A trigger interface declares a number of triggers that can be sent by an trigger source."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._trigger_children=[]

class TriggerInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of unequal named Triggers in context of two different TriggerInterfaces."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._triggerMapping_children=[]

class TriggerMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular unequally named Triggers in the given context."""
	def __init__(self):
		super().__init__()
		self._firstTrigger_child=ModelNone
		self._secondTrigger_child=ModelNone

class TriggerPortAnnotation(complexbase.GroupBase):
	"""Annotation to a port used for calibration regarding a certain Trigger."""
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone

class TriggerRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._Trigger_child=ModelNone
		self._variationPoint_child=ModelNone

class TriggerToSignalMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a trigger to a SystemSignal of size 0. The Trigger does not transport any other information than its existence, therefore the limitation in terms of signal length."""
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone
		self._systemSignal_child=ModelNone

class Tt(complexbase.GroupBase):
	"""This meta-class represents the ability to express specific technical terms.  The kind of term is denoted in the attribute \"type\"."""
	type=complexbase.Attribute("type",SimpleTypes.NameToken,'TYPE',False,"""This attribute specifies the type of the technical term. Values are such as \"VARIABLE\" \"CALPRM\". It is no longer an enum in order to support process specific extensions.""")
	def __init__(self):
		super().__init__()
		self._term_child=ModelNone

class TtcanAbsolutelyScheduledTiming(complexbase.GroupBase):
	"""Each frame in TTCAN is identified by its slot id and communication cycle. A description is provided by the usage of AbsolutelyScheduledTiming. 

A frame can be sent multiple times within one communication cycle. For describing this case multiple AbsolutelyScheduledTimings have to be used. The main use case would be that a frame is sent twice within one communication cycle."""
	def __init__(self):
		super().__init__()
		self._communicationCycle_child=[]
		self._timeMark_child=ModelNone
		self._trigger_child=ModelNone

class TtcanCluster(complexbase.GroupBase):
	"""TTCAN bus specific cluster attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._TtcanClusterVariant_children=[]
		self._basicCycleLength_child=ModelNone
		self._ntu_child=ModelNone
		self._operationMode_child=ModelNone

class TtcanClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TtcanClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class TtcanCommunicationConnector(complexbase.GroupBase):
	"""TTCAN bus specific communication connector attributes."""
	_references=defaultdict(list)

class TtcanCommunicationController(complexbase.GroupBase):
	"""TTCAN bus specific communication port attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._TtcanCommunicationControllerVariant_children=[]
		self._applWatchdogLimit_child=ModelNone
		self._expectedTxTrigger_child=ModelNone
		self._externalClockSynchronisation_child=ModelNone
		self._initialRefOffset_child=ModelNone
		self._master_child=ModelNone
		self._timeMasterPriority_child=ModelNone
		self._timeTriggeredCanLevel_child=ModelNone
		self._txEnableWindowLength_child=ModelNone

class TtcanCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TtcanCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class TtcanPhysicalChannel(complexbase.GroupBase):
	"""TTCAN bus specific physical channel attributes."""
	_references=defaultdict(list)

class UdpNmCluster(complexbase.GroupBase):
	"""Udp specific NmCluster attributes"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmCbvPosition_child=ModelNone
		self._nmChannelActive_child=ModelNone
		self._nmMessageTimeoutTime_child=ModelNone
		self._nmMsgCycleTime_child=ModelNone
		self._nmNetworkTimeout_child=ModelNone
		self._nmNidPosition_child=ModelNone
		self._nmRemoteSleepIndicationTime_child=ModelNone
		self._nmRepeatMessageTime_child=ModelNone
		self._nmUserDataLength_child=ModelNone
		self._nmWaitBusSleepTime_child=ModelNone

class UdpNmClusterCoupling(complexbase.GroupBase):
	"""Udp attributes that are valid for each of the referenced (coupled) UdpNm clusters."""
	def __init__(self):
		super().__init__()
		self._coupledCluster_children=[]
		self._nmBusLoadReductionEnabled_child=ModelNone
		self._nmImmediateRestartEnabled_child=ModelNone

class UdpNmEcu(complexbase.GroupBase):
	"""Udp NM specific ECU attributes."""
	def __init__(self):
		super().__init__()
		self._nmRepeatMsgIndicationEnabled_child=ModelNone
		self._nmSynchronizationPointEnabled_child=ModelNone

class UdpNmNode(complexbase.GroupBase):
	"""Udp specific NM Node attributes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._nmMsgCycleOffset_child=ModelNone

class UdpTp(complexbase.GroupBase):
	"""Content Model for UDP configuration."""
	def __init__(self):
		super().__init__()
		self._udpTpPort_child=ModelNone

class UnassignFrameId(complexbase.GroupBase):
	"""Schedule entry for an Unassign Frame Id master request where the protected identifier is assigned the value 0x40. This will disable reception/transmission of a previously dynamically assigned frame identifier."""
	def __init__(self):
		super().__init__()
		self._unassignedFrameTriggering_child=ModelNone

class Unit(complexbase.GroupBase):
	"""This is a physical measurement unit. All units that might be defined should stem from SI units. In order to convert one unit into another factor and offset are defined. For the calculation from SI-unit to the defined unit the factor (factorSiToUnit ) and the offset (offsetSiToUnit ) are applied:

unit = siUnit * factorSiToUnit + offsetSiToUnit 

For the calculation from a unit to SI-unit the reciprocal of the factor (factorSiToUnit ) and the negation of the offset (offsetSiToUnit ) are applied:

siUnit = (unit - offsetSiToUnit) / factorSiToUnit"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._displayName_child=ModelNone
		self._factorSiToUnit_child=ModelNone
		self._offsetSiToUnit_child=ModelNone
		self._physicalDimension_child=ModelNone

class UnitGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a logical grouping of units.The category denotes the unit system that the referenced units are associated to. 

In this way, e.g. country-specific unit systems (CATEGORY=\"COUNTRY\") can be defined as well as specific unit systems for certain application domains. 

In the same way a group of equivalent units, can be defined which are used in different countries, by setting CATEGORY=\"EQUIV_UNITS\".  KmPerHour and MilesPerHour could such be combined to one group named \"vehicle_speed\". The unit MeterPerSec would not belong to this group because it is normally not used for vehicle speed. But all of the mentioned units could be combined to one group named \"speed\".

Note that the UnitGroup does not ensure the physical compliance of the units. This is maintained by the physical dimension."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._unit_children=[]

class UnlimitedIntegerValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for unlimited Integer attributes.

Note that this class might be used in the extended meta-model only."""

class Url(complexbase.GroupBase):
	"""This meta-class specifies an Uniform Resource Locator (URL)."""
	mimeType=complexbase.Attribute("mimeType",SimpleTypes.MimeTypeString,'MIME-TYPE',False,"""this denotes the mime type of the resource located by the url.""")
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class UserDefinedCluster(complexbase.GroupBase):
	"""This element allows the modelling of arbitrary Communication Clusters (e.g. bus systems that are not supported by AUTOSAR)."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._UserDefinedClusterVariant_children=[]

class UserDefinedClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class UserDefinedClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class UserDefinedCommunicationConnector(complexbase.GroupBase):
	"""This element allows the modelling of arbitrary Communication Connectors."""
	_references=defaultdict(list)

class UserDefinedCommunicationController(complexbase.GroupBase):
	"""This element allows the modelling of arbitrary Communication Controllers."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._UserDefinedCommunicationControllerVariant_children=[]

class UserDefinedCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class UserDefinedCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class UserDefinedEthernetFrame(complexbase.GroupBase):
	"""UserDefinedEthernetFrame allows the description of a frame-based communication to Complex Drivers that are located above the EthDrv."""
	_references=defaultdict(list)

class UserDefinedIPdu(complexbase.GroupBase):
	"""UserDefinedIPdu allows to describe PDU-based communication over Complex Drivers. If a new BSW module is added above the PduR (e.g. a Diagnostic Service ) then this IPdu element shall be used to describe the communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._cddType_child=ModelNone

class UserDefinedPdu(complexbase.GroupBase):
	"""UserDefinedPdu allows to describe PDU-based communication over Complex Drivers. If a new BSW module is added above the BusIf (e.g. a new Nm module) then this Pdu element shall be used to describe the communication."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._cddType_child=ModelNone

class UserDefinedPhysicalChannel(complexbase.GroupBase):
	"""This element allows the modelling of arbitrary Physical Channels."""
	_references=defaultdict(list)

class UserDefinedTransformationComSpecProps(complexbase.GroupBase):
	"""The UserDefinedTransformationComSpecProps is used to specify port specific configuration properties for custom transformers."""

class UserDefinedTransformationDescription(complexbase.GroupBase):
	"""The UserDefinedTransformationDescription is used to specify details and documentation for custom transformers."""

class UserDefinedTransformationISignalProps(complexbase.GroupBase):
	"""The UserDefinedTransformationISignalProps is used to specify ISignal specific configuration properties for custom transformers."""
	def __init__(self):
		super().__init__()
		self._UserDefinedTransformationISignalPropsVariant_children=[]

class UserDefinedTransformationISignalPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class UserDefinedTransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class ValueGroup(complexbase.GroupBase):
	"""This element enables valules  to be grouped. It can be used to perform row and column-orientated groupings, so that these can be rendered properly e.g. as a table."""
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone

class ValueList(complexbase.GroupBase):
	"""This is a generic list of numerical values."""
	def __init__(self):
		super().__init__()
		self._vf_children=[]
		self._v_child=ModelNone

class ValueSpecification(complexbase.GroupBase):
	"""Base class for expressions leading to a value which can be used to initialize a data object."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._variationPoint_child=ModelNone

class VariableAccess(complexbase.GroupBase):
	"""The presence of a VariableAccess implies that a RunnableEntity needs access to a VariableDataPrototype. 

The kind of access is specified by the role in which the class is used."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._accessedVariable_child=ModelNone
		self._scope_child=ModelNone
		self._variationPoint_child=ModelNone

class VariableAccessInEcuInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootComposition_child=ModelNone
		self._contextAtomicComponent_child=ModelNone
		self._targetVariableAccess_child=ModelNone

class VariableAndParameterInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of VariableDataPrototypes or ParameterDataPrototypes in context of two different SenderReceiverInterfaces, NvDataInterfaces or ParameterInterfaces."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._dataMapping_children=[]

class VariableDataPrototype(complexbase.GroupBase):
	"""A VariableDataPrototype is used to contain values in an ECU application. This means that most likely a VariableDataPrototype allocates \"static\" memory on the  ECU. In some cases optimization strategies might lead to a situation where the  memory allocation can be avoided.

In particular, the value of a VariableDataPrototype is likely to change as the ECU on which it is used executes."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._variationPoint_child=ModelNone

class VariableDataPrototypeInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a VariableDataPrototype in the context of a CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._contextPortPrototype_child=ModelNone
		self._targetVariableDataPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class VariableDataPrototypeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_children=[]
		self._contextComposition_child=ModelNone
		self._contextPort_child=ModelNone
		self._targetDataPrototype_child=ModelNone

class VariableInAtomicSWCTypeInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._rootVariableDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class VariableInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class VariableInComponentInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_children=[]
		self._contextPortPrototype_child=ModelNone
		self._rootVariableDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataProtoype_child=ModelNone

class VariationPoint(complexbase.GroupBase):
	"""This meta-class represents the ability to express a \"structural variation point\". The container of the variation point is part of the selected variant if swSyscond evaluates to true and each postBuildVariantCriterion is fulfilled."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._desc_child=ModelNone
		self._blueprintCondition_child=ModelNone
		self._formalBlueprintCondition_child=ModelNone
		self._swSyscond_child=ModelNone
		self._postBuildVariantCondition_children=[]
		self._sdg_child=ModelNone

class VariationPointProxy(complexbase.GroupBase):
	"""The VariationPointProxy represents variation points of the C/C++ implementation. In case of bindingTime = compileTime the RTE provides defines which can be used for Pre Processor directives to implement compileTime variability."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._conditionAccess_child=ModelNone
		self._implementationDataType_child=ModelNone
		self._postBuildValueAccess_child=ModelNone
		self._postBuildVariantCondition_children=[]
		self._valueAccess_child=[]

class VfbTiming(complexbase.GroupBase):
	"""A model element used to define timing descriptions and constraints at VFB level.

TimingDescriptions aggregated by VfbTiming are restricted to event chains referring to events which are derived from the class TDEventVfb."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone

class ViewMap(complexbase.GroupBase):
	"""The ViewMap allows to relate any number of elements on the \"first\" side to any number of elements on the \"second\" side. Since the ViewMap does not address a specific mapping use-case the roles \"first\" and \"second\" shall imply this generality.

This mapping allows to trace transformations of artifacts within the AUTOSAR environment. The references to the mapped elements can be plain references and/or InstanceRefs."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._firstElement_children=[]
		self._secondElement_children=[]
		self._firstElementInstance_children=[]
		self._secondElementInstance_children=[]

class ViewMapSet(complexbase.GroupBase):
	"""Collection of ViewMaps that are used to establish relationships between different AUTOSAR artifacts."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._viewMap_children=[]

class VlanConfig(complexbase.GroupBase):
	"""VLAN Configuration attributes"""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._vlanIdentifier_child=ModelNone

class VlanMembership(complexbase.GroupBase):
	"""Static logical channel or VLAN binding to a switch-port."""
	def __init__(self):
		super().__init__()
		self._defaultPriority_child=ModelNone
		self._dhcpAddressAssignment_child=ModelNone
		self._sendActivity_child=ModelNone
		self._vlan_child=ModelNone

class WaitPoint(complexbase.GroupBase):
	"""This defines a wait-point for which the RunnableEntity can wait."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone
		self._trigger_child=ModelNone

class WarningIndicatorRequestedBitNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to explicitly request the existence of the WarningIndicatorRequestedBit."""
	_references=defaultdict(list)

class WhitespaceControlled(complexbase.GroupBase):
	"""This meta-class represents the ability to control the white-space handling  e.g. in xml serialization. This is implemented by adding the attribute \"space\"."""
	xmlSpace=complexbase.Attribute("xmlSpace",SimpleTypes.String,'space',True,"""This attribute is used to signal an intention that in that element, white space should be preserved by applications. It is defined according to xml:space as declared by W3C.""")

class WorstCaseHeapUsage(complexbase.GroupBase):
	"""Provides a formal worst case heap usage."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class WorstCaseStackUsage(complexbase.GroupBase):
	"""Provides a formal worst case stack usage."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class XcpPdu(complexbase.GroupBase):
	"""This element is deprecated and will be removed in future. The GeneralPurposeIPdu with the category \"XCP\" shall be used instead.

Old description:
AUTOSAR XCP Pdu."""
	_references=defaultdict(list)

class Xdoc(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to an external document which can be rendered as printed matter."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._number_child=ModelNone
		self._state_child=ModelNone
		self._date_child=ModelNone
		self._publisher_child=ModelNone
		self._url_child=ModelNone
		self._position_child=ModelNone

class Xfile(complexbase.GroupBase):
	"""This represents to reference an external file within a documentation."""
	_references=defaultdict(list)
	def __init__(self):
		super().__init__()
		self._url_child=ModelNone
		self._tool_child=ModelNone
		self._toolVersion_child=ModelNone

class Xref(complexbase.GroupBase):
	"""This represents a cross-reference within documentation."""
	resolutionPolicy=complexbase.Attribute("resolutionPolicy",SimpleTypes.ResolutionPolicyEnum,'RESOLUTION-POLICY',False,"""Indicates if the content of the xref element follow a dedicated resolution policy. The default is \"NO-SLOPPY\".""")
	showContent=complexbase.Attribute("showContent",SimpleTypes.ShowContentEnum,'SHOW-CONTENT',False,"""Indicates if the content of the xref element shall be rendered. The default is \"NO-SHOW-CONTENT\".""")
	showResourceAliasName=complexbase.Attribute("showResourceAliasName",SimpleTypes.ShowResourceAliasNameEnum,'SHOW-RESOURCE-ALIAS-NAME',False,"""This indicates if the alias names of the referenced objects shall be rendered. This means this is some kind of backward searching: look whether there is an  alias for the referenced object, if yes, print it. 

If there is more than one AliasNameSet, Xref might render all of those.

If no alilas is found and showResourceShortName is set to NoShowShortName, then the shortName of the reference target shall be displayed. By this showResourceAliasName is similar to showResourceShortName but shows the aliasName instead of the shortName.

Default is NO-SHOW-ALIAS-NAME.""")
	showResourceCategory=complexbase.Attribute("showResourceCategory",SimpleTypes.ShowResourceCategoryEnum,'SHOW-RESOURCE-CATEGORY',False,"""Indicates if the category of the referenced resource shall be rendered. Default is \"NO-SHOW-CATEGORY\".""")
	showResourceLongName=complexbase.Attribute("showResourceLongName",SimpleTypes.ShowResourceLongNameEnum,'SHOW-RESOURCE-LONG-NAME',False,"""Indicates if the longName of the referenced resource shall be rendered. Default is \"SHOW-LONG-NAME\".""")
	showResourceNumber=complexbase.Attribute("showResourceNumber",SimpleTypes.ShowResourceNumberEnum,'SHOW-RESOURCE-NUMBER',False,"""Indicates if the Number of the referenced resource shall be shown. Default is \"SHOW--NUMBER\"""")
	showResourcePage=complexbase.Attribute("showResourcePage",SimpleTypes.ShowResourcePageEnum,'SHOW-RESOURCE-PAGE',False,"""Indicates if the page number of the referenced resource shall be shown. Default is \"SHOW-PAGE\"""")
	showResourceShortName=complexbase.Attribute("showResourceShortName",SimpleTypes.ShowResourceShortNameEnum,'SHOW-RESOURCE-SHORT-NAME',False,"""Indicates if the shortJName of the referenced resource shall be shown. Default is \"SHOW-SHORT-NAME\"""")
	showResourceType=complexbase.Attribute("showResourceType",SimpleTypes.ShowResourceTypeEnum,'SHOW-RESOURCE-TYPE',False,"""Indicates if the type of the referenced Resource shall be shown. Default is \"SHOW-TYPE\"""")
	showSee=complexbase.Attribute("showSee",SimpleTypes.ShowSeeEnum,'SHOW-SEE',False,"""Indicates if the word \"see \" shall be shown before the reference. Default is \"NO-SHOW-SEE\". Note that this is there for compatibility reasons only.""")
	def __init__(self):
		super().__init__()
		self._label1_child=ModelNone
		self._referrable_child=ModelNone

class XrefTarget(complexbase.GroupBase):
	"""This element specifies a reference target which can be scattered throughout the text."""
	_references=defaultdict(list)