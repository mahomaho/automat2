# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

from AutoMAT import *
from .support import *
import re

def __repr__EcucParameterValue_EcucAbstractReferenceValue(self):
	try:
		binding=self.ecucBinding()
		ret=self.parent().__repr__()+'.'+binding.name()
		if self.ecucBinding().upperMultiplicity()>1:
			return ret+f'[{binding.index(self)}]'
		return ret
	except:
		return self.parent().__repr__()+'.'+self.definition.val().rsplit('/',1)[-1]
autosar_r4p0.Group.EcucAbstractReferenceValue.__repr__=__repr__EcucParameterValue_EcucAbstractReferenceValue
autosar_r4p0.Group.EcucParameterValue.__repr__=__repr__EcucParameterValue_EcucAbstractReferenceValue

#autosar_r4p0.Group.EcucIndexableValue #alla
#AutoMAT.autosar_r4p0.Group.EcucContainerValue #containers
#autosar_r4p0.Group.EcucAbstractReferenceValue #refar
#autosar_r4p0.Group.EcucParameterValue #values

def __dir__Referrable_AUTOSAR(self):
	ret=[o for o in object.__dir__(self) if o[0]!='_' and getattr(self,o)!=ModelNone]
	ret.extend(c.shortName.val() for c in self._children if isinstance(c,autosar_r4p0.Group.Referrable))
	return ret
autosar_r4p0.Group.Referrable.__dir__=__dir__Referrable_AUTOSAR
autosar_r4p0.Group.AUTOSAR.__dir__=__dir__Referrable_AUTOSAR

def __getattr__Referrable_AUTOSAR(self,name):
	#ret=next((child for child in self._children if child._xml[0].findtext('{http://autosar.org/schema/r4.0}SHORT-NAME',None)==name),None)
	ret=next((child for child in self._children if isinstance(child,autosar_r4p0.Group.Referrable) and child.shortName.val()==name),None)
	if ret is None:
		raise AttributeError(f'No element named {name} in {self.__repr__()}')
	return ret
autosar_r4p0.Group.Referrable.__getattr__=__getattr__Referrable_AUTOSAR
autosar_r4p0.Group.AUTOSAR.__getattr__=__getattr__Referrable_AUTOSAR

def __dir__EcucModuleConfigurationValues_EcucContainerValue(self):
	ret=__dir__Referrable_AUTOSAR(self)
	try:
		_def=self.definition.ref()
		for c in self._children:
			try:
				cdef=c.definition.ref()
				if cdef._parent==_def and cdef.upperMultiplicity.val()==1:
					ret.append(cdef.shortName.val())
			except:pass
		try:
			ret.extend(c.shortName.val() for c in _def._children if isinstance(c,autosar_r4p0.Group.Referrable) and c.upperMultiplicity.val()!=1)
		except: pass
	except:pass
	return ret
autosar_r4p0.Group.EcucModuleConfigurationValues.__dir__=__dir__EcucModuleConfigurationValues_EcucContainerValue
autosar_r4p0.Group.EcucContainerValue.__dir__=__dir__EcucModuleConfigurationValues_EcucContainerValue

def __getattr__EcucModuleConfigurationValues_EcucContainerValue(self,name):
	_def=self.definition.ref()
	try:
		pdef=getattr(_def,name)
		if isinstance(pdef,autosar_r4p0.Group.EcucDefinitionElement):
			if not (pdef.upperMultiplicityInfinite.val() is True or default(1).pdef.upperMultiplicity.val()>1):
				try:
					return next((child for child in self._children if isinstance(child,autosar_r4p0.Group.EcucIndexableValue) and child.definition.ref() is pdef))
				except:
					if pdef.lowerMultiplicity.val()==1 and hasattr(pdef,'defaultValue') and pdef.defaultValue!=None:
						return support.complexbase.DefaultValueType(self,pdef)
					return ModelNone
			else:
				def generator():
					for child in self._children:
						if isinstance(child,autosar_r4p0.Group.EcucIndexableValue):
							try:
								if child.definition.ref() is not  pdef:
									continue
							except:
								continue
							yield child
				return support.complexbase.EcucList(generator,self,pdef)
	except:
		pass
	return __getattr__Referrable_AUTOSAR(self,name)
autosar_r4p0.Group.EcucModuleConfigurationValues.__getattr__=__getattr__EcucModuleConfigurationValues_EcucContainerValue
autosar_r4p0.Group.EcucContainerValue.__getattr__=__getattr__EcucModuleConfigurationValues_EcucContainerValue

def references_Referrable(self):
	def generator():
		path=self.__repr__()
		for base in type(self).__mro__[1:-5]: # ignore self class, and obj and complexbase, groupbase and containerbase
			if hasattr(base,'_references'):
				for ref in base._references.get(path,()):
					yield ref
	return ModelList(generator)
autosar_r4p0.Group.Referrable.references=references_Referrable

def ecucAggregations_EcucContainerValue(self):
	definition=self.definition.ref()
	if hasattr(definition,'choice'):
		ret=[support.complexbase.EcucAggregation(self,d) for d in definition.choice]
	else:
		ret=[support.complexbase.EcucAggregation(self,d) for d in definition.subContainer]
		ret.extend([support.complexbase.EcucAggregation(self,d) for d in definition.parameter])
		ret.extend([support.complexbase.EcucAggregation(self,d) for d in definition.reference])
	ret.sort(key=lambda e:e.name())
	return ret
autosar_r4p0.Group.EcucContainerValue.ecucAggregations=ecucAggregations_EcucContainerValue

def ecucAggregations_EcucModuleConfigurationValues(self):
	definition=self.definition.ref()
	ret=[support.complexbase.EcucAggregation(self,d) for d in definition.container]
	ret.sort(key=lambda e:e.name())
	return ret
autosar_r4p0.Group.EcucModuleConfigurationValues.ecucAggregations=ecucAggregations_EcucModuleConfigurationValues

def ecucBinding_EcucIndexableValue(self):
	return support.complexbase.EcucAggregation(self.parent(),self.definition.ref())
autosar_r4p0.Group.EcucIndexableValue.ecucBinding=ecucBinding_EcucIndexableValue

def module_EcucIndexableValue_EcucContainerValue(self):
	parent=self.parent()
	while not isinstance(parent,autosar_r4p0.EcucModuleConfigurationValues):
		parent=parent.parent()
	return parent
autosar_r4p0.Group.EcucIndexableValue.module=module_EcucIndexableValue_EcucContainerValue
autosar_r4p0.Group.EcucContainerValue.module=module_EcucIndexableValue_EcucContainerValue

def val_LimitValueVariationPoint(self):
	val=super(autosar_r4p0.Group.LimitValueVariationPoint,self).val()
	intervalType=self.intervalType
	binding=self.binding().name()
	if intervalType=='infinite':
		if binding in ('lowerLimit','min'):
			return -math.inf
		return math.inf
	elif intervalType=='open':
		if binding in ('lowerLimit','min'):
			return val+sys.float_info.epsilon
		elif binding in ('upperLimit','max'):
			return val-sys.float_info.epsilon
	return val
autosar_r4p0.Group.LimitValueVariationPoint.val=val_LimitValueVariationPoint

class InstanceRef: #remove
	def __get__(self,instance,owner):
		return print('todo')
autosar_r4p0.Group.AtpInstanceRef.InstanceRef=InstanceRef()

def validate_EcucIndexableValue(self):
	definition=self.definition.ref()
	assert definition!=None,'Definition is missing'
	definition._validate(self)
	for vc in definition._validationConds:
		vc(self)
autosar_r4p0.Group.EcucIndexableValue.validate=validate_EcucIndexableValue
def validate_EcucModuleConfigurationValues(self):
	definition=self.definition.ref()
	assert definition!=None,'Definition is missing'
	definition._validate(self)
	for vc in definition._validationConds:
		vc(self)
autosar_r4p0.Group.EcucModuleConfigurationValues.validate=validate_EcucModuleConfigurationValues
# EcucDefinitionElement
def _validate_EcucDefinitionElement(self,instance):
	pass
autosar_r4p0.Group.EcucDefinitionElement._validate=_validate_EcucDefinitionElement
def _validate_EcucIntegerParamDef(self,instance):
	val=instance.val()
	assert isinstance(val,int),f'Value {val} is not a valid integer'
	assert self.min==None or val >= self.min.val(), f'Value below min {self.min.val()}'
	assert self.max==None or val <= self.max.val(), f'Value above max {self.max.val()}'
autosar_r4p0.Group.EcucIntegerParamDef._validate=_validate_EcucIntegerParamDef
def _validate_EcucFloatParamDef(self,instance):
	val=instance.val()
	assert isinstance(val,float),f'Value {val} is not a valid float value'
	assert self.min==None or val >= self.min.val(), f'Value below min {self.min.val()}'
	assert self.max==None or val <= self.max.val(), f'Value above max {self.max.val()}'
autosar_r4p0.Group.EcucFloatParamDef._validate=_validate_EcucFloatParamDef
def _validate_EcucBooleanParamDef(self,instance):
	val=instance.val()
	assert isinstance(val,int) and int(val) in (0,1),f'Value {val} is not a valid boolean value'
autosar_r4p0.Group.EcucBooleanParamDef._validate=_validate_EcucBooleanParamDef
def _validate_EcucEnumerationParamDef(self,instance):
	val=instance.val()
	assert val in self.literal.shortName.val(), f'Value {val} not in valid range'
autosar_r4p0.Group.EcucEnumerationParamDef._validate=_validate_EcucEnumerationParamDef
def _validate_EcucLinkerSymbolDef(self,instance):
	val=instance.val()
	pattern='[a-zA-Z][a-zA-Z0-9_.$%]{0,254}'
	assert re.fullmatch(pattern, val), f'Value {val} does not fulfill regex {pattern}'
autosar_r4p0.Group.EcucLinkerSymbolDef._validate=_validate_EcucLinkerSymbolDef
def _validate_EcucFunctionNameDef(self,instance):
	val=instance.val()
	pattern='[a-zA-Z_][a-zA-Z0-9_]*'
	assert re.fullmatch(pattern, val), f'Value {val} does not fulfill regex {pattern}'
autosar_r4p0.Group.EcucFunctionNameDef._validate=_validate_EcucFunctionNameDef
def _validate_EcucStringParamDef_EcucMultilineStringParamDef(self,instance):
	val=instance.val()
	assert isinstance(val,str),f'Value {val} is not a valid string'
	pattern=self.regularExpression.val()
	if pattern!=None:
		assert re.fullmatch(pattern, val), f'Value {val} does not fulfill regex {pattern}'
autosar_r4p0.Group.EcucStringParamDef._validate=_validate_EcucStringParamDef_EcucMultilineStringParamDef
autosar_r4p0.Group.EcucMultilineStringParamDef._validate=_validate_EcucStringParamDef_EcucMultilineStringParamDef
def _validate_EcucAbstractReferenceDef(self,instance):
	val=instance.ref()
	assert val!=None,f'referred object {instance.val()} not in loaded model'
	values=support.complexbase.EcucAggregation(instance.parent(),self).values()
	assert val in values, f'referred object not in valid list {[str(e) for e in values]}'
autosar_r4p0.Group.EcucAbstractReferenceDef._validate=_validate_EcucAbstractReferenceDef
def _validate_EcucChoiceContainerDef(self,instance):
	numchilds=len(instance.subContainer)
	assert numchilds>0,'Missing subcontainer for choice element'
	assert numchilds<=1,'Only one subcontainer allowed for choice container'
autosar_r4p0.Group.EcucChoiceContainerDef._validate=_validate_EcucChoiceContainerDef
