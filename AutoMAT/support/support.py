# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

class default:
	__exception=False
	__stat=None
	__default=None
	def __init__(self,default,env=None,*,stat=None):
		self.__default=default
		if stat is ModelNone:
			self.__stat=default
			self.__exception=True
		elif stat is not None:
			self.__stat=stat
		else:
			if env is None:
				import inspect
				env=inspect.currentframe().f_back.f_locals
			self.__stat=type('',(),env)()
	def __dir__(self):
		return self.__stat.__dir__()
	def __repr__(self):
		return self.__stat.__repr__()
	def __str__(self):
		return str(self.__stat)
	def __getattr__(self,key):
		if self.__exception:
			return self
		try:
			stat=self.__stat
			return default(self.__default,stat=getattr(stat,key))
		except Exception as e:
			self.__stat=self.__default
			self.__exception=True
			return self
	def __call__(self,*arg,**kwarg):
		if self.__exception:
			return self
		try:
			stat=self.__stat
			return default(self.__default,stat=stat.__call__(*arg,**kwarg))
		except:
			self.__exception=True
			self.__stat=self.__default
		return self
	def __getitem__(self,key):
		if self.__exception:
			return self
		try:
			return default(self.__default,stat=self.__stat[key])
		except:
			self.__exception=True
			self.__stat=self.__default
		return self
	
	@property
	def __class__(self):
		return type(self.__stat)
	def __lt__(self, other):
		return self.__stat<other
	def __le__(self, other):
		return self.__stat<=other
	def __eq__(self, other):
		return self.__stat==other
	def __ne__(self, other):
		return self.__stat!=other
	def __gt__(self, other):
		return self.__stat>other
	def __ge__(self, other):
		return self.__stat>=other

	def __hash__(self):
		return hash(self.__stat)
	def __add__(self, other):
		return self.__stat+other
	def __sub__(self, other):
		return self.__stat-other
	def __mul__(self, other):
		return self.__stat*other
	def __truediv__(self, other):
		return self.__stat/other
	def __floordiv__(self, other):
		return self.__stat//other
	def __mod__(self, other):
		return self.__stat%other
	def __divmod__(self, other):
		return divmod(self.__stat,other)
	def __pow__(self, other, *modulo):
		return pow(self.__stat,float(other),*modulo)
	def __lshift__(self, other):
		return int(self.__stat)<<int(other)
	def __rshift__(self, other):
		return int(self.__stat)>>int(other)
	def __and__(self, other):
		return int(self.__stat)&int(other)
	def __xor__(self, other):
		return int(self.__stat)^int(other)
	def __or__(self, other):
		return int(self.__stat)|int(other)
	def __radd__(self, other):
		return other+self.__stat
	def __rsub__(self, other):
		return other-self.__stat
	def __rmul__(self, other):
		return other*self.__stat
	def __rtruediv__(self, other):
		return other/self.__stat
	def __rfloordiv__(self, other):
		return other//self.__stat
	def __rmod__(self, other):
		return other%self.__stat
	def __rdivmod__(self, other):
		return divmod(other,self.__stat)
	def __rpow__(self, other, *modulo):
		return pow(other,float(other),*modulo)
	def __rlshift__(self, other):
		return int(other)<<int(self.__stat)
	def __rrshift__(self, other):
		return int(other)>>int(self.__stat)
	def __rand__(self, other):
		return int(other)&int(self.__stat)
	def __rxor__(self, other):
		return int(other)^int(self.__stat)
	def __ror__(self, other):
		return int(other)|int(self.__stat)
	def __neg__(self):
		return -self.__stat
	def __abs__(self):
		return abs(self.__stat)
	def __invert__(self):
		return 1/self.__stat
	def __complex__(self):
		return complex(self.__stat)
	def __int__(self):
		return int(self.__stat)
	def __float__(self):
		return float(self.__stat)
	def __round__(self, *ndigits):
		return round(self.__stat,*ndigits)
	def __trunc__(self):
		return trunc(self.__stat)
	def __floor__(self):
		return floor(self.__stat)
	def __ceil__(self):
		return ceil(self.__stat)

class Select:
	def __init__(self,containerlist):
		self._contList=containerlist
	def findall(self, function, *args,**kwargs):
		def generator():
			for cont in self._contList:
				if function(cont,*args,**kwargs):
					yield cont
		return ModelList(generator)
	def foreach(self,function, *args,**kwargs):
		def generator():
			for cont in self._contList:
				try:
					yield function(cont,*args,**kwargs)
				except:pass
		return ModelList(generator)
	def findfirst(self, function, *args,default=None,**kwargs):
		for cont in self._contList:
			if function(cont,*args,**kwargs):
				return cont
		if default is not None:
			return default
		raise StopIteration()
	def __dir__(self):
		return list((e for e in object.__dir__(self) if e[0]!='_'))

class ModelList:
	@property
	def __doc__(self):
		return self._g.__doc__
	@property
	def select(self):
		return Select(self)
	def __dir__(self):
		i=self.__iter__()
		try:
			ret=set(dir(next(i)))
		except:
			ret=set()
		else:
			for e in i:
				ret&=set(dir(e))
		ret.add('select')
		return list(ret)
	def __init__(self,generator):#todo ,*args,**kwargs):
		self._g=generator
	def __hash__(self):
		vals=tuple(self.__iter__())
		if len(vals)==0:
			return hash(None)
		try:
			return hash(vals)
		except:
			raise
	def __iter__(self):
		return self._g()# todo*self._args,**self._kwargs)
	def __getitem__(self,key):
		vals=tuple(self.__iter__())
		if isinstance(key,int):
			return vals[key]
		elif isinstance(key,slice):
			vals=vals[key]
			def generator():
				for v in vals:
					yield v
			return ModelList(generator)
		else:
			try:
				return next((i for i in vals if getattr(i,'shortName','').val()==key))
			except:
				raise IndexError(f'{key} is not a valid index')
			#return next((i for i in vals if i._matchkey(key)),ModelNone)
	def index(self,value, *args, **kwargs):
		vals=tuple(self.__iter__())
		return vals.index(value, *args, **kwargs)
	def __len__(self):
		return sum(1 for _ in self.__iter__())
	def __contains__(self,val):
		try:
			next((e for e in self if e==val))
			return True
		except:
			return False
	def __bool__(self):
		return next(self.__iter__(),None) is not None
	def __add__(self,other):
		def generator():
			for a in self._g():
				yield a
			for a in other:
				yield a
		return ModelList(generator)
	def __getattr__(self,val):
		first=next(self.__iter__(),None)
		if first is None:
			raise AttributeError('Cannot get attribute from empty list')
		try:
			if callable(getattr(first,val)):
				def call(*args,**kwargs):
					def generator():
						for i in self.__iter__():
							res=getattr(i,val)(*args,**kwargs)
							if isinstance(res,ModelList):
								for r in res:
									yield r
							else:
								yield res
					return ModelList(generator)
				return call
		except:
			raise
		def generator():
			for i in self.__iter__():
				res=getattr(i,val,None)
				if res is None:
					continue
				if isinstance(res,ModelList):
					for j in res:
						yield j
				else:
					yield res
		return ModelList(generator)
	def __repr__(self):
		vals=list(self.__iter__())
		return repr(vals)
	def __str__(self):
		vals=list((str(i) for i in self.__iter__()))
		return str(vals)
	def __eq__(self,other):
		try:
			return hash(other)==hash(self)
		except TypeError:
			try:
				otheriter=iter(other)
				for i in self.__iter__():
					if i!=next(otheriter):
						return False
				try:
					next(otheriter)
				except:
					return True
			except:
				pass
			return False
	def __ne__(self,other):
		return not self==other

class ModelNoneType:
	_hash=hash(None)
	def __dir__(self):
		return []
	def val(self):
		return self
	def __init__(self):
		assert 'ModelNone' not in globals()
	@classmethod
	def __hash__(cls):
		return cls._hash
	@classmethod
	def __iter__(cls):
		return ().__iter__()
	def __getitem__(self,key):
		raise KeyError('Cannot get item from ModelNone')
	def __len__(self):
		return 0
	def __bool__(self):
		return False
	def __contains__(self,val):
		return False
	def __setattr__(self,name,val):
		raise AttributeError(f'Cannot set attributes on ModelNone')
	def __getattr__(self,val):
		raise AttributeError(f'Cannot get attribute {val} from ModelNone')
	def __repr__(self):
		return 'ModelNone'
	@classmethod
	def __eq__(cls,val):
		return hash(val)==cls._hash
	@classmethod
	def __ne__(cls,val):
		return hash(val)!=cls._hash
	def __instancecheck__(self,cls):
		return cls is ModelNoneType or cls is type(None)

ModelNone=ModelNoneType()	

class singleiterator:
	__slots__=('_ret')
	def __iter__(self):
		return self
	def __init__(self,ret):
		self._ret=ret
	@property
	def __doc__(self):
		return self._ret.__doc__
	def __next__(self):
		if self._ret is None:
			raise StopIteration()
		ret=self._ret
		self._ret=None
		return ret

class FileViewer:
	__slots__=('_instance','_file')
	@property
	def __class__(self):
		return type(self._instance)
	@property
	def __dict__(self):
		return self._instance.__dict__
	def __repr__(self):
		return self._instance.__repr__()
	def __float__(self):
		return self._instance.__float__()
	def __int__(self):
		return self._instance.__int__()
	def __bool__(self):
		return self._instance.__bool__()
	def __str__(self):
		return self._instance.__str__()
	def __eq__(self, other):
		return self._instance==other
	def __ne__(self, other):
		return self._instance!=other
	def __iter__(self):
		def generator():
			for i in self._instance.__iter__():
				yield FileViewer(i,self._file)
		return generator()
	def __getitem__(self,key):
		return FileViewer(self._instance[key],self._file)
	def model(self):
		''' Returns the model referenced by editor'''
		return self._instance
	def __init__(self, instance,file):
		self._instance=instance
		self._file=file
	def __dir__(self):
		from .complexbase import ContainerBase
		return list(d for d in self._instance.__dir__() if not isinstance(child:=getattr(self._instance,d),ContainerBase) or self._file in child.file())
	def __getattr__(self,name):
		from .complexbase import ContainerBase
		ret=getattr(self._instance,name)
		if isinstance(ret,ContainerBase):
			if self._file in ret.file():
				return FileViewer(ret,self._file)
			if not getattr(ret,'shortName',None)==name: 
				return ModelNone
			raise AttributeError(f'No child named {name} in {self.__repr__()}')
		if isinstance(ret,ModelList) or isinstance(ret,list):
			def generator():
				for i in ret:
					if self._file in i.file():
						yield FileViewer(i,self._file)
			return ModelList(generator)
		if callable(ret):
			def op(*args,**kwargs):
				return FileViewer(ret(*args,**kwargs),self._file)
			return op
		return FileViewer(ret,self._file)
	def edit(self,undo=None):
		#todo: get fileInfo
		from . import edit
		return edit.Editor(self,self._file,undo)
	def file(self):
		return (self._file,)
	