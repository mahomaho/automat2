# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2020 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

from .support import *
import lxml.etree as ET
from collections import defaultdict
import types,sys
import math
import inspect
import functools


AR_NS='{http://autosar.org/schema/r4.0}'
AR={None : AR_NS}
def no_ns(tag):
	return tag.replace(AR_NS,'')

class ContainerBase(object):
	_atpMixed=False
	_xml=()
	_parent=None
	_binding=None
	def _setup(self):
		self._instances.append(self)
	def _unsetup(self):
		self._instances.remove(self)
	def _setxml(self,xml,binding,parent,sort=False):
		self._parent=parent
		self._valueType._validate(xml.text)
		self._xml=(xml,)
		self._binding=binding
		if parent is not None:
			parent._children.append(self)
	def _appendxml(self,other):
		if self._xml[0].text!=other._xml[0].text:
			print(f'Error: Merging simple content at {self.__repr__()} where value differs {self._xml[0].text}:{self._xml[0].base} != {other._xml[0].text}:{other._xml[0].base}')
		if self._xml[0].attrib!=other._xml[0].attrib:
			print(f'Error: Merging simple content at {self.__repr__()} where value differs {self._xml[0].attrib}:{self._xml[0].base} != {other._xml[0].attrib}:{other._xml[0].base}')
		self._xml=(*self._xml,other._xml[0])
	def _merge(self,other,splitkey=None):
		if self._xml[0].text!=other._xml[0].text:
			print(f'Error: Merging simple content at {self.__repr__()} where value differs {self._xml[0].text}:{self._xml[0].base} != {other._xml[0].text}:{other._xml[0].base}')
		if self._xml[0].attrib!=other._xml[0].attrib:
			print(f'Error: Merging simple content at {self.__repr__()} where value differs {self._xml[0].attrib}:{self._xml[0].base} != {other._xml[0].attrib}:{other._xml[0].base}')
		self._xml=(*self._xml,other._xml[0])

	def __iter__(self):
		return singleiterator(self)
	def __getitem__(self,key):
		from ..autosar_r4p0 import Group
		if isinstance(key,int):
			assert key==0 or key==-1,f'{self.__repr__()} is not an array'
			return self
		assert isinstance(self,Group.Referrable),f'Trying to index {self.__repr__()} with {key} which is not possible'
		assert self.shortName.val()==key,f'Cannot index {self.__repr__()} with {key}'
		return self
	def __len__(self):
		return 1
	def __bool__(self):
		return True
	def editor(self,file=None,undo=None):
		#todo: get fileInfo
		from . import edit
		if file is not None:
			from .. import LoadFile
			file=LoadFile(file)
		return edit.Editor(self,file,undo)
	def __str__(self):
		return self.__repr__()[7:].replace('.','/')
	def __repr__(self):
		names=[]
		cont=self
		from ..autosar_r4p0 import Group
		while cont._parent is not None:
			if isinstance(cont,Group.Referrable):
				shortname=cont._xml[0].find('{http://autosar.org/schema/r4.0}SHORT-NAME')
				if shortname is not None:
					names.append(shortname.text)
				else:
					child=cont._parent.__dict__[cont._binding._childname]
					if child is cont:
						names.append(cont._binding._name)
					else:
						names.append(cont._binding._name+'['+str(child.index(cont))+']')
			else:
				child=cont._parent.__dict__[cont._binding._childname]
				if child is cont:
					names.append(cont._binding._name)
				else:
					names.append(cont._binding._name+'['+str(child.index(cont))+']')
			cont=cont._parent
		names.append('autosar')
		return '.'.join(reversed(names))
	def parent(self):
		return self._parent
	def file(self):
		def generator():
			for xml in self._xml:
				yield xml.base
		return ModelList(generator)
	def model(self,file=None):
		if file is None:
			return self
		assert file in self.file(),f'file {file} not in merged content of {self.__repr__()}'
		return FileViewer(self,file)
	def attributes(self):
		ret=[name for name,e in inspect.getmembers(self.__class__, lambda e:isinstance(e,Attribute))]
		ret.sort()
		return ret		
	def binding(self):
		return Aggregation(self.parent(),self._binding)
	def validate(self):
		pass

class ValueTypeBase():
	def _xmlstr(self):
		try:
			return self._xml[0].text
		except:
			return ''
	def val(self):
		ret=self._xml[0].text
		assert ret is not None
		#if ret is None:
		#	return ''
		return ret

class ModelReference(ValueTypeBase):
	"""This primitive denotes a name based reference. For detailed syntax see the xsd.pattern.

* first slash (relative or absolute reference) [optional]
* Identifier  [required]
* a sequence of slashes and Identifiers [optional]

This primitive is used by the meta-model tools to create the references."""
	from ..autosar_r4p0 import SimpleTypes
	_valueType=SimpleTypes.Ref
	_basetype=str
	def _setup(self):
		self._instances.append(self)
		try:
			self._dests[self.DEST.value]._references[self._valuepath()].append(self)
		except:
			pass
	def _unsetup(self):
		self._instances.remove(self)
		try:
			self._dests[self.DEST.value]._references[self._valuepath()].remove(self)
		except:
			pass
	def _valuepath(self):
		"get absolute path to reference if possible, else throw exeption"
		cont=self._xmlstr()
		if cont[0]!='/':
			return cont.rsplit('.',1)[-1]
		return 'autosar'+cont.replace('/','.')
	def ref(self):
		from .. import autosar
		try:
			return functools.reduce(getattr,self.val().split('/')[1:],autosar)
		except:
			return ModelNone

class EnumTypeBase(ValueTypeBase):
	_basetype=str
	def val(self):
		#try:
			return self._valueType(self._xml[0].text)
		#except:
			#return self._valueType.values()[0]

class StringTypeBase(ValueTypeBase):
	_basetype=str

class IntegerTypeBase(ValueTypeBase):
	_basetype=int
	def val(self):
		return int(self._xml[0].text,0)

class BooleanTypeBase(ValueTypeBase):
	_basetype=int
	def val(self):
		x=self._xml[0].text
		if x in ('true','1'):
			return True
		assert x in ('false','0')
		return False

class FloatTypeBase(ValueTypeBase):
	_basetype=float
	def val(self):
		return float(self._xml[0].text)

class NumericalTypeBase(ValueTypeBase):
	def val(self):
		return self._valueType(self._xml[0].text)
	@property
	def _basetype(self):
		try:
			return self.val()._valtype
		except:
			return float

class GroupBase(object):
	@classmethod
	def instances(cls):
		if issubclass(cls,ContainerBase):
			return cls._instances[:]
		ret=[]
		for subclass in cls.__subclasses__():
			if issubclass(subclass,ContainerBase):
				ret.extend(subclass._instances)
		return ret

class ComplexTypeBase(ContainerBase):
	def __init__(self):
		self._children=[]
	def children(self):
		def generator():
			for c in self._children:
				yield c
		return ModelList(generator)
	def _setup(self):
		self._instances.append(self)
		for element in self._xmlchildren.values():
			me=self
			while not isinstance(me,element._parenttype):
				me=me._parent
			element._setup(me)
	def _unsetup(self):
		self._instances.remove(self)
		for element in self._xmlchildren.values():
			me=self
			while not isinstance(me,element._parenttype):
				me=me._parent
			element._unsetup(me)
	def _sortxml(self,xml):
		if not self._atpMixed:
			order=tuple(self._xmlchildren.keys())
			def index(val):
				try:
					return order.index(no_ns(val.tag))
				except:
					return 0
			xml[:]=sorted(xml,key=index)
	def _setxml(self,xml,binding,parent,sort=False):
		self._xml=(xml,)
		self._binding=binding
		self._parent=parent
		if sort:
			self._sortxml(xml)
		for child in xml:
			tag=child.tag.removeprefix('{http://autosar.org/schema/r4.0}')
			if tag in self._xmlchildren:
				element=self._xmlchildren[tag]
				me=self
				while not isinstance(me,element._parenttype):
					me=me._parent
				element._setxml(me,child,sort)
		if parent:
			# replace with special solution for root element
			parent._children.append(self)
	def _appendxml(self,other):
		self._xml=(*self._xml,other._xml[0])
		for element in self._xmlchildren.values():
			me=self
			that=other
			while not isinstance(me,element._parenttype):
				me=me._parent
				that=that._parent
			element._appendxml(me,that)
	def _merge(self,other,splitkey=None):
		if splitkey is not None:
			splitelements= tuple(key.split('.')[0] for key in splitkey)
		else:
			splitelements=()
		self._xml=(*self._xml,other._xml[0])
		for element in self._xmlchildren.values():
			me=self
			that=other
			while not isinstance(me,element._parenttype):
				me=me._parent
				that=that._parent
			if element._name in splitelements:
				element._appendxml(me,that)
			else:
				element._merge(me,that)
	def _matchkey(self,*key):
		try:
			splitkeys=iter(self._binding._splitkey)
			for keyval in key:
				splitkey=next(splitkeys)
				ref=self
				for cont in splitkey.split('.'):
					ref=getattr(ref,cont)
				if str(ref.val())!=keyval:
					return False
			return True
		except StopIteration as e:
			raise AssertionError('Trying to index array with more arguments than splitkeys available')
		
	def __dir__(self):
		ret=[]
		for d in object.__dir__(self):
			if d[0]!='_':
				a=getattr(self,d)
				if a!=ModelNone:
					ret.append(d)
		return ret
	def aggregations(self):
		return tuple((Aggregation(self,e) for e in self._aggregations))

class MixedStringBase(ComplexTypeBase):
	def mixedstring(self):
		def generator():
			text=self._xml[0].text
			yield text.strip() if text else ''
			for child in self.children():
				yield child
				tail=child._xml[0].tail
				yield tail.strip() if tail else ''
		return ModelList(generator)

class InstanceRefBase(ComplexTypeBase):
	def iref(self):
		children=self.children()
		return InstanceRef([e.ref() for e in children[:-1]],children[-1].ref())

class PrototypeBase(ComplexTypeBase):
	def isOfType(self):
		return InstanceRef([self,],self.__dict__[self._isOfType].ref())

class EcucValueTypeBase(ComplexTypeBase):
	def _xmlstr(self):
		try:
			return str(self.value._xml[0].text)
		except:
			return ''
	def val(self):
		try:
			return self._valueType(self.value._xml[0].text)
		except:
			return default(ModelNone).self.definition.ref().defaultValue.val()
	@property
	def _valueType(self):
		from .. import autosar_r4p0
		#try:
		deftype=type(self.definition.ref())
		#except:
		#	return ModelNone
		if deftype is autosar_r4p0.EcucBooleanParamDef:
			return autosar_r4p0.SimpleTypes.Boolean
		elif deftype is autosar_r4p0.EcucIntegerParamDef:
			return autosar_r4p0.SimpleTypes.Integer
		elif deftype is autosar_r4p0.EcucFloatParamDef:
			return autosar_r4p0.SimpleTypes.Float
		elif deftype in (autosar_r4p0.EcucEnumerationParamDef,autosar_r4p0.EcucFunctionNameDef,autosar_r4p0.EcucLinkerSymbolDef,autosar_r4p0.EcucMultilineStringParamDef,autosar_r4p0.EcucStringParamDef):
			return autosar_r4p0.SimpleTypes.VerbatimString
		elif issubclass(deftype,autosar_r4p0.Group.EcucAbstractReferenceDef):
			return autosar_r4p0.SimpleTypes.Ref
		else:
			raise AssertionError('Automat error')

	@property
	def _basetype(self):
		from .. import autosar_r4p0
		try:
			deftype=type(self.definition.ref())
		except:
			return ModelNone
		if deftype is autosar_r4p0.EcucBooleanParamDef:
			return bool
		elif deftype is autosar_r4p0.EcucIntegerParamDef:
			return int
		elif deftype is autosar_r4p0.EcucFloatParamDef:
			return float
		elif deftype in (autosar_r4p0.EcucEnumerationParamDef,autosar_r4p0.EcucFunctionNameDef,autosar_r4p0.EcucLinkerSymbolDef,autosar_r4p0.EcucMultilineStringParamDef,autosar_r4p0.EcucStringParamDef):
			return str
		elif issubclass(deftype,autosar_r4p0.Group.EcucAbstractReferenceDef):
			return str
		else:
			raise AssertionError('Automat error')
	
class InstanceRef():
	def __init__(self,contexts,target):
		self._contexts=contexts
		self._target=target
	def isOfType(self):
		contexts=self._contexts[:]
		contexts.append(self._target)
		return InstanceRef(contexts,self._target.__dict__[self._target._isOfType].ref())
	def __dir__(self):
		return self._target.__dir__()
	def __repr__(self):
		ref=self._contexts[0]
		ret=ref.__repr__()
		contexts=self._contexts[1:]
		contexts.append(self._target)
		for context in contexts:
			targetpath=context.__repr__()
			root=ref.__dict__[ref._isOfType].ref().__repr__()
			assert targetpath.startswith(root)
			if root==targetpath:
				ret+='.isOfType()'
			else:
				ret+='.isOfType().'+targetpath.removeprefix(root+'.')
			ref=context
		return ret
	def __getattr__(self,name):
		from .. import autosar_r4p0
		ret=getattr(self._target,name)
		if isinstance(ret,autosar_r4p0.Group.ARObject):
			return InstanceRef(self._contexts[:],ret)
		return ret

class Aggregation(ModelList):
	_selected=None
	@property
	def __doc__(self):
		return self._element.__doc__
	def upperMultiplicity(self):
		return self._element._numInstancesMax
	def lowerMultiplicity(self):
		return self._element._numInstancesMin
	def name(self):
		return self._element._name
	def parent(self):
		return self._instance
	def isProperty(self):
		return self._element._property
	def desttype(self):
		return self._element._desttype if not isinstance(self._element._desttype,dict) else list(self._element._desttype.values())
	def __dir__(self):
		ret=list(e for e in object.__dir__(self) if e[0]!='_')
		if self._selected is None and self._element._numInstancesMax>1:
			ret.extend(e.__name__ for e in self._element._desttype.values())
		return ret
	def __repr__(self):
		ret=self._instance.__repr__()+'.'+self.name()
		if self._selected!=None:
			ret+='.'+self._selected
		return ret+super().__repr__()
	def __init__(self,instance,element):
		self._instance=instance
		self._element=element
		if element._numInstancesMax==1:
			def generator():
				e=getattr(self._instance,self._element._name)
				if e!=None:
					yield e
		else:
			def generator():
				elist=getattr(self._instance,self._element._name)
				for e in elist:
					if self._selected is None or type(e).__name__==self._selected:
						yield e			
		super().__init__(generator)
		if element._property:
			if not isinstance(element._desttype,type):
				desttype=next(iter(self._element._desttype.values()))
			else:
				desttype=self._element._desttype
			if issubclass(desttype,ModelReference):
				def values():
					ret=set()
					for group in desttype._dests.values():
						ret.update(group.instances())
					return [str(e) for e in ret]
				self.values=values
			elif hasattr(desttype._valueType,'values'):
				def values(self):
					return desttype._valueType.values()
				self.values=types.MethodType(values, self)
			elif(hasattr(desttype._valueType,'_regex')):
				def regex(self):
					return desttype._valueType._regex
				self.regex=types.MethodType(regex, self)
			def valuetype(self):
				return desttype._valueType.valuetype()
			self.valuetype=types.MethodType(valuetype, self)
	def __getattr__(self,val):
		if self._selected is None and self._element._numInstancesMax>1 and val in (e.__name__ for e in self._element._desttype.values()):
			ret=Aggregation(self._instance,self._element)
			ret._selected=val
			return ret
		return super().__getattr__(val)
	def validate(self):
		if not self._instance._atpMixed:
			mult=len(self)
			assert self.lowerMultiplicity()<=mult, 'multiplicity low'
			assert mult<=self.upperMultiplicity(), 'multiplicity to high'

class EcucAggregation(Aggregation):
	@property
	def __doc__(self):
		return self._definition.desc.l2[0].val()
	def upperMultiplicity(self):
		return math.inf if self._definition.upperMultiplicityInfinite.val() is True else default(1).self._definition.upperMultiplicity.val()
	def lowerMultiplicity(self):
		ret=default(0).self._definition.lowerMultiplicity.val()
		return ret
	def name(self):
		return self._definition.shortName.val()
	def definition(self):
		return self._definition
	def parent(self):
		return self._instance
	def isProperty(self):
		from .. import autosar_r4p0
		return isinstance(self._definition,(autosar_r4p0.Group.EcucParameterDef,autosar_r4p0.Group.EcucAbstractReferenceDef))
	def desttype(self):
		return self._definition.shortName.val()
	def _values_general(self):
		from .. import autosar_r4p0
		ret=[]
		#fixa: finns redan metod, och returnerqa objekt istallet for strangar
		try:
			dests=[self._definition.destination.ref()]
			for dest in dests[:]:
				path=[dest.shortName.val()]
				module=dest.parent()
				while type(module) is not autosar_r4p0.EcucModuleDef:
					path.append(module.shortName.val())
					module=module.parent()
				refined=module.references().parent().select.findall(lambda e: type(e) is autosar_r4p0.EcucModuleDef)
				pathreversed=tuple(path.__reversed__())
				refined=refined.select.foreach(lambda e:functools.reduce(getattr,pathreversed,e))
				dests.extend(refined)
			
			for d in dests:
				try:
					a=d.references().parent().select.findall(lambda e: isinstance(e,autosar_r4p0.Group.EcucIndexableValue))
					ret.extend(a)
				except:
					pass
		except:pass
		return ret
	def _values_EcucSymbolicNameReferenceDef(self):
		return [e for e in self._values_general() if e.definition.ref().symbolicNameValue.val()==True]
	def _values_EcucForeignReferenceDef(self):
		from .. import autosar_r4p0
		desttypexml=self._definition.destinationType.val()
		group=autosar_r4p0.EcucReferenceValue.value._desttype._dests.get(desttypexml,None)
		return list(group.instances())
	def _values_EcucInstanceReferenceDef(self):
		from .. import autosar_r4p0
		#InstanceRef()
		desttypexml=self._definition.destinationType.val()
		group=autosar_r4p0.EcucInstanceReferenceValue.value._desttype.target._desttype._dests.get(desttypexml,None)
		dests=set()
		dests.update(group.instances())
		contexttypexml=self._definition.destinationContext.val()
		group=autosar_r4p0.EcucInstanceReferenceValue.value._desttype.contextElement._desttype._dests.get(contexttypexml,None)
		contexts=set()
		contexts.update(group.instances())
		ret=[]
		for context in contexts:
			isOfType=context.isOfType().model().__repr__()
			for dest in dests:
				if dest.__repr__().startswith(isOfType):
					ret.append(InstanceRef([context,],dest))
		return ret
	def __init__(self,instance,definition):
		from .. import autosar_r4p0
		self._instance=instance
		if isinstance(definition,autosar_r4p0.Group.EcucContainerDef):
			self._element=instance.__class__.container if isinstance(instance,autosar_r4p0.EcucModuleConfigurationValues) else instance.__class__.subContainer
		elif isinstance(definition,autosar_r4p0.Group.EcucParameterDef):
			self._element=instance.__class__.parameterValue
		elif isinstance(definition,autosar_r4p0.Group.EcucAbstractReferenceDef):
			self._element=instance.__class__.referenceValue
		self._definition=definition
		def generator():
			elist=getattr(self._instance,self._element._name)
			defpath=self._definition.__repr__()[7:].replace('.', '/')
			for e in elist:
				if e.definition.val() == defpath:
					yield e			
		ModelList.__init__(self,generator)
		if self.isProperty():
			deftype=type(self._definition)
			if isinstance(definition,autosar_r4p0.Group.EcucAbstractReferenceDef):
				if deftype is autosar_r4p0.EcucForeignReferenceDef:
					values=self._values_EcucForeignReferenceDef
				elif deftype is autosar_r4p0.EcucReferenceDef:
					values=self._values_general
				elif deftype is autosar_r4p0.EcucSymbolicNameReferenceDef:
					values=self._values_EcucSymbolicNameReferenceDef
				elif deftype is autosar_r4p0.EcucChoiceReferenceDef:
					values=self._values_general
				elif deftype is autosar_r4p0.EcucInstanceReferenceDef:
					values=self._values_EcucInstanceReferenceDef
				else:
					raise AssertionError('Automat error')
				self.values=values
				def valuetype():
					return str
			else:
				if deftype is autosar_r4p0.EcucBooleanParamDef:
					def valuetype():
						return bool
					def values():
						return ['true','false']
					self.values=values
				elif deftype is autosar_r4p0.EcucIntegerParamDef:
					def valuetype():
						return int
				elif deftype is autosar_r4p0.EcucFloatParamDef:
					def valuetype():
						return float
				elif deftype in (autosar_r4p0.EcucEnumerationParamDef,autosar_r4p0.EcucFunctionNameDef,autosar_r4p0.EcucLinkerSymbolDef,autosar_r4p0.EcucMultilineStringParamDef,autosar_r4p0.EcucStringParamDef):
					def valuetype():
						return str
					if deftype is autosar_r4p0.EcucEnumerationParamDef:
						def values(self):
							return list((name.val() for name in self._definition.literal.shortName))
						self.values=types.MethodType(values, self)
					else:
						if deftype is autosar_r4p0.EcucLinkerSymbolDef:
							# see TPS_ECUC_02030
							def regex():
								return '[a-zA-Z][a-zA-Z0-9_.$%]{0,254}'
							self.regex=regex
						elif deftype is autosar_r4p0.EcucFunctionNameDef:
							# see TPS_ECUC_06075
							def regex():
								return '[a-zA-Z_][a-zA-Z0-9_]*'
							self.regex=regex
						elif self._definition.regularExpression!=None:
							def regex(self):
								return self._definition.regularExpression.val()
							self.regex=types.MethodType(regex, self)
				else:
					raise AssertionError('Automat error')
			self.valuetype=valuetype
	def __getattr__(self,val):
		ModelList.__getattr__(self, val)	
	def validate(self):
		assert self._definition!=None,'missing or not loaded module definition'
		mult=len(self)
		assert self.lowerMultiplicity()<=mult, 'multiplicity low'
		assert mult<=self.upperMultiplicity(), 'multiplicity to high'

class DefaultValueType():
	def __init__(self,parent,definition):
		self._parent=parent
		self._def=definition
	def val(self):
		return self._def.defaultValue.val()
	def parent(self):
		return self._parent
	@property
	def definition(self):
		return self._def
	def validate(self):
		pass
	def editor(self,*args,**kwargs):
		name=self._def.shortName.val()
		setattr(self._parent.editor(*args,**kwargs),name,self.val())
		return getattr(self._parent,name).editor(*args,**kwargs)
	def model(self):
		return self
	def __repr__(self):
		return repr(self._parent)+'.'+self._def.shortName.val()
	def __str__(self):
		return repr(self._parent)+'/'+self._def.shortName.val()

class ElementList(ModelList):
	_selected=None
	def __dir__(self):
		ret=ModelList.__dir__(self)
		if self._selected is None:
			ret.extend((e.__name__ for e in self._element._desttype.values()))
		if self._element._numInstancesMax==1 and (self._selected is None or len(self)==1 and type(self[0]) is self._selected):
			ret.extend(dir(self[0]))
		return ret
	@property
	def __doc__(self):
		return 'List of all objects in '+self._element._name+('' if self._selected is None else 'of type '+self._selected.__name__)
	def __init__(self,generator,instance,element):
		super().__init__(generator)
		self._instance=instance
		self._element=element
	def __getattr__(self,val):
		if self._selected is None and type(self._element._desttype) is dict:
			subtype=next(iter(e[1] for e in self._element._desttype.items() if e[1].__name__==val),None)
			if subtype is not None:
				def generator():
					for e in self._g():
						if type(e) is subtype:
							yield e
				ret=ElementList(generator,self._instance,self._element)
				ret._selected=subtype
				return ret
		if self._element._numInstancesMax==1 and (self._selected is None or len(self)==1 and type(self[0]) is self._selected):
				return getattr(self[0],val)
		return super().__getattr__(val)
	def binding(self):
		return Aggregation(self._instance, self._element)
	def validate(self):
		return self.binding().validate()

class EcucList(ElementList):
	def __init__(self,generator,instance,definition):
		ModelList.__init__(self,generator)
		self._instance=instance
		self._definition=definition
	def __getattr__(self,val):
		if self._definition.upperMultiplicity.val()==1 and len(self)==1:
			return getattr(next(self.__iter__()),val)
		return ModelList.__getattr__(self,val)
	def __getitem__(self,key):
		vals=tuple(self.__iter__())
		assert len(vals)>0,'Cannot get item from empty list'
		if isinstance(key,int):
			return vals[key]
		elif isinstance(key,slice):
			print('todo')
		else:
			from ..autosar_r4p0 import Group
			return next((i for i in vals if isinstance(i,Group.Referrable) and i.shortName.val()==key),ModelNone)
	def binding(self):
		return EcucAggregation(self._instance,self._definition)
		
	
class Element(object):
	_numInstancesMax=1
	__slots__=('_parenttype','_name','_property','_numInstancesMin','_splitable','_splitkey','_xmlname','_desttype','_childname','__doc__')
	def _setup(self,parent):
		child=parent.__dict__[self._childname]
		if child is not ModelNone:
			child._setup()
	def _unsetup(self,parent):
		child=parent.__dict__[self._childname]
		if child!=ModelNone:
			child._unsetup()
	def __init__(self,parenttype,name,property,numInstancesMin,splitable,splitkey,xmlname,desttype,childname,doc):
		self._parenttype=parenttype
		self._name=name
		self._property=property
		self._numInstancesMin=numInstancesMin
		self._splitable     =splitable     
		self._splitkey       =splitkey
		self._xmlname        =xmlname
		self._desttype       =desttype
		self._childname=childname
		self.__doc__=doc
	def _setxml(self,instance,xml,sort):
		childobj=self._desttype()
		try:
			instance.__dict__[self._childname]=childobj # must be set to be able to get path
			childobj._setxml(xml,self,instance,sort)
		except:
			instance.__dict__[self._childname]=ModelNone
			print(f'Invalid content {xml.text} at {instance.__str__()}/{self._name}, object will be deleted')
			xml.getparent().remove(xml)
	def _appendxml(self,to,other):
		otherval=other.__dict__[self._childname]
		if otherval==ModelNone:
			return
		otherval._parent=to
		toval=to.__dict__[self._childname]
		if toval==ModelNone:
			to.__dict__[self._childname]=otherval
			to._children.append(otherval)
			otherval._setup()
			return
		toval._appendxml(otherval)
	def _merge(self,to,other):
		otherval=other.__dict__[self._childname]
		if otherval==ModelNone:
			return
		otherval._parent=to
		toval=to.__dict__[self._childname]
		if toval==ModelNone:
			to.__dict__[self._childname]=otherval
			to._children.append(otherval)
			otherval._setup()
			return
		if not self._splitable:
			print(f'Error: Trying to merge unmergeable content at {toval.__repr__()} from files {[otherval._xml[0].base,*(xml.base for xml in toval._xml)]}')
			return
		toval._merge(otherval)
	def __get__(self, instance, owner):
		if instance is None:
			return self
		return instance.__dict__[self._childname]
	def __set__(self,instance,value):
		raise AttributeError() # to make it a data descriptor

class Elements(object):
	__slots__=('_parenttype','_name','_property','_numInstancesMin','_splitable','_splitkey','_numInstancesMax','_xmlname','_desttype','_childname','__doc__')
	def __init__(self,parenttype,name,property,numInstancesMin,splitable,splitkey,numInstancesMax,xmlname,childtypes,childname,doc):
		self._parenttype=parenttype
		self._name=name
		self._property=property
		self._numInstancesMin=numInstancesMin
		self._splitable=splitable     
		self._splitkey=splitkey
		self._numInstancesMax=numInstancesMax
		self._xmlname=xmlname
		self._desttype=childtypes
		self._childname=childname
		self.__doc__=doc
	def _setup(self,parent):
		children=parent.__dict__[self._childname]
		for child in children:
			child._setup()
	def _unsetup(self,parent):
		children=parent.__dict__[self._childname]
		for child in children:
			child._unsetup()
	def __get__(self, instance, owner):
		if instance is None:
			return self
		def generator():
			for i in instance.__dict__[self._childname]:
				yield i
		generator.__doc__=self.__doc__
		return ElementList(generator,instance,self)
	def __set__(self,instance,value):
		raise AttributeError() # to make it a data descriptor
	def _appendxml(self,to,other):
		otherval=other.__dict__[self._childname]
		if len(otherval)==0:
			return
		for val in otherval:
			val._parent=to
		toval=to.__dict__[self._childname]
		if len(toval)==0:
			to.__dict__[self._childname]=otherval
			to._children.extend(otherval)
			for v in otherval:
				v._setup()
			return
		assert 0,f'Trying to merge unmergeable content'
	def _key(self,cont):
		def generator(cont,splitkey):
			for key in splitkey:
				ret=cont
				for spl in key.split('.'):
					ret=getattr(ret,spl,ModelNone)
				yield str(ret.val())
			yield type(cont).__name__
		return ','.join(generator(cont,self._splitkey))
	def _sortxml(self,instance):
		if isinstance(self._desttype,dict):
			children=instance.__dict__[self._childname]
			if len(children)>1:
				xml=children[0]._xml[0].getparent()
				children.sort(key=lambda e:e._binding._key(e))
				xml[:]=[child._xml[0] for child in children]
	def _merge(self,to,other):
		otherval=other.__dict__[self._childname]
		if len(otherval)==0:
			return
		for val in otherval:
			val._parent=to
		toval=to.__dict__[self._childname]
		if len(toval)==0:
			to.__dict__[self._childname]=otherval
			to._children.extend(otherval)
			for v in otherval:
				v._setup()
			return
		if not self._splitable:
			print(f'Error: Trying to merge unmergeable content arrays at {to.__repr__()}.{self._name}, content from {otherval[0]._xml[0].base} will be ignored')
			return
		merge={}
		for t in toval:
			key=self._key(t)
			merge[key]=t
		for o in otherval:
			key=self._key(o)
			if key in merge:
				merge[key]._merge(o,self._splitkey)
			else:
				to._children.append(o)
				o._setup()
				merge[key]=o
		children=list(merge.values())
		assert len(children) <= self._numInstancesMax, f'To many child instances ({len(children)} > {self._numInstancesMax}) of {self._name} in {to.__repr__()} after merge'
		to.__dict__[self._childname]=children
	def _setxml(self,instance,xml,sort):
		if isinstance(self._desttype,dict):
			children=instance.__dict__[self._childname]
			for child in xml:
				tag=no_ns(child.tag)
				if tag in self._desttype:
					assert len(children) < self._numInstancesMax, f'To many child instances in {no_ns(xml.tag)}, file {child.base}, line {child.sourceline}'
					childtype=self._desttype[tag]
					childobj=childtype()
					try:
						children.append(childobj) # must be set to be able to get path
						childobj._setxml(child,self,instance,sort)
					except:
						children.pop()
						print(f'Invalid content {child.text} at {instance.__str__()}, object will be deleted')
						child.getparent().remove(child)
			if sort:
				self._sortxml(instance)
		else:
			children=instance.__dict__[self._childname]
			assert len(children) < self._numInstancesMax, f'To many child instances in {no_ns(xml.tag)}, file {child.base}, line {child.sourceline}'
			childobj=self._desttype()
			try:
				children.append(childobj) # must be set to be able to get path
				childobj._setxml(xml,self,instance,sort)
			except:
				children.pop()
				print(f'Invalid content  {xml.text} at {instance.__str__()}, object will be deleted')
				xml.getparent().remove(xml)

class Attribute:
	__slots__=('_name','_numInstancesMin','_valueType','_xmlname','_required','__doc__')
	_property=True
	_numInstancesMax=1
	def __init__(self,name,valuetype,xmlname,required,doc):
		self._name=name
		self._numInstancesMin=int(required)
		self._valueType=valuetype
		self._xmlname   =xmlname
		self._required  =required
		self.__doc__=doc
	def __get__(self, instance, owner):
		if instance is None:
			return self
		xml=instance._xml[0]
		if self._xmlname in xml.attrib:
			return self._valueType(xml.attrib[self._xmlname])
		return ModelNone
	def __set__(self,instance,value):
		raise AttributeError() # to make it a data descriptor
