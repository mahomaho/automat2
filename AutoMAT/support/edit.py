# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2020 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

from lxml import etree as ET
from .complexbase import *
from .support import *
from copy import deepcopy
from ..autosar_r4p0 import Group
from .. import *
from .. import files as fileH
import math

def _dirmembers(cont):
	ret=set()
	for c in cont.__iter__():
		ret.update((d for d in object.__dir__(c) if d[0]!='_'))
	return ret

def _createxml(cont,element,file,newtype,setup):
	fileH.modified(file)
	parentlist=((type(cont),[]),)
	xmlparent=None
	while True:
		nextlist=[]
		for parent,path in parentlist:
			for xmlname,e in parent._xmlchildren.items():
				if e is element:
					xmlparent=xmlname
					break
					#return path,xmlname
				elif type(e._desttype) is dict:
					if len(e._desttype)==1:
						desttype=e._desttype.values().__iter__().__next__()
						nextlist.append((desttype,(*path,(e._name,desttype))))
				elif issubclass(e._desttype,ComplexTypeBase):
					nextlist.append((e._desttype,(*path,(e._name,e._desttype))))
		if xmlparent is not None:
			break
		assert len(nextlist) > 0, 'bug in automat'
		parentlist=nextlist
	xmlparent=cont
	_complement(xmlparent,file)
	for p,desttype in path:
		ret=getattr(xmlparent,p)
		if ret==ModelNone:
			xmlparent=_create(xmlparent,getattr(type(xmlparent),p),file,newtype=desttype,setup=setup)
		else:
			if isinstance(ret,ModelList):
				xmlparent=ret[0]
			else:
				xmlparent=ret				
			_complement(xmlparent,file)

	parentxml=next((xml for xml in xmlparent._xml if xml.base==file))
	if isinstance(element,Elements):
		# add eventual group
		if isinstance(element._desttype,dict):
			groupparentxml=parentxml.find(AR_NS+xmlname)
			if groupparentxml is None:
				groupparentxml=ET.SubElement(parentxml,AR_NS+xmlname)
			assert newtype is not None,'Cannot create child to node where multiple types are supported without type specified'
			newxml=ET.SubElement(groupparentxml,AR_NS+next((xname for xname,atype in element._desttype.items() if atype is newtype)))
			newxml.text=''
		else:
			newtype=element._desttype
			newxml=ET.SubElement(parentxml,AR_NS+xmlname)
			newxml.text=''
	else:
		newtype=element._desttype
		newxml=ET.SubElement(parentxml,AR_NS+xmlname)
		newxml.text=''
	return newxml

def _complement(cont,file):
	if str(file) in (xml.base for xml in cont._xml):
		return
	assert cont._parent is not None,'automat error'
	assert cont._binding._splitable, f'current action will result in creation of a container split at {cont.__repr__()} for a non splitable container'

	assert len(cont._xml) > 0, 'automat error'
	newxml=_createxml(cont._parent,cont._binding,file,type(cont),setup=False)
	val=cont._xml[0]
	newxml.text=val.text
	newxml.attrib.update(val.attrib)
	for key in cont._binding._splitkey:
		keyroot=key.split('.')[0]
		try:
			elem=getattr(type(cont),keyroot)
		except:
			#sheck for shortname if key does not exist
			elem=getattr(type(cont),'shortName',None)
			if elem is None:
				continue
		subelement=val.find(AR_NS+elem._xmlname)
		if subelement is not None:
			newxml.append(deepcopy(subelement))
	e=type(cont)()
	e._setxml(newxml,cont._binding,cont._parent)
	cont._appendxml(e)
	
def _create(parent,element,file,val=None,newtype=None,setup=True):
	newxml=_createxml(parent,element,file,newtype,setup=setup)
	if val is not None:
		if issubclass(newtype,Group.Referrable):
			binding=getattr(newtype,'shortName')
			val=binding._desttype._valueType(val)
			shortName=ET.SubElement(newxml,AR_NS+'SHORT-NAME')
			shortName.text=str(val)
		elif issubclass(newtype,ModelReference) and isinstance(val,Group.Referrable):
			if type(val) is Editor:
				val=val._instance
			value='/'+'/'.join(val.__repr__().split('.')[1:])
			newxml.text=value
			xmlname=None
			for subtype in val.__class__.__mro__[1:]:
				try:
					xmlname=next((xmlname for xmlname,group in newtype._dests.items() if group is subtype))
					break
				except:
					pass
			assert xmlname is not None, f'{val} is not a valid destination for property'
			newxml.attrib['DEST']=xmlname			
		else:
			val=newtype._valueType(val)
			val=val.__str__() if not isinstance(val,support.simplebase.Enum) else val.value
			newxml.text=val
	e=newtype()
	childref=parent.__dict__[element._childname]
	if type(childref) is list:
		childref.append(e)
	else:
		parent.__dict__[element._childname]=e
	e._setxml(newxml,element,parent)
	if setup:
		e._setup()
	return e
		
def _createchild_ContainerBase(self,name,selectedFile,val=None):
	binding=getattr(type(self),name,None)
	assert binding is not None,f'No child named {name} in {self.__repr__()}'
	if type(binding) is Attribute:
		if val is not None:
			assert selectedFile is None or len(self._xml)==1 and selectedFile==self._xml[0].base,'File selection makes it impossible to update attribute'
			val=str(binding._valueType(val))
			for xml in self._xml:
				fileH.modified(xml.base)
				xml.attrib[binding._xmlname]=val
		return ModelNone
	assert type(binding) in (Element,Elements), f'{name} is not a valid property of container'
	if binding._property:
		if val is None:
			return ModelNone
	else:
		assert val is None,'Currently only support for setting values to properties'
	if isinstance(binding._desttype,dict):
		assert val is None,'Currently only support for setting values to properties, no array updates'
		assert len(binding._desttype)==1, 'Cannot create arraymember when multiple member types supported and no type selected'
		newtype=next(binding._desttype.values().__iter__())
	else:
		newtype=binding._desttype
	files=iter((xml.base for xml in self._xml) if selectedFile is None else (selectedFile,))
	newcont=_create(self,binding,next(files),val,newtype)
	for file in files:
		_complement(newcont,file)
	return newcont
ContainerBase._createchild=_createchild_ContainerBase

def _set_ModelReference(self,selectedFile,value):
	if isinstance(value,autosar_r4p0.Group.Referrable):
		if type(value) is Editor:
			value=value._instance
		if self.val()==str(value):
			return
		self._unsetup()
		xmlname=None
		for subtype in value.__class__.__mro__:
			if subtype in type(self)._dests.values():
				xmlname,group=next(((xmlname,group) for xmlname,group in type(self)._dests.items() if group is subtype))
				break
		assert xmlname is not None, f'{value} is not a valid destination for property'
		value='/'+'/'.join(value.__repr__().split('.')[1:])
		for xml in self._xml:
			fileH.modified(xml.base)
			xml.text=value
			xml.attrib['DEST']=xmlname
		self._setup()
		return
	super(ModelReference,self)._set(selectedFile,value)
ModelReference._set=_set_ModelReference
	
def _set_ValueTypeBase(self,selectedFile,value):
	toval=self._valueType(value)
	if toval==self.val():
		return
	if self._binding._name=='shortName':
		if selectedFile is not None:
			assert selectedFile in (xml.base for xml in self._xml), 'Automat error'
			if len(self._xml)>1:
				raise AssertionError('Currently no support for model copy by rename')
# 					editor=Edit(self._parent,selectedFile,self._undo)
# 					xml=next(xml for xml in self._parent._xml if xml.base==str(selectedFile))
# 					newxml=deepcopy(xml)
# 					newxml.find(AR_NS+'SHORT-NAME').text=str(value)
# 					xml.getparent().append(newxml)
# 					editor.delete()
# 					newchild=type(self._parent)()
# 					newchild._setxml(newxml,self._parent._binding,self._parent._parent)
# 					self._parent._merge(newchild,self._parent._binding._splitkey)
# 					return
		self._parent._unsetup()
		for xml in self._xml:
			fileH.modified(xml.base)
			xml.text=value
		self._parent._setup()
		return
	if selectedFile is not None:
		assert len(self._xml)==1 and selectedFile==self._xml[0].base,'Cannot set splitted property when not all variants are updated'
	for xml in self._xml:
		fileH.modified(xml.base)
		toval=toval.__str__() if not isinstance(toval,support.simplebase.Enum) else toval.value
		xml.text=toval
	#self._setup()
ValueTypeBase._set=_set_ValueTypeBase

def _set_ComplexTypeBase(self,selectedFile,value):
	raise AssertionError('Currently no support for setting non properties')
ComplexTypeBase._set=_set_ComplexTypeBase

def _set_EcucTextualParamValue(self,selectedFile,value):
	#todo validate value
	Editor(self).value=value
Group.EcucTextualParamValue._set=_set_EcucTextualParamValue
def _set_EcucNumericalParamValue(self,selectedFile,value):  
	#todo validate value
	if isinstance(value,str) and isinstance(self.definition.ref(),autosar_r4p0.EcucBooleanParamDef):
		if value=='true':
			value=1
		elif value=='false':
			value=0
		else:
			raise ValueError(f'{value} is not a valid value for {self.definition.ref().shortName.val()}')
	Editor(self).value=value
Group.EcucNumericalParamValue._set=_set_EcucNumericalParamValue
def _set_EcucAddInfoParamValue(self,selectedFile,value):
	#todo validate value
	Editor(self).value=value
Group.EcucAddInfoParamValue._set=_set_EcucAddInfoParamValue
def _set_EcucReferenceValue(self,selectedFile,value):
	#todo validate value
	Editor(self).value=value
Group.EcucReferenceValue._set=_set_EcucReferenceValue
def _set_EcucInstanceReferenceValue(self,selectedFile,value):
	#todo validate value
	Editor(self).value=value
Group.EcucInstanceReferenceValue._set=_set_EcucInstanceReferenceValue
# def _createchild_EcucNumericalParamValue(self,name,selectedFile,val=None):
# 	#todo: validate value
# 	super(Group.EcucNumericalParamValue,self)._createchild(name,selectedFile,val)
# Group.EcucNumericalParamValue._createchild=_createchild_EcucNumericalParamValue

def _createchild_EcucModuleConfigurationValues(self,name,selectedFile,val=None):
	try:
		_def=self.definition.ref()
		newdef=getattr(_def,name)
		if not isinstance(newdef,Group.EcucDefinitionElement):
			raise
	except:
		return super(Group.EcucModuleConfigurationValues,self)._createchild(name,selectedFile,val)
	if val is None:
		val=name
	ret=self.editor(file=selectedFile).container[val]
	ret.definition=newdef
	return ret._instance
Group.EcucModuleConfigurationValues._createchild=_createchild_EcucModuleConfigurationValues

def _createchild_EcucContainerValue(self,name,selectedFile,val=None):
	try:
		_def=self.definition.ref()
		newdef=getattr(_def,name)
		if not isinstance(newdef,Group.EcucDefinitionElement):
			raise
	except:
		return super(Group.EcucContainerValue,self)._createchild(name,selectedFile,val)
	if isinstance(newdef,Group.EcucContainerDef):
		if val is None:
			val=name
		ret=self.editor(file=selectedFile).subContainer[val]
		ret.definition=newdef
		return ret._instance
	elif isinstance(newdef,Group.EcucParameterDef):
		assert val is not None,'Property must have value specified'
		ret=self.editor(file=selectedFile)
		if isinstance(newdef,Group.EcucIntegerParamDef):
			ret=ret.parameterValue.EcucNumericalParamValue
		elif isinstance(newdef,Group.EcucEnumerationParamDef):
			ret=ret.parameterValue.EcucTextualParamValue
		elif isinstance(newdef,Group.EcucFloatParamDef):
			ret=ret.parameterValue.EcucNumericalParamValue
		elif isinstance(newdef,Group.EcucFunctionNameDef):
			ret=ret.parameterValue.EcucTextualParamValue
		elif isinstance(newdef,Group.EcucLinkerSymbolDef):
			ret=ret.parameterValue.EcucTextualParamValue
		elif isinstance(newdef,Group.EcucMultilineStringParamDef):
			ret=ret.parameterValue.EcucTextualParamValue
		elif isinstance(newdef,Group.EcucStringParamDef):
			ret=ret.parameterValue.EcucTextualParamValue
		elif isinstance(newdef,Group.EcucAddInfoParamDef):
			ret=ret.parameterValue.EcucAddInfoParamValue
		elif isinstance(newdef,Group.EcucBooleanParamDef):
			if val==True or val=='true':
				val=1
			elif val==False or val=='false':
				val=0
			else:
				raise ValueError(f'Cannot set value {value} to boolean parameter')
			ret=ret.parameterValue.EcucNumericalParamValue
		else:
			raise AssertionError('automat error')
		if newdef.upperMultiplicity.val()==1:
			# add or use first with correct definition
			newcont=next((e for e in ret.model() if e.definition!=None and e.definition.ref()==newdef),None)
			if newcont is None:
				ret=ret.append()
			else:
				ret=newcont.editor(file=selectedFile)
		else:
			# add one container
			ret=ret.append()
		ret.definition=newdef
		ret.value=val
		return ret._instance
	elif isinstance(newdef,Group.EcucAbstractReferenceDef):
		assert val is not None,'Property must have value specified'
		ret=self.editor(file=selectedFile)
		if isinstance(newdef,Group.EcucInstanceReferenceDef):
			ret=ret.referenceValue.EcucInstanceReferenceValue.append()
		else:
			ret=ret.referenceValue.EcucReferenceValue.append()
		ret.definition=newdef
		ret.value=val
		return ret._instance
	else:
		raise AssertionError('Automat error')
Group.EcucContainerValue._createchild=_createchild_EcucContainerValue
#todo: add value validator class
#autosar_r4p0.NumericalValueVariationPoint._valueType=

def _createchild_ElementList(self,selectedFile,val=None):
	if isinstance(self._element._desttype,dict):
		#assert val is None,'Currently only support for setting values to properties, no array updates'
		newtype=self.__dict__.get('_selected')
		if newtype==None:
			assert len(self._element._desttype)==1, 'Cannot create arraymember when multiple member types supported and no type selected'
			newtype=next(self._element._desttype.values().__iter__())
	else:
		newtype=self._element._desttype
	if self._element._property:
		assert val is not None,'Cannot create property array memeber without value specified'
	else:
		assert val is None or issubclass(newtype,Group.Referrable),'Currenlty no support for setting non property array members'
	files=iter((xml.base for xml in self._instance._xml) if selectedFile is None else (selectedFile,))
	newcont=_create(self._instance,self._element,next(files),val,newtype)
	for file in files:
		_complement(newcont,file)
	return newcont
ElementList._createchild=_createchild_ElementList

def _createchild_EcucList(self,selectedFile,val=None):
	if isinstance(self._definition,Group.EcucContainerDef): #EcucParamConfContainerDef
		if val is None:
			val=self._definition.shortName.val()
		assert val not in (child.shortName.val() for child in self._instance._children if isinstance(child,Group.Referrable))
		if isinstance(self._instance,Group.EcucModuleConfigurationValues):
			ret=self._instance.editor(file=selectedFile).container[val]
		else:
			ret=self._instance.editor(file=selectedFile).subContainer[val]
		ret.definition=self._definition
		return ret._instance
	elif isinstance(self._definition,Group.EcucAbstractReferenceDef): #EcucReferenceDef
		_def=self._definition
		assert len(self)<default(math.inf)._def.upperMultiplicity.val(),'upperMultiplicity already reached'
		return self._instance._createchild(_def.shortName.val(),selectedFile,val)
	else:
		_def=self._definition
		assert len(self)<default(math.inf)._def.upperMultiplicity.val(),'upperMultiplicity already reached'
		return self._instance._createchild(_def.shortName.val(),selectedFile,val)
EcucList._createchild=_createchild_EcucList

class Editor(object):
	__slots__=('_instance','_file','_undo','append')
	def _createchild(self,binding,val=None,newtype=None):
		if isinstance(self._instance,ElementList):
			parent=self._instance._instance
		else:
			parent=self._instance
		if isinstance(binding._desttype,dict):
			if newtype==None:
				assert len(binding._desttype)==1, 'Cannot create arraymember when multiple member types supported and no type selected'
				newtype=next(binding._desttype.values().__iter__())
			else:
				newtype=next(t for t in binding._desttype.values() if t.__name__==newtype)
		else:
			newtype=binding._desttype
		files=iter((xml.base for xml in parent._xml) if self._file is None else (self._file,))
		newcont=_create(parent,binding,next(files),val,newtype)
		for file in files:
			_complement(newcont,file)
		#newcont._setup()
		return newcont
	@property
	def __class__(self):
		return type(self._instance)
	@property
	def __dict__(self):
		return self._instance.__dict__
	def __repr__(self):
		args=[]
		if self._file is not None:
			args.append("file="+self._file._file)
		if self._undo is not None:
			args.append("undo="+self._undo.__repr__())
		return f'{self._instance.__repr__()}.editor({",".join(args)})'
	def __eq__(self, other):
		return self._instance==other
	def __ne__(self, other):
		return self._instance!=other
	def model(self):
		''' Returns the model referenced by editor'''
		return self._instance
	def __init__(self, instance,file=None,undo=None):
		object.__setattr__(self,'_instance',instance)
		object.__setattr__(self,'_file',file)
		object.__setattr__(self,'_undo',undo)
		if isinstance(instance,ContainerBase):
			if file is not None:
				_complement(instance, file)
		elif isinstance(instance, ElementList):
			object.__setattr__(self,'append',lambda val=None:Editor(self._instance._createchild(self._file,val),self._file,self._undo))
			if file is not None:
				_complement(instance._instance,file)
	def __dir__(self):
		ret=list((d for d in type.__dir__(Editor) if d[0]!='_'))
		ret+=list((d for d in object.__dir__(self._instance) if d[0]!='_'))
		ret+=self._instance.__dir__()
		return list(set(ret))
	def set(self,value):
		vals=tuple(self._instance)
		assert len(vals)==1,'Cannot set value to non existing element or array of elements'
		val=vals[0]
		val._set(self._file,value)
		return
	def __iter__(self):
		def generator():
			for i in self._instance.__iter__():
				yield Editor(i,self._file,self._undo)
		return generator()
	def __getattr__(self,name):
		ret=getattr(self._instance,name)
		if callable(ret):
			def op(*args,**kwargs):
				res=ret(*args,**kwargs)
				if issubclass(type(res),ContainerBase): # use subclass instead of isinstance to handle edit call
					return Editor(res,self._file,self._undo)
				return res
			return op
		elif type(ret) is ElementList:
			if ret._element._numInstancesMax==1:
				#if len(ret)==1:
				#	return Editor(ret[0],self._file,self._undo)
				if isinstance(ret._element._desttype,type) or ret._selected is not None:
					return Editor(ret,self._file,self._undo)[0]
			return Editor(ret,self._file,self._undo)
		elif isinstance(ret,ModelList):
			return Editor(ret,self._file,self._undo)
		elif isinstance(ret,ContainerBase):	
			return Editor(ret,self._file,self._undo)
		elif ret==ModelNone:
			assert isinstance(self._instance,ContainerBase),'Automat error'
			ret=self._instance._createchild(name,self._file)
			if ret is ModelNone:
				return ret
			return Editor(ret,self._file,self._undo)
		else:
			return ret
	def __setattr__(self, name, value):
		if not isinstance(self._instance,ContainerBase):
			assert self._instance._element._numInstancesMax==1,'Cannot set parameters on arrays'
			if len(self._instance)==0:
				setattr(self[0],name,value)
				return
			object.__setattr__(self,'_instance',self._instance.__iter__().__next__())
		binding=getattr(type(self._instance),name,None)
		if isinstance(binding,Attribute):
			valuetypevalue=binding._valueType(value)
			value=valuetypevalue.__str__() if not isinstance(valuetypevalue,support.simplebase.Enum) else valuetypevalue.value
			assert self._file is None or len(self._xml)==1 and self._file==self._xml[0].base,'Cannot update attribute when file specified on splitted model'			
			for xml in self._xml:
				xml.attrib[binding._xmlname]=value
				fileH.modified(xml.base)
			return
		res=getattr(self._instance,name)
		if res==None or isinstance(res,DefaultValueType):
			self._instance._createchild(name,self._file, value)
		else:
			Editor(res,self._file,self._undo).set(value)
	def __getitem__(self,key):
		#if not isinstance(self._instance,ModelList):
		#	raise AttributeError('Object is not a list')
		if isinstance(key,int):
			length=len(self._instance)
			if key >=0:
				if key>=length:
					for i in range(length,key):
						self.append()
					return self.append()
			else:
				key=1-key
				if key>=length:
					assert False,"currently no support for insert before"
			ret=self._instance[key]
			return Editor(ret,self._file,self._undo)
		elif isinstance(key,str):
			try:
				ret=self._instance[key]
				if ret!=ModelNone:
					return Editor(ret,self._file,self._undo)				
			except:
				ret=ModelNone
			assert isinstance(self._instance,ElementList),'Cannot create named container on this list.'
			#assert issubclass(self._instance._element._desttype.values().__iter__().__next__(),Group.Referrable), 'This element does not contain named containers'
			return self.append(key)
		else:
			raise AssertionError('Index key type not supported')
	def __setitem__(self,key,value):
		if not isinstance(self._instance,ModelList):
			raise AttributeError('Object is not a list')
		if isinstance(key,int):
			length=len(self._instance)
			if key>=0:
				if key>=length:
					for i in range(length,key):
						self.append()
					return self.append(value)
			else:
				key=1-key
				if key>=length:
					assert 0,'insert before currently not supported'
			res=self._instance[key]
			return Editor(res,self._file,self._undo).set(value)
		else:
			raise AttributeError('Cannot index arrays with names when setting properties')
	def delete(self):
		parent=self._instance._parent
		self._delete(self._instance)
		if parent is None:
			if self._file:
				self._file.delete()
			else:
				fileH.deleteallfiles()
	def _delete(self,cont):
		if self._file:
			fileH.modified(self._file)
			remaining=tuple((x for x in cont._xml if x.base!=self._file))
			if len(cont._xml)==len(remaining):
				return
		else:
			for xml in cont._xml:
				fileH.modified(xml.base)
			remaining=()
		if isinstance(cont,ComplexTypeBase):
			for element in cont._xmlchildren.values():
				parent=cont
				while not isinstance(parent,element._parenttype):
					parent=parent._parent
				c=parent.__dict__[element._childname]
				if isinstance(c,list):
					for child in c[:]:
						self._delete(child)
				elif c is not ModelNone:
					self._delete(c)
		if len(remaining)==0 and cont._parent is not None:
			cont._unsetup()
			cont._parent._children.remove(cont)
			c=cont._parent.__dict__[cont._binding._childname]
			if isinstance(c,list):
				c.remove(cont)
			else:
				cont._parent.__dict__[cont._binding._childname]=ModelNone
			cont._parent=None
			cont._binding=None
		cont._xml=remaining
	def __delattr__(self, name):
		for child in getattr(self._instance,name):
			Editor(child,self._file,self._undo).delete()
	def save(self):
		def savefile(file):
			if fileH.clearmodified(file):
				with open(str(file),'wb') as f:
					from ..autosar_r4p0 import AUTOSAR
					model=AUTOSAR()
					model._setxml(file._root.getroot(),None,None,sort=True)
					ET.indent(file._root, space="  ")
					f.write(ET.tostring(file._root, xml_declaration=False, encoding='utf-8', doctype='<?xml version="1.0" encoding="utf-8"?>'))#,pretty_print=True
		if self._file:
			savefile(self._file)
		else:
			filenames=set((xml.base for cont in self._instance for xml in cont._xml))
			for filename in filenames:
				file=fileH.get(filename)
				savefile(file)
	def readonly(self):
		filenames=set((xml.base for cont in self._instance for xml in cont._xml))
		for filename in filenames:
			filename=filename.removeprefix('file:/')
			if fileH.get(filename).readonly():
				return True
		return False
		