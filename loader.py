# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2020 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

from collections import defaultdict, OrderedDict
import sys
import signal
import os
from AutoMAT import *

	
if __name__ == '__main__':
	import argparse
	import time
	start_time = time.time()

	parser = argparse.ArgumentParser(description='AutoMat arxml script tool')
	parser.add_argument('-s', '--scripts', nargs='*', help='script files to be executed. It is also possible to pass arguments to the script. To do so, surround the script and arguments with quotations', default=[])

	parser.add_argument('-d', '--debug', action='store_true', help='debug mode, rerun scrips forever', default=False)
	#parser.add_argument('-D', '--enironment_debug', action='store_true', help='debug mode, reload environment on every debug rerun', default=False)
	parser.add_argument('-v', '--verbose', action='store_true', help='enable debug printouts', default=False)
	#parser.add_argument('-n', '--no_validation', action='store_true', help='disable arxml content schema validation', default=False)
	parser.add_argument('-e', '--stop_on_error', action='store_true', help='return on first script error instead of finish all scripts', default=False)

	#parser.add_argument('-j', '--processes', nargs='?', type=int, help='max number of processes used to parse the model', default=None)
	parser.add_argument('-a', '--arxml', nargs='*', help='arxml files to load', default=[])
	parser.add_argument('-l', '--dirs', nargs='*', help='dirs to search for arxmls', default=[])
	parser.add_argument('-L', '--basedirs', nargs='*', help='dirs to search for arxmls recursively', default=[])

	parser.add_argument('-R', '--autosar_release', choices=['AUTOSAR_4-2-2.xsd', 'AUTOSAR_00046.xsd'],default='AUTOSAR_4-2-2.xsd',help='AUTOSAR release schema version')

	args = parser.parse_args()
	global vprint
	def dummyprint(*args,**kwargs):
		pass
	vprint=print if args.verbose else dummyprint

	if args.debug:
		import faulthandler
		faulthandler.enable()

	for dir in args.dirs:
		files = (os.path.join(dir, name)
             for name in os.listdir(dir)
             if name.endswith(".arxml"))
		args.arxml.extend(files)
	for dir in args.basedirs:
		files = (os.path.join(root, name)
             for root, dirs, files in os.walk(dir)
             for name in files
             if name.endswith(".arxml"))
		args.arxml.extend(files)
	
	for arxml in args.arxml:
		LoadFile(arxml)
		
	from importlib.machinery import SourceFileLoader
	import traceback
	sys.path.append('')
	numerrs=0
	exitcode=0
	try:
		for script in args.scripts:
			#import gc
			#gc.collect()
			vprint('execute :' + script)
			script = script.split()
			#loader.configureAutoMAT()
			scriptname=os.path.abspath(script[0])
			mymodule = SourceFileLoader('script', scriptname).load_module()
			if hasattr(mymodule,'main'):
				ret=mymodule.main(script[1:])
				if isinstance(ret,int) and ret > 0:
					numerrs+=1
					if ret>exitcode:
						exitcode=ret
					if args.stop_on_error:
						sys.exit(exitcode)
			del sys.modules['script']
	except SystemExit as e:
		raise
	except:
		print(traceback.format_exc())
		numerrs+=1
		if args.stop_on_error:
			sys.exit(1)
		elif exitcode==0:
			exitcode=1
	if exitcode>0:
		sys.exit(exitcode)
		
	vprint("all scripts run, total execution time was %s seconds" % (time.time() - start_time))
	sys.exit(exitcode)
