# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2019 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

from AutoMAT import *
from enum import Enum
from collections import defaultdict, OrderedDict
import csv
import sys,os
sys.path.append(os.path.abspath('.'))
import helpers.rtehelper
from importlib import reload
reload(helpers.rtehelper)
from helpers.rtehelper import *

# and finally get the conf
EcucValueCollection = autosar.BSW_pkg.AQ_E41
extract = EcucValueCollection.ECU_EXTRACT_REF.dest
rte = EcucValueCollection.ECUC_VALUES.ECUC_MODULE_CONFIGURATION_VALUES_REF_CONDITIONAL.ECUC_MODULE_CONFIGURATION_VALUES_REF.dest.next(lambda e:e.DEFINITION_REF.dest.REFINED_MODULE_DEF_REF.dest_name()=='Rte').conf()
#com = EcucValueCollection.ECUC_VALUES.ECUC_MODULE_CONFIGURATION_VALUES_REF_CONDITIONAL.ECUC_MODULE_CONFIGURATION_VALUES_REF.dest.next(lambda e:e.DEFINITION_REF.dest.REFINED_MODULE_DEF_REF.dest_name()=='Com')
com = EcucValueCollection.ECUC_VALUES.ECUC_MODULE_CONFIGURATION_VALUES_REF_CONDITIONAL.ECUC_MODULE_CONFIGURATION_VALUES_REF.dest.next(lambda e:e.DEFINITION_REF.dest_name()=='Com')
rootcomposition = extract.ROOT_SOFTWARE_COMPOSITIONS.ROOT_SW_COMPOSITION_PROTOTYPE[0]


class CWriter():
	_indent = 0
	_filename = None
	_content=None
	_file=None
	def __init__(self, filename):
		self._filename = filename
		self._content=''
		print('opened file ' + filename + ' for write')
	
	def __lshift__(self, val):
		lines=(x.strip() for x in val.split('\n'))
		for line in lines:
			indent = self._indent
			self._indent+=line.count('{') - line.count('}') + line.count('(') - line.count(')')
			if line.startswith('case') or line.startswith('}'):
				indent-=1
			line='\t'*indent + line
			print(line)
			self._content+=line + '\n'
		return self
	
	def close(self):
		with open('Rte/'+self._filename,'w') as f:
			f.write(self._content)
		print(f'file {self._filename} closed')

class _Rte_type_h():
	_handledtypes = None
	_rte_types_includes = None
	_typedefs = None
	_pimtypes = None
	def __init__(self):
		self._handledtypes = set()
		self._rte_types_includes = {}
		self._typedefs = OrderedDict()
		self._pimtypes = ''
	
		impltypes = {}
		for impltype in autosar_r4p0.IMPLEMENTATION_DATA_TYPE.instances:
			impltypes[impltype.name()] = impltype
		
		for name,impltype in impltypes.items():
			try:
				deps = ModelList()
				props = impltype.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0]
				if not impltype.TYPE_EMITTER.value.isNone() and impltype.TYPE_EMITTER.value not in  ('RTE', 'BSW'):
					self._rte_types_includes[impltype.TYPE_EMITTER.value] = f'#include "{impltype.TYPE_EMITTER.value}"'
					self._handledtypes.add(name)
					continue
				elif impltype.CATEGORY.value in ('VALUE','BOOLEAN'):
					if not props.BASE_TYPE_REF.dest.NATIVE_DECLARATION.value.isNone():
						res = f'typedef {props.BASE_TYPE_REF.dest.NATIVE_DECLARATION.value} {name};'
					else:
						#type should be emitted in this case
						self._handledtypes.add(name)
						continue
				elif impltype.CATEGORY.value == 'TYPE_REFERENCE':
					# redefinition type
					typename = props.IMPLEMENTATION_DATA_TYPE_REF.dest.name()
					res = f'typedef {typename} {name};'
					deps.append(typename)
				elif impltype.CATEGORY.value == 'ARRAY':
					impltypeelem = impltype.SUB_ELEMENTS.IMPLEMENTATION_DATA_TYPE_ELEMENT[-1]
					props = impltypeelem.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0]
					if impltypeelem.CATEGORY.value in ('VALUE','BOOLEAN'):
						typename = props.BASE_TYPE_REF.dest.NATIVE_DECLARATION.value
					elif impltypeelem.CATEGORY.value == 'TYPE_REFERENCE':
						typename = props.IMPLEMENTATION_DATA_TYPE_REF.dest.name()
						deps.append(typename)
					else:
						raise 'not supported'
					res = f'''typedef {typename} {name}'''
					for dim in impltype.SUB_ELEMENTS.IMPLEMENTATION_DATA_TYPE_ELEMENT:
						res += f'[{dim.ARRAY_SIZE.value}]'
					res += ';'
				elif impltype.CATEGORY.value == 'DATA_REFERENCE':
					tqlA=props.SW_POINTER_TARGET_PROPS.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].SW_IMPL_POLICY.value.default("").lower()
					tqlB=props.SW_IMPL_POLICY.value.default("").lower()
					addtqlA=props.SW_POINTER_TARGET_PROPS.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].ADDITIONAL_NATIVE_TYPE_QUALIFIER.value.default("").lower()
					addtqlB=props.ADDITIONAL_NATIVE_TYPE_QUALIFIER.value.default("").lower()
					if props.SW_POINTER_TARGET_PROPS.TARGET_CATEGORY.value in ('VALUE','BOOLEAN'):
						typename = props.SW_POINTER_TARGET_PROPS.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].BASE_TYPE_REF.dest.NATIVE_DECLARATION.value
					elif props.SW_POINTER_TARGET_PROPS.TARGET_CATEGORY.value == 'TYPE_REFERENCE':
						typename = props.SW_POINTER_TARGET_PROPS.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].IMPLEMENTATION_DATA_TYPE_REF.dest.name()
						deps.append(typename)
					else:
						raise 'not supported'
					res=f'typedef {tqlA} {addtqlA} {typename} * {tqlB} {addtqlB} {name};'
						
				else:
					raise 'not supported'
				#print(res)
			except:
				print(f'Failed to include implementationtype {name}')
				continue
			self._typedefs[name] = (res,deps)

	def AddPimType(self, pimtype):
		self._pimtypes += pimtype + '\n'
		
	def Write_Rte_type(self):
		c = CWriter('Rte_Type.h')
		c << '''#ifndef RTE_TYPE_H
#define RTE_TYPE_H

#include "Rte.h"'''
		for h in self._rte_types_includes:
			c << '#include "'+h+'"'
		c << ''
		numfailed=0
		while len(self._typedefs) > 0:
			try:
				name,(val,deps) = self._typedefs.popitem(last=False)
				for dep in deps:
					if dep.isNone():
						print(f'error: implementation dependency for {name} missing')
					elif dep not in self._handledtypes:
						numfailed+=1
						self._typedefs[name] = (val,deps)
						raise Exception('wait with type until deps generated')
				c << f'#define _DEFINED_TYPEDEF_FOR_{name}_'
				c << val
				self._handledtypes.add(name)
				numfailed=0
			except Exception as ignore:
				# this is expected, generate the type when all deps are generated
				if numfailed>=len(self._typedefs):
					#cannot generate the last types
					print('circular dependencies makes it impossible to generate all types')
					break
		c << self._pimtypes
		c << '''
#endif // RTE_TYPE_H
'''
		c.close()

Rte_type_h = _Rte_type_h()

class CSV():
	class Instance():
		class DataElement():
			_dataelementref=None
			_comSignalRef=None
			_type=None
			_compu=None
			#_initval=None
			_rxcomspec=ModelList()
			_txcomspec=None
			_runnables=None
			_storeLocal=None
			_buf_name=None
			def __init__(self, dataelement,port,behavior,instance):
				self._dataelementref=ModelList()
				self._comSignalRef=ModelList()
				self._type=TYPE(dataelement.TYPE_TREF.dest,behavior)
				self._runnables=defaultdict(ModelList)
				self._storeLocal=False
				self._buf_name=f'Rte_{instance.name()}_{port.name()}_{dataelement.name()}'
				if isinstance(port, autosar_r4p0.R_PORT_PROTOTYPE):
					if not port.REQUIRED_COM_SPECS.isNone():
						self._rxcomspec=dataelement.references().enclosing_container().next(lambda self:self.enclosing_container() == port.REQUIRED_COM_SPECS)
				else:
					if not port.PROVIDED_COM_SPECS.isNone():
						self._txcomspec=dataelement.references().enclosing_container().next(lambda self:self.enclosing_container() == port.PROVIDED_COM_SPECS)
			def needProtection(self):
				if self._storeLocal:
					if self._type.isAtomic():
						return False
				elif  min(self._dataelementref.select(lambda e:e._storeLocal)._type.isAtomic().default([1])) > 0:
					return False
				runnables=ModelList(self._runnables.values())
				minprio=min(runnables._minprio)
				maxprio=max(ModelList(self._dataelementref._runnables.values())._maxprio + runnables._maxprio)
				return minprio < maxprio
			def print_read(self,to_varptr):
				res=""
				if self._type.isArray():
					to_var=to_varptr
				else:
					if to_varptr[0]=='&':
						to_var=to_varptr[1:]
					else:
						to_var='*'+to_varptr
				if self._dataelementref.__len__() > 0:
					if self._storeLocal:
						res+=f'extern {self._type.name()} {self._buf_name};\n'
						res+=self._type.copy(f'{to_var}',self._type, self._buf_name)
					else:
						res+=f'extern {self._dataelementref[0]._type.name()} {self._dataelementref[0]._buf_name};\n'
						res+=self._type.copy(f'{to_var}',self._dataelementref[0]._type, self._dataelementref[0]._buf_name)
				for (systemsig,isig,comSig) in self._comSignalRef:
					basetype=isig.NETWORK_REPRESENTATION_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].BASE_TYPE_REF.dest
					# todo: this is wrong, should take compu method from system signal instead
					compu=isig.NETWORK_REPRESENTATION_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest
					comType=TYPE(basetype)
					comType._compu=compu
					if comType.ScalingNeeded(self._type):
						res+=f'{{\n{basetype.NATIVE_DECLARATION.value} temp;\n'
						res+=f'Com_ReceiveSignal(ComConf_{comSig.DEFINITION_REF.dest_name()}_{comSig.name()}, &temp);\n'
						res+=self._type.copy(f'{to_var}',comType,'temp') + '}\n'
					else:
						res+=f'Com_ReceiveSignal(ComConf_{comSig.DEFINITION_REF.dest_name()}_{comSig.name()}, {to_varptr});\n'
				return res
			def print_write(self, var):
				res=""
				if self._storeLocal:
					res+=f'extern {self._type.name()} {self._buf_name};\n'
					res+=self._type.copy(self._buf_name,self._type,var)
				for rxdataelement in self._dataelementref.select(lambda e: e._storeLocal):
					res+=f'extern {rxdataelement._type.name()} {rxdataelement._buf_name};\n'
					res+=rxdataelement._type.copy(rxdataelement._buf_name,self._type,var)
						
				for (systemsig,isig,comSig) in self._comSignalRef:
					basetype=isig.NETWORK_REPRESENTATION_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].BASE_TYPE_REF.dest
					# todo: this is wrong, should take compu method from system signal instead
					compu=isig.NETWORK_REPRESENTATION_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest
					comType=TYPE(basetype)
					comType._compu=compu
					if comType.ScalingNeeded(self._type):
						res+=f'{{\n{basetype.NATIVE_DECLARATION.value} temp;\n'
						res+=comType.copy('temp',self._type,var)
						res+=f'Com_SendSignal(ComConf_{comSig.DEFINITION_REF.dest_name()}_{comSig.name()}, &temp);\n}}\n'
					else:
						res+=f'Com_SendSignal(ComConf_{comSig.DEFINITION_REF.dest_name()}_{comSig.name()}, &{var});\n'
				return res
		class CalParam():
			_runnables=None
			_type=None
			def __init__(self,param,behavior):
				self._runnables=ModelList()
				self._type=TYPE(param.TYPE_TREF.dest,behavior)
		class CSServer():
			_dataelementref=None
			_operation=None
			_port=None
			_runnable=None
			_mapping=None
			_args=None
			def __init__(self,operation,port,runnable,mapping,intbeh):
				self._dataelementref=ModelList()
				self._operation=operation
				self._port=port
				self._runnable=runnable
				runnable._csServer=self
				self._mapping=mapping
				self._args=ModelList()
				portApiOption=intbeh.PORT_API_OPTIONS.PORT_API_OPTION.select(lambda e:e.PORT_REF.dest == port)
				for i,arg in enumerate(portApiOption.PORT_ARG_VALUES.PORT_DEFINED_ARGUMENT_VALUE):
					self._args.append(('IN',TYPE(arg.VALUE_TYPE_TREF.dest,intbeh),f'arg_{i}'))
				for arg in operation.ARGUMENTS.ARGUMENT_DATA_PROTOTYPE:
					self._args.append((arg.DIRECTION.value,TYPE(arg.TYPE_TREF.dest,intbeh),arg.name()))
			def print_def(self):
				if not self._operation.POSSIBLE_ERROR_REFS.POSSIBLE_ERROR_REF.isNone():
					ret='extern Std_ReturnType '
				else:
					ret='extern void '
				ret+=self._runnable._runnable.SYMBOL.value+'('
				if self._args.isNone():
					ret+='void'
				else:
					args=[]
					for direction,typeref,varname, in self._args:
						if direction=='IN':
							if typeref.isArray():
								args.append(f'\n/* IN */ const {typeref.impl_name()} {varname}')
							elif typeref.isRecord():
								args.append(f'\n/* IN */ const {typeref.impl_name()} *{varname}')
							else:
								args.append(f'\n/* IN */ {typeref.impl_name()} {varname}')
						else:
							if typeref.isArray():
								args.append(f'\n/* {direction} */ {typeref.impl_name()} {varname}')
							else:
								args.append(f'\n/* {direction} */ {typeref.impl_name()} *{varname}')
					ret+=','.join(args)
				ret+=');\n'
				return ret
				
		class CSClient():
			_dataelementref=None
			_operation=None
			_port=None
			_runnables=None
			_assync=False
			def __init__(self,operationref):
				operation=operationref.TARGET_REQUIRED_OPERATION_REF.dest
				port=operationref.CONTEXT_R_PORT_REF.dest
				self._assync = isinstance(operationref.enclosing_container(),autosar_r4p0.ASYNCHRONOUS_SERVER_CALL_POINT)
				self._operation=operation
				self._port=port
				self._runnables=ModelList()
		class Mode():
			_dataelementref=None
			_modeGroup=None
			_type=None
			_port=None
			_runnables=None
			def __init__(self,modeGroup,port,instance):
				self._dataelementref=ModelList()
				self._modeGroup=modeGroup
				self._type=TYPE(modeGroup.TYPE_TREF.dest,instance._behavior)
				self._port=port
				self._runnables=defaultdict(ModelList)
			
		class Runnable():
			_csServer=None
			_runnable=None
			_taskMappings=None
			_minprio=None
			_maxprio=None
			_tasks=None
			_activatedByRunnables=None
			_exlusiveAreas=None
			_implicits=None
			_pre=None
			_post=None
			def __init__(self,runnable,mappings):
				self._runnable=runnable
				self._taskMappings=mappings
				self._tasks=mappings.RteMappedToTaskRef.dest.foreach(lambda e:OsTask.get(e))
				self._activatedByRunnables=set()
				self._exlusiveAreas=ModelList()
				self._implicits={}
				self._pre=""
				self._post=""
			def calcprio(self):
				if self._maxprio is None:
					self._minprio=0
					self._maxprio=0xffffffff
					if self._activatedByRunnables.__len__() > 0:
						ModelList(self._activatedByRunnables).calcprio()
						self._maxprio=max(ModelList(self._activatedByRunnables)._maxprio)
						self._maxprio=max(self._tasks.prio.default(ModelList()).append(self._maxprio))
						self._minprio=min(ModelList(self._activatedByRunnables)._minprio)
						self._minprio=min(self._tasks.prio.default(ModelList()).append(self._minprio))
					elif self._tasks.__len__() > 0:
						self._minprio=min(self._tasks.prio)
						self._maxprio=max(self._tasks.prio)
			def runsWithinExclArea(self):
				if self._exlusiveAreas.__len__() > 0:
					exclAreaImpl=defaultdict(list)
					exlusiveAreas=self._exlusiveAreas.select(lambda e:self in e._runnables_protectionNeeded)
					for exlusiveArea in exlusiveAreas:
						exclAreaImpl[exlusiveArea._implementation.RteExclusiveAreaImplMechanism.value].append(exlusiveArea)
					if 'ALL_INTERRUPT_BLOCKING' in exclAreaImpl:
						self._excareaImpl=1
						self._minprio=0xffffffff
					elif 'OS_INTERRUPT_BLOCKING' in exclAreaImpl:
						self._excareaImpl=2
						self._minprio=0xffffffff
					elif 'OS_RESOURCE' in exclAreaImpl:
						self._excareaImpl=3
						self._exclareaResources=set(exclAreaImpl['OS_RESOURCE']._implementation.RteExclusiveAreaOsResourceRef.dest)
						# todo: calc prio from resource
						#self._minprio=0xffffffff
					else:
						self._excareaImpl=0
			def print_def(self):
				if self._csServer is not None:
					return self._csServer.print_def()
				return f'extern void {self._runnable.SYMBOL.value}(void);\n'
			def print_call(self,command=None):
				if command is None:
					command=self._runnable.SYMBOL.value+'()'
				res= f'{{\n{self.print_def()}'
				if self._pre !='':
					res+=f'Rte_EnterExclusive();\n{self._pre}Rte_ExitExclusive();\n'
				if self._exlusiveAreas.__len__() > 0:
					sep='\n// \tand '
					res += f'// runs within exclusive area {sep.join(self._exlusiveAreas._exclusiveArea.name())}\n'
					if self._excareaImpl==1:
						res+=f'SuspendAllInterrupts();\n{command};\nResumeAllInterrupts();\n'
					elif self._excareaImpl==2:
						res+=f'SuspendOSInterrupts();\n{command};\nResumeOSInterrupts();\n'
					elif self._excareaImpl==3:
						resource=exlusiveArea._implementation.RteExclusiveAreaOsResourceRef[0].dest.name()
						for resource in self._exclareaResources:
							res+=f'GetResource({resource.name()});\n'
						res+=f'{command};\n'
						for resource in self._exclareaResources:
							res+=f'ReleaseResource({resource.name()});\n'
					else:
						res+='// all exclusive areas optimized away due to running highest prio\n'
						res+=f'{command};\n'
				else:		
					res+=f'{command};\n'
				if self._post !='':
					res+=f'Rte_EnterExclusive();\n{self._post}Rte_ExitExclusive();\n'
				return res+'}'
		class ExclusiveArea():
			_runnables=None
			_runnables_canEnter=None
			_exclusiveArea=None
			_implementation=None
			_runnables_protectionNeeded=None
			def __init__(self,exclArea,rteSwComponentInstace):
				self._runnables=ModelList()
				self._runnables_canEnter=ModelList()
				self._exclusiveArea=exclArea
				self._implementation=rteSwComponentInstace.RteExclusiveAreaImplementation.next(lambda e:e.RteExclusiveAreaRef.dest == exclArea)
				self._runnables_protectionNeeded=ModelList()
			def analyse(self):
				if self._implementation.RteExclusiveAreaImplMechanism.isNone() or self._implementation.RteExclusiveAreaImplMechanism.isCOOPERATIVE_RUNNABLE_PLACEMENT():
					# no protection needed
					return
				maxPrio=max(self._runnables._maxprio)
				for runnable in self._runnables:
					if runnable._minprio < maxPrio:
						self._runnables_protectionNeeded.append(runnable)
		class Irv():
			_runnable=None,
			_read=True
			def __init__(self,runnable,read):
				self._runnable=runnable
				self._read=read
		_behavior=None
		_rxdataelements=None
		_txdataelements=None
		_calparams=None
		_irvdataelements=None
		_clients=None
		_servers=None
		_runnables=None
		_exclusiveAreas=None
		_error_codes=None
		_csv=None
		def _set_dataref(self,dataref,dataelements,runnable,accesstype,beh,instance):
			id=(dataref.PORT_PROTOTYPE_REF.dest,dataref.TARGET_DATA_PROTOTYPE_REF.dest)
			dataelement = dataelements.setdefault(id,self.DataElement(dataref.TARGET_DATA_PROTOTYPE_REF.dest,dataref.PORT_PROTOTYPE_REF.dest,beh,instance))
			dataelement._runnables[accesstype].append(runnable)
		def _set_operationref(self,operationref,operations,runnable):
			operationObj=operationref.TARGET_REQUIRED_OPERATION_REF.dest
			id=(operationref.CONTEXT_R_PORT_REF.dest,operationObj)
			operation = operations.setdefault(id,self.CSClient(operationref))
			operation._runnables.append(runnable)
			for error in operationObj.POSSIBLE_ERROR_REFS.POSSIBLE_ERROR_REF.dest:
				if error.name() != 'RTE_E_INVALID':
					self._csv._error_codes[f'RTE_E_{operationObj.parent().name()}_{error.name()}'] = error.ERROR_CODE.value
			
		def __init__(self,instance,beh,csv):
			rteSwComponentInstace=rte.RteSwComponentInstance.next(lambda self:self.RteSoftwareComponentInstanceRef.dest==instance)
			self._behavior=beh
			runnables=beh.RUNNABLES.RUNNABLE_ENTITY
			self._runnables={}
			self._rxdataelements={}
			self._txdataelements={}
			self._calparams={}
			self._irvdataelements=defaultdict(ModelList)
			self._clients={}
			self._servers={}
			self._modes={}
			self._rxmodes={}
			self._exclusiveAreas={}
			self._csv=csv
			for runnable in runnables:
				events=runnable.references().parent().select(lambda self:self.enclosing_container() == beh.EVENTS)
				mappings=rteSwComponentInstace.RteEventToTaskMapping.select(lambda self:self.RteEventRef.dest in events)
				runnableObj=self.Runnable(runnable,mappings)
				self._runnables[runnable]=runnableObj
				for exlusiveArea in runnable.RUNS_INSIDE_EXCLUSIVE_AREA_REFS.RUNS_INSIDE_EXCLUSIVE_AREA_REF.dest:
					exlusiveAreaObj=self._exclusiveAreas.setdefault(exlusiveArea, self.ExclusiveArea(exlusiveArea,rteSwComponentInstace))
					exlusiveAreaObj._runnables.append(runnableObj)
					runnableObj._exlusiveAreas.append(exlusiveAreaObj)
				for exlusiveArea in runnable.CAN_ENTER_EXCLUSIVE_AREA_REFS.CAN_ENTER_EXCLUSIVE_AREA_REF.dest:
					exlusiveAreaObj=self._exclusiveArea.setdefault(exlusiveArea, ExclusiveArea(exlusiveArea,rteSwComponentInstace))
					exlusiveAreaObj._runnables.append(runnableObj)
					exlusiveAreaObj_runnables_canEnter.append(runnableObj)
				for localvar in runnable.READ_LOCAL_VARIABLES.VARIABLE_ACCESS.ACCESSED_VARIABLE.LOCAL_VARIABLE_REF.dest:
					self._irvdataelements[localvar].append(self.Irv(runnable,True))
				for localvar in runnable.WRITTEN_LOCAL_VARIABLES.VARIABLE_ACCESS.ACCESSED_VARIABLE.LOCAL_VARIABLE_REF.dest:
					self._irvdataelements[localvar].append(self.Irv(runnable,False))
				for dataref in runnable.DATA_RECEIVE_POINT_BY_VALUES.VARIABLE_ACCESS.ACCESSED_VARIABLE.AUTOSAR_VARIABLE_IREF:
					self._set_dataref(dataref,self._rxdataelements,runnableObj,'DATA_RECEIVE_POINT_BY_VALUES',beh,instance)
				for dataref in runnable.DATA_RECEIVE_POINT_BY_ARGUMENTS.VARIABLE_ACCESS.ACCESSED_VARIABLE.AUTOSAR_VARIABLE_IREF:
					self._set_dataref(dataref,self._rxdataelements,runnableObj,'DATA_RECEIVE_POINT_BY_ARGUMENTS',beh,instance)
				for dataref in runnable.DATA_READ_ACCESSS.VARIABLE_ACCESS.ACCESSED_VARIABLE.AUTOSAR_VARIABLE_IREF:
					self._set_dataref(dataref,self._rxdataelements,runnableObj,'DATA_READ_ACCESSS',beh,instance)
				for dataref in runnable.DATA_SEND_POINTS.VARIABLE_ACCESS.ACCESSED_VARIABLE.AUTOSAR_VARIABLE_IREF:
					self._set_dataref(dataref,self._txdataelements,runnableObj,'DATA_SEND_POINTS',beh,instance)
				for dataref in runnable.DATA_WRITE_ACCESSS.VARIABLE_ACCESS.ACCESSED_VARIABLE.AUTOSAR_VARIABLE_IREF:
					self._set_dataref(dataref,self._txdataelements,runnableObj,'DATA_WRITE_ACCESSS',beh,instance)
				for calparamref in runnable.PARAMETER_ACCESSS.PARAMETER_ACCESS.ACCESSED_PARAMETER.LOCAL_PARAMETER_REF.dest:
					calparam=self._calparams.setdefault(calparamref,self.CalParam(calparamref,beh))
					calparam._runnables.append(runnableObj)
				for operationref in runnable.SERVER_CALL_POINTS.SYNCHRONOUS_SERVER_CALL_POINT.OPERATION_IREF:
					self._set_operationref(operationref,self._clients,runnableObj)
				for operationref in runnable.SERVER_CALL_POINTS.ASYNCHRONOUS_SERVER_CALL_POINT.OPERATION_IREF:
					self._set_operationref(operationref,self._clients,runnableObj)
				for operationref in beh.EVENTS.OPERATION_INVOKED_EVENT.OPERATION_IREF.select(lambda self:self.parent().START_ON_EVENT_REF.dest == runnable):
					operationObj=operationref.TARGET_PROVIDED_OPERATION_REF.dest
					id=(operationref.CONTEXT_P_PORT_REF.dest,operationObj)
					server = self._servers[id]=self.CSServer(operationObj,operationref.CONTEXT_P_PORT_REF.dest, runnableObj, mappings.next(lambda e:e.RteEventRef.dest==operationref.parent()),beh)
					for error in operationObj.POSSIBLE_ERROR_REFS.POSSIBLE_ERROR_REF.dest:
						if error.name() != 'RTE_E_INVALID':
							self._csv._error_codes[f'RTE_E_{operationObj.parent().name()}_{error.name()}'] = error.ERROR_CODE.value
				for moderef in beh.EVENTS.SWC_MODE_SWITCH_EVENT.MODE_IREFS.MODE_IREF.select(lambda self:self.parent().START_ON_EVENT_REF.dest == runnable):
					id=(moderef.CONTEXT_PORT_REF.dest,moderef.CONTEXT_MODE_DECLARATION_GROUP_PROTOTYPE_REF.dest)
					mode = self._rxmodes.setdefault(id,self.Mode(moderef.CONTEXT_MODE_DECLARATION_GROUP_PROTOTYPE_REF.dest,moderef.CONTEXT_PORT_REF.dest,self))
					mode._runnables[(moderef.parent().ACTIVATION.value,moderef.TARGET_MODE_DECLARATION_REF.dest)].append(runnableObj)
					mode._mapping=mappings.next(lambda e:e.RteEventRef.dest==moderef.parent())
				for modegroupref in runnable.MODE_SWITCH_POINTS.MODE_SWITCH_POINT.MODE_GROUP_IREF:
					id=(modegroupref.CONTEXT_P_PORT_REF.dest,modegroupref.TARGET_MODE_GROUP_REF.dest)
					mode = self._modes.setdefault(id,self.Mode(modegroupref.TARGET_MODE_GROUP_REF.dest,modegroupref.CONTEXT_P_PORT_REF.dest,self))
					mode._runnables['MODE_SWITCH_POINT'].append(runnableObj)
				for modegroupref in runnable.MODE_ACCESS_POINTS.MODE_ACCESS_POINT.MODE_GROUP_IREF.R_MODE_GROUP_IN_ATOMIC_SWC_INSTANCE_REF:
					id=(modegroupref.CONTEXT_R_PORT_REF.dest,modegroupref.TARGET_MODE_GROUP_REF.dest)
					mode = self._rxmodes.setdefault(id,self.Mode(modegroupref.TARGET_MODE_GROUP_REF.dest,modegroupref.CONTEXT_R_PORT_REF.dest,self))
					mode._runnables['MODE_ACCESS_POINT'].append(runnableObj)
				for modegroupref in runnable.MODE_ACCESS_POINTS.MODE_ACCESS_POINT.MODE_GROUP_IREF.P_MODE_GROUP_IN_ATOMIC_SWC_INSTANCE_REF:
					id=(modegroupref.CONTEXT_P_PORT_REF.dest,modegroupref.TARGET_MODE_GROUP_REF.dest)
					mode = self._modes.setdefault(id,self.Mode(modegroupref.TARGET_MODE_GROUP_REF.dest,modegroupref.CONTEXT_P_PORT_REF.dest,self))
					mode._runnables['MODE_ACCESS_POINT'].append(runnableObj)
	_csv = None
	_instances=None
	_internal_behavior=None
	_error_codes=None
	def __init__(self, csv):
		self._csv = csv
		self._instances={}
		self._error_codes={}
	def add(self,instance, behavior):
		instanceObj = self._instances[instance]=self.Instance(instance,behavior,self)
		self._internal_behavior=behavior
	def Write_Rte_SWC_type_h(self):
		c = CWriter(f'Rte_{self._csv.name()}_Type.h')
		c << f'''#ifndef RTE_{self._csv.name().upper()}_TYPE_H
#define RTE_{self._csv.name().upper()}_TYPE_H

#include "Rte_Type.h"

'''
		interfaces=self._csv.PORTS.P_PORT_PROTOTYPE.PROVIDED_INTERFACE_TREF.dest + self._csv.PORTS.R_PORT_PROTOTYPE.REQUIRED_INTERFACE_TREF.dest
		reftypes=interfaces.DATA_ELEMENTS.VARIABLE_DATA_PROTOTYPE.TYPE_TREF.dest
		reftypes.extend(interfaces.OPERATIONS.siblings().ARGUMENTS.ARGUMENT_DATA_PROTOTYPE.TYPE_TREF.dest)
		reftypes.extend(self._internal_behavior.AR_TYPED_PER_INSTANCE_MEMORYS.VARIABLE_DATA_PROTOTYPE.TYPE_TREF.dest)
		for pimtype in set(self._internal_behavior.PER_INSTANCE_MEMORYS.PER_INSTANCE_MEMORY.TYPE_DEFINITION.value):
			try:
				reftypes.append(autosar_r4p0.IMPLEMENTATION_DATA_TYPE.instances.next(lambda e:e.name() == pimtype.__str__()))
			except:
				pass
		# explicit and implicit irvs, rtyped pims etc
		reftypes.extend(self._internal_behavior.siblings().VARIABLE_DATA_PROTOTYPE.TYPE_TREF.dest)
		reftypes.extend(self._internal_behavior.DATA_TYPE_MAPPING_REFS.DATA_TYPE_MAPPING_REF.dest.DATA_TYPE_MAPS.DATA_TYPE_MAP.foreach(lambda e:(e.APPLICATION_DATA_TYPE_REF.dest,e.IMPLEMENTATION_DATA_TYPE_REF.dest)))
		reftypes.extend(self._internal_behavior.siblings().PARAMETER_DATA_PROTOTYPE.TYPE_TREF.dest)
		refs={}
		for reftype in reftypes:
			if reftype.SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL[0].COMPU_METHOD_REF.dest.CATEGORY.value=='TEXTTABLE':
				refs[reftype.name()]=reftype
		#refs=ReferencedObjects(self._internal_behavior)
		scales={}
		#for scale in (refs['APPLICATION-PRIMITIVE-DATA-TYPE'] + refs['IMPLEMENTATION-DATA-TYPE']).SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL.COMPU_METHOD_REF.dest.select(lambda e:e.CATEGORY.value == 'TEXTTABLE').COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE:
		for scale in ModelList(refs.values()).SW_DATA_DEF_PROPS.SW_DATA_DEF_PROPS_VARIANTS.SW_DATA_DEF_PROPS_CONDITIONAL.COMPU_METHOD_REF.dest.select(lambda e:e.CATEGORY.value == 'TEXTTABLE').COMPU_INTERNAL_TO_PHYS.COMPU_SCALES.COMPU_SCALE:
			if not scale.SYMBOL.isNone():
				name = scale.SYMBOL.value
			elif not scale.COMPU_CONST.VT.isNone():
				name = scale.COMPU_CONST.VT.value
			elif not scale.SHORT_LABEL.isNone():
				name = scale.SHORT_LABEL.value
			else:
				continue
			if scale.LOWER_LIMIT.value != scale.UPPER_LIMIT.value:
				continue
			scales[name] = scale.LOWER_LIMIT.value
				
		for name, val in scales.items():
			c << f'''#ifndef {name}
				#define {name} {val}
				#endif // {name}
				'''
		# get all the used mode ports
		refmodedeclgroups=set(interfaces.MODE_GROUP.TYPE_TREF.dest)
		for modedeclgroup in refmodedeclgroups:
			# should use sorted index if category is ALPHABETIC_ORDER and specified value if EXPLICIT_ORDER
			modetype=TYPE(modedeclgroup,self._internal_behavior)
			# cannot find that this type should be generated but necessary to get example to run remove?
			c << f'''
				#ifndef RTE_MODETYPE_{modedeclgroup.name()}
				#define RTE_MODETYPE_{modedeclgroup.name()}
				typedef {modetype.impl_name()} Rte_ModeType_{modedeclgroup.name()};
				#endif'''
			for i,mode in enumerate(modedeclgroup.MODE_DECLARATIONS.MODE_DECLARATION.sort(lambda e:e.name())):
				name=f'RTE_MODE_{modedeclgroup.name()}_{mode.name()}'
				c << f'''#ifndef {name}
					#define {name} {i}
					#endif'''
			c << f'''#ifndef RTE_TRANSITION_{modedeclgroup.name()}
				#define RTE_TRANSITION_{modedeclgroup.name()} {len(modedeclgroup.MODE_DECLARATIONS.MODE_DECLARATION)}
				#endif'''

		c << f'''
			#endif // RTE_{self._csv.name().upper()}_TYPE_H
			'''
		c.close()
		
	def Write_Rte_SWC_h(self):
		c = CWriter(f'Rte_{self._csv.name()}.h')
		c << f'''#ifndef RTE_{self._csv.name().upper()}_H
#define RTE_{self._csv.name().upper()}_H

#ifdef RTE_APPLICATION_HEADER_FILE
#error Multiple application header files included.
#endif // RTE_APPLICATION_HEADER_FILE
#define RTE_APPLICATION_HEADER_FILE

#ifdef __cplusplus
extern "C" {{
#endif

#include "Rte_{self._csv.name()}_Type.h"
#include <Os.h>
#include <Com.h>

#define Rte_EnterExclusive()	SuspendAllInterrupts()
#define Rte_ExitExclusive()		ResumeAllInterrupts()
'''
		used_runnables = self._internal_behavior.RUNNABLES.RUNNABLE_ENTITY
		res = ''
		for i,runnable in enumerate(used_runnables.name()):
			if i == 0:
				res = f'#if  !defined(RTE_RUNNABLEAPI_{runnable})'
			else:
				res += f' && !defined(RTE_RUNNABLEAPI_{runnable})'
			if i == len(used_runnables)-1:
				res += '''
	#define RTE_RUNNABLEAPI
#else
	#undef RTE_APPLICATION_HEADER_FILE
#endif
'''
		c << res
		
		#application data types
		c << '\n// define all used application data types'
		for map in self._internal_behavior.DATA_TYPE_MAPPING_REFS.DATA_TYPE_MAPPING_REF.dest.DATA_TYPE_MAPS.DATA_TYPE_MAP:
			if map.APPLICATION_DATA_TYPE_REF.dest_name() != map.IMPLEMENTATION_DATA_TYPE_REF.dest_name():
				c << f'#define {map.APPLICATION_DATA_TYPE_REF.dest_name()} {map.IMPLEMENTATION_DATA_TYPE_REF.dest_name()}'

		c << '\n// Define all initial values'
		for port in self._csv.PORTS.P_PORT_PROTOTYPE:
			for initval in port.PROVIDED_COM_SPECS.NONQUEUED_SENDER_COM_SPEC.select(lambda self:self.DATA_ELEMENT_REF.dest.TYPE_TREF.dest.CATEGORY.value in ('VALUE','BOOLEAN')).INIT_VALUE:
				type_=TYPE(initval.enclosing_container().DATA_ELEMENT_REF.dest.TYPE_TREF.dest,self._internal_behavior)
				c << f'#define Rte_InitValue_{port.name()}_{initval.enclosing_container().DATA_ELEMENT_REF.dest.name()} {type_.VALUE2str(initval)}' #<suffix>
		for port in self._csv.PORTS.R_PORT_PROTOTYPE:
			for initval in port.REQUIRED_COM_SPECS.NONQUEUED_RECEIVER_COM_SPEC.select(lambda self:self.DATA_ELEMENT_REF.dest.TYPE_TREF.dest.CATEGORY.value in ('VALUE','BOOLEAN')).INIT_VALUE:
				type_=TYPE(initval.enclosing_container().DATA_ELEMENT_REF.dest.TYPE_TREF.dest,self._internal_behavior)
				c << f'#define Rte_InitValue_{port.name()}_{initval.enclosing_container().DATA_ELEMENT_REF.dest.name()} {type_.VALUE2str(initval)}' #<suffix>
		
		c << '\n// Define all Application error codes'
		for error,val in self._error_codes.items():
			c << f'#define {error} {val}'
		
		c << '\n// Define all PIM types'
		pimtypes = {}
		for pim in self._internal_behavior.PER_INSTANCE_MEMORYS.PER_INSTANCE_MEMORY:
			pimtypes[pim.TYPE.value] = pim.TYPE_DEFINITION.value
		for type,typedefinition in pimtypes.items():
			c << f'\ntypedef Rte_PimType_{self._csv.name()}_{type} {type};'
			Rte_type_h.AddPimType(f'typedef {typedefinition} Rte_PimType_{self._csv.name()}_{type};')
		
		c << '\n// Define all PIM access functions'
		for pim in self._internal_behavior.PER_INSTANCE_MEMORYS.PER_INSTANCE_MEMORY:
			name=f'Rte_Pim_{self._internal_behavior.name()}_{pim.name()}'
			c << f'''static inline {pim.TYPE.value}* Rte_Pim_{pim.name()}(void) {{
				extern {pim.TYPE.value} {name};
				return &{name};
			}}'''
		for pim in self._internal_behavior.AR_TYPED_PER_INSTANCE_MEMORYS.VARIABLE_DATA_PROTOTYPE:
			_type=TYPE(pim.TYPE_TREF.dest,self._internal_behavior)
			name=f'Rte_Pim_{self._internal_behavior.name()}_{pim.name()}'
			if _type.isArray():
				c << f'''static inline {_type.element_type().name()}* Rte_Pim_{pim.name()}(void) {{
					extern {_type.name()} {name};
					return {name};
				}}'''
			else:
				c << f'''static inline {_type.name()}* Rte_Pim_{pim.name()}(void) {{
					extern {_type.name()} {name};
					return &{name};
				}}'''
		
		# read and write functions
		if self._internal_behavior.SUPPORTS_MULTIPLE_INSTANTIATION.value.default(False):
			adsfasdf.thisShouldCrash()
		else:
			instanceobj,instance=self._instances.items().__iter__().__next__()
			
			for runnable in instance._runnables.values():
				c << runnable.print_def()
			for localvar,accesslist in instance._irvdataelements.items():
				vartype=TYPE(localvar.TYPE_TREF.dest,instance._behavior)
				if isinstance(localvar.enclosing_container(),autosar_r4p0.SWC_INTERNAL_BEHAVIOR.IMPLICIT_INTER_RUNNABLE_VARIABLES):
					maxprio=max(accesslist._runnable._maxprio)
					for irv in accesslist:
						needsprotection=irv._runnable._minprio < maxprio
						irvvarname=f'Rte_Irv_{instanceobj.name()}_{localvar.name()}'
						if needsprotection:
							varname=f'Rte_Implicit_{irv._runnable.name()}_Irv_{instanceobj.name()}_{localvar.name()}'
							accesslist._runnable._implicits[varname] = vartype
							decl = f'''extern {vartype.impl_name()} {varname};
							extern {vartype.impl_name()} {irvvarname};
							'''
							if irv._read:
								irv._runnable._pre+=decl+vartype.copy(varname,vartype,irvvarname)
							else:
								irv._runnable._post+=decl+vartype.copy(irvvarname,vartype,varname)
						else:
							varname=irvvarname
						res = RunnableApiOpen([irv._runnable])
						if irv._read:
							if vartype.isPrimitive():
								rettype=vartype.name()
							elif vartype.isArray():
								rettype='const ' + vartype.element_type().name()+'*'
							elif vartype.isRecord():
								rettype='const ' + vartype.name()+'*'
							else:
								rettype='error not supported type category'
							res+=f'static inline {rettype} Rte_IrvIRead_{irv._runnable.name()}_{localvar.name()}(void) {{\n'
							res+=f'extern {vartype.name()} {varname};\n'
							if vartype.isRecord():
								res+=f'return &{varname};\n}}\n'
							else:
								res+=f'return {varname};\n}}\n'
						else:
							res+=f'static inline void Rte_IrvWrite_{irv._runnable.name()}_{localvar.name()}({vartype.name()} data) {{\n'
							res+=f'extern {vartype.name()} {varname};\n'
							res+=vartype.copy(varname,vartype,'data')+'}\n'
						res+=RunnableApiClose()
						c << res
							
				else:
					atomic=vartype.isAtomic()
					if not atomic:
						readers=accesslist.select(lambda e:e._read)
						maxreadprio=max(readers._runnable._maxprio)
						writers=accesslist.select(lambda e:e._read)
						maxwriteprio=max(writers._runnable._maxprio)
					#return minprio < maxprio
					for irv in accesslist:
						res = RunnableApiOpen([irv._runnable])
						if irv._read:
							if not atomic:
								needsprotection=irv._runnable._minprio < maxwriteprio
							else:
								needsprotection=False
							if vartype.isPrimitive():
								res+=f'static inline {vartype.name()} Rte_IrvRead_{irv._runnable.name()}_{localvar.name()}(void) {{\n'
								res+=f'extern {vartype.name()} Rte_Irv_{instanceobj.name()}_{localvar.name()};\n'
								if needsprotection:
									res+='Rte_EnterExclusive();\n'
								res+=f'{vartype.name()} ret = Rte_Irv_{instanceobj.name()}_{localvar.name()};\n'
								if needsprotection:
									res+='Rte_ExitExclusive();\n'
								res+='return ret;\n'
							else:
								if vartype.isArray():
									rettype=vartype.element_type().name()+'*'
								elif vartype.isRecord():
									rettype=vartype.name()+'*'
								else:
									rettype='error not supported type category'
								res+=f'static inline void Rte_IrvRead_{irv._runnable.name()}_{localvar.name()}({rettype} data) {{\n'
								to.do()
						else:
							if not atomic:
								needsprotection=irv._runnable._minprio < maxreadprio
							else:
								needsprotection=False
							res+=f'static inline void Rte_IrvWrite_{irv._runnable.name()}_{localvar.name()}({vartype.name()} data) {{\n'
							res+=f'extern {vartype.name()} Rte_Irv_{instanceobj.name()}_{localvar.name()};\n'
							if needsprotection:
								res+='Rte_EnterExclusive();\n'
							res+=f'Rte_Irv_{instanceobj.name()}_{localvar.name()} = data;\n'
							if needsprotection:
								res+='Rte_ExitExclusive();\n'
						res+='}\n'
						res+=RunnableApiClose()
						c << res
					
				#(read,runnable)
			
			for id, dataelement in instance._rxdataelements.items():
				runnables = dataelement._runnables['DATA_READ_ACCESSS']
				for runnable in runnables:
					res = RunnableApiOpen([runnable._runnable])
					if dataelement._type.isPrimitive():
						rettype=dataelement._type.name()
					elif dataelement._type.isArray():
						rettype='const ' + dataelement._type.element_type().name()+'*'
					elif dataelement._type.isRecord():
						rettype='const ' + dataelement._type.name()+'*'
					else:
						rettype='error not supported type category'
					res+=f'static inline {rettype} Rte_IRead_{runnable._runnable.name()}_{id[0].name()}_{id[1].name()}(void) {{\n'
					if dataelement._dataelementref.__len__() == 0 and dataelement._comSignalRef.__len__() == 0:
						#unconnected
						res+=f'return ({dataelement._type.name()}){dataelement._type.VALUE2str(dataelement._rxcomspec.INIT_VALUE)};\n}}\n'
					else:
						if dataelement._comSignalRef.__len__() > 0 or runnable._minprio < max(dataelement._dataelementref.foreach(lambda e:ModelList(e._runnables.values()))._maxprio.default([]).append(runnable._maxprio)) or dataelement._type.isArray():
							# protected variable
							vartype=dataelement._type
							varname=f'Rte_Implicit_{runnable._runnable.name()}_SR_{id[0].name()}_{id[1].name()}'
							runnable._implicits[varname]=vartype
							if not vartype.isArray():
								varnameptr='&'+varname
							else:
								varnameptr=varname
							runnable._pre+=f'extern {vartype.impl_name()} {varname};\n'
							runnable._pre+=dataelement.print_read(varnameptr)
							res+=f'extern {vartype.name()} {varname};\n'
							res+=f'return {varname};\n}}\n'
						else:
							# no need for access protection
							res+=f' {dataelement._type.impl_name()} temp;\n'
							res+=dataelement.print_read('&temp')
							res+='return temp;\n}\n'
					res+=RunnableApiClose()
					c << res
				runnables = dataelement._runnables['DATA_RECEIVE_POINT_BY_VALUES']._runnable
				if len(runnables) > 0:
					dataelement=instance._rxdataelements[id]
					res = RunnableApiOpen(runnables)
					res+=f'''static inline {dataelement._type.name()} Rte_DRead_{id[0].name()}_{id[1].name()}(void) {{
	extern {dataelement._type.name()} Rte_CDATA_{id[0].name()}_{id[1].name()};
	return Rte_CDATA_{id[0].name()}_{id[1].name()};
}}
'''
					res+=RunnableApiClose()
					c << res

#			for id, dataelement in instance._rxdataelements.items():
				runnables = dataelement._runnables['DATA_RECEIVE_POINT_BY_ARGUMENTS']._runnable
				if len(runnables) > 0:
					res = RunnableApiOpen(runnables)
					res+=f'static inline Std_ReturnType Rte_Read_{id[0].name()}_{id[1].name()}({dataelement._type.name()} {"" if dataelement._type.isArray() else "*"}data) {{\n'
					if dataelement._dataelementref.__len__() == 0 and dataelement._comSignalRef.__len__() == 0:
						#unconnected
						res+=f'static const {dataelement._type.name()} init = {dataelement._type.VALUE2str(dataelement._rxcomspec.INIT_VALUE)};\n'
						res+=dataelement._type.copy('*data',dataelement._type, 'init')
						res+='return RTE_E_UNCONNECTED;\n}\n'
					else:
						needsprotection= dataelement.needProtection()
						if needsprotection:
							res+='Rte_EnterExclusive();\n'
						res+=dataelement.print_read('data')
						if needsprotection:
							res+='Rte_ExitExclusive();\n'
						res+='return RTE_E_OK;\n}\n'
					res+=RunnableApiClose()
					c << res
				
			for id, dataelement in instance._txdataelements.items():
				runnables = dataelement._runnables['DATA_WRITE_ACCESSS']
				for runnable in runnables:
					res = RunnableApiOpen([runnable._runnable])
					res+=f'static inline Std_ReturnType Rte_IWrite_{runnable._runnable.name()}_{id[0].name()}_{id[1].name()}( const {dataelement._type.name()} {"*" if dataelement._type.isRecord() else ""}data) {{\n'
					if dataelement._comSignalRef.__len__() > 0 or runnable._minprio < max(ModelList(dataelement._dataelementref._runnables.values())._maxprio.append(runnable._maxprio)):
						# protected variable
						vartype=dataelement._type
						varname=f'Rte_Implicit_{runnable._runnable.name()}_SR_{id[0].name()}_{id[1].name()}'
						runnable._implicits[varname]=vartype
						runnable._post+=f'extern {vartype.impl_name()} {varname};\n'
						runnable._post+=dataelement.print_write(varname)
					else:
						res+=dataelement.print_write('data')
					res+='return RTE_E_OK;\n}\n'
					res+=RunnableApiClose()
					c << res

				runnables = dataelement._runnables['DATA_SEND_POINTS']._runnable
				if len(runnables) > 0:
					res = RunnableApiOpen(runnables)
					res+=f'static inline Std_ReturnType Rte_Write_{id[0].name()}_{id[1].name()}(const {dataelement._type.name()} {"*" if dataelement._type.isRecord() else ""}data) {{\n'
					needsprotection= dataelement.needProtection()
					if needsprotection:
						res+='Rte_EnterExclusive();\n'
					res+=dataelement.print_write('data')
					if needsprotection:
						res+='Rte_ExitExclusive();\n'
					res+='return RTE_E_OK;\n}\n'
					res+=RunnableApiClose()
					c << res
			for calparamobj,calparam in instance._calparams.items():
				res=RunnableApiOpen(calparam._runnables._runnable)
				if calparam._type.isPrimitive():
					rettype=calparam._type.name()
				elif calparam._type.isArray():
					rettype=f'const {calparam._type.element_type().name()} *'
				elif calparam._type.isRecord():
					rettype='const ' + calparam._type.name()+'*'
				else:
					rettype='error not supported type category'
				if isinstance(calparamobj.enclosing_container(),autosar_r4p0.SWC_INTERNAL_BEHAVIOR.SHARED_PARAMETERS):
					name=f'Rte_CalPrm_Rom_{self._csv.name()}_{calparamobj.name()}'
				else:
					name=f'Rte_CalPrm_Rom_{instanceobj.name()}_{calparamobj.name()}'					
				res+=f'static inline {rettype} Rte_CData_{calparamobj.name()}(void) {{\n'
				res+=f'extern {calparam._type.name()} {name};\n'
				res+=f'return {name};\n}}\n'
				res+=RunnableApiClose()
				c << res
			for id,operation in instance._clients.items():
				res = RunnableApiOpen(operation._runnables._runnable)
				arglist=ModelList()
				callarglist=ModelList()
				for arg in id[1].ARGUMENTS.ARGUMENT_DATA_PROTOTYPE:
					argtype=TYPE(arg.TYPE_TREF.dest,instance._behavior)
					callarglist.append((argtype,arg.name()))
					arglist.append(f'\n\t/* {arg.DIRECTION.value} */ {"const " if arg.DIRECTION.isIN() else ""}{argtype.name()} ' +
						('*' if not arg.DIRECTION.isIN() and not argtype.isArray() or arg.DIRECTION.isIN() and argtype.isRecord() else '') +
						arg.name())
				args=','.join(arglist)
				res += f'static inline Std_ReturnType Rte_Call_{id[0].name()}_{id[1].name()}({args}) {{\n'
				if operation._dataelementref is None:
					res += 'return RTE_E_UNCONNECTED;\n}\n'
				elif not operation._dataelementref._mapping.RteMappedToTaskRef.dest.isNone():
					fname=f'Rte_Call_{self._csv.name()}_{id[0].name()}_{id[1].name()}'
					res += f'''extern Std_ReturnType {fname}({args});
	return {fname}({', '.join(id[1].ARGUMENTS.ARGUMENT_DATA_PROTOTYPE.name())});
}}
'''
				else:
					op=operation._dataelementref
					intbeh=op._runnable._runnable.parent()
					opname=op._runnable._runnable.SYMBOL.value
					portApiOption=intbeh.PORT_API_OPTIONS.PORT_API_OPTION.select(lambda self:self.PORT_REF.dest == operation._dataelementref._port)
					unnamedarg=0
					runnableargs=ModelList()
					for arg in portApiOption.PORT_ARG_VALUES.PORT_DEFINED_ARGUMENT_VALUE:
						type_=TYPE(arg.VALUE_TYPE_TREF.dest,intbeh)
						runnableargs.append(type_.VALUE2str(arg.VALUE))
						unnamedarg+=1
					for arg, callarg in zip(op._operation.ARGUMENTS.ARGUMENT_DATA_PROTOTYPE, callarglist):
						argtype=TYPE(arg.TYPE_TREF.dest,intbeh)
						argname=arg.name()
						if argtype.ScalingNeeded(callarg[0]):
							res += f'{argtype.impl_name()} arg_{unnamedarg};\n'
							res+=argtype.copy(f'arg_{unnamedarg}',callarg[0],callarg[1])
							runnableargs.append(f'arg_{unnamedarg}')
							unnamedarg+=1
						else:
							runnableargs.append(callarg[1])
					voidtype=op._operation.POSSIBLE_ERROR_REFS.POSSIBLE_ERROR_REF.isNone()
					res+='Std_ReturnType ret = RTE_E_OK;\n'
					res+=op._runnable.print_call(f'{"ret = " if not voidtype else ""}{opname}({", ".join(runnableargs)})')
					res+='\nreturn ret;\n}\n'
				res+=RunnableApiClose()
				c << res
			
			for id,mode in instance._modes.items():
				res = RunnableApiOpen(mode._runnables['MODE_SWITCH_POINT']._runnable)
				name=f'{id[0].name()}_{id[1].name()}'
				res += f'static inline Std_ReturnType Rte_Switch_{name}({mode._type.impl_name()} mode){{\n'
				if mode._dataelementref.__len__() > 0:
					res+=f'''extern Std_ReturnType Rte_Switch_{self._csv.name()}_{name}({mode._type.impl_name()} mode);
						return Rte_Switch_{self._csv.name()}_{name}(mode);
					}}
					'''
				else:
					res+='return RTE_E_UNCONNECTED;\n}\n'
				res+=RunnableApiClose()
				c << res
			
			for id,mode in {**instance._rxmodes, **instance._modes}.items():
				if 'MODE_ACCESS_POINT' not in mode._runnables:
					continue
				res = RunnableApiOpen(mode._runnables['MODE_ACCESS_POINT']._runnable)
				res+=f'static inline {mode._type.impl_name()} Rte_Mode_{id[0].name()}_{id[1].name()}(void) {{\n'
				if mode._dataelementref.__len__() == 0:
					#unconnected
					if mode._modeGroup.TYPE_TREF.dest.INITIAL_MODE_REF.value.isNone():
						initialMode = '0'
					else:
						initialMode=f'RTE_MODE_{mode._modeGroup.TYPE_TREF.dest.name()}_{mode._modeGroup.TYPE_TREF.dest.INITIAL_MODE_REF.dest_name()}'
					res+=f'''return {initialMode};
					}}
					'''
				else:
					res += f'''if() {{
							return RTE_TRANSITION_{mode._modeGroup.name()};
						}} else {{
						
						}}
					}}
					'''
				res+=RunnableApiClose()
				c << res
					
				
		# per runnable med event: gen
		
		#cleanup in the end
		for runnable in used_runnables.name():
			c << f'''#ifdef RTE_RUNNABLEAPI_{runnable}
	#undef RTE_RUNNABLEAPI_{runnable}
#endif'''
		c << f'''#ifdef RTE_RUNNABLEAPI
	#undef RTE_RUNNABLEAPI
#endif
#ifdef __cplusplus
}} // extern "C"
#endif
#endif // RTE_{self._csv.name().upper()}_H
'''
		c.close()

csvs = {}
# add all instances
for instmap in extract.MAPPINGS.SYSTEM_MAPPING.SW_IMPL_MAPPINGS.SWC_TO_IMPL_MAPPING:
	behavior = instmap.COMPONENT_IMPLEMENTATION_REF.dest.BEHAVIOR_REF.dest
	for inst in instmap.COMPONENT_IREFS.COMPONENT_IREF.select(lambda self:self.CONTEXT_COMPOSITION_REF.dest == rootcomposition).TARGET_COMPONENT_REF.dest:
		if behavior.parent() not in csvs:
			csvs[behavior.parent()] = CSV(behavior.parent())
		csvs[behavior.parent()].add(inst,behavior)

# map the ports
for conn in rootcomposition.SOFTWARE_COMPOSITION_TREF.dest.CONNECTORS.ASSEMBLY_SW_CONNECTOR:
	provider_port=conn.PROVIDER_IREF.TARGET_P_PORT_REF.dest
	requester_port=conn.REQUESTER_IREF.TARGET_R_PORT_REF.dest
	provider_instance=conn.PROVIDER_IREF.CONTEXT_COMPONENT_REF.dest
	provider_csv=csvs[provider_instance.TYPE_TREF.dest]
	provider_inst_obj=provider_csv._instances[provider_instance]
	requester_instance=conn.REQUESTER_IREF.CONTEXT_COMPONENT_REF.dest
	requester_csv=csvs[requester_instance.TYPE_TREF.dest]
	requester_inst_obj=requester_csv._instances[requester_instance]
	if isinstance(provider_port.PROVIDED_INTERFACE_TREF.dest,autosar_r4p0.SENDER_RECEIVER_INTERFACE):
		for dataelement in provider_port.PROVIDED_INTERFACE_TREF.dest.DATA_ELEMENTS.VARIABLE_DATA_PROTOTYPE:
			if (provider_port,dataelement) in provider_inst_obj._txdataelements:
				# search for requester port
				requester_dataelement=None
				if dataelement in conn.MAPPING_REF.dest.DATA_MAPPINGS.DATA_PROTOTYPE_MAPPING.FIRST_DATA_PROTOTYPE_REF.dest:
					requester_dataelement=conn.MAPPING_REF.dest.DATA_MAPPINGS.DATA_PROTOTYPE_MAPPING.next(lambda self:self.FIRST_DATA_PROTOTYPE_REF.dest==dataelement).SECOND_DATA_PROTOTYPE_REF.dest
				elif dataelement in conn.MAPPING_REF.dest.DATA_MAPPINGS.DATA_PROTOTYPE_MAPPING.SECOND_DATA_PROTOTYPE_REF.dest:
					requester_dataelement=conn.MAPPING_REF.dest.DATA_MAPPINGS.DATA_PROTOTYPE_MAPPING.next(lambda self:self.SECOND_DATA_PROTOTYPE_REF.dest==dataelement).FIRST_DATA_PROTOTYPE_REF.dest
				else:
					for (rport,relement) in requester_inst_obj._rxdataelements.keys():
						if rport == requester_port and relement.name() == dataelement.name():
							requester_dataelement=relement
							break
				if requester_dataelement is not None and (requester_port,requester_dataelement) in requester_inst_obj._rxdataelements:
					rxdataelem=requester_inst_obj._rxdataelements[(requester_port,requester_dataelement)]
					txdataelem=provider_inst_obj._txdataelements[(provider_port,dataelement)]
					rxdataelem._dataelementref.append(txdataelem)
					txdataelem._dataelementref.append(rxdataelem)
	elif isinstance(provider_port.PROVIDED_INTERFACE_TREF.dest,autosar_r4p0.CLIENT_SERVER_INTERFACE):
		for operation in provider_port.PROVIDED_INTERFACE_TREF.dest.OPERATIONS.CLIENT_SERVER_OPERATION:
			if (provider_port,operation) in provider_inst_obj._servers:
				# search for requester port
				client_op=None
				if operation in conn.MAPPING_REF.dest.OPERATION_MAPPINGS.CLIENT_SERVER_OPERATION_MAPPING.FIRST_OPERATION_REF.dest:
					client_op=conn.MAPPING_REF.dest.OPERATION_MAPPINGS.CLIENT_SERVER_OPERATION_MAPPING.next(lambda self:self.FIRST_OPERATION_REF.dest==operation).SECOND_OPERATION_REF.dest
				elif operation in conn.MAPPING_REF.dest.OPERATION_MAPPINGS.CLIENT_SERVER_OPERATION_MAPPING.SECOND_OPERATION_REF.dest:
					client_op=conn.MAPPING_REF.dest.OPERATION_MAPPINGS.CLIENT_SERVER_OPERATION_MAPPING.next(lambda self:self.SECOND_OPERATION_REF.dest==operation).FIRST_OPERATION_REF.dest
				else:
					for (rport,rop) in requester_inst_obj._clients.keys():
						if rport == requester_port and rop.name() == operation.name():
							client_op=rop
							break
				if client_op is not None and (requester_port,client_op) in requester_inst_obj._clients:
					rxdataelem=requester_inst_obj._clients[(requester_port,client_op)]
					txdataelem=provider_inst_obj._servers[(provider_port,operation)]
					rxdataelem._dataelementref=txdataelem
					txdataelem._dataelementref.append(rxdataelem)
					if txdataelem._mapping.RteMappedToTaskRef.dest.isNone():
						txdataelem._runnable._activatedByRunnables.update(rxdataelem._runnables)
	elif isinstance(provider_port.PROVIDED_INTERFACE_TREF.dest,autosar_r4p0.MODE_SWITCH_INTERFACE):
		#for operation in provider_port.PROVIDED_INTERFACE_TREF.dest.OPERATIONS.CLIENT_SERVER_OPERATION:
		modegroup=provider_port.PROVIDED_INTERFACE_TREF.dest.MODE_GROUP
		if not modegroup.isNone():
			if (provider_port,modegroup) in provider_inst_obj._modes:
				# search for requester port
				modeclient=None
				for (rport,rop) in requester_inst_obj._rxmodes.keys():
					if rport == requester_port and rop.name() == modegroup.name():
						modeclient=rop
						break
				if modeclient is not None and (requester_port,modeclient) in requester_inst_obj._rxmodes:
					rxdataelem=requester_inst_obj._rxmodes[(requester_port,modeclient)]
					txdataelem=provider_inst_obj._modes[(provider_port,modegroup)]
					rxdataelem._dataelementref.append(txdataelem)
					txdataelem._dataelementref.append(rxdataelem)
					if rxdataelem._mapping.RteMappedToTaskRef.dest.isNone():
						ModelList(rxdataelem._runnables.values())._activatedByRunnables.update(txdataelem._runnables['MODE_SWITCH_POINT'])
		#provider_port.PROVIDED_INTERFACE_TREF.
for mapping in extract.MAPPINGS.SYSTEM_MAPPING.DATA_MAPPINGS.SENDER_RECEIVER_TO_SIGNAL_MAPPING:
	port=mapping.DATA_ELEMENT_IREF.CONTEXT_PORT_REF.dest
	instance=mapping.DATA_ELEMENT_IREF.CONTEXT_COMPONENT_REF[0].dest
	csv=csvs[instance.TYPE_TREF.dest]
	inst_obj=csv._instances[instance]
	dataelement=mapping.DATA_ELEMENT_IREF.TARGET_DATA_PROTOTYPE_REF.dest
	i=(port,dataelement)
	if i in inst_obj._txdataelements:
		dataelement=inst_obj._txdataelements[i]
		systemsig=mapping.SYSTEM_SIGNAL_REF.dest
		isig=systemsig.references().parent().select(lambda e:isinstance(e,autosar_r4p0.I_SIGNAL))
		isigMapping=isig.references().parent().select(lambda e:isinstance(e,autosar_r4p0.I_SIGNAL_TO_I_PDU_MAPPING))
		comSig=isigMapping.references().parent().next(lambda e:e.parent().parent()==com)
		if comSig.isNone():
			print(f'Error 001: Cannot find Com signal for referenced system signal {mapping.SYSTEM_SIGNAL_REF.value}')
			continue
		dataelement._comSignalRef.append((systemsig,isig,comSig))
	elif i in inst_obj._rxdataelements:
		dataelement=inst_obj._rxdataelements[i]
		systemsig=mapping.SYSTEM_SIGNAL_REF.dest
		isig=systemsig.references().parent().select(lambda e:isinstance(e,autosar_r4p0.I_SIGNAL))
		isigMapping=isig.references().parent().select(lambda e:isinstance(e,autosar_r4p0.I_SIGNAL_TO_I_PDU_MAPPING))
		comSig=isigMapping.references().parent().next(lambda e:e.parent().parent()==com)
		if comSig.isNone():
			print(f'Error 001: Cannot find Com signal for referenced system signal {mapping.SYSTEM_SIGNAL_REF.value}')
			continue
		dataelement._comSignalRef.append((systemsig,isig,comSig))
	else:
		print(f'Error 002: Cannot find port mapped signal {i}')

for csv in csvs.values():
	for instanceobj,instance in csv._instances.items():
		for runnable in instance._runnables.values():
			runnable.calcprio()
		for exclarea in instance._exclusiveAreas.values():
			exclarea.analyse()
		for runnable in instance._runnables.values():
			runnable.runsWithinExclArea()
		for txdataelem in instance._txdataelements.values():
			txstorage=False
			if txdataelem._dataelementref.__len__() > 0:
				for rxdataelem in txdataelem._dataelementref:
					if rxdataelem._dataelementref.__len__() > 1:
						rxdataelem._storeLocal=True
					else:
						txstorage=True
			elif txdataelem._comSignalRef.__len__() == 0:
				txstorage=True
			txdataelem._storeLocal=txstorage

for csv in csvs.values():
	csv.Write_Rte_SWC_h()
	csv.Write_Rte_SWC_type_h()

c = CWriter('Rte.c')
c << '''#include <Rte.h>
#include <Os.h>
#include <Com.h>

#define Rte_EnterExclusive()	SuspendAllInterrupts()
#define Rte_ExitExclusive()		ResumeAllInterrupts()
'''
tasks=defaultdict(ModelList)
# identify sender receiver storage location
for csv in csvs.values():
	for instanceobj,instance in csv._instances.items():
		c << f'\n// instance: {instanceobj.name()}'
		c << '// IRV buffers'
		for irv in instance._irvdataelements:
			irvType=TYPE(irv.TYPE_TREF.dest,instance._behavior)
			c << f'{irvType.impl_name()} Rte_Irv_{instanceobj.name()}_{irv.name()};'
		c << '// SR buffers'
		for txdataelem in instance._txdataelements.values():
			if txdataelem._storeLocal == True:
				c << f'{txdataelem._type.impl_name()} {txdataelem._buf_name};'
		for rxdataelem in instance._rxdataelements.values():
			if rxdataelem._storeLocal == True:
				c << f'{rxdataelem._type.impl_name()} {rxdataelem._buf_name};'
						
		c << '// implicit buffers'
		for runnable in instance._runnables.values():
			for implicit,type in runnable._implicits.items():
				c << f'{type.impl_name()} {implicit};'
		c << '// pim buffers'
		for pim in instance._behavior.PER_INSTANCE_MEMORYS.PER_INSTANCE_MEMORY:
			if pim.INIT_VALUE.value.default('') != '':
				init=f' = {pim.INIT_VALUE.value}'
			else:
				init=''
			c << f'Rte_PimType_{csv._csv.name()}_{pim.TYPE.value} Rte_Pim_{instance._behavior.name()}_{pim.name()}{init};'
		for pim in instance._behavior.AR_TYPED_PER_INSTANCE_MEMORYS.VARIABLE_DATA_PROTOTYPE:
			_type=TYPE(pim.TYPE_TREF.dest,instance._behavior)
			if not pim.INIT_VALUE.isNone():
				init=f' = {_type.VALUE2str(pim.INIT_VALUE)}'
			else:
				init=''
			c << f'{_type.name()} Rte_Pim_{instance._behavior.name()}_{pim.name()}{init};'

		# identify runnables to be scheduled
		for timingEvent in instance._behavior.EVENTS.TIMING_EVENT:
			# all runnables triggered by a timing event should be scheduled
			runnable=instance._runnables[timingEvent.START_ON_EVENT_REF.dest]
			mapping=runnable._taskMappings.next(lambda e:e.RteEventRef.dest == timingEvent)
			tasks[mapping.RteMappedToTaskRef.dest].append((mapping,'TIMING-EVENT',runnable))
		for id,mode in instance._modes.items():
			# all modes shall have a function to set the mode and the mode clients may be scheduled to a runnable
			if mode._dataelementref.__len__() > 0:
				onExit=defaultdict(ModelList)
				onTrans=defaultdict(ModelList)
				onEntry=defaultdict(ModelList)
				for modeswitchpoint in mode._dataelementref:
					for (point,modeObj),runnable in modeswitchpoint._runnables.items():
						if point == 'ON-EXIT':
							onExit[modeObj.name()].extend(runnable)
						elif point == 'ON-ENTRY':
							onEntry[modeObj.name()].extend(runnable)
						elif point == 'ON-TRANS':
							onTrans[modeObj.name()].extend(runnable)
				def PrintSwitch(c, mode, runnableDict):
					for i,m in enumerate(mode._modeGroup.TYPE_TREF.dest.MODE_DECLARATIONS.MODE_DECLARATION):
						runnables=runnableDict[m.name()].sort(key=lambda e:e._taskMappings[0].RtePositionInTask.value.default(0xffffffff))
						if runnables.__len__() > 0:
							c << f'case {i}:'
							for runnable in runnables:
								c << runnable.print_call()
					c << '}'
				if onTrans.__len__() > 0:
					c << f'static void Rte_OnTrans_{instanceobj.name()}_{id[0].name()}_{id[1].name()}({mode._type.impl_name()} mode) {{'
					c << 'switch(mode) {'
					PrintSwitch(c,mode,onTrans)
				c << f'{mode._type.impl_name()} Rte_mode_current_{instanceobj.name()}_{id[0].name()}_{id[1].name()} = 0; // todo: set correct init value'
				c << f'{mode._type.impl_name()} Rte_mode_next_{instanceobj.name()}_{id[0].name()}_{id[1].name()} = 0; // todo: set correct init value'
				c << f'static void Rte_mode_{instanceobj.name()}_{id[0].name()}_{id[1].name()}(void) {{'
				if onExit.__len__() > 0:
					c << f'switch(Rte_mode_current_{instanceobj.name()}_{id[0].name()}_{id[1].name()}({mode._type.impl_name()}) {{'
					PrintSwitch(c,mode,onExit)
				if onTrans.__len__() > 0:
					c << f'Rte_OnTrans_{instanceobj.name()}_{id[0].name()}_{id[1].name()}(Rte_mode_current_{instanceobj.name()}_{id[0].name()}_{id[1].name()});'
				c << f'Rte_mode_current_{instanceobj.name()}_{id[0].name()}_{id[1].name()} = Rte_mode_next_{instanceobj.name()}_{id[0].name()}_{id[1].name()};'
				if onTrans.__len__() > 0:
					c << f'Rte_OnTrans_{instanceobj.name()}_{id[0].name()}_{id[1].name()}(Rte_mode_current_{instanceobj.name()}_{id[0].name()}_{id[1].name()});'
				if onEntry.__len__() > 0:
					c << f'switch(Rte_mode_current_{instanceobj.name()}_{id[0].name()}_{id[1].name()}) {{'
					PrintSwitch(c,mode,onEntry)
				c << '}'
				c << f'Std_ReturnType Rte_Switch_{csv._csv.name()}_{id[0].name()}_{id[1].name()}({mode._type.impl_name()} mode) {{'
				c << f'if(Rte_mode_next_{instanceobj.name()}_{id[0].name()}_{id[1].name()} != mode) {{\n'
				c << f'Rte_mode_next_{instanceobj.name()}_{id[0].name()}_{id[1].name()} = mode;'
				# use one task mapping, doesn't matter which since all modeswitches must be scheduled by the same task
				mappedRunnable=mode._dataelementref[0]._runnables.values().__iter__().__next__()
				taskMapping=mappedRunnable._taskMappings.next(lambda e:e.RteEventRef.dest.START_ON_EVENT_REF.dest == mappedRunnable._runnable)
				task = taskMapping.RteMappedToTaskRef.dest
				if not task.isNone():
					if not taskMapping.RteUsedOsEventRef.dest.isNone():
						c << f'SetEvent(TASK_ID_{task.name()},EVENT_MASK_{taskMapping.RteUsedOsEventRef.dest.name()});' 
					elif not taskMapping.RteUsedOsAlarmRef.dest.isNone():
						c << f'ActivateTask(TASK_ID_{task.name()});'
					tasks[task].append((taskMapping,'MODE-SWITCH-EVENT',f'{instanceobj.name()}_{id[0].name()}_{id[1].name()}'))
				else:
					# no task reference
					c << f'Rte_mode_{instanceobj.name()}_{id[0].name()}_{id[1].name()}();'
				c << '}\nreturn RTE_E_OK;\n}'

for task,events in tasks.items():
	if not task.isNone():
		events.sort(key=lambda e:e[0].RtePositionInTask.value.default(0xFFFFFFFF))
		c << f'\nTASK({task.name()}) {{'
		if not events[0][0].RteUsedOsEventRef.dest.isNone():
			osEvents=ModelList(set(events.foreach(lambda e:e[0].RteUsedOsEventRef.dest)))
			c << f'''while(1) {{
				EventMaskType event;
				WaitEvent(EVENT_MASK_{" | EVENT_MASK_".join(osEvents.name())});
				GetEvent(TASK_ID_{task.name()}, &event);
				ClearEvent(event & (EVENT_MASK_{" | EVENT_MASK_".join(osEvents.name())}));'''
			prevEvent = ModelList()
			for (mapping,type,obj) in events:
				ev = mapping.RteUsedOsEventRef.dest
				if prevEvent != ev:
					if not prevEvent.isNone():
						c << '}'
					c << f'if(event & EVENT_MASK_{ev.name()}) {{'
					prevEvent=ev
				if type == 'TIMING-EVENT':
					c << obj.print_call()
				elif type == 'MODE-SWITCH-EVENT':
					c << f'Rte_mode_{obj}();'
			c << '}\n}'
		else:
			for (mapping,type,obj) in events:
				if type == 'TIMING-EVENT':
					c << obj.print_call()
				elif type == 'MODE-SWITCH-EVENT':
					c << f'if(Rte_mode_next_{obj} != Rte_mode_current_{obj}) {{'
					c << f'Rte_mode_{obj}();\n}}'
		c << '}'
c.close()

c = CWriter('Rte_Param.c')
c << '#include <Rte_Type.h>\n'
for csv in csvs.values():
	for instanceobj,instance in csv._instances.items():
		for calparamobj,calparam in instance._calparams.items():
			if isinstance(calparamobj.enclosing_container(),autosar_r4p0.SWC_INTERNAL_BEHAVIOR.SHARED_PARAMETERS):
				name=f'Rte_CalPrm_Rom_{csv._csv.name()}_{calparamobj.name()}'
			else:
				name=f'Rte_CalPrm_Rom_{instanceobj.name()}_{calparamobj.name()}'					
			c << f'const {calparam._type.impl_name()} {name} = {calparam._type.VALUE2str(calparamobj.INIT_VALUE)};'

c.close()


Rte_type_h.Write_Rte_type()

c = CWriter('Rte.h')
c << '''#ifndef RTE_H_
#define RTE_H_

#include "Std_Types.h"
#include "Rte_Type.h"

// error codes
#define RTE_E_OK						0U
#define RTE_E_NOK						1U
#define RTE_E_INVALID					1U
#define RTE_E_COM_STOPPED				128U
#define RTE_E_TIMEOUT					129U
#define RTE_E_LIMIT						130U
#define RTE_E_NO_DATA					131U
#define RTE_E_TRANSMIT_ACK				132U
#define RTE_E_NEVER_RECEIVED			133U
#define RTE_E_UNCONNECTED				134U
#define RTE_E_IN_EXCLUSIVE_AREA			135U
#define RTE_E_SEG_FAULT					136U
#define RTE_E_OUT_OF_RANGE				137U
#define RTE_E_SERIALIZATION_ERROR		138U
#define RTE_E_HARD_TRANSFORMER_ERROR	138U
#define RTE_E_SERIALIZATION_LIMIT		139U
#define RTE_E_TRANSFORMER_LIMIT			139U
#define RTE_E_SOFT_TRANSFORMER_ERROR	140U
#define RTE_E_COM_BUSY					141U
#define RTE_E_LOST_DATA                 64U
#define RTE_E_MAX_AGE_EXCEEDED          64U

#endif // RTE_H_
'''

c.close()

c = CWriter('Rte_Main.h')
c << '''#ifndef RTE_MAIN_H_
#define RTE_MAIN_H_

#include "Rte.h"

Std_ReturnType Rte_Start(void);
Std_ReturnType Rte_Stop(void);
//void Rte_PartitionTerminated_<PID>(void)
//void Rte_PartitionRestarting_<PID>(void)
//Std_ReturnType Rte_RestartPartition_<PID>(void)
//void Rte_Init_<InitContainer>(void)
void Rte_StartTiming(void);

#endif // RTE_MAIN_H_
'''
c.close()

c = CWriter('Rte_Main.c')
c << '''#include "Rte_Main.h"

Std_ReturnType Rte_Start(void) {
	return E_OK;
}
Std_ReturnType Rte_Stop(void) {
	return E_OK;
}
//void Rte_PartitionTerminated_<PID>(void)
//void Rte_PartitionRestarting_<PID>(void)
//Std_ReturnType Rte_RestartPartition_<PID>(void)
//void Rte_Init_<InitContainer>(void)
void Rte_StartTiming(void) {
}
'''
c.close()

print('done')