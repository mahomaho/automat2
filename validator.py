# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

from AutoMAT import *
import argparse

def validate(val):
	try:
		val.validate()
	except AssertionError as e:
		print('Error in ',val,': ',e)
	if isinstance(val,(autosar_r4p0.Group.EcucModuleConfigurationValues,autosar_r4p0.Group.EcucContainerValue)):
		aggregations=val.ecucAggregations()
	elif isinstance(val,support.complexbase.ComplexTypeBase):
		aggregations=val.aggregations()
	else:
		return
	for aggregation in aggregations:
		try:
			aggregation.validate()
		except AssertionError as e:
			print('Error in',str(val)+'.'+aggregation.name()+':',e)
		for child in aggregation:
			validate(child)

def main(argv):
	_parser = argparse.ArgumentParser(description='Autosar model validator from AutoMAT')
	#_parser.add_argument('-o', '--outputFile', nargs='?', help='file to save merged model to, default files with extension replaced to arxml are created', default=None)
	_parser.add_argument('-r', '--validationroot', nargs='?', help='directory for saved files. Ignored if ouputFile set', default='')
	_args = _parser.parse_args(argv)
	root=_args.validationroot.strip('/').split('/')
	root=eval('autosar'+'.'.join(root))
	validate(root)
	
